<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware' => 'auth:api'],function(){

	//artist
	Route::get('/artistcat', 'Api\ArtistCatController@index');
	Route::get('/artisttype', 'Api\ArtistTypeController@index');
	Route::get('/artists', 'Api\ArtistController@index');

	Route::post('/artist/getwithcategory', 'Api\ArtistController@getWithCategory');
	//Music
	Route::get('/feelings', 'Api\FeelingController@index');
	Route::get('/songcat', 'Api\SongCatController@index');
	Route::post('/song', 'Api\SongController@index');
	Route::post('/album', 'Api\AlbumController@index');
	Route::post('/playlist', 'Api\PlayListController@index');
	Route::post('/albumlist', 'Api\AlbumListController@index');
	Route::get('/aggregator', 'Api\AggregatorController@index');
	//new release for album cms
	Route::post('/album/newrelease','Api\AlbumCmsController@getNewRelease');
	Route::post('/album/topalbum','Api\AlbumCmsController@getTopAlbum');

	//Detail album
	Route::post('/albumdetail','Api\AlbumController@albumDetail');
	//playlist cms
	Route::post('/music/playlist','Api\PlaylistCmsController@index');
	Route::post('/music/collection','Api\AlbumlistCmsController@index');
	//video
	Route::get('/videocat', 'Api\VideoCatController@index');
	Route::get('/video', 'Api\VideoController@index');
	Route::get('/video/newrelease','Api\VideoCmsController@getNewRelease');
	Route::get('/video/playlist','Api\VideoCmsController@getPlaylist');
	//news
	Route::get('/event', 'Api\EventController@index');
	// sliders
	Route::get('/slider', 'Api\SliderController@index');
	// Currency
	Route::get('/currency', 'Api\CurrencyController@index');
	//promotion
	Route::get('/promotion', 'Api\PromotionController@index');
	//Video Playlist
	Route::get('/videoplaylist', 'Api\VideoPlaylistController@index');
	//prices 

	Route::get('/albumprice', 'Api\PriceController@albumprice');
	Route::get('/songprice', 'Api\PriceController@songprice');
	Route::get('/subprice', 'Api\PriceController@subscriptionprice');
	//Notification
	Route::get('/notification', 'Api\NotificationController@index');

	//user register
	Route::post('/register','Api\UserController@register');
	Route::post('/login','Api\UserController@login');
	//get song with feeling
	Route::post('/feeling/getsong','Api\SongController@getwithFeeling');
	Route::post('/songgenre/getsong','Api\SongController@getwithGenre');
	//get album with aggregator
	Route::post('/album/getalbum','Api\AlbumController@getwithAggregator');
	//get video with category
	Route::post('/video/getvideo','Api\VideoController@getwithCategory');
	//get Song with Artist
	Route::post('/artist/getsong','Api\SongController@getwithArtist');
	//get Song with Artist
	Route::post('/search','Api\SearchController@index');
	//get Song with Recent
	Route::post('/getrecents','Api\SongController@getwithRecent');
	//send recent song
	Route::post('/recents','Api\SongController@recentSong');
	
	//get Song Favourite
	Route::post('/getfavourite','Api\SongController@getwithFavourite');
	//send favourite song
	Route::post('/favourite','Api\SongController@favouriteSong');
	//send recent song
	Route::post('/removefavouritesong','Api\SongController@removeFavouriteSong');
	//play count
	Route::post('/playcount','Api\SongController@playCount');
	//play count album
	Route::post('/album/viewcount','Api\AlbumController@playCount');
	//play count album
	Route::post('/playlist/viewcount','Api\PlayListController@playCount');
	//get order
	Route::post('/getorder','Api\SongController@getOrder');
	//buy song
	Route::post('/buysong','Api\UserController@buySong');
	//buy album
	Route::post('/buyalbum','Api\UserController@buyAlbum');
	//get payment history
	Route::post('/getpayment','Api\UserController@getPayment');

	//get user for edit
	Route::post('/edituser','Api\UserController@editUser');
	Route::post('/updateuser','Api\UserController@updateUser');
	Route::post('/changepassword','Api\UserController@changePassword');
	// user scribe 
	Route::post('/subscribe','Api\UserController@subscribe');
	// user scribe detail
	Route::post('/subscribedetail','Api\UserController@subscribeDetail');
	// user new scribe 
	Route::post('/newsubscribe','Api\UserController@newsubscribe');
	//soclia register or logini
	Route::post('/social','Api\UserController@social');
	//get song with gender
	Route::post('/song/getwithgender','Api\SongController@getwithGender');

	//get free music
	Route::post('/song/getfreemusic','Api\SongController@getFreeSong');
	//get Weekly Top 20 
	Route::post('/song/getweeklytop','Api\SongController@getWeeklyTop');

	//get comment
	Route::get('/comments','Api\CommentController@index');
	Route::post('/comments/create','Api\CommentController@save');
	Route::post('/comments/edit','Api\CommentController@edit');
	Route::post('/comments/update','Api\CommentController@update');
	Route::post('/comments/delete','Api\CommentController@destory');

	//get version
	Route::get('/getversion','Api\VersionController@index');
});