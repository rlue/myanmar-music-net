<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes(['verify' => true]);


Route::get('/test', function() {

    $full_file = 'https://s3-ap-southeast-1.amazonaws.com/mmn01/Music/Maung+Maung_%E1%80%B1%E1%80%9B%E1%80%81%E1%80%BA%E1%80%AD%E1%80%B3%E1%80%B8%E1%80%81%E1%80%B2%E1%82%94%E1%80%95%E1%80%AB/05+Ngar+Ma+Thi+Tae+Thi+Chin+Myar+(%E1%80%84%E1%80%AB%E1%80%99%E1%80%9E%E1%80%AD%E1%80%90%E1%80%B2%E1%82%94%E1%80%9E%E1%80%AE%E1%80%81%E1%80%BA%E1%80%84%E1%80%B9%E1%80%B8%E1%80%99%E1%80%BA%E1%80%AC%E1%80%B8).mp3';
    $filename = basename($full_file);

    return response()->streamDownload(function () use ($full_file){
        echo file_get_contents($full_file);
    }, $filename,[
        'Pragma' => 'public',
        'Expires' => 0,
        'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
        'Cache-Control' => 'public',
        'Content-Description' => 'File Transfer',
        'Content-Type' => 'application/octet-stream',
        'Content-Disposition' => 'attachment; filename="'.$filename.'"',
        'Content-Transfer-Encoding' => 'binary'
    ]);
});

// Social Login
Route::get('auth/{provider}', 'SocialController@getSocialRedirect');
Route::get('auth/{provider}/callback', 'SocialController@getSocialHandle');

Route::get('/', 'PagesController@index');
Route::get('/home', 'PagesController@index')->name('home');

Route::get('/new-releases', 'PagesController@newReleases');
Route::get('/top-albums', 'PagesController@topAlbums');
Route::get('/recommended-playlist', 'PagesController@recommendedPlaylist');

Route::get('/album/{id}', 'PagesController@album');
Route::get('/albums', 'PagesController@albums');
Route::get('/music/all_free_music', 'PagesController@freeMusic');
Route::get('/music/top_tracks', 'PagesController@topSongs');

Route::get('/collections', 'PagesController@showCollectionList');
Route::get('/collection/{id}', 'PagesController@showCollection');

Route::get('/artist/{id}', 'PagesController@artist');
Route::get('/artists', 'PagesController@artists');
Route::get('/artists/{id}/{slug}', 'PagesController@artistsByCategory');

Route::get('/genres','PagesController@genres');
Route::get('/genre/{genre}','PagesController@genre');

Route::get('/feelings','PagesController@feelings');
Route::get('/feeling/{feeling}','PagesController@feeling');

Route::get('/playlist','PagesController@playlist');
Route::get('/playlist/{id}','PagesController@playlistDetail');

Route::get('search', 'SearchController@showAll');

Route::get('/search-by-song', 'SearchController@bySong');
Route::get('/search-by-artist', 'SearchController@byArtist');
Route::get('/search-by-album', 'SearchController@byAlbum');
Route::get('/search-by-playlist', 'SearchController@byPlaylist');

Route::get('/videos', 'PagesController@videos');
Route::get('/video/{id}', 'PagesController@video');
Route::get('/videos/{id}/{slug}', 'PagesController@videosByCategory');

Route::post('/song_play/{id}', 'ClickCountAndRecentController@create');
Route::get('/download_free_song/{id}', 'PagesController@downloadFreeSong');

Route::get('/events', 'PagesController@events');
Route::get('/event/{id}', 'PagesController@event');

Route::get('/privacy_policy', 'PagesController@privacy');

Route::get('support_guide','PagesController@supportGuide');

/*
|--------------------------------------------------------------------------
| For Mobile Payment And Payment Gateway
|--------------------------------------------------------------------------
*/

/*
 * require user_id and amount
 */
Route::get('/mobile/coda-pay', 'PaymentMobileController@showCodaPayPayment');

/*
 * require user_id and amount.
 */
Route::post('/mobile/mpu-pay', 'PaymentMobileController@payWithMPU');
Route::get('/mobile/mpu-pay', 'PaymentMobileController@showMpuPayPayment');

/*
 * require user_id and amount.
 */
Route::post('/mobile/paypal', 'PaypalMobileController@payWithPaypal');
Route::get('/mobile/paypal', 'PaymentMobileController@showPayPal');


Route::get('/transaction_success','PaymentMobileController@transactionSuccess');
Route::get('/transaction_failed','PaymentMobileController@transactionFailed');

/*
 * Payment Gateway callback.
 */
Route::get('/payment/paypal-response', 'PaypalController@getPaymentStatus');

Route::get('/coda-response', 'HandelPaymentRespondController@codaResponse');

Route::post('/mpu_front', 'HandelPaymentRespondController@getMPUFrondEndRespond');
Route::post('/mpu_back', 'HandelPaymentRespondController@getMPUBackEndRespond');

/*
|--------------------------------------------------------------------------
| End of Mobile Payment And Payment Gateway
|--------------------------------------------------------------------------
*/

Route::middleware(['verified','auth'])->group(function() {
    Route::post('/favourite/song/{id}', 'UserController@favouriteSong');
    Route::post('/favourite/album/{uuid}', 'UserController@favouriteAlbum');

    Route::post('/like/album/{uuid}', 'UserController@likeAlbum');

    Route::delete('/favourite/song/{id}', 'UserController@deleteFavouriteSong');
    Route::delete('/favourite/album/{uuid}', 'UserController@deleteFavouriteAlbum');


    Route::get('/user/profile','UserController@profile');
    Route::get('/account/edit_account_info','UserController@showProfileEditPage');
    Route::post('/account/edit_account_info','UserController@editProfile');
    Route::get('/account/change_password','UserController@showChangePasswordPage');
    Route::post('/password/change','UserController@passwordChange')->name('user.password.change');

    Route::get('/user/favourite','UserController@showUserFav');

    Route::get('/mymusic/favourite_songs','UserController@showUserFavSongs');
    Route::get('/mymusic/favourite_albums','UserController@showUserFavAlbums');

    Route::get('/user/purchased','UserController@purchasedPage');
    Route::get('/mymusic/purchased_songs','UserController@showUserPurchasedSongs');
    Route::get('/mymusic/purchased_albums','UserController@showUserPurchasedAlbums');
    Route::get('/mymusic/purchased_album/{id}','UserController@showUserPurchasedAlbum');

    Route::get('/recent','UserController@showRecentPage');

    Route::post('/user/subscribe','UserController@subscribe');
    Route::post('/user/streaming_quality','UserController@updateStreamingQuality');

    Route::post('/user/buy_song', 'UserController@buySong');
    Route::post('/user/buy_album', 'UserController@buyAlbum');
    Route::get('/user/download_song/{id}', 'UserController@downloadSong');
    Route::get('/user/{album_id}/download_album/{song_id}', 'UserController@downloadSongByAlbum');

    /*
    |--------------------------------------------------------------------------
    | Payment
    |--------------------------------------------------------------------------
    */

    // Coda Pay
    Route::get('/user/payment', 'PaymentController@showPayment');
    Route::post('/payment/coda-pay', 'PaymentController@payWithCodaPay');

    // MPU
    Route::post('/payment/mpu-pay', 'PaymentController@payWithMPU');

    // Paypal
    Route::post('/payment/paypal', 'PaypalController@payWithPaypal');

    /*
    |--------------------------------------------------------------------------
    | End of Mobile Payment
    |--------------------------------------------------------------------------
*/
});


Route::get('/backend/login','Admin\AuthController@index')->name('backend.login');
Route::post('/backend/login','Admin\AuthController@loginValidate');
Route::get('/logout', 'Admin\AuthController@logout')->name('logout' );

Route::group(['middleware' => 'backendauth', 'prefix' => 'backend'], function () {

	Route::get("/dashboard/getdata", 'Admin\DashboardController@getSongData')->name('dashboard.getData');
	Route::get("/dashboard", 'Admin\DashboardController@index')->name('dashboard');

	Route::get("/", 'Admin\UserController@index');
	Route::get("users/getdata", 'Admin\UserController@getData')->name("users.getData");
	Route::resource('users','Admin\UserController');

	Route::get("members/getdata", 'Admin\MemberController@getData')->name("members.getData");
	Route::resource('members','Admin\MemberController');

	Route::get('/laravel-filemanager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show')->name('medias');
    Route::post('/laravel-filemanager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');
	//role route

	Route::get("roles/getdata", 'Admin\RoleController@getData')->name("roles.getData");
	Route::resource('roles','Admin\RoleController');

	//artist category

	Route::get("artistcategory/getdata", 'Admin\ArtistCategoryController@getData')->name("artistcategory.getData");
	Route::resource('artistcategory','Admin\ArtistCategoryController');

	Route::get("artisttype/getdata", 'Admin\ArtistTypeController@getData')->name("artisttype.getData");
	Route::resource('artisttype','Admin\ArtistTypeController');

	Route::get("artists/getdata", 'Admin\ArtistController@getData')->name("artists.getData");
	Route::resource('artists','Admin\ArtistController');

	//music
	//Feelings
	Route::get("feelings/getdata", 'Admin\FeelingController@getData')->name("feelings.getData");
	Route::resource('feelings','Admin\FeelingController');
	//Genr category
	Route::get("songcategory/getdata", 'Admin\SongCategoryController@getData')->name("songcategory.getData");
	Route::resource('songcategory','Admin\SongCategoryController');
	//Song
	Route::get("songs/getdata", 'Admin\SongController@getData')->name("songs.getData");
	Route::resource('songs','Admin\SongController');
	//search song
	Route::post("songs/getsearchsong","Admin\SongController@getSearchSong");
	//Album
	Route::get("albums/getdata", 'Admin\AlbumController@getData')->name("albums.getData");
	Route::resource('albums','Admin\AlbumController');
	//PlayList
	Route::get("playlists/getdata", 'Admin\PlayListController@getData')->name("playlists.getData");
	Route::resource('playlists','Admin\PlayListController');

	//VideoPlayList
	Route::get("videoplaylists/getdata", 'Admin\VideoPlaylistController@getData')->name("videoplaylists.getData");
	Route::resource('videoplaylists','Admin\VideoPlaylistController');
	//AlbumList
	Route::get("albumlists/getdata", 'Admin\AlbumListController@getData')->name("albumlists.getData");
	Route::resource('albumlists','Admin\AlbumListController');
	//Price for song
	Route::get("prices/getdata", 'Admin\PriceController@getData')->name("prices.getData");
	Route::resource('prices','Admin\PriceController');

	//Price for album
	Route::get("albumprices/getdata", 'Admin\AlbumPriceController@getData')->name("albumprices.getData");
	Route::resource('albumprices','Admin\AlbumPriceController');
	// currency
	Route::get("currency","Admin\CurrencyController@create")->name('currency.create');
	Route::post("currency/store","Admin\CurrencyController@store")->name('currency.store');
	//Price for Subscription
	Route::get("subprice/getdata", 'Admin\SubscriptionPriceController@getData')->name("subprice.getData");
	Route::resource('subprice','Admin\SubscriptionPriceController');
	//Video Category
	Route::get("videocategory/getdata", 'Admin\VideoCatController@getData')->name("videocategory.getData");
	Route::resource('videocategory','Admin\VideoCatController');
	// Aggregator
	Route::get("aggregator/getdata", 'Admin\AggregatorController@getData')->name("aggregator.getData");
	Route::resource('aggregator','Admin\AggregatorController');
	//Video
	Route::get("videos/getdata", 'Admin\VideoController@getData')->name("videos.getData");
	Route::resource('videos','Admin\VideoController');

	//News Category
	Route::get("newcategory/getdata", 'Admin\NewCatController@getData')->name("newcategory.getData");
	Route::resource('newcategory','Admin\NewCatController');

	//News
	Route::get("news/getdata", 'Admin\NewsController@getData')->name("news.getData");
	Route::resource('news','Admin\NewsController');
	//Event
	Route::get("events/getdata", 'Admin\EventController@getData')->name("events.getData");
	Route::resource('events','Admin\EventController');
	//Slider
	Route::get("sliders/getdata", 'Admin\SliderController@getData')->name("sliders.getData");
	Route::resource('sliders','Admin\SliderController');

	//Song CMS
	Route::get("cms/getdata", 'Admin\CmsController@getData')->name("cms.getData");
	Route::resource('cms','Admin\CmsController');
	//video cms
	Route::get("videocms/getdata", 'Admin\VideoCmsController@getData')->name("videocms.getData");
	Route::resource('videocms','Admin\VideoCmsController');

	Route::get("video/{name}/videocms","Admin\VideoCmsController@createCms")->name('video.videocms');
	Route::post("video/videocms/storecms","Admin\VideoCmsController@storeCms")->name('video.videocms.storecms');
	//album cms
	Route::get("albumcms/getdata", 'Admin\AlbumCmsController@getData')->name("albumcms.getData");
	Route::resource('albumcms','Admin\AlbumCmsController');

	Route::get("cms/{name}/albumcms","Admin\AlbumCmsController@createCms")->name('cms.albumcms');
	Route::post("cms/albumcms/storecms","Admin\AlbumCmsController@storeCms")->name('cms.albumcms.storecms');
	//cms for playlist
	Route::get("cms/{name}/playlistcms","Admin\PlaylistCmsController@createCms")->name('cms.playlistcms');
	Route::post("cms/playlistcms/storecms","Admin\PlaylistCmsController@storeCms")->name('cms.playlistcms.storecms');
	//cms for collection
	Route::get("cms/{name}/albumlistcms","Admin\AlbumlistCmsController@createCms")->name('cms.albumlistcms');
	Route::post("cms/albumlistcms/storecms","Admin\AlbumlistCmsController@storeCms")->name('cms.albumlistcms.storecms');
	//promotions
	Route::get("promotions/getdata", 'Admin\PromotionController@getData')->name("promotions.getData");
	Route::resource('promotions','Admin\PromotionController');
	//notification
	Route::get("notification/getdata", 'Admin\NotificationController@getData')->name("notification.getData");
	Route::resource('notification','Admin\NotificationController');

	Route::get('reports/payment','Admin\ReportController@payment')->name('reports.payment');
	//for report

	Route::get('reports/getpayment','Admin\ReportController@getPaymentData')->name('report.getPaymentData');

	//for transaction
	Route::post('reports/withpayment','Admin\ReportController@wihtPayment')->name('withpayment');

	Route::get('reports/getsonglintendata','Admin\ReportController@getSongListenData')->name('reports.getSongListenData');
	//for Get song order
	Route::get('reports/getsongorderdata','Admin\ReportController@getSongOrderData')->name('reports.getSongOrderData');
	Route::get('reports/getsongorder','Admin\ReportController@getSongOrder')->name('reports.getsongorder');
	Route::post('reports/songorder','Admin\ReportController@songOrder')->name('reports.songorder');
	//for Get Album order
	Route::get('reports/getalbumorderdata','Admin\ReportController@getAlbumOrderData')->name('reports.getAlbumOrderData');
	Route::get('reports/getalbumorder','Admin\ReportController@getAlbumOrder')->name('reports.getalbumorder');
	Route::post('reports/albumorder','Admin\ReportController@albumOrder')->name('reports.albumorder');
	//for Get song listen
	Route::get('reports/getsonglinten','Admin\ReportController@getSongListen')->name('reports.getsonglisten');
	Route::post('reports/songlinten','Admin\ReportController@songListen')->name('reports.songListen');

	//for Subscribes report
	Route::get('reports/getsubscribe','Admin\ReportController@getSubscription')->name('reports.getsubscribe');
	Route::post('reports/subscribe','Admin\ReportController@subscribe')->name('reports.subscribe');

	Route::get('reports/getsubscribedata','Admin\ReportController@getSubscriptionData')->name('reports.getsubscribedata');
	//bill add manually
	Route::get("bills/getdata", 'Admin\BillController@getData')->name("bills.getData");
	Route::resource('bills','Admin\BillController');
	//
	Route::get("tracklisten/getdata", 'Admin\SongViewController@getData')->name("tracklisten.getData");
	Route::get("tracklisten","Admin\SongViewController@index")->name('tracklisten');
	//for support
	// currency
	Route::get("support","Admin\SupportController@create")->name('support.create');
	Route::post("support/store","Admin\SupportController@store")->name('support.store');

    Route::get("weekly-top","Admin\WeeklyTopController@show")->name('weekly.show');
    Route::post("weekly-top","Admin\WeeklyTopController@update")->name('weekly.update');
});



