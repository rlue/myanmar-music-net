<?php

return [

    /*
     * Coda URL.
     * Sandbox: https://sandbox.codapayments.com/
     * Production: https://airtime.codapayments.com/
     *
     */
    'airtimeURL' => env('CODAPAY_AIRTIME_URL', ''),

    /*
     * Merchant API key that has been assigned by Coda Payments
     *
     */
    'apikey' => env('CODAPAY_API_KEY', ''),

    /*
     * the country code in which you’ll be processing payments (ISO 3166).
     * i.e. Indonesia(360), Malaysia(458), Sri Lanka(144), Thailand(764), Myanmar(104)
     */
    'country' => 104,

    /*
     * the currency code in which you’ll be processing payments (ISO 4217).
     * i.e. Indonesia(360), Malaysia(458), Sri Lanka(144), Thailand(764), Myanmar(104)
     *
     */
    'currency' => 104,

    /*
     * Required if Merchant is using Restful web service.
     */
    'airtimeRestURL' => env('CODAPAY_AIRTIME_REST_URL', ''),

    /*
     * Required if Merchant is using SOAP web service.
     *
     */
    'airtimeWSDLURL' => 'https://sandbox.codapayments.com /airtime/api/soap/v1.0?wsdl',

    /*
     * The transaction type (1 - Web, 2 - MobileApp, 3 - MobileWeb).
     *
     */
    'txnType' => 0,

    /*
     * Should be 'json' or 'xml' and it is required only if merchant use Restful web service.
     *
     */
    'requestType' => 'json',

];
