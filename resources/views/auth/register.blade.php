@extends('frontend-layouts.app')

@section('content')
    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_login_wrapper padder_top90">
        @include('frontend-layouts.partial.header')
        <div class="ms_login">
            <div class="container-fluid">
                <div class="row">
                    <div class="col"></div>
                    <div class="col-md-9">
                        <div class="ms_register_img">
                            <img src="/images/register_img.png" alt="" class="img-fluid" />
                        </div>
                        <div class="ms_register_form">
                            <h2>Register / Sign Up</h2>

                            <div class="social-or">
                                <a href="{!! URL::to('auth/facebook') !!}" class="ms_btn"><i class="fa fa-facebook-square"></i> SIGN UP WITH FACEBOOK</a>
                            </div>

                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="name" placeholder="Enter Your Name" class="form-control{{$errors->has('name') ? ' is-invalid' : '' }}">
                                    <span class="form_icon"><i class="fa_icon form-user" aria-hidden="true"></i></span>
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback text-left text-white" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="text" name="email" placeholder="Enter Your Email" class="form-control{{$errors->has('email') ? ' is-invalid' : '' }}">
                                    <span class="form_icon"><i class="fa_icon form-envelope" aria-hidden="true"></i></span>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback text-left text-white" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" placeholder="Enter Password" class="form-control{{$errors->has('password') ? ' is-invalid' : '' }}">
                                    <span class="form_icon"><i class="fa_icon form-lock" aria-hidden="true"></i></span>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback text-left text-white" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password_confirmation" placeholder="Confirm Password" class="form-control">
                                    <span class="form_icon"><i class=" fa_icon form-lock" aria-hidden="true"></i></span>
                                </div>
                                <button type="submit" class="ms_btn">register now</button>
                            </form>

                            <p>Already Have An Account? <a href="/login" class="ms_modal hideCurrentModel">login here</a></p>
                        </div>


                    </div>
                    <div class="col"></div>

                    <br>

                </div>
            </div>
        </div>
    </div>
@endsection
