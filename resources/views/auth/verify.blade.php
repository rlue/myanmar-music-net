@extends('frontend-layouts.app')

@section('content')
    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top80">
        @include('frontend-layouts.partial.header')

        <div class="ms_profile_wrapper">
            <h1>{{ __('Verify Your Email Address') }}</h1>
            <div class="ms_profile_box">

                <div class="ms_profile_list" style="color: #fff">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                </div>

            </div>
        </div>

    </div>
@endsection