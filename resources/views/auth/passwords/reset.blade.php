@extends('frontend-layouts.app')

@section('content')
    <!---Main Content Start--->
    <div class="padder_top80 ms_profile">

    @include('frontend-layouts.partial.header')

    <!----Edit Profile Wrapper Start---->
        <div class="ms_profile_wrapper">
            <h1>{{ __('Reset Password') }}</h1>
            <div class="ms_profile_box">

                <div class="ms_pro_form">

                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group">
                            <label for="email">{{ __('E-Mail Address') }}</label>

                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password">{{ __('Password') }}</label>

                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password-confirm">{{ __('Confirm Password') }}</label>

                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>

                        <div class="pro-form-btn text-center marger_top15">
                            <button type="submit" class="ms_btn">{{ __('Reset Password') }}</button>
                            <a href="{{ url('/') }}" class="ms_btn">cancel</a>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
