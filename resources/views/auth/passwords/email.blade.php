@extends('frontend-layouts.app')

@section('content')
    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top80">
        @include('frontend-layouts.partial.header')

        <div class="ms_profile_wrapper">
            <h1>{{ __('Reset Password') }}</h1>
            <div class="ms_profile_box">

                <div class="ms_pro_form" style="color: #fff">
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group">
                            <label for="email">{{ __('E-Mail Address') }}</label>

                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="pro-form-btn text-center marger_top15">
                            <button type="submit" class="ms_btn">{{ __('Reset') }}</button>
                            <a href="{{ url('/') }}" class="ms_btn">cancel</a>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>
@endsection
