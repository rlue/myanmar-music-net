@extends('frontend-layouts.app')

@section('content')
    <!---Main Content Start--->
    <div class="padder_top80 ms_profile">

    @include('frontend-layouts.partial.header')

    <!----Edit Profile Wrapper Start---->
        <div class="ms_profile_wrapper">
            <h1>Change Password</h1>
            <div class="ms_profile_box">
                <div class="ms_pro_form">
                    {!! Form::open(['method' => 'POST', 'route' => 'user.password.change' ]) !!}
                    <div class="form-group">
                        <label>Current password *</label>
                        <input type="password" name="old_password" placeholder="Current password" class="form-control{{$errors->has('old_password') ? ' is-invalid' : '' }}">
                        @if ($errors->has('old_password'))
                            <span class="invalid-feedback text-left" role="alert">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>New Password *</label>
                        <input type="password" name="password" placeholder="New Password" class="form-control{{$errors->has('password') ? ' is-invalid' : '' }}">
                        @if ($errors->has('password'))
                            <span class="invalid-feedback text-left" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Confirm Password *</label>
                        <input type="password" name="password_confirmation" placeholder="Re-type new password" class="form-control">
                    </div>
                    <div class="pro-form-btn text-center marger_top15">
                        <button type="submit" class="ms_btn">save</button>
                        <a href="{{ url('user/profile') }}" class="ms_btn">cancel</a>
                    </div>
                    {{ Form::close() }}
                </div>

            </div>
        </div>
    </div>
@endsection