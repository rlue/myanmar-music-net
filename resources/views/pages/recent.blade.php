@extends('frontend-layouts.app')

@section('content')

    <!---Main Content Start--->
    <div class="ms_album_single_wrapper ms_profile">

    @include('frontend-layouts.partial.header')

    <!----Free Download Css Start---->
        <div class="ms_heading">
            <h1>Your Recent Songs</h1>
        </div>
        <div class="album_inner_list">
            <div class="album_list_wrapper">
                <ul class="album_list_name">
                    <li>#</li>
                    <li>Title</li>
                    <li>Album</li>
                    <li class="text-center">Duration</li>
                    <li class="text-center">Fav</li>
                    <li class="text-center">Share</li>
                    <li class="text-center">Buy</li>
                </ul>
                @forelse($recent_songs as $key => $recent_song)
                    <ul>
                        <li><a href="javascript:;" class="mmn-play"><span class="play_no">{{ str_pad($key+1, 2, '0', STR_PAD_LEFT) }}</span><span class="play_hover"></span></a></li>
                        <li><a href="javascript:;" class="mmn-play">{{ $recent_song->name_mm }}</a></li>
                        @if($recent_song->albums->isNotEmpty())
                            <li><a href="{{ url('album/' . $recent_song->albums->first()->uuid) }}">{{ $recent_song->albums->first()->album_name_mm }}</a></li>
                            <li class="text-center"><a href="javascript:;">{{ $recent_song->duration }}</a></li>
                            <li class="text-center"><a href="javascript:;" onclick="sendFav('/favourite/song/{{ $recent_song->id }}', this)" class="{{ $recent_song->getIsFavourite() ? ' text-danger' : ''  }}"><span class="fa fa-heart"></span> </a></li>
                            <li class="text-center"><a class="btn btn-primary fb_share_btn" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url("album/{$recent_song->albums->first()->uuid}")) }}" target="_blank"><span class="fa fa-facebook-official"></span> share</a></li>

                            @if($recent_song->getIsBought())
                                <li class="text-center">
                                    <a href="/user/download_song/{{ $recent_song->id }}" class="song_download">
                                        <span class="ms_close"><img src="/images/svg/download.svg" alt=""></span>
                                    </a>
                                </li>
                            @elseif($recent_song->sub_menu['store'])
                                <li class="text-center">
                                    <a href="javascript:;" onclick="showBuySongModal({{ $recent_song->id }})">Buy ({{ $recent_song->price->name }} Kyat)</a>
                                </li>
                            @elseif($recent_song->sub_menu['free'])
                                <li class="text-center">
                                    <a href="/download_free_song/{{ $recent_song->id }}" class="song_download">
                                        <span class="ms_close"><img src="/images/svg/download.svg" alt=""></span>
                                    </a>
                                </li>
                            @else
                                <li></li>
                            @endif
                        @else
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        @endif
                    </ul>
                @empty
                    <ul>
                        <li class="w-100 text-center"><a href="#">No Data</a></li>
                    </ul>
                @endforelse
            </div>
        </div>

        <!----Main div close---->
    </div>

    <div id="mmn-songs" style="display: none;">
        @foreach($recent_songs as $key => $recent_song)
            @if($recent_song->albums->isNotEmpty())
                <a href="{{ $recent_song->getAudioFile() }}" data-image="{{ $recent_song->getAlbumImage() }}" data-id="{{ $recent_song->id }}" title="{{ $recent_song->name_mm }}" data-artist="{{ $recent_song->artists->implode('artist_name_eng', ', ') }}" class="mmn-single-track" rel="">{{ $recent_song->name_mm }}</a>
            @endif
        @endforeach
    </div>
@endsection
