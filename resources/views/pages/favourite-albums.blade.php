@extends('frontend-layouts.app')

@section('css')
    <style>
        .ms_free_download.ms_purchase_wrapper .album_list_wrapper>ul>li {
            width: 16%;
        }
    </style>
@endsection
@section('content')


    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top50">

        @include('frontend-layouts.partial.header')
    <!----Free Download Css Start---->
        <div class="ms_free_download ms_purchase_wrapper">
            <div class="ms_heading">
                <h1>Your Favourite Albums</h1>
            </div>
            <div class="album_inner_list">
                <div class="album_list_wrapper">
                    <ul class="album_list_name">
                        <li>#</li>
                        <li>Album Title</li>
                        <li>Artist</li>
                        <li>Count</li>
                        <li class="text-center">price</li>
                        <li class="text-center">remove</li>
                    </ul>

                    @forelse($albums as $key => $album)
                        <ul>
                            <li><a href="{{ url('album/'. $album->uuid) }}"><span class="play_no">{{ str_pad($key+1, 2, '0', STR_PAD_LEFT) }}</span><span class="play_hover"></span></a></li>
                            <li><a href="{{ url('album/'. $album->uuid) }}">{{ $album->album_name_mm }}</a></li>
                            <li>
                                @if($album->artists->count() > 3)
                                    <a href="javascript:;">Various Artists</a>
                                @else
                                    @foreach($album->artists as $artist)
                                        <a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }}</a>
                                        @if(!$loop->last) , @endif
                                    @endforeach
                                @endif
                            </li>
                            <li><a href="#">{{ $album->songs->count() }}</a></li>
                            <li class="text-center"><a href="#">{{ $album->prices->name }}</a></li>
                            <li class="text-center">
                                <a href="javascript:;" onclick="removeFav('/favourite/album/{{ $album->uuid }}')">
                                    <span class="ms_close"><img src="/images/svg/close.svg" alt=""></span></a>
                            </li>
                        </ul>
                    @empty
                        <ul>
                            <li class="w-100 text-center"><a href="#">No Albums</a></li>
                        </ul>
                    @endforelse

                </div>
            </div>
        </div>

        <!----Main div close---->
    </div>
@endsection


@push('js')
    <script>

        function removeFav(url) {
            $.ajax({
                type: 'DELETE',
                url: url,
                success:function(response){
                    alert(response.message);
                    location.reload();
                }
            });
        }

    </script>
@endpush
