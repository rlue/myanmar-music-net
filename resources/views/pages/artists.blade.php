@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="Artists - Myanmar Music Network">
    <meta property="og:image" content="{{ asset('images/social-logo.png') }}">

    <meta name="twitter:title" content="Artists - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset('images/social-logo.png') }}">
    <meta name="twitter:card" content="{{ asset('images/social-logo.png') }}">
@endsection

@section('content')

    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top50">

        @include('frontend-layouts.partial.header')

        <!----Top Artist Section---->
            @foreach($categories as $category)
                <div class="ms_top_artist">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ms_heading">
                                    <h1>{{ $category->name }}</h1>
                                    <span class="veiw_all"><a href="{{ url('artists/' . $category->id . '/' . str_slug($category->name)) }}">view more</a></span>
                                </div>
                            </div>

                            @foreach($category->artists as $artist)
                            <div class="col-lg-2 col-md-2">
                                <div class="ms_rcnt_box marger_bottom30">
                                    <div class="ms_rcnt_box_img">
                                        <img src="{{ asset($artist->artist_image) }}" alt="{{ $artist->name }}" class="img-fluid">
                                        <div class="ms_main_overlay">
                                            <div class="ms_box_overlay"></div>
                                            <div class="ms_play_icon">
                                                <a href="{{ url('artist/' . $artist->id) }}"><img src="/images/svg/play.svg" alt=""></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ms_rcnt_box_text">
                                        <h3><a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }}</a></h3>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach


        <!----Main div close---->
    </div>
@endsection

