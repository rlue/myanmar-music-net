@extends('frontend-layouts.app')

@section('css')
    <style>
        .ms_free_download.ms_purchase_wrapper .album_list_wrapper>ul>li {
            width: 19%;
        }
    </style>
@endsection

@section('content')

    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top50">

    @include('frontend-layouts.partial.header')

        <!----Free Download Css Start---->
        <div class="ms_free_download ms_purchase_wrapper">
            <div class="ms_heading">
                <h1>Your Purchased Albums</h1>
            </div>
            <div class="album_inner_list">
                <div class="album_inner_list">
                    <div class="album_list_wrapper">
                        <ul class="album_list_name">
                            <li>#</li>
                            <li>Artwork</li>
                            <li>Album Title</li>
                            <li>Artist</li>
                            <li class="text-center">Action</li>
                        </ul>

                        @forelse($albums as $key => $album)
                            <ul>
                                <li><a href="#"><span class="play_no">{{ str_pad($key+1, 2, '0', STR_PAD_LEFT) }}</span><span class="play_hover"></span></a></li>
                                <li><a href="#"><img src="{{ $album->album_image }}" alt="{{ $album->album_name_mm }}" width="100"></a></li>
                                <li><a href="#">{{ $album->album_name_mm }}</a></li>
                                <li><a href="#">{{ $album->artists->implode('artist_name_eng', ', ') }}</a></li>
                                <li class="text-center">
                                    <a href="/mymusic/purchased_album/{{ $album->uuid }}" class="ms_btn" style="color:#ffffff !important;">Go to Album</a>
                                </li>
                            </ul>
                        @empty
                            <ul>
                                <li class="w-100 text-center"><a href="#">No Albums</a></li>
                            </ul>
                        @endforelse

                    </div>
                </div>
            </div>
        </div>

        <!----Main div close---->
    </div>
@endsection
