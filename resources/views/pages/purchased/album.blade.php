@extends('frontend-layouts.app')

@section('css')
    <style>
        .ms_free_download.ms_purchase_wrapper .album_list_wrapper>ul>li {
            width: 16%;
        }
    </style>
@endsection

@section('content')

    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top50">

    @include('frontend-layouts.partial.header')

        <!----Free Download Css Start---->
        <div class="ms_free_download ms_purchase_wrapper">
            <div class="ms_heading">
                <h1>Your Purchased Album ( {{ $album->album_name_mm }} )</h1>
            </div>

            <div class="album_single_data">
                <div class="album_single_img">
                    <img src="{{ $album->album_image }}" alt="" class="img-fluid">
                </div>
                <div class="album_single_text">
                    <h2>{{ $album->album_name_mm }}</h2>
                    <p class="singer_name">By -
                        @foreach($album->artists as $artist)
                            <a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }}</a>
                            @if(!$loop->last)
                                ,
                            @endif
                        @endforeach
                    </p>
                    <div class="album_feature">
                        <a href="#" class="album_date">{{ $album->songs->count() }} song</a>
                        <a href="#" class="album_date">Released {{ carbon_parse($album->release_date)->format('M d, Y') }}</a>
                        @if($album->sub_menu['streaming'])
                            <span style="font-size: 15px; color: #ed1557; border: 2px solid #ed1557;padding:5px; border-radius: 10px; opacity: 0.7;">
    						<i class="fa fa-play-circle" style="vertical-align: middle;"></i> mPlay
                    </span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="album_inner_list">
                <div class="album_list_wrapper">
                    <ul class="album_list_name">
                        <li>#</li>
                        <li>Song Title</li>
                        <li>Artist</li>
                        <li class="text-center">Duration</li>
                        <li class="text-center">Favourite</li>
                        <li class="text-center">Download</li>
                    </ul>
                    @forelse($songs as $key => $song)
                        <ul>
                            <li><a href="javascript:;" class="mmn-play"><span class="play_no">{{ str_pad($key+1, 2, '0', STR_PAD_LEFT) }}</span><span class="play_hover"></span></a></li>
                            <li><a href="javascript:;" class="mmn-play">{{ $song->name_mm }}</a></li>
                            <li>
                                @foreach($album->artists as $artist)
                                    <a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }}</a>
                                    @if(!$loop->last)
                                        ,
                                    @endif
                                @endforeach
                            </li>
                            <li class="text-center"><a href="javascript:;">{{ $song->duration }}</a></li>
                            <li class="text-center"><a href="javascript:;" onclick="sendFav('/favourite/song/{{ $song->id }}', this)" class="{{ $song->getIsFavourite() ? ' text-danger' : ''  }}"><span class="fa fa-heart"></span> </a></li>
                            <li class="text-center">
                                <a href="/user/{{ $album->uuid }}/download_album/{{ $song->id }}" class="song_download">
                                    <span class="ms_close"><img src="/images/svg/download.svg" alt=""></span>
                                </a>
                            </li>
                        </ul>
                    @empty
                        <ul>
                            <li class="w-100 text-center"><a href="#">No Data</a></li>
                        </ul>
                    @endforelse
                </div>
            </div>
        </div>

        <!----Main div close---->
    </div>

    <div id="mmn-songs" style="display: none;">
        @foreach($songs as $key => $song)
            <a href="{{ $song->full_file }}" data-image="{{ $album->album_image }}" data-id="{{ $song->id }}" title="{{ $song->name_mm }}" data-artist="{{ $song->artists->implode('artist_name_eng', ', ') }}" class="mmn-single-track" rel="">{{ $song->name_mm }}</a>
        @endforeach
    </div>
@endsection
