@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="{{ $artist->artist_name_eng }} - Myanmar Music Network">
    <meta property="og:image" content="{{ asset(str_replace(' ', '%20',$artist->artist_image)) }}">

    <meta name="twitter:title" content="{{ $artist->artist_name_eng }} - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset(str_replace(' ', '%20',$artist->artist_image)) }}">
    <meta name="twitter:card" content="{{ asset(str_replace(' ', '%20',$artist->artist_image)) }}">
@endsection

@section('content')

    <!----Album Single Section Start---->
        <div class="ms_album_single_wrapper ms_artist_single ms_profile">
            @include('frontend-layouts.partial.header')
            <div class="album_single_data">
                <div class="album_single_img">
                    <img src="{{ $artist->artist_image }}" alt="" class="img-fluid">
                </div>
                <div class="album_single_text">
                    <h2>{{ $artist->artist_name_eng }}</h2>
                    <div class="about_artist">
                        {{ $artist->artist_description }}
                    </div>
                </div>
            </div>
            <div class="ms_top_artist">
                <div class="container-fluid">
                    <div class="row">
                        @foreach($albums as $album)
                            <div class="col-lg-2 col-md-2 col-6">
                                <div class="ms_rcnt_box marger_bottom30">
                                    <div class="ms_rcnt_box_img">
                                        <img src="{{ $album->album_image }}" alt="" class="img-fluid">
                                        <div class="ms_main_overlay">
                                            <div class="ms_box_overlay"></div>

                                            <div class="ms_more_icon">
                                                <img src="/images/svg/more.svg" alt="">
                                            </div>

                                            <ul class="more_option">
                                                @auth
                                                    <li><a href="javascript:;" onclick="sendFav('/favourite/album/{{ $album->uuid }}')"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                                                    @if($album->sub_menu['store'])
                                                        <li><a href="javascript:showBuyAlbumModal({{ $album->uuid }})"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $album->prices->name }} Kyat)</a></li>
                                                    @endif

                                                @else
                                                    <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                                                    @if($album->sub_menu['store'])
                                                        <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $album->prices->name }} Kyat)</a></li>
                                                    @endif
                                                @endauth

                                                <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url("album/{$album->uuid}")) }}" target="_blank"><span class="opt_icon"><span class="fa fa-facebook-official"></span></span>Share</a></li>
                                            </ul>

                                            <div class="ms_play_icon">
                                                <a href="{{ url("album/{$album->uuid}") }}">
                                                    <img src="/images/svg/play.svg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ms_rcnt_box_text">
                                        <h3><a href="{{ url("album/{$album->uuid}") }}">{{ $album->album_name_mm }}</a></h3>
                                        <p>
                                            @if($album->artists->count() > 3)
                                                <a href="javascript:;">Various Artists</a>
                                            @else
                                                @foreach($album->artists as $artist)
                                                    <a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }}</a>
                                                    @if(!$loop->last) , @endif
                                                @endforeach
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>


        </div>

@endsection
