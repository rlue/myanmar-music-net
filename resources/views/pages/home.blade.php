@extends('frontend-layouts.app')

@section('content')

    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile">

        @include('frontend-layouts.partial.header')

        <div class="header_padder_top"></div>

        <!---Banner--->
        @if($sliders->isNotEmpty())
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @foreach($sliders as $key => $slider)
                        <li data-target="#carouselExampleIndicators" data-slide-to="{{ $key }}" class="{{ $loop->first ? 'active' : '' }}"></li>
                    @endforeach
                </ol>
                <div class="carousel-inner">
                    @foreach($sliders as $slider)
                        <div class="carousel-item{{ $loop->first ? ' active' : '' }}">
                            <a href="{{ $slider->slider_link }}">
                                <img class="d-block w-100" src="{{ $slider->slider_image }}" alt="{{ $slider->name }}">
                            </a>
                        </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <br>
        @endif


    <!----New Releases Section Start---->
        @if(!is_null($new_albums))
            <div class="ms_radio_wrapper">
                <div class="ms_heading">
                    <h1>New Releases</h1>
                    <span class="veiw_all"><a href="/new-releases">view more</a></span>
                </div>
                <div class="ms_radio_slider swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($new_albums as $new_album)
                            <div class="swiper-slide">
                                <div class="ms_rcnt_box">
                                    <div class="ms_rcnt_box_img">
                                        <img src="{{ $new_album->album_image }}" alt="">
                                        <div class="ms_main_overlay">
                                            <div class="ms_box_overlay"></div>

                                            <div class="ms_more_icon">
                                                <img src="/images/svg/more.svg" alt="">
                                            </div>

                                            <ul class="more_option">
                                                @auth
                                                    <li><a href="javascript:;" onclick="sendFav('/favourite/album/{{ $new_album->uuid }}')"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                                                    @if($new_album->sub_menu['store'])
                                                        <li><a href="javascript:showBuyAlbumModal({{ $new_album->uuid }})"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $new_album->prices->name }} Kyat)</a></li>
                                                    @endif

                                                @else
                                                    <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                                                    @if($new_album->sub_menu['store'])
                                                        <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $new_album->prices->name }} Kyat)</a></li>
                                                    @endif
                                                @endauth

                                                    <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url("album/{$new_album->uuid}")) }}" target="_blank"><span class="opt_icon"><span class="fa fa-facebook-official"></span></span>Share</a></li>
                                            </ul>

                                            <div class="ms_play_icon">
                                                <a href="{{ url("album/{$new_album->uuid}") }}">
                                                    <img src="/images/svg/play.svg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ms_rcnt_box_text">
                                        <h3><a href="{{ url("album/{$new_album->uuid}") }}">{{ $new_album->album_name_mm }}</a></h3>
                                        <p>
                                            @if($new_album->artists->count() > 3)
                                                <a href="javascript:;">Various Artists</a>
                                            @else
                                                @foreach($new_album->artists as $artist)
                                                    <a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }}</a>
                                                    @if(!$loop->last) , @endif
                                                @endforeach
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next4 slider_nav_next"></div>
                <div class="swiper-button-prev4 slider_nav_prev"></div>
            </div>
        @endif

        @if(!is_null($top_albums))
        <!----Featured Albumn Section Start---->
            <div class="ms_fea_album_slider">
                <div class="ms_heading">
                    <h1>Top Albums</h1>
                    <span class="veiw_all"><a href="/top-albums">view more</a></span>
                </div>
                <div class="ms_album_slider swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($top_albums as $top_album)
                            <div class="swiper-slide">
                                <div class="ms_rcnt_box">
                                    <div class="ms_rcnt_box_img">
                                        <img src="{{ $top_album->album_image }}" alt="">
                                        <div class="ms_main_overlay">
                                            <div class="ms_box_overlay"></div>
                                            <div class="ms_more_icon">
                                                <img src="/images/svg/more.svg" alt="">
                                            </div>

                                            <ul class="more_option">
                                                @auth
                                                    <li><a href="javascript:;" onclick="sendFav('/favourite/album/{{ $top_album->uuid }}')"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                                                    @if($top_album->sub_menu['store'])
                                                        <li><a href="javascript:showBuyAlbumModal({{ $top_album->uuid }})"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $top_album->prices->name }} Kyat)</a></li>
                                                    @endif
                                                @else
                                                    <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                                                    @if($top_album->sub_menu['store'])
                                                        <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $top_album->prices->name }} Kyat)</a></li>
                                                    @endif
                                                @endauth

                                                <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url("album/{$top_album->uuid}")) }}" target="_blank"><span class="opt_icon"><span class="fa fa-facebook-official"></span></span>Share</a></li>
                                            </ul>

                                            <div class="ms_play_icon">
                                                <a href="{{ url("album/{$top_album->uuid}") }}">
                                                    <img src="/images/svg/play.svg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ms_rcnt_box_text">
                                        <h3><a href="{{ url("album/{$top_album->uuid}") }}">{{ $top_album->album_name_mm }}</a></h3>
                                        <p>
                                            @if($top_album->artists->count() > 3)
                                                <a href="javascript:;">Various Artists</a>
                                            @else
                                                @foreach($top_album->artists as $artist)
                                                    <a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }} @if(!$loop->last) , @endif</a>
                                                @endforeach
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next3 slider_nav_next"></div>
                <div class="swiper-button-prev3 slider_nav_prev"></div>
            </div>
        @endif

    <!---Weekly Top 15--->
        <div class="ms_weekly_wrapper">
            <div class="ms_weekly_inner">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ms_heading">
                            <h1>Weekly Top 20</h1>
                            <span class="veiw_all"><a href="/music/top_tracks">view more</a></span>
                        </div>
                    </div>
                    @php $count=0; @endphp
                    @foreach($songs->chunk(5) as $chunk_songs)
                        <div class="col-lg-4 col-md-12 padding_right40">
                            @foreach($chunk_songs as $song)
                                @php $count++; @endphp
                                <div class="ms_weekly_box">
                                    <div class="weekly_left">
                                    <span class="w_top_no">
										{{ str_pad($count, 2, 0, STR_PAD_LEFT) }}
									</span>
                                        <div class="w_top_song">
                                            <div class="w_tp_song_img">
                                                <a href="javascript:;" class="mmn-play" data-id="{{ $song->id }}" >
                                                    <img src="{{ $song->getAlbumImage() }}" alt="" class="img-fluid" onError="this.onerror=null;this.src='/images/album/album.jpg';">
                                                    <div class="ms_song_overlay">
                                                    </div>
                                                    <div class="ms_play_icon">
                                                        <img src="{{ asset('/images/svg/play.svg') }}" alt="">
                                                    </div>
                                                </a>

                                            </div>
                                            <div class="w_tp_song_name">
                                                <h3><a href="javascript:;" class="mmn-play" data-id="{{ $song->id }}">{{ $song->name_mm }}</a></h3>
                                                <p class="text-white">
                                                    @if($song->artists->count() > 3)
                                                        <a href="javascript:;" class="text-white">Various Artists</a>
                                                    @else
                                                        @foreach($song->artists as $artist)
                                                            <a href="{{ url('artist/' . $artist->id) }}" class="text-white">{{ $artist->artist_name_eng }} @if(!$loop->last) , @endif</a>
                                                        @endforeach
                                                    @endif
                                                    @if($song->feats->isNotEmpty())
                                                            <a href="javascript:;" class="text-white">, ft : </a>
                                                            @foreach($song->feats as $feat_artist)
                                                                <a href="{{ url('artist/' . $feat_artist->id) }}" class="text-white">{{ $feat_artist->artist_name_eng }} @if(!$loop->last) , @endif</a>
                                                            @endforeach
                                                    @endif
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="weekly_right">
                                    <span class="ms_more_icon" data-other="1">
										<img src="/images/svg/more.svg" alt="">
									</span>
                                    </div>

                                    <ul class="more_option">
                                        @auth
                                            <li><a href="javascript:;" onclick="sendFav('/favourite/song/{{ $song->id }}')"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                                            @if($song->sub_menu['store'])
                                                <li><a href="javascript:showBuySongModal({{ $song->id }})"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $song->price->name }} Kyat)</a></li>
                                            @elseif($song->sub_menu['free'])
                                                <li>
                                                    <a href="/download_free_song/{{ $song->id }}" class="song_download">
                                                        <span class="opt_icon"><span class="fa fa-download"></span></span>Download
                                                    </a>
                                                </li>
                                            @endif
                                        @else
                                            <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites</a></li>
                                            @if($song->getIsBought())
                                                <li>
                                                    <a href="/user/download_song/{{ $song->id }}" class="song_download">
                                                        <span class="opt_icon"><span class="fa fa-download"></span></span>Download
                                                    </a>
                                                </li>
                                            @elseif($song->sub_menu['store'])
                                                <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $song->price->name }} Kyat)</a></li>
                                            @elseif($song->sub_menu['free'])
                                                <li>
                                                    <a href="/download_free_song/{{ $song->id }}" class="song_download">
                                                        <span class="opt_icon"><span class="fa fa-download"></span></span>Download
                                                    </a>
                                                </li>
                                            @endif
                                        @endauth
                                            <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url("album/{$song->albums->first()->uuid}")) }}" target="_blank"><span class="opt_icon"><span class="fa fa-facebook-official"></span></span>Share</a></li>
                                    </ul>

                                </div>
                                <div class="ms_divider"></div>
                            @endforeach
                        </div>

                    @endforeach
                </div>
            </div>
        </div>

        @if($artists->isNotEmpty())
        <!---Featured Artists Music--->
            <div class="ms_featured_slider">
                <div class="ms_heading">
                    <h1>Featured Artists</h1>
                    <span class="veiw_all"><a href="/artists">view more</a></span>
                </div>
                <div class="ms_feature_slider swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($artists as $artist)
                            <div class="swiper-slide">
                                <div class="ms_rcnt_box">
                                    <div class="ms_rcnt_box_img">
                                        <img src="{{ asset($artist->artist_image) }}" alt="{{ $artist->artist_name_en }}">
                                        <div class="ms_main_overlay">
                                            <div class="ms_box_overlay"></div>
                                            <div class="ms_play_icon">
                                                <a href="{{ url('artist/' . $artist->id) }}"><img src="/images/svg/play.svg" alt=""></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ms_rcnt_box_text">
                                        <h3><a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }}</a></h3>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next1 slider_nav_next"></div>
                <div class="swiper-button-prev1 slider_nav_prev"></div>
            </div>
        @endif


        {{--@if(!is_null($top_playlists))--}}
        {{--<!----Featured Albumn Section Start---->--}}
            {{--<div class="ms_fea_album_slider">--}}
                {{--<div class="ms_heading">--}}
                    {{--<h1>Top Playlist</h1>--}}
                    {{--<span class="veiw_all"><a href="/playlist">view all</a></span>--}}
                {{--</div>--}}
                {{--<div class="ms_album_slider swiper-container">--}}
                    {{--<div class="swiper-wrapper">--}}
                        {{--@foreach($top_playlists as $top_playlist)--}}
                            {{--<div class="swiper-slide">--}}
                                {{--<div class="ms_rcnt_box">--}}
                                    {{--<div class="ms_rcnt_box_img">--}}
                                        {{--<img src="{{ $top_playlist->playlist_image }}" alt="">--}}
                                        {{--<div class="ms_main_overlay">--}}
                                            {{--<div class="ms_box_overlay"></div>--}}

                                            {{--<div class="ms_play_icon">--}}
                                                {{--<a href="{{ url("playlist/{$top_playlist->id}") }}">--}}
                                                    {{--<img src="/images/svg/play.svg" alt="">--}}
                                                {{--</a>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="ms_rcnt_box_text">--}}
                                        {{--<h3><a href="{{ url("playlist/{$top_playlist->id}") }}">{{ $top_playlist->playlist_name_mm }}</a></h3>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--@endforeach--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<!-- Add Arrows -->--}}
                {{--<div class="swiper-button-next3 slider_nav_next"></div>--}}
                {{--<div class="swiper-button-prev3 slider_nav_prev"></div>--}}
            {{--</div>--}}
        {{--@endif--}}


        @if(!is_null($recommended_playlists))
        <!----Featured Albumn Section Start---->
            <div class="ms_fea_album_slider">
                <div class="ms_heading">
                    <h1>Recommended Playlist</h1>
                    <span class="veiw_all"><a href="/recommended-playlist">view more</a></span>
                </div>
                <div class="ms_album_slider swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($recommended_playlists as $recommended_playlist)
                            <div class="swiper-slide">
                                <div class="ms_rcnt_box">
                                    <div class="ms_rcnt_box_img">
                                        <img src="{{ $recommended_playlist->playlist_image }}" alt="">
                                        <div class="ms_main_overlay">
                                            <div class="ms_box_overlay"></div>

                                            <div class="ms_play_icon">
                                                <a href="{{ url("playlist/{$recommended_playlist->id}") }}">
                                                    <img src="/images/svg/play.svg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ms_rcnt_box_text">
                                        <h3><a href="{{ url("playlist/{$recommended_playlist->id}") }}">{{ $recommended_playlist->playlist_name_mm }}</a></h3>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next3 slider_nav_next"></div>
                <div class="swiper-button-prev3 slider_nav_prev"></div>
            </div>
        @endif

        <!----Top Genres Section Start---->
        <div class="ms_genres_wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ms_heading">
                        <h1>Genres</h1>
                        <span class="veiw_all"><a href="/genres">view more</a></span>
                    </div>
                </div>
                @foreach($genres as $genre)
                    <div class="col-lg-2 col-md-2 col-6">
                        <div class="ms_genres_box">
                            <img src="{{ $genre->songCategory_image }}" alt="" class="img-fluid" />
                            <div class="ms_main_overlay">
                                <div class="ms_box_overlay"></div>
                                <div class="ms_play_icon">
                                    <a href="/genre/{{ $genre->id }}">
                                        <img src="/images/svg/play.svg" alt="">
                                    </a>
                                </div>
                                <div class="ovrly_text_div">
                                    <span class="ovrly_text1"><a href="/genre/{{ $genre->id }}">{{ $genre->name }}</a></span>
                                    <span class="ovrly_text2"><a href="/genre/{{ $genre->id }}">view songs</a></span>
                                </div>
                            </div>
                            <div class="ms_box_overlay_on">
                                <div class="ovrly_text_div">
                                    <span class="ovrly_text1"><a href="/genre/{{ $genre->id }}">{{ $genre->name }}</a></span>
                                    <span class="ovrly_text2"><a href="/genre/{{ $genre->id }}">view songs</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <!----Collection Section Start---->
        @if(!is_null($collections))

            <div class="ms_radio_wrapper">
                <div class="ms_heading">
                    <h1>Collections</h1>
                    <span class="veiw_all"><a href="/collections">view more</a></span>
                </div>
                <div class="ms_collection_slider swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($collections as $collection)
                            <div class="swiper-slide">
                                <div class="ms_rcnt_box">
                                    <div class="ms_rcnt_box_img">
                                        <img src="{{ $collection->albumlist_image }}" alt="">
                                        <div class="ms_main_overlay">
                                            <div class="ms_box_overlay"></div>
                                            <div class="ms_play_icon">
                                                <a href="{{ url("collection/{$collection->id}") }}">
                                                    <img src="/images/svg/play.svg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next-collection slider_nav_next" style="top: 68%;"></div>
                <div class="swiper-button-prev-collection slider_nav_prev" style="top: 68%;"></div>
            </div>

        @endif

        <div class="ms_radio_wrapper">
            <div class="ms_heading">
                <h1>Free Albums</h1>
                <span class="veiw_all"><a href="/music/all_free_music">view more</a></span>
            </div>
            <div class="ms_radio_slider swiper-container">
                <div class="swiper-wrapper">
                    @foreach($free_albums as $album)
                        <div class="swiper-slide">
                            <div class="ms_rcnt_box">
                                <div class="ms_rcnt_box_img">
                                    <img src="{{ $album->album_image }}" alt="">
                                    <div class="ms_main_overlay">
                                        <div class="ms_box_overlay"></div>
                                        <div class="ms_more_icon">
                                            <img src="/images/svg/more.svg" alt="">
                                        </div>
                                        <ul class="more_option">
                                            <li><a href="javascript:;" onclick="sendFav('/favourite/album/{{ $album->uuid }}')"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites</a></li>
                                        </ul>

                                        <ul class="more_option">
                                            @auth
                                                <li><a href="javascript:;" onclick="sendFav('/favourite/album/{{ $album->uuid }}')"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                                            @else
                                                <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                                            @endauth

                                                <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url("album/{$album->uuid}")) }}" target="_blank"><span class="opt_icon"><span class="fa fa-facebook-official"></span></span>Share</a></li>
                                        </ul>
                                        <div class="ms_play_icon">
                                            <a href="{{ url("album/{$album->uuid}") }}">
                                                <img src="/images/svg/play.svg" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="ms_rcnt_box_text">
                                    <h3><a href="{{ url("album/{$album->uuid}") }}">{{ $album->album_name_mm }}</a></h3>
                                    <p>
                                        @if($album->artists->count() > 3)
                                            <a href="javascript:;">Various Artists</a>
                                        @else
                                            @foreach($album->artists as $artist)
                                                <a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }}</a>
                                                @if(!$loop->last) , @endif
                                            @endforeach
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next4 slider_nav_next"></div>
            <div class="swiper-button-prev4 slider_nav_prev"></div>
        </div>

        <!----Main div close---->
    </div>

    <div id="mmn-songs" style="display: none;">
        @foreach($songs as $key => $song)
            <a href="{{ $song->getAudioFile() }}" data-low="{{ $song->getAudioLowFile() }}" data-image="{{ $song->getAlbumImage() }}" data-id="{{ $song->id }}" title="{{ $song->name_mm }}" data-artist="{{ $song->artists->implode('artist_name_eng', ', ') }}" class="mmn-single-track" rel="">{{ $song->name_mm }}</a>
        @endforeach
    </div>
@endsection
