@extends('frontend-layouts.app')

@section('css')
    <link rel="stylesheet" href="{{ env('CODAPAY_AIRTIME_URL') }}css/airtime_v1.0.css">
@endsection

@section('content')
    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top80">
    @include('frontend-layouts.partial.header')
    <!---Featured Artists Music--->
        <form name="itemForm" action="payment" METHOD="post" id="itemForm">
            @csrf
            <div class="ms_featured_slider">
                <div class="ms_heading">
                    <h1>Select Your Payment Method</h1>
                </div>
                <div class="ms_feature_slider swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="ms_rcnt_box">
                                <div class="ms_rcnt_box_img">
                                    <img src="{{ asset('images/codapay1.png') }}" alt="">
                                </div>
                                <div class="ms_rcnt_box_text">
                                    <label for="coda_pay"><input type="radio" id="coda_pay" value="coda_pay" name="payment_service" checked> Coda Pay</label>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="ms_rcnt_box">
                                <div class="ms_rcnt_box_img">
                                    <img src="{{ asset('images/paypal.png') }}" alt="">
                                </div>
                                <div class="ms_rcnt_box_text">
                                    <label for="paypal"><input type="radio" id="paypal" value="paypal" name="payment_service"> Paypal</label>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="ms_rcnt_box">
                                <div class="ms_rcnt_box_img">
                                    <img src="{{ asset('images/mpu_logo.png') }}" alt="">
                                </div>
                                <div class="ms_rcnt_box_text">
                                    <label for="mpu"><input type="radio" id="mpu" value="mpu" name="payment_service"> MPU</label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!----Download Latest---->
            <div class="ms_fea_album_slider">
                <div class="ms_heading">
                    <h1>Choose Your Amount</h1>
                </div>
                <div class="ms_album_slider swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="ms_rcnt_box">
                                <div class="ms_rcnt_box_img">
                                    <img src="/images/1000redeem.png" alt="">
                                </div>
                                <div class="ms_rcnt_box_text">
                                    <label for="1000"><input type="radio" id="1000" name="amount" value="1000" checked> 1000 Kyat</label>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="ms_rcnt_box">
                                <div class="ms_rcnt_box_img">
                                    <img src="/images/3000redeem.png" alt="">
                                </div>
                                <div class="ms_rcnt_box_text">
                                    <label for="3000"><input type="radio" id="3000" name="amount" value="3000"> 3000 Kyat</label>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="ms_rcnt_box">
                                <div class="ms_rcnt_box_img">
                                    <img src="/images/5000redeem.png" alt="">
                                </div>
                                <div class="ms_rcnt_box_text">
                                    <label for="5000"><input type="radio" id="5000" name="amount" value="5000"> 5000 Kyat</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="ms_view_more padder_bottom20">
                <a href="javascript:processPayment(this);" class="ms_btn" id="view_more" >Next</a>
            </div>
        </form>

        <form method="POST" action="{{ config('mpu.url') }}" id="mpuForm" style="display: none;">
            <input type="text" id="merchantID" name="merchantID" value="" />
            <input type="text" id="invoiceNo" name="invoiceNo" value="" />
            <input type="text" id="productDesc" name="productDesc" value="" >
            <input type="text" id="amount" name="amount" value="" />
            <input type="text" id="currencyCode" name="currencyCode" value="" />
            <input type="text" id="userDefined1" name="userDefined1" value="" />
            <input type="text" id="userDefined2" name="userDefined2" value="" />
            <input type="text" id="userDefined3" name="userDefined3" value="" />
            <input type="text" id="hashValue" name="hashValue" value="" />
        </form>

    </div>
@endsection

@push('js')
    <script src="{{ env('CODAPAY_AIRTIME_URL') }}js/airtime_v1.0.js"></script>

    <script type="text/javascript">
        function processPayment (obj) {
            var form = $("#itemForm").serializeArray();
            var payment = form[1]['value'];

            if (form[1]['value'] === 'coda_pay') {
                codaPay();
            }else if(form[1]['value'] === 'paypal') {
                paypal();
            }else if(form[1]['value'] === 'mpu') {
                mpu();
            }
        }

        function codaPay () {
            $.ajax ( {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "/payment/coda-pay",
                data: $('#itemForm').serialize() + "&type=InitTxn",
                success: function (data) {
                    airtime_checkout(data);
                },
                error: function (jqXHR, textSatus, err) {
                    alert (err);
                }
            } );
        }

        function paypal () {
            $('#itemForm').attr('action', '/payment/paypal').submit();
        }

        function mpu() {
            $.ajax ( {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "/payment/mpu-pay",
                data: $('#itemForm').serialize(),
                success: function (data) {

                    $('#merchantID').val(data.merchant_id);
                    $('#invoiceNo').val(data.invoice_no);
                    $('#productDesc').val(data.product_desc);
                    $('#amount').val(data.amount);
                    $('#currencyCode').val(data.currency);
                    $('#userDefined1').val(data.user_defined_1);
                    $('#userDefined2').val(data.user_defined_2);
                    $('#userDefined3').val(data.user_defined_3);
                    $('#hashValue').val(data.HashValue);

                    $('#mpuForm').submit();
                },
                error: function (jqXHR, textSatus, err) {
                    alert (err);
                }
            } );
        }
    </script>

@endpush