<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>@yield('title', config('app.name', 'MMN'))</title>

    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="description" content="Myanmar Music Network | There's another way">
    <meta name="author" content="tnw">
    <meta name="keywords" content="mmn, myanmar music network, music store, myanmar music store, pro music, myanmar songs, myanmar online music streaming, myanmar online music store, myanmar music streaming, pro music studio">

    <meta name="MobileOptimized" content="320">
    <link rel="stylesheet" href="//mmwebfonts.comquas.com/fonts/?font=padauk" media="all">

    <style>
        body iframe{
            margin: 0;
            font-family: padauk,Yunghkio,Myanmar3,sans-serif;
            font-size: 16px;
            font-weight: 400;
            line-height: 24px;
            color: #777777;
            background-color: #14182a;
        }
    </style>

</head>

<body>

<iframe width="100%" height="800px" frameborder="1" src="https://airtime.codapayments.com/airtime/begin?type=3&txn_id={{ $txtId }}&browser_type=mobile-web" id="inneriframe">
    &lt;p&gt;iframes are not supported by your browser.&lt;/p&gt;
</iframe>

<!-- /footer -->
</body>
</html>
