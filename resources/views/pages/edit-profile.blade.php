@extends('frontend-layouts.app')

@section('content')
    <!---Main Content Start--->
    <div class="padder_top80 ms_profile">

        @include('frontend-layouts.partial.header')

    <!----Edit Profile Wrapper Start---->
        <div class="ms_profile_wrapper">
            <h1>Edit Your Accont Information</h1>
            <div class="ms_profile_box">
                {!! Form::open(['method' => 'POST', 'url' => '/account/edit_account_info' ]) !!}

                <div class="ms_pro_form">
                    <div class="form-group">
                        <label>Your Name *</label>
                        <input type="text" name="name" placeholder="Your Name" value="{{ Auth::user()->name }}" class="form-control{{$errors->has('name') ? ' is-invalid' : '' }}">
                        @if ($errors->has('name'))
                            <span class="invalid-feedback text-left" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Your Phone </label>
                        <input type="text" name="phone" placeholder="Your Phone" value="{{ Auth::user()->phone }}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="quality">Streaming Quality </label>
                        <select name="streaming_quality" id="quality" class="form-control">
                            <option value="256" {{ auth()->user()->streaming_quality === 256 ? 'selected' : '' }}>High Quality</option>
                            <option value="96" {{ auth()->user()->streaming_quality === 96 ? 'selected' : '' }}>Low Quality</option>
                        </select>
                    </div>

                    <div class="pro-form-btn text-center marger_top15">
                        <button type="submit" class="ms_btn">save</button>
                        <a href="{{ url('user/profile') }}" class="ms_btn">cancel</a>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@endsection