@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="{{ $category->name }} - Myanmar Music Network">
    <meta property="og:image" content="{{ asset('images/social-logo.png') }}">

    <meta name="twitter:title" content="Top Songs - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset('images/social-logo.png') }}">
    <meta name="twitter:card" content="{{ asset('images/social-logo.png') }}">
@endsection

@section('content')
    <!----Top Genres Section Start---->
    <div class="ms_content_wrapper ms_genres_single ms_profile">

        @include('frontend-layouts.partial.header')

        <div class="header_padder_top"></div>


        <div class="row" id="video">
            <div class="col-lg-12">
                <div class="ms_heading">
                    <h1>{{ $category->name }} </h1>
                </div>
            </div>

            @forelse($videos as $video)
                <div class="col-lg-2 col-md-2 col-6">
                    <div class="ms_genres_box">
                        <img src="{{ "https://img.youtube.com/vi/". youtube_id($video->video_link) . "/0.jpg" }}" alt="" class="img-fluid" />
                        <div class="ms_main_overlay">
                            <div class="ms_box_overlay"></div>
                            <div class="ms_play_icon">
                                <a href="/video/{{ $video->id }}">
                                    <img src="/images/svg/play.svg" alt="">
                                </a>
                            </div>
                            <div class="ovrly_text_div">
                                <span class="ovrly_text1"><a href="/video/{{ $video->id }}">{{ $video->name_mm }}</a></span>
                                <span class="ovrly_text2"><a href="/video/{{ $video->id }}">view video</a></span>
                            </div>
                        </div>
                        <div class="ms_box_overlay_on">
                            <div class="ovrly_text_div">
                                <span class="ovrly_text1"><a href="/video/{{ $video->id }}">{{ $video->name_mm }}</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-lg-12 text-center padder_bottom60" style="color: #fff">
                    No Video List
                </div>
            @endforelse

        </div>

        <div class="more_loading" style="display: none;">
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
        </div>

        @if($videos->currentPage() !== $videos->lastPage())
            <div class="ms_view_more padder_bottom20">
                <a href="javascript:;" class="ms_btn" id="view_more" data-link="{{ $videos->nextPageUrl() }}">view more</a>
            </div>
        @endif

    </div>

@endsection

@push('js')
    <script>
        var loading = $('.more_loading');
        var view_more = $('#view_more');

        $('.ms_btn').on('click', function () {
            loading.show();
            view_more.hide();

            var url = $(this).data('link');
            $.ajax({
                type: 'GET',
                url: url,
                success:function(response){
                    loading.hide();

                    $.each(response.data, function (key, video) {
                        html = '<div class="col-lg-2 col-md-2 col-6">' +
                            '<div class="ms_genres_box">' +
                            '<img src="https://img.youtube.com/vi/'+ video.youtube_id + '/0.jpg" alt="" class="img-fluid" />' +
                            '<div class="ms_main_overlay">' +
                            '<div class="ms_box_overlay"></div>' +
                            '<div class="ms_play_icon">' +
                            '<a href="/video/' + video.id + '">' +
                            '<img src="/images/svg/play.svg" alt="">' +
                            '</a>' +
                            '</div>' +
                            '<div class="ovrly_text_div">' +
                            '<span class="ovrly_text1"><a href="/video/' + video.id + '">'+ video.name_mm  +'</a></span>' +
                            '<span class="ovrly_text2"><a href="/video/' + video.id + '">view video</a></span>' +
                            '</div>' +
                            '</div>' +
                            '<div class="ms_box_overlay_on">' +
                            '<div class="ovrly_text_div">' +
                            '<span class="ovrly_text1"><a href="/video/' + video.id + '">'+ video.name_mm  +'</a></span>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>';

                        $('#video').append(html);
                    });

                    if (response.current_page !== response.last_page) {
                        view_more.data('link', response.next_page_url);
                        view_more.show();
                    }else {
                        hulla.send("all videos have been loaded", "success");
                    }

                }
            });
        })
    </script>
@endpush

