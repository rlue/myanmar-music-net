@extends('frontend-layouts.app')

@section('content')
    <!----Top Genres Section Start---->
    <div class="ms_content_wrapper ms_genres_single ms_profile">

        @include('frontend-layouts.partial.header')

        <div class="header_padder_top"></div>

        @foreach($video_categories as $video_category)
        <div class="row">
            <div class="col-lg-12">
                <div class="ms_heading">
                    <h1>{{ $video_category->name }} </h1>
                    <span class="veiw_all"><a href="/videos/{{ $video_category->id }}/{{ str_slug($video_category->name) }}">view more</a></span>
                </div>
            </div>

            @forelse($video_category->videos as $video)
                <div class="col-lg-2 col-md-2 col-6">
                    <div class="ms_genres_box">
                        <img src="{{ "https://img.youtube.com/vi/". youtube_id($video->video_link) . "/0.jpg" }}" alt="" class="img-fluid" />
                        <div class="ms_main_overlay">
                            <div class="ms_box_overlay"></div>
                            <div class="ms_play_icon">
                                <a href="/video/{{ $video->id }}">
                                    <img src="/images/svg/play.svg" alt="">
                                </a>
                            </div>
                            <div class="ovrly_text_div">
                                <span class="ovrly_text1"><a href="/video/{{ $video->id }}">{{ $video->name_mm }}</a></span>
                                <span class="ovrly_text2"><a href="/video/{{ $video->id }}">view video</a></span>
                            </div>
                        </div>
                        <div class="ms_box_overlay_on">
                            <div class="ovrly_text_div">
                                <span class="ovrly_text1"><a href="/video/{{ $video->id }}">{{ $video->name_mm }}</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-lg-12 text-center padder_bottom60" style="color: #fff">
                    No Video List
                </div>
            @endforelse

        </div>
        @endforeach

    </div>

@endsection

