@extends('frontend-layouts.app')

@section('css')
    <style type="text/css">
        * {padding:0;margin:0;box-sizing:border-box;}
        #video {
            position: relative;
            padding-bottom: 56.25%; /* 16:9 */
            height: 0;
            border-bottom: 5px solid #14182A;

        }
        #video iframe {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;

        }
    </style>
@endsection

@section('content')
    <!----Top Genres Section Start---->
    <div class="ms_content_wrapper ms_genres_single ms_profile">

        @include('frontend-layouts.partial.header')

        <div class="header_padder_top"></div>


        <div class="row">
            <div class="col-lg-12">
                <div class="ms_heading">
                    <h1>{{ $video->name_mm }} </h1>
                </div>
            </div>

            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                @if(youtube_id($video->video_link))
                    <div id="video">
                        <iframe width="100%" height="100%" id="youtube" src="https://www.youtube.com/embed/{{ youtube_id($video->video_link) }}?rel=0&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                @endif
            </div>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">

                <div class="row">
                    <div class="col-lg-12">
                        <div style="color: #ff4865;">OTHER VIDEOS</div>

                        @foreach($other_videos as $other_video)
                            <div class="ms_genres_box">
                                <img src="{{ "https://img.youtube.com/vi/". youtube_id($other_video->video_link) . "/0.jpg" }}" alt="" class="img-fluid" />
                                <div class="ms_main_overlay">
                                    <div class="ms_box_overlay"></div>
                                    <div class="ms_play_icon">
                                        <a href="/video/{{ $other_video->id }}">
                                            <img src="/images/svg/play.svg" alt="">
                                        </a>
                                    </div>
                                    <div class="ovrly_text_div">
                                        <span class="ovrly_text1"><a href="/video/{{ $other_video->id }}">{{ $other_video->name_mm }}</a></span>
                                        <span class="ovrly_text2"><a href="/video/{{ $other_video->id }}">view video</a></span>
                                    </div>
                                </div>
                                <div class="ms_box_overlay_on">
                                    <div class="ovrly_text_div">
                                        <span class="ovrly_text1"><a href="/video/{{ $other_video->id }}">{{ $other_video->name_mm }}</a></span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>

        </div>
    </div>

@endsection

