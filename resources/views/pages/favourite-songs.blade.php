@extends('frontend-layouts.app')

@section('css')
    <style>
        .ms_free_download.ms_purchase_wrapper .album_list_wrapper>ul>li {
            width: 16%;
        }
    </style>
@endsection
@section('content')

    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top50">

    @include('frontend-layouts.partial.header')

        <!----Free Download Css Start---->
        <div class="ms_free_download ms_purchase_wrapper">
            <div class="ms_heading">
                <h1>Your Favourites Songs</h1>
            </div>
            <div class="album_inner_list">
                <div class="album_list_wrapper">
                    <ul class="album_list_name">
                        <li>#</li>
                        <li>Song Title</li>
                        <li>Album</li>
                        <li class="text-center">Duration</li>
                        <li class="text-center"><i class="fa fa-headphones"></i></li>
                        <li class="text-center">remove</li>
                    </ul>
                    @forelse($songs as $key => $song)
                        <ul>
                            <li><a href="javascript:;" class="mmn-play"><span class="play_no">{{ str_pad($key+1, 2, '0', STR_PAD_LEFT) }}</span><span class="play_hover"></span></a></li>
                            <li><a href="javascript:;" class="mmn-play">{{ $song->name_mm }}</a></li>
                            @if($song->albums->isNotEmpty())
                                <li><a href="{{ url('album/' . $song->albums->first()->uuid) }}">{{ $song->albums->first()->album_name_mm }}</a></li>
                            @else
                                <li><a href="#"></a></li>
                            @endif
                            <li class="text-center"><a href="javascript:;">{{ $song->duration }}</a></li>
                            <li class="text-center"><a href="javascript:;">{{ $song->getPlayCount() }}</a></li>
                            <li class="text-center">
                                <a href="javascript:;" onclick="removeFav('/favourite/song/{{ $song->id }}')">
                                    <span class="ms_close"><img src="/images/svg/close.svg" alt=""></span></a>
                            </li>
                        </ul>
                    @empty
                        <ul>
                            <li class="w-100 text-center"><a href="#">No Data</a></li>
                        </ul>
                    @endforelse
                </div>
            </div>
        </div>

        <!----Main div close---->
    </div>

    <div id="mmn-songs" style="display: none;">
        @foreach($songs as $key => $song)
            <a href="{{ $song->getAudioFile() }}" data-image="{{ $song->getAlbumImage() }}" data-id="{{ $song->id }}" title="{{ $song->name_mm }}" data-artist="{{ $song->artists->implode('artist_name_eng', ', ') }}" class="mmn-single-track" rel="">{{ $song->name_mm }}</a>
        @endforeach
    </div>
@endsection


@push('js')
    <script>

        function removeFav(url) {
            $.ajax({
                type: 'DELETE',
                url: url,
                success:function(response){
                    alert(response.message);
                    location.reload();
                }
            });
        }

    </script>
@endpush
