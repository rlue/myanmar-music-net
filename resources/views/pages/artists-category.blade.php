@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="Artists - Myanmar Music Network">
    <meta property="og:image" content="{{ asset('images/social-logo.png') }}">

    <meta name="twitter:title" content="Artists - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset('images/social-logo.png') }}">
    <meta name="twitter:card" content="{{ asset('images/social-logo.png') }}">
@endsection

@section('content')

    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top80">
    @include('frontend-layouts.partial.header')

    <!----Top Artist Section---->
        <div class="ms_top_artist">
            <div class="container-fluid">
                <div class="row" id="artist">
                    <div class="col-lg-12">
                        <div class="ms_heading">
                            <h1>{{ $category->name }}</h1>
                        </div>
                    </div>

                    @foreach($artists as $artist)
                        <div class="col-lg-2 col-md-2">
                            <div class="ms_rcnt_box marger_bottom30">
                                <div class="ms_rcnt_box_img">
                                    <img src="{{ asset($artist->artist_image) }}" alt="{{ $artist->artist_name_eng }}" class="img-fluid">
                                    <div class="ms_main_overlay">
                                        <div class="ms_box_overlay"></div>
                                        <div class="ms_play_icon">
                                            <a href="{{ url('artist/' . $artist->id) }}"><img src="/images/svg/play.svg" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="ms_rcnt_box_text">
                                    <h3><a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }}</a></h3>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="more_loading" style="display: none;">
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                </div>

                <div class="ms_view_more padder_bottom20">
                    <a href="javascript:;" class="ms_btn" id="view_more" data-link="{{ $artists->nextPageUrl() }}">view more</a>
                </div>

            </div>
        </div>

        <!----Main div close---->
    </div>
@endsection

@push('js')
    <script>
        var loading = $('.more_loading');
        var view_more = $('#view_more');

        $('.ms_btn').on('click', function () {
            loading.show();
            view_more.hide();

            var url = $(this).data('link');
            $.ajax({
                type: 'GET',
                url: url,
                success:function(response){
                    loading.hide();

                    $.each(response.data, function (key, value) {
                        html = '<div class="col-lg-2 col-md-2">' +
                            '<div class="ms_rcnt_box marger_bottom30">' +
                            '<div class="ms_rcnt_box_img">' +
                            '<img src="' + value.artist_image + '" alt="' + value.name + '" class="img-fluid" onerror="this.src=\'/photos/1/Artist_Default.jpg\'">' +
                            '<div class="ms_main_overlay">' +
                            '<div class="ms_box_overlay"></div>' +
                            '<div class="ms_play_icon">' +
                            '<a href="/artist/' + value.id + '"><img src="/images/svg/play.svg" alt=""></a>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="ms_rcnt_box_text">' +
                            '<h3><a href="/artist/' + value.id +'">' + value.artist_name_eng + '</a></h3>' +
                            '</div>' +
                            '</div>' +
                            '</div>';

                        $('#artist').append(html);
                    });

                    if (response.current_page !== response.last_page) {
                        view_more.data('link', response.next_page_url);
                        view_more.show();
                    }else {
                        hulla.send("all artists have been loaded", "success");
                    }

                }
            });
        })
    </script>
@endpush

