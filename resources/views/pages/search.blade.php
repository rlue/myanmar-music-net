@extends('frontend-layouts.app')

@section('content')
    <!----Album Single Section Start---->
    <div class="ms_album_single_wrapper ms_profile">

        @include('frontend-layouts.partial.header')

        <div class="ms_heading">
            <h1>Search Result for <b>"{{ $query }}"</b></h1>
        </div>

        @if($albums->isNotEmpty())
            <div class="album_single_data">
                <h3 style="font-size: 20px">Albums</h3>
            </div>

            <div class="row">
                @foreach($albums as $key => $album)
                    <div class="col-lg-2 col-md-6 col-6">
                        <div class="ms_rcnt_box marger_bottom30">
                            <div class="ms_rcnt_box_img">
                                <img src="{{ $album->album_image }}" alt="{{ $album->artist_name_eng }}" class="img-fluid">
                                <div class="ms_main_overlay">
                                    <div class="ms_box_overlay"></div>
                                    <div class="ms_play_icon">
                                        <a href="{{ url("album/{$album->uuid}") }}">
                                            <img src="{{ asset('/images/svg/play.svg') }}" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="ms_rcnt_box_text">
                                <h3><a href="{{ url("album/{$album->uuid}") }}">{{ $album->album_name_mm }}</a></h3>
                                <p>
                                    @if($album->artists->count() > 3)
                                        <a href="javascript:;">Various Artists</a>
                                    @else
                                        @foreach($album->artists as $artist)
                                            <a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }} @if(!$loop->last) , @endif</a>
                                        @endforeach
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <br>
            <br>
        @endif

        @if($songs->isNotEmpty())
            <div class="album_single_data mt-5 pt-5">
                <h3 style="font-size: 20px">Songs</h3>
            </div>

            <!----Song List---->
            <div class="album_inner_list">
                <div class="album_list_wrapper">
                    <ul class="album_list_name">
                        <li>#</li>
                        <li>Title</li>
                        <li>Artist</li>
                        <li class="text-center">Duration</li>
                        <li class="text-center">Fav</li>
                        <li class="text-center">Share</li>
                        <li class="text-center">Buy</li>
                    </ul>
                    @foreach($songs as $key => $song)
                        <ul>
                            <li><a href="javascript:;" class="mmn-play" data-id="{{ $song->id }}">
                                    <span class="play_no">{{ str_pad($key+1, 2, 0, STR_PAD_LEFT) }}</span>
                                    <span class="play_hover"></span></a>
                            </li>

                            <li><a href="javascript:;" class="mmn-play" data-id="{{ $song->id }}" data-title="{{ $song->name_mm }}">{{ $song->name_mm }}</a></li>

                            <li>
                                @if($song->artists->count() > 3)
                                    <a href="javascript:;">Various Artists</a>
                                @else
                                    @foreach($song->artists as $artist)
                                        <a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }} @if(!$loop->last) , @endif</a>
                                    @endforeach
                                @endif

                                @if($song->feats->isNotEmpty())
                                    <a href="javascript:;">, ft : </a>
                                    @foreach($song->feats as $feat_artist)
                                        <a href="{{ url('artist/' . $feat_artist->id) }}">{{ $feat_artist->artist_name_eng }} @if(!$loop->last) , @endif</a>
                                    @endforeach
                                @endif
                            </li>

                            <li class="text-center"><a href="#">{{ $song->duration }}</a></li>
                            @auth
                                <li class="text-center"><a href="javascript:;" onclick="sendFav('/favourite/song/{{ $song->id }}', this)" class="{{ $song->getIsFavourite() ? ' text-danger' : ''  }}"><span class="fa fa-heart"></span> </a></li>
                                <li class="text-center"><a class="btn btn-primary fb_share_btn" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url("album/{$song->albums->first()->uuid}")) }}" target="_blank"><span class="fa fa-facebook-official"></span> share</a></li>
                                @if($song->getIsBought())
                                    <li class="text-center">
                                        <a href="/user/download_song/{{ $song->id }}" class="song_download">
                                            <span class="ms_close"><img src="/images/svg/download.svg" alt=""></span>
                                        </a>
                                    </li>
                                @elseif($song->sub_menu['store'])
                                    <li class="text-center">
                                        <a href="javascript:;" onclick="showBuySongModal({{ $song->id }})">Buy ({{ $song->price->name }} Kyat)</a>
                                    </li>
                                @elseif($song->sub_menu['free'])
                                    <li class="text-center">
                                        <a href="/download_free_song/{{ $song->id }}" class="song_download">
                                            <span class="ms_close"><img src="/images/svg/download.svg"></span>
                                        </a>
                                    </li>
                                @endif
                            @else
                                <li class="text-center"><a href="#" data-toggle="modal" data-target="#loginModal"><span class="fa fa-heart"></span> </a></li>
                                <li class="text-center"><a class="btn btn-primary fb_share_btn" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url("album/{$song->albums->first()->uuid}")) }}" target="_blank"><span class="fa fa-facebook-official"></span> share</a></li>
                                @if($song->sub_menu['store'])
                                    <li class="text-center"><a href="#" data-toggle="modal" data-target="#loginModal">Buy ({{ $song->price->name }} Kyat)</a></li>
                                @elseif($song->sub_menu['free'])
                                    <li class="text-center">
                                        <a href="/download_free_song/{{ $song->id }}" class="song_download">
                                            <span class="ms_close"><img src="/images/svg/download.svg"></span>
                                        </a>
                                    </li>
                                @endif
                            @endauth
                        </ul>
                    @endforeach
                </div>
            </div>

            <br>
            <br>
        @endif

       @if($artists->isNotEmpty())
            <div class="album_single_data mt-5 pt-5">
                <h3 style="font-size: 20px">Artists</h3>
            </div>

            <div class="row">
                @foreach($artists as $key => $artist)
                <div class="col-lg-2 col-md-6">
                    <div class="ms_rcnt_box marger_bottom30">
                        <div class="ms_rcnt_box_img">
                            <img src="{{ $artist->artist_image }}" alt="{{ $artist->artist_name_eng }}" class="img-fluid">
                            <div class="ms_main_overlay">
                                <div class="ms_box_overlay"></div>
                                <div class="ms_play_icon">
                                    <a href="{{ url('artist/' . $artist->id) }}">
                                        <img src="{{ asset('/images/svg/play.svg') }}" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="ms_rcnt_box_text">
                            <h3><a href="{{ url('artist/' . $artist->id) }}">{{ is_null($artist->artist_name_en) ? $artist->artist_name_eng : $artist->artist_name_en }}</a></h3>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

       @endif


        @if($playlist->isNotEmpty())
            <div class="album_single_data mt-5 pt-5">
                <h3 style="font-size: 20px">Playlist</h3>
            </div>

            <div class="row">
                @foreach($playlist as $key => $item)
                    <div class="col-lg-2 col-md-6">
                        <div class="ms_rcnt_box marger_bottom30">
                            <div class="ms_rcnt_box_img">
                                <img src="{{ $item->playlist_image }}" alt="{{ $item->playlist_name_eng }}" class="img-fluid">
                                <div class="ms_main_overlay">
                                    <div class="ms_box_overlay"></div>
                                    <div class="ms_play_icon">
                                        <a href="{{ url('playlist/' . $item->id) }}">
                                            <img src="{{ asset('/images/svg/play.svg') }}" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="ms_rcnt_box_text">
                                <h3><a href="{{ url('playlist/' . $item->id) }}">{{ $item->playlist_name_eng }}</a></h3>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        @endif

    </div>

    <div id="mmn-songs" style="display: none;">
        @foreach($songs as $key => $song)
            <a href="{{ $song->getAudioFile() }}" data-image="{{ $song->getAlbumImage() }}" data-id="{{ $song->id }}" title="{{ $song->name_mm }}" data-artist="{{ $song->artists->implode('artist_name_en', ', ') }}" class="mmn-single-track" rel="">{{ $song->name_mm }}</a>
        @endforeach
    </div>
@endsection
