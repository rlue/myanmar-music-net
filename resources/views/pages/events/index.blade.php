@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="Events - Myanmar Music Network">
    <meta property="og:image" content="{{ asset('images/social-logo.png') }}">

    <meta name="twitter:title" content="Events - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset('images/social-logo.png') }}">
    <meta name="twitter:card" content="{{ asset('images/social-logo.png') }}">
@endsection

@section('content')
    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top80">
        @include('frontend-layouts.partial.header')

        <div class="ms_top_artist">
            <div class="container-fluid">
                <div class="row" id="collection">
                    <div class="col-lg-12">
                        <div class="ms_heading">
                            <h1>Events</h1>
                        </div>
                    </div>

                    <div class="ms_blog_wrapper">
                        <div class="row">
                            @foreach($events as $event)
                                <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                                    <div class="ms_blog_section marger_bottom30">
                                        <div class="ms_blog_img">
                                            <img src="{{ $event->event_image }}" alt="" class="img-fluid">
                                        </div>
                                        <div class="ms_main_overlay">
                                            <div class="ms_box_overlay"></div>
                                            <div class="ovrly_text_div">
                                                <span class="ovrly_text1"><a href="/event/{{ $event->id }}">{{ $event->name }}</a></span>
                                                <div class="bottom">
                                                    <span class="ovrly_text1">{{ $event->start_date }} - {{ $event->end_date }}</span>
                                                    <span class="ovrly_text2"><a href="/event/{{ $event->id }}"><i class="fa fa-long-arrow-right"></i></a></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ms_box_overlay_on">
                                            <div class="ovrly_text_div">
                                                <span class="ovrly_text1"><a href="/event/{{ $event->id }}">{{ $event->name }}</a></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
@endsection