@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="{{ $event->name }} - Myanmar Music Network">
    <meta property="og:image" content="{{ asset(str_replace(' ', '%20',$event->event_image)) }}">

    <meta name="twitter:title" content="{{ $event->name }} - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset(str_replace(' ', '%20',$event->event_image)) }}">
    <meta name="twitter:card" content="{{ asset(str_replace(' ', '%20',$event->event_image)) }}">
@endsection

@section('content')
    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top80">
    @include('frontend-layouts.partial.header')

    <!--- blog single section start --->
        <div class="ms_blog_single_wrapper">
            <div class="row">
                <div class="col-lg-9 col-md-9">
                    <div class="ms_blog_single">
                        <div class="blog_single_img">
                            <img src="{{ $event->event_image }}" alt="" class="img-fluid">
                        </div>
                        <div class="blog_single_content">
                            <h3 class="ms_blog_title">{{ $event->name }}</h3>
                            <div class="ms_post_meta">
                                <ul>
                                    <li>{{ $event->start_date }} - {{ $event->end_date }} / </li>
                                    <li>{{ $event->location }} </li>
                                </ul>
                            </div>
                            {!! $event->event_detail !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <!----Sidebar Start---->
                    <div class="ms_sidebar">
                        <!--Feature Post-->
                        <div class="widget widget_recent_entries">
                            <h2 class="widget-title">Latest Events</h2>
                            <ul>
                                @foreach($latest_events as $latest_event)
                                <li>
                                    <div class="recent_cmnt_img">
                                        <img src="{{ $latest_event->event_image }}" width="50" alt="">
                                    </div>
                                    <div class="recent_cmnt_data">
                                        <h4><a href="/event/{{ $latest_event->id }}">{{ $latest_event->name }}</a></h4>
                                        <span>{{ $event->start_date }} - {{ $event->end_date }}</span>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection