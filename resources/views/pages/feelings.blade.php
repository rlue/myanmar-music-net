@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="Feelings - Myanmar Music Network">
    <meta property="og:image" content="{{ asset('images/social-logo.png') }}">

    <meta name="twitter:title" content="Feelings - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset('images/social-logo.png') }}">
    <meta name="twitter:card" content="{{ asset('images/social-logo.png') }}">
@endsection

@section('content')
    <!---- Feeling Section Start---->
    <div class="ms_content_wrapper ms_genres_single ms_profile padder_top90">

        @include('frontend-layouts.partial.header')

        <div class="row">
            <div class="col-lg-12">
                <div class="ms_heading">
                    <h1>Feelings</h1>
                </div>
            </div>

            @foreach($feelings as $feeling)
                <div class="col-lg-2 col-md-2 col-6">
                    <div class="ms_genres_box">
                        <img src="{{ $feeling->feeling_image }}" alt="" class="img-fluid" />
                        <div class="ms_main_overlay">
                            <div class="ms_box_overlay"></div>
                            <div class="ms_play_icon">
                                <a href="/feeling/{{ $feeling->id }}">
                                    <img src="/images/svg/play.svg" alt="{{ $feeling->name }}">
                                </a>
                            </div>
                            <div class="ovrly_text_div">
                                <span class="ovrly_text1"><a href="/feeling/{{ $feeling->id }}">{{ $feeling->name }}</a></span>
                                <span class="ovrly_text2"><a href="/feeling/{{ $feeling->id }}">view song</a></span>
                            </div>
                        </div>
                        <div class="ms_box_overlay_on">
                            <div class="ovrly_text_div">
                                <span class="ovrly_text1"><a href="#">{{ $feeling->name }}</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
@endsection

