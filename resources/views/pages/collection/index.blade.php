@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="Collections - Myanmar Music Network">
    <meta property="og:image" content="{{ asset('images/social-logo.png') }}">

    <meta name="twitter:title" content="Collections - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset('images/social-logo.png') }}">
    <meta name="twitter:card" content="{{ asset('images/social-logo.png') }}">
@endsection

@section('content')

    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top80">
    @include('frontend-layouts.partial.header')

    <!----Top Artist Section---->
        <div class="ms_top_artist">
            <div class="container-fluid">
                <div class="row" id="collection">
                    <div class="col-lg-12">
                        <div class="ms_heading">
                            <h1>Collections</h1>
                        </div>
                    </div>

                    @foreach($collections as $collection)
                        <div class="col-lg-3 col-md-3">
                            <div class="ms_genres_box">
                                <img src="{{ $collection->albumlist_image }}" alt="{{ $collection->albumlist_name_mm }}" class="img-fluid" />
                                <div class="ms_main_overlay">
                                    <div class="ms_box_overlay"></div>
                                    <div class="ms_play_icon">
                                        <a href="/collection/{{ $collection->id }}">
                                            <img src="/images/svg/play.svg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="more_loading" style="display: none;">
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                </div>

                @if($collections->currentPage() !== $collections->lastPage())
                    <div class="ms_view_more padder_bottom20">
                        <a href="javascript:;" class="ms_btn" id="view_more" data-link="{{ $collections->nextPageUrl() }}">view more</a>
                    </div>
                @endif

            </div>
        </div>

        <!----Main div close---->
    </div>
@endsection

@push('js')
    <script>
        var loading = $('.more_loading');
        var view_more = $('#view_more');

        $('.ms_btn').on('click', function () {
            loading.show();
            view_more.hide();

            var url = $(this).data('link');
            $.ajax({
                type: 'GET',
                url: url,
                success:function(response){
                    loading.hide();

                    $.each(response.data, function (key, collection) {

                        html = '<div class="col-lg-3 col-md-3">' +
                            '<div class="ms_genres_box">' +
                            '<img src="' + collection.albumlist_image + '" alt="'+ collection.albumlist_name_mm +'" class="img-fluid" />' +
                            '<div class="ms_main_overlay">' +
                            '<div class="ms_box_overlay"></div>' +
                            '<div class="ms_play_icon">' +
                            '<a href="#">' +
                            '<img src="/images/svg/play.svg" alt="">' +
                            '</a>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>';

                        $('#collection').append(html);
                    });

                    if (response.current_page !== response.last_page) {
                        view_more.data('link', response.next_page_url);
                        view_more.show();
                    }else {
                        hulla.send("all collection have been loaded", "success");
                    }

                }
            });
        })
    </script>
@endpush

