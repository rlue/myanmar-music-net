@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="{{ $collection->albumlist_name_eng }} - Myanmar Music Network">
    <meta property="og:image" content="{{ asset(str_replace(' ', '%20',$collection->albumlist_image)) }}">

    <meta name="twitter:title" content="{{ $collection->albumlist_name_eng }} - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset(str_replace(' ', '%20',$collection->albumlist_image)) }}">
    <meta name="twitter:card" content="{{ asset(str_replace(' ', '%20',$collection->albumlist_image)) }}">
@endsection

@section('content')

    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top80">

    @include('frontend-layouts.partial.header')

    <!----Top Artist Section---->
        <div class="ms_top_artist">
            <div class="container-fluid">
                <div class="row" id="collection">
                    <div class="col-lg-12">
                        <div class="ms_heading">
                            <h1>{{ $collection->albumlist_name_mm }}</h1>
                        </div>
                    </div>

                    @foreach($collection->albums as $album)
                        <div class="col-lg-2 col-md-2">
                            <div class="ms_rcnt_box marger_bottom30">
                                <div class="ms_rcnt_box_img">
                                    <img src="{{ $album->album_image }}" alt="">
                                    <div class="ms_main_overlay">
                                        <div class="ms_box_overlay"></div>
                                        <div class="ms_more_icon">
                                            <img src="/images/svg/more.svg" alt="">
                                        </div>

                                        @auth
                                            <ul class="more_option">
                                                <li><a href="javascript:;" onclick="sendFav('/favourite/album/{{ $album->uuid }}')"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                                                @if($album->sub_menu['store'])
                                                    <li><a href="javascript:;" onclick="showBuyAlbumModal({{ $album->uuid }})"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $album->prices->name }} Kyat)</a></li>
                                                @endif
                                            </ul>
                                        @else
                                            <ul class="more_option">
                                                <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                                                @if($album->sub_menu['store'])
                                                    <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $album->prices->name }} Kyat)</a></li>
                                                @endif
                                            </ul>
                                        @endauth


                                        <div class="ms_play_icon">
                                            <a href="{{ url("album/{$album->uuid}") }}">
                                                <img src="/images/svg/play.svg" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="ms_rcnt_box_text">
                                    <h3><a href="{{ url("album/{$album->uuid}") }}">{{ $album->album_name_mm }}</a></h3>
                                    <p>
                                        @if($album->artists->count() > 3)
                                            <a href="javascript:;">Various Artists</a>
                                        @else
                                            @foreach($album->artists as $artist)
                                                <a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }}</a>
                                                @if(!$loop->last) , @endif
                                            @endforeach
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>

        <!----Main div close---->
    </div>
@endsection

