@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="Genres - Myanmar Music Network">
    <meta property="og:image" content="{{ asset('images/social-logo.png') }}">

    <meta name="twitter:title" content="Genres - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset('images/social-logo.png') }}">
    <meta name="twitter:card" content="{{ asset('images/social-logo.png') }}">
@endsection

@section('content')
    <!---- Genres Section Start---->
    <div class="ms_content_wrapper ms_genres_single ms_profile padder_top90">

        @include('frontend-layouts.partial.header')

        <div class="row">
            <div class="col-lg-12">
                <div class="ms_heading">
                    <h1>Genres</h1>
                </div>
            </div>

            @foreach($genres as $genre)
                <div class="col-lg-2 col-md-2 col-6">
                    <div class="ms_genres_box">
                        <img src="{{ $genre->songCategory_image }}" alt="" class="img-fluid" />
                        <div class="ms_main_overlay">
                            <div class="ms_box_overlay"></div>
                            <div class="ms_play_icon">
                                <a href="/genre/{{ $genre->id }}">
                                    <img src="/images/svg/play.svg" alt="">
                                </a>
                            </div>
                            <div class="ovrly_text_div">
                                <span class="ovrly_text1"><a href="/genre/{{ $genre->id }}">{{ $genre->name }}</a></span>
                                <span class="ovrly_text2"><a href="/genre/{{ $genre->id }}">view songs</a></span>
                            </div>
                        </div>
                        <div class="ms_box_overlay_on">
                            <div class="ovrly_text_div">
                                <span class="ovrly_text1"><a href="/genre/{{ $genre->id }}">{{ $genre->name }}</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
    <!----Live Radio Section Start---->
@endsection

