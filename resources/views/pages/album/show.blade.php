@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="{{ $album->album_name_eng }} - Myanmar Music Network">
    <meta property="og:image" content="{{ asset(str_replace(' ', '%20',$album->album_image)) }}">

    <meta name="twitter:title" content="{{ $album->album_name_eng }} - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset(str_replace(' ', '%20',$album->album_image)) }}">
    <meta name="twitter:card" content="{{ asset(str_replace(' ', '%20',$album->album_image))  }}">
@endsection

@section('content')
    <!----Album Single Section Start---->
    <div class="ms_album_single_wrapper ms_profile">

        @include('frontend-layouts.partial.header')

        <div class="album_single_data">
            <div class="album_single_img">
                <img src="{{ $album->album_image }}" alt="" class="img-fluid">
            </div>
            <div class="album_single_text">
                <h2>{{ $album->album_name_mm }}</h2>
                <p class="singer_name">By -
                    @foreach($album->artists as $artist)
                        <a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }}</a>
                        @if(!$loop->last)
                            ,
                        @endif
                    @endforeach
                    @if($album->feats->isNotEmpty())
                        <br>
                        Feat:
                        @foreach($album->feats as $feat)
                            <a href="{{ url('artist/' . $feat->id) }}">{{ $feat->artist_name_eng }}</a>
                            @if(!$loop->last)
                                ,
                            @endif
                        @endforeach
                    @endif
                </p>
                <div class="album_feature">
                    @auth
                        <a href="javascript:sendLike('/like/album/{{ $album->uuid }}')" id="album-like" style="font-size: 15px; color: #ed1557; border: 2px solid #ed1557;padding:5px; border-radius: 10px; opacity: 0.7;margin-bottom: 5px;display: inline-block;"><span class="fa fa-thumbs-up"></span> {{ $album->getLiked() ? 'Liked' : 'Like' }}</a>
                    @else
                        <a href="#" data-toggle="modal" data-target="#loginModal" style="font-size: 15px; color: #ed1557; border: 2px solid #ed1557;padding:5px; border-radius: 10px; opacity: 0.7;margin-bottom: 5px;display: inline-block;"><span class="fa fa-thumbs-up"></span> Like</a>
                    @endauth
                    <a href="#" class="album_date">{{ $album->songs->count() }} {{ str_plural('song', $album->songs->count()) }}</a>
                    <a href="#" class="album_date"><i class="fa fa-headphones"></i> - {{ $album->view->view }}</a>
                    <a href="#" class="album_date">Released {{ carbon_parse($album->release_date)->format('M d, Y') }}</a>

                    @if($album->sub_menu['free'])
                        <p class="album_date text-white">သီချင်းတစ်ပုဒ်စီတွင် အခမဲ့ ဒေါင်းလုပ်ယူနိုင်ပါသည်။</p>
                    @endif

                    @if($album->sub_menu['streaming'])
                        <span style="font-size: 15px; color: #ed1557; border: 2px solid #ed1557;padding:5px; border-radius: 10px; opacity: 0.7;"><i class="fa fa-play-circle" style="vertical-align: middle;"></i> mPlay</span>
                    @endif

                    @if($album->sub_menu['store'])
                        @auth
                            <a href="javascript:showBuyAlbumModal({{ $album->uuid }})" style="font-size: 15px; color: #ed1557; border: 2px solid #ed1557;padding:5px; border-radius: 10px; opacity: 0.7;"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span> Buy ({{ $album->prices->name }} Kyat)</a>
                        @else
                            <a href="#" data-toggle="modal" data-target="#loginModal" style="font-size: 15px; color: #ed1557; border: 2px solid #ed1557;padding:5px; border-radius: 10px; opacity: 0.7;"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span> Buy ({{ $album->prices->name }} Kyat)</a>
                        @endauth
                    @endif

                </div>
            </div>
            <div class="album_more_optn ms_more_icon">
                <span><img src="/images/svg/more.svg" alt=""></span>
            </div>
            @auth
                <ul class="more_option">
                    <li><a href="javascript:;" onclick="sendFav('/favourite/album/{{ $album->uuid }}')"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                    @if($album->getIsBought())
                    @elseif($album->sub_menu['store'])
                        <li><a href="javascript:showBuyAlbumModal({{ $album->uuid }})"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $album->prices->name }} Kyat)</a></li>
                    @endif
                </ul>
            @else
                <ul class="more_option">
                    <li><a href="#" data-toggle="modal" data-target ="#loginModal"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                    @if($album->sub_menu['store'])
                        <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $album->prices->name }} Kyat)</a></li>
                    @endif
                </ul>
            @endauth
        </div>
        <!----Song List---->
        <div class="album_inner_list">
            <div class="album_list_wrapper">
                <ul class="album_list_name">
                    <li>#</li>
                    <li>Title</li>
                    <li>Artist</li>
                    <li class="text-center">Duration</li>
                    <li class="text-center">Fav</li>
                    <li class="text-center">Share</li>
                    <li class="text-center">Buy</li>
                </ul>
                @foreach($album->songs as $key => $song)
                    <ul>
                        <li><a href="javascript:;" class="mmn-play" data-id="{{ $song->id }}" data-title="{{ $song->name_mm }}">
                                <span class="play_no">{{ str_pad($key+1, 2, 0, STR_PAD_LEFT) }}</span>
                                <span class="play_hover"></span></a>
                        </li>

                        <li><a href="javascript:;" class="mmn-play" data-id="{{ $song->id }}" >{{ $song->name_mm }}</a></li>

                        <li>
                            @if($song->artists->count() > 3)
                                <a href="javascript:;">Various Artists</a>
                            @else
                                @foreach($song->artists as $artist)
                                    <a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }} @if(!$loop->last) , @endif</a>
                                @endforeach
                            @endif

                            @if($song->feats->isNotEmpty())
                                <a href="javascript:;">, ft : </a>
                                @foreach($song->feats as $feat_artist)
                                    <a href="{{ url('artist/' . $feat_artist->id) }}">{{ $feat_artist->artist_name_eng }} @if(!$loop->last) , @endif</a>
                                @endforeach
                            @endif
                        </li>

                        <li class="text-center"><a href="javascript:;">{{ $song->duration }}</a></li>

                        @auth
                            <li class="text-center"><a href="javascript:;" onclick="sendFav('/favourite/song/{{ $song->id }}', this)" class="{{ $song->getIsFavourite() ? ' text-danger' : ''  }}"><span class="fa fa-heart"></span> </a></li>
                            <li class="text-center"><a class="btn btn-primary fb_share_btn" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url("album/{$album->uuid}")) }}" target="_blank"><span class="fa fa-facebook-official"></span> share</a></li>
                            @if($album->getIsBought())
                                <li class="text-center">
                                    <a href="/user/download_song/{{ $song->id }}" class="song_download">
                                        <span class="ms_close"><img src="/images/svg/download.svg" alt=""></span>
                                    </a>
                                </li>
                            @elseif($song->getIsBought())
                                <li class="text-center">
                                    <a href="/user/download_song/{{ $song->id }}" class="song_download">
                                        <span class="ms_close"><img src="/images/svg/download.svg" alt=""></span>
                                    </a>
                                </li>
                            @elseif($song->sub_menu['store'])
                                <li class="text-center">
                                    <a href="javascript:;" onclick="showBuySongModal({{ $song->id }})">Buy ({{ $song->price->name }} Kyat)</a>
                                </li>
                            @elseif($song->sub_menu['free'])
                                <li class="text-center">
                                    <a href="/download_free_song/{{ $song->id }}" class="song_download">
                                        <span class="ms_close"><img src="/images/svg/download.svg" alt=""></span>
                                    </a>
                                </li>
                            @else
                                <li></li>
                            @endif
                        @else
                            <li class="text-center"><a href="#" data-toggle="modal" data-target="#loginModal"><span class="fa fa-heart"></span> </a></li>
                            <li class="text-center"><a class="btn btn-primary fb_share_btn" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url("album/{$album->uuid}")) }}" target="_blank"><span class="fa fa-facebook-official"></span> share</a></li>
                            @if($song->sub_menu['store'])
                                <li class="text-center"><a href="#" data-toggle="modal" data-target="#loginModal">Buy ({{ $song->price->name }} Kyat)</a></li>
                            @elseif($song->sub_menu['free'])
                                <li class="text-center">
                                    <a href="/download_free_song/{{ $song->id }}" class="song_download">
                                        <span class="ms_close"><img src="/images/svg/download.svg" alt=""></span>
                                    </a>
                                </li>
                            @else
                                <li></li>
                            @endif
                        @endauth
                    </ul>
                @endforeach
            </div>
        </div>
    </div>
    @if($related_albums->isNotEmpty())
        <!---Main Content Start--->
        <div class="ms_content_wrapper ms_album_content">
            <!----Featured Albumn Section Start---->
            <div class="ms_fea_album_slider padder_top5">
                <div class="ms_heading">
                    <h1>Related Album</h1>
                </div>
                <div class="ms_related_album_slider swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($related_albums as $related_album)
                            <div class="swiper-slide">
                                <div class="ms_rcnt_box ms_related_album">
                                    <div class="ms_rcnt_box_img">
                                        <img src="{{ $related_album->album_image }}" alt="">
                                        <div class="ms_main_overlay">
                                            <div class="ms_box_overlay"></div>
                                            <div class="ms_more_icon">
                                                <img src="/images/svg/more.svg" alt="">
                                            </div>

                                            @auth
                                                <ul class="more_option">
                                                    <li><a href="javascript:;" onclick="sendFav('/favourite/album/{{ $related_album->uuid }}')"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                                                    @if($related_album->sub_menu['store'])
                                                        <li><a href="javascript:showBuyAlbumModal({{ $related_album->uuid }})"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $related_album->prices->name }} Kyat)</a></li>
                                                    @endif
                                                </ul>
                                            @else
                                                <ul class="more_option">
                                                    <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                                                    @if($related_album->sub_menu['store'])
                                                        <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $related_album->prices->name }} Kyat)</a></li>
                                                    @endif
                                                </ul>
                                            @endauth

                                            <div class="ms_play_icon">
                                                <a href="{{ url("album/{$related_album->uuid}") }}">
                                                    <img src="/images/svg/play.svg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ms_rcnt_box_text">
                                        <h3><a href="{{ url("album/{$related_album->uuid}") }}">{{ $related_album->album_name_mm }}</a></h3>
                                        <p>
                                            @if($related_album->artists->count() > 3)
                                                <a href="javascript:;">Various Artists</a>
                                            @else
                                                @foreach($related_album->artists as $artist)
                                                    <a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }}</a>
                                                    @if(!$loop->last) , @endif
                                                @endforeach
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next-related slider_nav_next"></div>
                <div class="swiper-button-prev-related slider_nav_prev"></div>
            </div>

            <!----Main div close---->
        </div>
    @endif

    <div id="mmn-songs" style="display: none;">
        @foreach($album->songs as $key => $song)
            <a href="{{ $song->getAudioFile() }}" data-image="{{ $album->album_image }}" data-id="{{ $song->id }}" title="{{ $song->name_mm }}" data-artist="{{ $song->artists->implode('artist_name_eng', ', ') }}" class="mmn-single-track" rel="">{{ $song->name_mm }}</a>
        @endforeach
    </div>

@endsection
