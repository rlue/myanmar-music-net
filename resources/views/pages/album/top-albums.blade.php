@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="Top Albums - Myanmar Music Network">
    <meta property="og:image" content="{{ asset('images/social-logo.png') }}">

    <meta name="twitter:title" content="Top Albums - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset('images/social-logo.png') }}">
    <meta name="twitter:card" content="{{ asset('images/social-logo.png') }}">
@endsection

@section('content')
    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top50">
    @include('frontend-layouts.partial.header')
        <div class="ms_top_artist">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ms_heading">
                            <h1>Top Albums</h1>
                        </div>
                    </div>
                    @foreach($top_albums as $top_album)
                        <div class="col-lg-2 col-md-2 col-6">
                            <div class="ms_rcnt_box marger_bottom30">
                                <div class="ms_rcnt_box_img">
                                    <img src="{{ $top_album->album_image }}" alt="" class="img-fluid">
                                    <div class="ms_main_overlay">
                                        <div class="ms_box_overlay"></div>

                                        <div class="ms_more_icon">
                                            <img src="/images/svg/more.svg" alt="">
                                        </div>

                                        <ul class="more_option">
                                            @auth
                                                <li><a href="javascript:;" onclick="sendFav('/favourite/album/{{ $top_album->uuid }}')"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                                                @if($top_album->sub_menu['store'])
                                                    <li><a href="javascript:showBuyAlbumModal({{ $top_album->uuid }})"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $top_album->prices->name }} Kyat)</a></li>
                                                @endif

                                            @else
                                                <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                                                @if($top_album->sub_menu['store'])
                                                    <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $top_album->prices->name }} Kyat)</a></li>
                                                @endif
                                            @endauth

                                            <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url("album/{$top_album->uuid}")) }}" target="_blank"><span class="opt_icon"><span class="fa fa-facebook-official"></span></span>Share</a></li>
                                        </ul>

                                        <div class="ms_play_icon">
                                            <a href="{{ url("album/{$top_album->uuid}") }}">
                                                <img src="/images/svg/play.svg" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="ms_rcnt_box_text">
                                    <h3><a href="{{ url("album/{$top_album->uuid}") }}">{{ $top_album->album_name_mm }}</a></h3>
                                    <p>
                                        @if($top_album->artists->count() > 3)
                                            <a href="javascript:;">Various Artists</a>
                                        @else
                                            @foreach($top_album->artists as $artist)
                                                <a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }}</a>
                                                @if(!$loop->last) , @endif
                                            @endforeach
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
