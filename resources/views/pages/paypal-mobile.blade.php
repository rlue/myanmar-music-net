@extends('frontend-layouts.app')

@section('content')
    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top80">
    @include('frontend-layouts.partial.header')
    <!---Featured Artists Music--->
        <form name="itemForm" action="/mobile/paypal" method="POST" id="itemForm">
            @csrf
            <input type="hidden" value="{{ $data['user_id'] }}" name="user_id">
            <input type="hidden" value="paypal" name="payment_service">
            <input type="hidden" value="{{ $data['amount'] }}" name="amount">
        </form>

    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#itemForm').submit();
        });
    </script>

@endpush