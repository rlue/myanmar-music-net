@extends('frontend-layouts.app')

@section('css')
    <style>
        .ms_free_download.ms_purchase_wrapper .album_list_wrapper>ul>li {
            width: 16%;
        }
    </style>
@endsection

@section('content')

    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top50">

    @include('frontend-layouts.partial.header')

        <!----Free Download Css Start---->
        <div class="ms_free_download ms_purchase_wrapper">
            <div class="ms_heading">
                <h1>Your Favourites Songs</h1>
            </div>
            <div class="album_inner_list">
                <div class="album_list_wrapper">
                    <ul class="album_list_name">
                        <li>#</li>
                        <li>Song Title</li>
                        <li>Album</li>
                        <li class="text-center">Duration</li>
                        <li class="text-center"><i class="fa fa-headphones"></i></li>
                        <li class="text-center">remove</li>
                    </ul>
                    @forelse($songs as $key => $song)
                        @if($song->albums->isNotEmpty())
                            <ul>
                                <li><a href="javascript:;" class="mmn-play" data-id="{{ $song->id }}"><span class="play_no">{{ str_pad($key+1, 2, '0', STR_PAD_LEFT) }}</span><span class="play_hover"></span></a></li>
                                <li><a href="javascript:;" class="mmn-play" data-id="{{ $song->id }}">{{ $song->name_mm }}</a></li>
                                <li><a href="{{ url('album/' . $song->albums->first()->uuid) }}">{{ $song->albums->first()->album_name_mm }}</a></li>
                                <li class="text-center"><a href="#">{{ $song->duration }}</a></li>
                                <li class="text-center"><a href="#">{{ $song->getPlayCount() }}</a></li>
                                <li class="text-center">
                                    <a href="javascript:;" onclick="removeFav('/favourite/song/{{ $song->id }}', this)">
                                        <span class="ms_close"><img src="/images/svg/close.svg" alt=""></span></a>
                                </li>
                            </ul>
                        @endif
                    @empty
                        <ul>
                            <li class="w-100 text-center"><a href="#">No Songs</a></li>
                        </ul>
                    @endforelse
                </div>
            </div>
            @if($songs->isNotEmpty())
                <div class="ms_view_more">
                    <a href="/mymusic/favourite_songs" class="ms_btn">view more</a>
                </div>
            @endif
        </div>

        <!----Free Download Css Start---->
        <div class="ms_free_download ms_purchase_wrapper">
            <div class="ms_heading">
                <h1>Your Favourite Albums</h1>
            </div>
            <div class="album_inner_list">
                <div class="album_list_wrapper">
                    <ul class="album_list_name">
                        <li>#</li>
                        <li>Album Title</li>
                        <li>Artist</li>
                        <li>Count</li>
                        <li class="text-center">price</li>
                        <li class="text-center">remove</li>
                    </ul>

                    @forelse($albums as $key => $album)
                        <ul>
                            <li><a href="{{ url('album/'. $album->uuid) }}"><span class="play_no">{{ str_pad($key+1, 2, '0', STR_PAD_LEFT) }}</span><span class="play_hover"></span></a></li>
                            <li><a href="{{ url('album/'. $album->uuid) }}">{{ $album->album_name_mm }}</a></li>
                            <li>
                                @if($album->artists->count() > 3)
                                    <a href="javascript:;">Various Artists</a>
                                @else
                                    @foreach($album->artists as $artist)
                                        <a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }}</a>
                                        @if(!$loop->last) , @endif
                                    @endforeach
                                @endif
                            </li>
                            <li><a href="#">{{ $album->songs->count() }}</a></li>
                            <li class="text-center"><a href="#">{{ $album->prices->name }}</a></li>
                            <li class="text-center">
                                <a href="javascript:;" onclick="removeFav('/favourite/album/{{ $album->uuid }}', this)">
                                    <span class="ms_close"><img src="/images/svg/close.svg" alt=""></span></a>
                            </li>
                        </ul>
                    @empty
                        <ul>
                            <li class="w-100 text-center"><a href="#">No Albums</a></li>
                        </ul>
                    @endforelse

                </div>
            </div>
            @if($albums->isNotEmpty())
                <div class="ms_view_more">
                    <a href="/mymusic/favourite_albums" class="ms_btn">view more</a>
                </div>
            @endif
        </div>

        <!----Main div close---->
    </div>

    <div id="mmn-songs" style="display: none;">
        @foreach($songs as $key => $song)
            <a href="{{ $song->getAudioFile() }}" data-image="{{ $song->getAlbumImage() }}" data-id="{{ $song->id }}" title="{{ $song->name_mm }}" data-artist="{{ $song->artists->implode('artist_name_eng', ', ') }}" class="mmn-single-track" rel="">{{ $song->name_mm }}</a>
        @endforeach
    </div>
@endsection


@push('js')
    <script>

        function removeFav(url,elm) {

            $.ajax({
                type: 'DELETE',
                url: url,
                success:function(response){
                    alert(response.message);

                    if (response.status) {
                        $(elm).closest('ul').remove();
                    }
                }
            });
        }

    </script>
@endpush
