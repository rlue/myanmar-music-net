@extends('frontend-layouts.app')

@section('content')
    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top80">
        @include('frontend-layouts.partial.header')

        <div class="ms_free_download ms_purchase_wrapper">
            <div class="ms_heading">
                <h1>Privacy Policy</h1>
            </div>
            <div class="row">
                <div class="container">
                    <!-- How to Register -->

                    <p>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;By entering into these Terms of Service, you agree to mMusicNet's collection, use and disclosure of your personal information in accordance with the mMusicNet Privacy Policy.<br>

                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Privacy Policy covers mMusicNet's treatment of information that mMusicNet gathers when you are accessing mMusicNet's Website as a consumer and when you use mMusicNet services as a customer (the "Information"). Also, this Privacy Policy covers mMusicNet's treatment of your information that mMusicNet's business partners share with mMusicNet. This Privacy Policy does not apply to the practices of third parties that mMusicNet does not own or control (such as third-party websites that you may access from the Website), or to individuals that mMusicNet does not employ or manage.
                        <br>

                    </p><h3>Information You Provide to Us:<hr></h3>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We receive and store any information you enter on our Website or provide to us in any other way. The types of Information collected include, without limitation, your full name, email address, mailing address, phone number, IP address, browser information, password, contact information, transactional information based on your activities on the Website, and media consumed on the Website including, but not limited to, media viewed, played, downloaded, uploaded, and shared. You can choose not to provide us with certain information, but then you may not be able to take advantage of many of our special features.
                    <br>

                    <h3>Information Collected Automatically:<hr></h3>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We receive and store certain types of information whenever you interact with our Website or services. mMusicNet automatically receives and records information on our server logs from your browser including your IP address, mMusicNet cookie information, the page you requested, and the media you have consumed (e.g., viewed, played, downloaded, uploaded, and shared). Generally, our service automatically collects usage information, such as the numbers and frequency of visitors to our site and its components, similar to TV ratings that indicate how many people watched a particular show. This type of data enables us to figure out how often consumers use parts of the Website or services so that we can make the Website appealing to as many consumers as possible, and improve those services.
                    <br>

                    <h3>Cookies.<hr></h3>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cookies are alphanumeric identifiers that we transfer to your computer's hard drive through your browser to enable our systems to recognize your browser and tell us how and when pages in our site are visited and by how many people. mMusicNet cookies do not collect Information, and except as necessary to investigate claims of fraud or other misconduct or as otherwise necessary to track purchases for purposes of calculating credit under our affiliate program, we do not combine the general information collected through cookies with other personal Information to tell us who you are or what your screen name or email address is.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Most browsers have an option for turning off the cookie feature, which will prevent your browser from accepting new cookies, as well as (depending on the sophistication of your browser software) allowing you to decide on acceptance of each new cookie in a variety of ways. We strongly recommend that you leave the cookies activated, however, because cookies enable you to take advantage of some of our Website's most attractive features.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ads and offers appearing on the Website may be delivered to consumers and customers by our advertising partners, who may set cookies. These cookies allow the ad server to recognize your computer each time they send you an online advertisement to compile information about you or others who use your computer. This information allows ad networks to, among other things, deliver targeted advertisements that they believe will be of most interest to you. This Privacy Policy covers the use of cookies by mMusicNet and does not cover the use of cookies by any advertisers or other third parties with whom mMusicNet may work. mMusicNet does not access or control such third party cookies; please refer to those companies' privacy policies.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mMusicNet may use automatically collected information and cookies information to:
                    (a) remember your information so that you will not have to re-enter it during your visit or the next time you visit the Website;
                    (b) provide custom, personalized advertisements, content, and information;
                    (c) monitor the effectiveness of our marketing campaigns; and
                    (d) monitor aggregate usage metrics such as total number of visitors and pages viewed.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mMusicNet may use your email address or other personally identifiable information to you promotional or marketing information or to provide you with information about a specific program or feature you have elected to participate in or receive information about.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;We provide Information, (i) to the content owners (e.g., artists and record labels) that relates to their fans (e.g., regarding which fans are consuming and sharing what pieces of media at what levels), subject to the ability of such fans to opt-out of promotional communications, and (ii) we may provide aggregate information to our partners about how our consumers or customers, collectively, use our site (e.g., we share this type of statistical data so that our partners also understand how often people use their services and our Website, so that they, too, may provide you with an optimal online experience). For the avoidance of doubt, when you purchase a particular artist's music, that artist will receive your name and e-mail address unless you opt-out of disclosing this information. An artist will also be able to identify the web page or link (included e-mail addresses to the extent a link was embedded in an e-mail) from which you have been referred to an artist's page on mMusicNet.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Third Party Service Providers: We employ other companies and people to perform tasks on our behalf and need to share your information with them to provide products or services to you. Examples of such information include, without limitation, fulfilling orders, sending postal mail and email, analyzing data, and providing customer service. Unless we tell you differently, such third party service providers do not have any right to use Information we share with them beyond what is reasonably necessary to assist us. You hereby consent to our sharing of Information for the above purposes.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Protection of mMusicNet and Others: We may release Information when we believe in good faith that release is necessary to comply with any applicable laws, rules or regulations; enforce or apply our conditions of use and other agreements; or protect the rights, property, or safety of mMusicNet, our employees, our users, or others. This includes exchanging information with other companies and organizations for fraud protection and credit risk reduction.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your mMusicNet account Information is protected by a password for your privacy and security. You need to protect unauthorized access to your account and Information by selecting and protecting your password appropriately and limiting access to your computer and browser by signing off after you have finished accessing your account. If you share your password or your Information with others, remember that you are responsible for all actions taken in the name of your account. If you lost control of your password, you may lose substantial control over your Information and may be subject to legally binding actions taken on your behalf. Therefore, if you password has been compromised for any reason, you should immediately notify mMusicNet and change your password.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mMusicNet attempts to protect user information to ensure that user account information is kept private; however, mMusicNet cannot guarantee or ensure the security of user account information. Not even the Myanmar government appears capable of securing highly sensitive personal and national security information from unauthorized access, use, and exploitation. Unauthorized entry or use by third parties, hardware or software failure, and other factors may compromise the security of user information at any time, including your Information.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Website contains links to other sites, including the sites of the bands, musicians and other artists from which you purchase or use products or services through the Website. mMusicNet is not responsible for the privacy policies and/or practices on other sites (including the bands, musicians and other artists from which you purchase or use products or services through the Website). When linking to another site you should read the privacy policy stated on that site. This Privacy Policy only governs information collected on the Website.
                    <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;By providing your personal data to mMusicNet through the Website, you consent to:
                    the use of your personal data for the uses identified above in accordance with the Privacy Policy; and
                    the transfer of your personal data to the Myanmar as indicated above.
                    <p></p>
                </div>
            </div>
        </div>

    </div>
@endsection