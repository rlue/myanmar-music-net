@extends('frontend-layouts.app')

@section('content')
    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top80">
    @include('frontend-layouts.partial.header')
    <!---Featured Artists Music--->
        <form name="itemForm" action="post" id="itemForm">
            <input type="hidden" value="{{ $data['user_id'] }}" name="user_id">
            <input type="hidden" value="mpu" name="payment_service">
            <input type="hidden" value="{{ $data['amount'] }}" name="amount">

        </form>

        <form method="POST" action="{{ config('mpu.url') }}" id="mpuForm" style="display: none;">
            <input type="text" id="merchantID" name="merchantID" value="" />
            <input type="text" id="invoiceNo" name="invoiceNo" value="" />
            <input type="text" id="productDesc" name="productDesc" value="" >
            <input type="text" id="amount" name="amount" value="" />
            <input type="text" id="currencyCode" name="currencyCode" value="" />
            <input type="text" id="userDefined1" name="userDefined1" value="" />
            <input type="text" id="userDefined2" name="userDefined2" value="" />
            <input type="text" id="userDefined3" name="userDefined3" value="" />
            <input type="text" id="hashValue" name="hashValue" value="" />
        </form>

    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $(document).ready(function () {
            initTxn();
        });

        function initTxn () {
            $.ajax ( {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "/mobile/mpu-pay",
                data: $('#itemForm').serialize(),
                success: function (data) {

                    $('#merchantID').val(data.merchant_id);
                    $('#invoiceNo').val(data.invoice_no);
                    $('#productDesc').val(data.product_desc);
                    $('#amount').val(data.amount);
                    $('#currencyCode').val(data.currency);
                    $('#userDefined1').val(data.user_defined_1);
                    $('#userDefined2').val(data.user_defined_2);
                    $('#userDefined3').val(data.user_defined_3);
                    $('#hashValue').val(data.HashValue);

                    $('#mpuForm').submit();
                },
                error: function (jqXHR, textSatus, err) {
                    alert (err);
                }
            } );
        }
    </script>

@endpush