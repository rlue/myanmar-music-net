@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="{{ $feeling->name }} - Myanmar Music Network">
    <meta property="og:image" content="{{ asset(str_replace(' ', '%20',$feeling->feeling_image)) }}">

    <meta name="twitter:title" content="{{ $feeling->name }} - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset(str_replace(' ', '%20',$feeling->feeling_image)) }}">
    <meta name="twitter:card" content="{{ asset(str_replace(' ', '%20',$feeling->feeling_image)) }}">
@endsection

@section('content')
    <!----Top Genres Section Start---->
    <div class="ms_content_wrapper ms_genres_single ms_profile padder_top90">

        @include('frontend-layouts.partial.header')

        <div class="row">
            <div class="col-lg-12">
                <div class="ms_heading">
                    <h1>Music List For Feeling "{{ $feeling->name }}" </h1>
                </div>
            </div>

            @forelse($songs as $song)
                <div class="col-lg-2 col-md-2 col-6">
                    <div class="ms_genres_box">
                        <img src="{{ $song->getAlbumImage() }}" alt="" class="img-fluid" />
                        <div class="ms_main_overlay">
                            <div class="ms_box_overlay"></div>
                            <div class="ms_more_icon">
                                <img src="/images/svg/more.svg" alt="">
                            </div>

                            <ul class="more_option">

                                @auth
                                    <li><a href="javascript:;" onclick="sendFav('/favourite/song/{{ $song->id }}')"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                                    @if($song->sub_menu['store'])
                                        <li><a href="javascript:showBuySongModal({{ $song->id }})"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $song->price->name }} Kyat)</a></li>
                                    @elseif($song->sub_menu['free'])
                                        <li>
                                            <a href="/download_free_song/{{ $song->id }}" class="song_download">
                                                <span class="opt_icon"><span class="fa fa-download"></span></span>Download
                                            </a>
                                        </li>
                                    @endif
                                @else
                                    <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites </a></li>
                                    @if($song->sub_menu['store'])
                                        <li><a href="#" data-toggle="modal" data-target="#loginModal"><span class="opt_icon"><span class="fa fa-shopping-cart"></span></span>Buy ({{ $song->price->name }} Kyat)</a></li>
                                    @elseif($song->sub_menu['free'])
                                        <li>
                                            <a href="/download_free_song/{{ $song->id }}" class="song_download">
                                                <span class="opt_icon"><span class="fa fa-download"></span></span>Download
                                            </a>
                                        </li>
                                    @endif
                                @endauth

                                <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url("album/{$song->albums->first()->uuid}")) }}" target="_blank"><span class="opt_icon"><span class="fa fa-facebook-official"></span></span>Share</a></li>
                            </ul>


                            <div class="ms_play_icon">
                                <a href="/album/{{ $song->albums->first()->uuid }}">
                                    <img src="/images/svg/play.svg" alt="">
                                </a>
                            </div>
                            <div class="ovrly_text_div">
                                <span class="ovrly_text1"><a href="/album/{{ $song->albums->first()->uuid }}">{{ $song->name_mm }}</a></span>
                                <span class="ovrly_text2"><a href="/album/{{ $song->albums->first()->uuid }}">view song</a></span>
                            </div>
                        </div>
                        <div class="ms_box_overlay_on">
                            <div class="ovrly_text_div">
                                <span class="ovrly_text1"><a href="/album/{{ $song->albums->first()->uuid }}">{{ $song->name }}</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-lg-12 text-center padder_bottom60" style="color: #fff">
                    No Song List
                </div>
            @endforelse

        </div>
    </div>

    <div id="mmn-songs" style="display: none;">
        @foreach($songs as $key => $song)
            <a href="{{ $song->getAudioFile() }}" data-image="{{ $song->getAlbumImage() }}" data-id="{{ $song->id }}" title="{{ $song->name_mm }}" data-artist="{{ $song->artists->implode('artist_name_eng', ', ') }}" class="mmn-single-track" rel="">{{ $song->name_mm }}</a>
        @endforeach
    </div>

@endsection

