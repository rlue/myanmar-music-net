@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="Free Music - Myanmar Music Network">
    <meta property="og:image" content="{{ asset('images/social-logo.png') }}">

    <meta name="twitter:title" content="Free Music - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset('images/social-logo.png') }}">
    <meta name="twitter:card" content="{{ asset('images/social-logo.png') }}">
@endsection

@section('content')

    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top80">
    @include('frontend-layouts.partial.header')

    <!----Top Artist Section---->
        <div class="ms_top_artist">
            <div class="container-fluid">
                <div class="row" id="albums">
                    <div class="col-lg-12">
                        <div class="ms_heading">
                            <h1>Free Music</h1>
                        </div>
                    </div>

                    @foreach($albums as $album)
                        <div class="col-lg-2 col-md-2">
                            <div class="ms_rcnt_box marger_bottom30">
                                <div class="ms_rcnt_box_img">
                                    <img src="{{ $album->album_image }}" alt="">
                                    <div class="ms_main_overlay">
                                        <div class="ms_box_overlay"></div>
                                        <div class="ms_more_icon">
                                            <img src="/images/svg/more.svg" alt="">
                                        </div>
                                        <ul class="more_option">
                                            <li><a href="javascript:;" onclick="sendFav('/favourite/album/{{ $album->uuid }}')"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites</a></li>
                                        </ul>
                                        <div class="ms_play_icon">
                                            <a href="{{ url("album/{$album->uuid}") }}">
                                                <img src="/images/svg/play.svg" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="ms_rcnt_box_text">
                                    <h3><a href="{{ url("album/{$album->uuid}") }}">{{ $album->album_name_mm }}</a></h3>
                                    <p>
                                        @if($album->artists->count() > 3)
                                            <a href="javascript:;">Various Artists</a>
                                        @else
                                            @foreach($album->artists as $artist)
                                                <a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }}</a>
                                                @if(!$loop->last) , @endif
                                            @endforeach
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="more_loading" style="display: none;">
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                    <div class="bar"></div>
                </div>

                <div class="ms_view_more padder_bottom20">
                    <a href="javascript:;" class="ms_btn" id="view_more" data-link="{{ $albums->nextPageUrl() }}">view more</a>
                </div>

            </div>
        </div>

        <!----Main div close---->
    </div>
@endsection

@push('js')
    <script>
        var loading = $('.more_loading');
        var view_more = $('#view_more');

        $('.ms_btn').on('click', function () {
            loading.show();
            view_more.hide();

            var url = $(this).data('link');
            $.ajax({
                type: 'GET',
                url: url,
                success:function(response){
                    loading.hide();

                    $.each(response.data, function (key, album) {

                        artists_html = '';

                        if (album.artists.length > 3) {
                            artists_html = '<a href="javascript:;">Various Artists</a>';
                        }else {
                            $.each(album.artists, function (artist_key, artist) {
                                artist_name = (artist_key + 1) === album.artists.length ? artist.artist_name_eng :  artist.artist_name_eng  + ', ';

                                artists_html = artists_html.concat('<a href="/artist/' + artist.id + '">' + artist_name + '</a>')
                            });
                        }

                        html = '<div class="col-lg-2 col-md-2">' +
                            '<div class="ms_rcnt_box marger_bottom30">' +
                            '<div class="ms_rcnt_box_img">' +
                            '<img src="'+ album.album_image +'" alt="">' +
                            '<div class="ms_main_overlay">' +
                            '<div class="ms_box_overlay"></div>' +
                            '<div class="ms_more_icon"><img src="/images/svg/more.svg" alt=""></div>' +
                            '<ul class="more_option">' +
                            '<li><a href="javascript:;" onclick="sendFav(\'/favourite/album/'+ album.uuid +'\')"><span class="opt_icon"><span class="icon icon_fav"></span></span>Add To Favourites</a></li>' +
                            '</ul>' +
                            '<div class="ms_play_icon">' +
                            '<a href="/album/' + album.uuid +'"><img src="/images/svg/play.svg" alt=""></a>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="ms_rcnt_box_text">' +
                            '<h3><a href="/album/'+ album.uuid +'">'+ album.album_name_mm +'</a></h3>' +
                            '<p> ' + artists_html +'</p>' +
                            '</div>' +
                            '</div>' +
                            '</div>';

                        $('#albums').append(html);
                    });

                    if (response.current_page !== response.last_page) {
                        view_more.data('link', response.next_page_url);
                        view_more.show();
                    }else {
                        hulla.send("all music have been loaded", "success");
                    }

                }
            });
        })
    </script>
@endpush

