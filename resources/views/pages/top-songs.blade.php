@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="Top Songs - Myanmar Music Network">
    <meta property="og:image" content="{{ asset('images/social-logo.png') }}">

    <meta name="twitter:title" content="Top Songs - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset('images/social-logo.png') }}">
    <meta name="twitter:card" content="{{ asset('images/social-logo.png') }}">
@endsection

@section('css')
    <style>
        .ms_free_download.ms_purchase_wrapper .album_list_wrapper>ul>li {
            width: 16%;
        }
    </style>
@endsection
@section('content')

    <!---Main Content Start--->
    <div class="ms_album_single_wrapper ms_profile">

    @include('frontend-layouts.partial.header')

        <!----Free Download Css Start---->
        <div class="album_single_data ms_purchase_wrapper">
            <div class="ms_heading">
                <h1>Weekly Top 20</h1>
            </div>
            <!----Song List---->
            <div class="album_inner_list">
                <div class="album_list_wrapper">
                    <ul class="album_list_name">
                        <li>#</li>
                        <li>Song Title</li>
                        <li>Artist</li>
                        <li class="text-center">Duration</li>
                        <li class="text-center">Fav</li>
                        <li class="text-center">Share</li>
                        <li class="text-center">Buy</li>
                    </ul>
                    @foreach($songs as $key => $song)
                        <ul>
                            <li><a href="javascript:;" class="mmn-play" data-id="{{ $song->id }}" data-title="{{ $song->name_mm }}">
                                    <span class="play_no">{{ str_pad($key+1, 2, 0, STR_PAD_LEFT) }}</span>
                                    <span class="play_hover"></span></a>
                            </li>

                            <li><a href="javascript:;" class="mmn-play" data-id="{{ $song->id }}" >{{ $song->name_mm }}</a></li>

                            <li>
                                @if($song->artists->count() > 3)
                                    <a href="javascript:;">Various Artists</a>
                                @else
                                    @foreach($song->artists as $artist)
                                        <a href="{{ url('artist/' . $artist->id) }}">{{ $artist->artist_name_eng }} @if(!$loop->last) , @endif</a>
                                    @endforeach
                                @endif

                                @if($song->feats->isNotEmpty())
                                    <a href="javascript:;">, ft : </a>
                                    @foreach($song->feats as $feat_artist)
                                        <a href="{{ url('artist/' . $feat_artist->id) }}">{{ $feat_artist->artist_name_eng }} @if(!$loop->last) , @endif</a>
                                    @endforeach
                                @endif
                            </li>

                            <li class="text-center"><a href="javascript:;">{{ $song->duration }}</a></li>
                            @auth
                                <li class="text-center"><a href="javascript:;" onclick="sendFav('/favourite/song/{{ $song->id }}', this)" class="{{ $song->getIsFavourite() ? ' text-danger' : ''  }}"><span class="fa fa-heart"></span> </a></li>
                                <li class="text-center"><a class="btn btn-primary fb_share_btn" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url("album/{$song->albums->first()->uuid}")) }}" target="_blank"><span class="fa fa-facebook-official"></span> share</a></li>
                                @if($song->getIsBought())
                                    <li class="text-center">
                                        <a href="/user/download_song/{{ $song->id }}" class="song_download">
                                            <span class="ms_close"><img src="/images/svg/download.svg" alt=""> Download</span>
                                        </a>
                                    </li>
                                @elseif($song->sub_menu['store'])
                                    <li class="text-center">
                                        <a href="javascript:;" onclick="showBuySongModal({{ $song->id }})"> Buy ({{ $song->price->name }} Kyat)</a>
                                    </li>
                                @elseif($song->sub_menu['free'])
                                    <li class="text-center">
                                        <a href="/download_free_song/{{ $song->id }}" class="song_download">
                                            <span class="ms_close"><img src="/images/svg/download.svg" alt=""> Download</span>
                                        </a>
                                    </li>
                                @endif
                            @else
                                <li class="text-center"><a href="#" data-toggle="modal" data-target="#loginModal"><span class="fa fa-heart"></span> </a></li>
                                <li class="text-center"><a class="btn btn-primary fb_share_btn" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url("album/{$song->albums->first()->uuid}")) }}" target="_blank"><span class="fa fa-facebook-official"></span> share</a></li>
                                @if($song->sub_menu['store'])
                                    <li class="text-center"><a href="#" data-toggle="modal" data-target="#loginModal"> Buy ({{ $song->price->name }} Kyat)</a></li>
                                @elseif($song->sub_menu['free'])
                                    <li class="text-center">
                                        <a href="/download_free_song/{{ $song->id }}" class="song_download">
                                            <span class="ms_close"><img src="/images/svg/download.svg" alt=""> Download</span>
                                        </a>
                                    </li>
                                @endif
                            @endauth
                        </ul>
                    @endforeach
                </div>
            </div>
        </div>

        <!----Main div close---->
    </div>

    <div id="mmn-songs" style="display: none;">
        @foreach($songs as $key => $song)
            <a href="{{ $song->getAudioFile() }}" data-image="{{ $song->getAlbumImage() }}" data-id="{{ $song->id }}" title="{{ $song->name_mm }}" data-artist="{{ $song->artists->implode('artist_name_eng', ', ') }}" class="mmn-single-track" rel="">{{ $song->name_mm }}</a>
        @endforeach
    </div>
@endsection


@push('js')
    <script>

        function removeFav(url) {
            $.ajax({
                type: 'DELETE',
                url: url,
                success:function(response){
                    alert(response.message);
                    location.reload();
                }
            });
        }

    </script>
@endpush
