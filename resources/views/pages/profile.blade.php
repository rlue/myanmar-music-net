@extends('frontend-layouts.app')

@section('content')
    <!---Main Content Start--->
    <div class="padder_top80 ms_profile">
    @include('frontend-layouts.partial.header')

    <!----Edit Profile Wrapper Start---->
        <div class="ms_profile_wrapper">
            <h1>Account Information</h1>
            <div class="ms_profile_box">

                <div class="ms_heading padder_top20"></div>

                <div class="ms_profile_list">
                    <ul>
                        <li>Your Name  <span>- {{ Auth::user()->name }}</span></li>
                        <li>Email  <span>- {{ Auth::user()->email }}</span></li>
                        <li>Phone   <span>- {{ Auth::user()->phone }}</span></li>
                        <li>Deposits      <span>- {{ Auth::user()->remained_amount }} MMK</span></li>
                        <li>Streaming Quality  <span>- {{ Auth::user()->streaming_quality === 256 ? 'High Quality' : 'Low Quality' }}</span></li>
                        <li>mPlay Subscribe<span>- {{ Auth::user()->subscribed() ? 'Active (' . Auth::user()->getLeftDays() . str_plural('-Day', Auth::user()->getLeftDays()) .')' : 'Unactive' }}</span></li>
                    </ul>
                    <a href="{{ url('/account/change_password') }}" class="ms_btn">Change Password</a>
                    <a href="{{ url('/account/edit_account_info') }}" class="ms_btn">Edit Your Account Information</a>
                </div>

            </div>
        </div>
    </div>
@endsection