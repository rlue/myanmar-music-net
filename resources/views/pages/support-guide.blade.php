@extends('frontend-layouts.app')

@section('content')
    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top80">
        @include('frontend-layouts.partial.header')

        <div class="ms_free_download ms_purchase_wrapper">
            <div class="ms_heading">
                <h1>Support Guide</h1>
            </div>
            <div class="row">
                <div class="container">
                    {!! $support_guide !!}
                </div>
            </div>
        </div>

    </div>
@endsection
