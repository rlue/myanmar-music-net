@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="{{ $genre->name }} - Myanmar Music Network">
    <meta property="og:image" content="{{ asset(str_replace(' ', '%20', $genre->songCategory_image)) }}">

    <meta name="twitter:title" content="{{ $genre->name }} - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset(str_replace(' ', '%20', $genre->songCategory_image))  }}">
    <meta name="twitter:card" content="{{ asset(str_replace(' ', '%20', $genre->songCategory_image)) }}">
@endsection

@section('content')
    <!---- Genres Section Start---->
    <div class="ms_content_wrapper ms_genres_single ms_profile padder_top90">

        @include('frontend-layouts.partial.header')

        <div class="row" id="genre">
            <div class="col-lg-12">
                <div class="ms_heading">
                    <h1>Music List For {{ $genre->name }} </h1>
                </div>
            </div>

            @forelse($songs as $song)
                <div class="col-lg-2 col-md-2 col-6">
                    <div class="ms_genres_box">
                        <img src="{{ $song->album_image }}" alt="" class="img-fluid" />
                        <div class="ms_main_overlay">
                            <div class="ms_box_overlay"></div>
                            <div class="ms_play_icon">
                                <a href="/album/{{ $song->uuid }}">
                                    <img src="/images/svg/play.svg" alt="">
                                </a>
                            </div>
                            <div class="ovrly_text_div">
                                <span class="ovrly_text1"><a href="/album/{{ $song->uuid }}">{{ $song->name_mm }}</a></span>
                                <span class="ovrly_text2"><a href="/album/{{ $song->uuid }}">view song</a></span>
                            </div>
                        </div>
                        <div class="ms_box_overlay_on">
                            <div class="ovrly_text_div">
                                <span class="ovrly_text1"><a href="/album/{{ $song->uuid }}">{{ $song->name_mm }}</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-lg-12 text-center padder_bottom60" style="color: #fff">
                    No Song
                </div>
            @endforelse

        </div>

        <div class="more_loading" style="display: none;">
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
        </div>

        @if($songs->currentPage() !== $songs->lastPage())
            <div class="ms_view_more padder_bottom20">
                <a href="javascript:;" class="ms_btn" id="view_more" data-link="{{ $songs->nextPageUrl() }}">view more</a>
            </div>
        @endif
    </div>

@endsection

@push('js')
    <script>
        var loading = $('.more_loading');
        var view_more = $('#view_more');

        $('.ms_btn').on('click', function () {
            loading.show();
            view_more.hide();

            var url = $(this).data('link');
            $.ajax({
                type: 'GET',
                url: url,
                success:function(response){
                    loading.hide();

                    $.each(response.data, function (key, value) {
                        html = '<div class="col-lg-2 col-md-2 col-6">' +
                            '<div class="ms_genres_box">' +
                            '<img src="' + value.album_image + '" alt="" class="img-fluid" />' +
                            '<div class="ms_main_overlay">' +
                            '<div class="ms_box_overlay"></div>' +
                            '<div class="ms_play_icon">' +
                            '<a href="/album/' + value.uuid + '">' +
                            '<img src="/images/svg/play.svg" alt="">' +
                            '</a>' +
                            '</div>' +
                            '<div class="ovrly_text_div">' +
                            '<span class="ovrly_text1"><a href="/album/' + value.uuid + '">' + value.name_mm + '</a></span>' +
                            '<span class="ovrly_text2"><a href="/album/' + value.uuid + '">view song</a></span>' +
                            '</div>' +
                            '</div>' +
                            '<div class="ms_box_overlay_on">' +
                            '<div class="ovrly_text_div">\n' +
                            '<span class="ovrly_text1"><a href="/album/' + value.uuid + '">' + value.name_mm + '</a></span>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>';

                        $('#genre').append(html);
                    });

                    if (response.current_page !== response.last_page) {
                        view_more.data('link', response.next_page_url);
                        view_more.show();
                    }else {
                        hulla.send("All {{ $genre->name }} genre have been loaded", "success");
                    }



                }
            });
        });
    </script>
@endpush