@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="Playlist - Myanmar Music Network">
    <meta property="og:image" content="{{ asset('images/social-logo.png') }}">

    <meta name="twitter:title" content="Playlist - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset('images/social-logo.png') }}">
    <meta name="twitter:card" content="{{ asset('images/social-logo.png') }}">
@endsection

@section('content')
    <!---- Genres Section Start---->
    <div class="ms_content_wrapper ms_genres_single ms_profile padder_top90">

        @include('frontend-layouts.partial.header')

        <div class="row">
            <div class="col-lg-12">
                <div class="ms_heading">
                    <h1>Playlist</h1>
                </div>
            </div>

            @foreach($playlist as $item)
                <div class="col-lg-2 col-md-2">
                    <div class="ms_genres_box">
                        <img src="{{ $item->playlist_image }}" alt="" class="img-fluid" />
                        <div class="ms_main_overlay">
                            <div class="ms_box_overlay"></div>
                            <div class="ms_play_icon">
                                <a href="/playlist/{{ $item->id }}">
                                    <img src="/images/svg/play.svg" alt="">
                                </a>
                            </div>
                            <div class="ovrly_text_div">
                                <span class="ovrly_text1"><a href="/playlist/{{ $item->id }}">{{ $item->playlist_name_mm }}</a></span>
                                <span class="ovrly_text2"><a href="/playlist/{{ $item->id }}">view songs</a></span>
                            </div>
                        </div>
                        <div class="ms_box_overlay_on">
                            <div class="ovrly_text_div">
                                <span class="ovrly_text1"><a href="/playlist/{{ $item->id }}">{{ $item->playlist_name_mm }}</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
    <!----Live Radio Section Start---->
@endsection

