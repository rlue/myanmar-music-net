@extends('frontend-layouts.app')

@section('social-meta')
    <meta property="og:title" content="Recommended Playlist - Myanmar Music Network">
    <meta property="og:image" content="{{ asset('images/social-logo.png') }}">

    <meta name="twitter:title" content="Recommended Playlist - Myanmar Music Network">
    <meta name="twitter:image" content="{{ asset('images/social-logo.png') }}">
    <meta name="twitter:card" content="{{ asset('images/social-logo.png') }}">
@endsection

@section('content')
    <!---Main Content Start--->
    <div class="ms_content_wrapper ms_profile padder_top50">
    @include('frontend-layouts.partial.header')

    <!----Live Playlist Section Start---->
        <div class="ms_top_artist">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ms_heading">
                            <h1>Recommended Playlist</h1>
                        </div>
                    </div>
                    @foreach($recommended_playlists as $recommended_playlist)
                        <div class="col-lg-2 col-md-2 col-6">
                        <div class="ms_rcnt_box marger_bottom30">
                            <div class="ms_rcnt_box_img">
                                <img src="{{ $recommended_playlist->playlist_image }}" alt="" class="img-fluid">
                                <div class="ms_main_overlay">
                                    <div class="ms_box_overlay"></div>
                                    <div class="ms_play_icon">
                                        <a href="{{ url("playlist/{$recommended_playlist->id}") }}">
                                            <img src="/images/svg/play.svg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="ms_rcnt_box_text">
                                <h3><a href="{{ url("playlist/{$recommended_playlist->id}") }}">{{ $recommended_playlist->playlist_name_mm }}</a></h3>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
@endsection