<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>@yield('title', config('app.name', 'MMN'))</title>

    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="description" content="Myanmar Music Network | There's another way">
    <meta name="author" content="tnw">
    <meta name="keywords" content="mmn, myanmar music network, music store, myanmar music store, pro music, myanmar songs, myanmar online music streaming, myanmar online music store, myanmar music streaming, pro music studio">

    <meta name="MobileOptimized" content="320">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon Link -->
    <link rel="apple-touch-icon" sizes="57x57" href="/fav/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/fav/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/fav/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/fav/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/fav/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/fav/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/fav/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/fav/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/fav/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/fav/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/fav/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/fav/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/fav/favicon-16x16.png">
    <meta name="msapplication-TileImage" content="/fav/ms-icon-144x144.png">


    <meta property="fb:app_id" content="317800862162728"/>
    <meta property="og:site_name" content="Myanmar Music Network"/>
    <meta property="og:description" content="Register Now Get 30 Days Free mPlay.">

    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{ url()->current() }}">

    <meta property="og:image:width" content="300" />
    <meta property="og:image:height" content="300" />

    <meta name="twitter:description" content="Register Now Get 30 Days Free mPlay.">

    @section('social-meta')
        <meta property="og:title" content="Myanmar Music Network">
        <meta property="og:image" content="{{ asset('images/social-logo.png') }}">

        <meta name="twitter:image" content="{{ asset('images/social-logo.png') }}">
        <meta name="twitter:card" content="{{ asset('images/social-logo.png') }}">
    @show


    <!--Start Style -->
    <link rel="stylesheet" type="text/css" href="/css/fonts.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-v4.1.1.css">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/js/plugins/swiper/css/swiper.min.css">
    <link rel="stylesheet" type="text/css" href="/js/plugins/nice_select/nice-select.css">
    <link rel="stylesheet" type="text/css" href="/js/plugins/player/volume.css">
    <link rel="stylesheet" type="text/css" href="/js/plugins/scroll/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="/css/music-style.css?v=1.13">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>

    <link rel="stylesheet" href="//mmwebfonts.comquas.com/fonts/?font=padauk" media="all">

    @yield('css')
</head>

<body>
<!----Loader Start---->
<div class="ms_loader">
    <div class="wrap">
        <img src="/images/loader.gif" alt="">
    </div>
</div>

<!----Main Wrapper Start---->
<div class="ms_main_wrapper">

    @include('frontend-layouts.partial.sidebar')

    @yield('content')

    @include('frontend-layouts.partial.footer')

    @include('frontend-layouts.partial.player')
</div>

<!---- Modal---->
<div class="ms_save_modal">
    <div id="subscribeModal" class="modal  centered-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="fa_icon form_close"></i>
                    </button>
                    <div class="modal-body">
                        <h1>Are you sure you want to subscribe ?</h1>
                        <div class="clr_modal_btn">
                            <a href="{{ url('/user/subscribe') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('subscribe-form').submit();">
                                subscribe
                            </a>
                            <a href="#" data-dismiss="modal"> cancel</a>
                            <form id="subscribe-form" action="{{ url('/user/subscribe') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!---- Modal---->
<div class="ms_save_modal">
    <div id="myModal" class="modal  centered-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="fa_icon form_close"></i>
                    </button>
                    <div class="modal-body">
                        <h1>You have insufficient amount to subscribe!</h1>
                        <div class="clr_modal_btn">
                            <a href="{{ url('/user/payment') }}">Click here to redeem (Top Up)</a>
                            <a href="#" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!---- Modal---->
<div class="ms_save_modal">
    <div id="musicModal" class="modal  centered-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="fa_icon form_close"></i>
                    </button>
                    <div class="modal-body">
                        <h1>Are you sure you want to buy ?</h1>
                        <div class="clr_modal_btn">
                            <a href="/user/buy_song" id="buy">Buy</a>
                            <a href="#" data-dismiss="modal">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!---- Modal---->
<div class="ms_save_modal">
    <div id="downloadModal" class="modal  centered-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="fa_icon form_close"></i>
                    </button>
                    <div class="modal-body">
                        <h1>Are you sure you want to download now ?</h1>
                        <div class="clr_modal_btn">
                            <a href="/user/buy/song" id="download_now" download="">Download Now</a>
                            <a href="#" data-dismiss="modal">Later</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!---- Modal---->
<div class="ms_save_modal">
    <div id="downloadAlbumModal" class="modal  centered-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="fa_icon form_close"></i>
                    </button>
                    <div class="modal-body">
                        <h1>Buying Successful</h1>
                        <div class="clr_modal_btn">
                            <a href="#" id="download_album_btn">Go to download album</a>
                            <a href="#" data-dismiss="modal">Later</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!---- Modal---->
<div class="ms_save_modal">
    <div id="loginModal" class="modal  centered-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="fa_icon form_close"></i>
                    </button>
                    <div class="modal-body">
                        <h1>You need to login firstly</h1>
                        <div class="clr_modal_btn">
                            <a href="{{ url('/login') }}">Click here to login</a>
                            <a href="#" data-dismiss="modal">Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!---- Modal---->
<div class="ms_save_modal">
    <div id="mmModal" class="modal  centered-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-content">
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="fa_icon form_close"></i>
                    </button>
                    <div class="modal-body">
                        <h1 id="modalHeader"></h1>
                        <div class="clr_modal_btn">
                            <a href="#" id="modalUrl">Click here</a>
                            <a href="#" data-dismiss="modal">Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!---- Modal---->
<div class="ms_save_modal">
    <div id="myQuality" class="modal  centered-modal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal">
                    <i class="fa_icon form_close"></i>
                </button>
                <div class="modal-body">
                    <h1>Streaming Quality Options</h1>
                    <br>

                    <form action="{{ url('user/streaming_quality') }}" method="POST">
                        @csrf
                        <div class="save_input_group">
                            @auth
                                <select name="streaming_quality" id="quality" class="form-control">
                                    <option value="256" {{ auth()->user()->streaming_quality === 256 ? 'selected' : '' }}>High Quality</option>
                                    <option value="96" {{ auth()->user()->streaming_quality === 96 ? 'selected' : '' }}>Low Quality</option>
                                </select>
                            @endauth
                        </div>
                        <br>
                        <button class="save_btn" type="submit">Save</button>
                        <button class="save_btn" type="button" data-dismiss="modal">Cancel</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<!--Main js file Style-->
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/plugins/swiper/js/swiper.min.js"></script>
<script type="text/javascript" src="/js/plugins/player/jplayer.playlist.min.js?v=0.3"></script>
<script type="text/javascript" src="/js/plugins/player/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="/js/plugins/player/audio-player.js?v=1.8"></script>
<script type="text/javascript" src="/js/plugins/player/volume.js"></script>
<script type="text/javascript" src="/js/plugins/nice_select/jquery.nice-select.min.js"></script>
<script type="text/javascript" src="/js/plugins/scroll/jquery.mCustomScrollbar.js"></script>
<script src="{{ asset('/js/typeahead-v1.2.0.js') }}"></script>
<script src="{{ asset('/plugins/hullabaloo/hullabaloo.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/music-custom.js?v=2.2') }}"></script>

<!-- Import typeahead.js -->

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


<script>
    var hulla = new hullabaloo();
    hulla.options.offset = {
        from: "top",
        amount: 50
    }

    @if (session()->has('status'))
        hulla.send("{{ session('message') }}", "{{ session('status') }}");
    @endif

</script>

@stack('js')

</body>

</html>
