<!----Footer Start---->
<div class="ms_footer_wrapper" style="margin-bottom:0;">
    <div class="ms_footer_logo">
        <a href="/"><img src="/img/logo.png" alt=""></a>
    </div>
    <div class="ms_footer_inner">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="footer_box">
                    <h1 class="footer_title">myanmar music network</h1>
                    <p>MMN ဟာ တေးချစ်သူတွေနဲ့ တေးဂီတအနုပညာရှင်တိုအကြား လျှင်လျှင်မြန်မြန်လွယ်လွယ်ကူကူ ပေါင်းကူးဆက်သွယ်ပေးနိုင်ဖို့ ရည်ရွယ်ချက်နဲ့ တည်ဆောက်ထားတဲ့ ကွန်ယက်တစ်ခု ဖြစ်ပါတယ်။ </p>
                </div>
                <div class="footer_box">

                    <h1 style="font-size: 18px;"><a href="/support_guide">Support Guide</a></h1>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="footer_box footer_app">
                    <h1 class="footer_title">Download our App</h1>
                    <p>Go Mobile with our app.<br> Listen to your favourite songs at just one click. Download Now !</p>
                    <a href="https://play.google.com/store/apps/details?id=com.mmusicnet.mmusicnet" id="play-store-link" target="_blank" class="foo_app_btn"><img src="/images/google_play.jpg" alt="" class="img-fluid"></a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="footer_box footer_contacts">
                    <h1 class="footer_title">contact us</h1>
                    <ul class="foo_con_info">
                        <li>
                            <div class="foo_con_icon">
                                <img src="/images/svg/phone.svg" alt="">
                            </div>
                            <div class="foo_con_data">
                                <span class="con-title">Call us :</span>
                                <span><a href="tel:09262277337">09262277337</a></span>
                                <span><a href="tel:09262277447">09262277447</a></span>
                            </div>
                        </li>
                        <li>
                            <div class="foo_con_icon">
                                <img src="/images/svg/message.svg" alt="">
                            </div>
                            <div class="foo_con_data">
                                <span class="con-title">email us :</span>
                                <span><a href="mailto:contact@mmusicnet.com">contact@mmusicnet.com </a></span>
                            </div>
                        </li>
                        <li>
                            <div class="foo_con_icon">
                                <img src="/images/svg/add.svg" alt="">
                            </div>
                            <div class="foo_con_data">
                                <span class="con-title">walk in :</span>
                                <span>No 15,Kanbawza Avenue Street,Shwe Taung Gyar 1st Quarter</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="footer_box footer_contacts">
                    <h1 class="footer_title">follow us</h1>

                    <div class="foo_sharing">
                        <ul>
                            <li><a href="https://www.facebook.com/mmusicnet" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.youtube.com/channel/UC9NiT3jXeIsXKpBIRHVD0UA" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                            <li><a href="https://twitter.com/mMusicNet" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="https://www.instagram.com/mmusicnet/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!----Copyright---->
    <div class="col-lg-12">
        <div class="ms_copyright">
            <div class="footer_border"></div>
            <p>Copyright &copy; 2014 - {{ date("Y") }}<a href="/"> GADI</a>. All Rights Reserved.</p>
        </div>
    </div>
</div>
