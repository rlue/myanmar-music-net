<!---Header--->
<div class="ms_header">
    <div class="ms_top_left">
        <div class="ms_top_search">
            <form action="/search" method="GET">
                <input type="text" name="q" class="form-control" id="search" autocomplete="off" placeholder="Artist, Song, Album, etc.....">
                <button type="submit" class="search_icon">
                    <img src="/images/svg/search.svg" alt="">
                </button>
            </form>

        </div>
    </div>
    <div class="ms_top_right">
        <div class="ms_top_btn">
            @auth
                <a href="javascript:;" class="ms_admin_name">Hello {{ Auth::user()->name }}
                </a>
                <ul class="pro_dropdown_menu">
                    <li><a href="{{ url('/user/profile') }}">Profile</a></li>
                    <li><a href="{{ url('/user/payment') }}" id="redeem_btn">Redeem (Top Up) - {{ Auth::user()->remained_amount }} (MMK)</a></li>
                    <li>
                        @if(Auth::user()->subscribed())
                            <a href="javascript:;"><span style="text-transform:none">mPlay</span> Subscribe <span id="subscribe_days" style="color:green;font-weight:bold">(Activated)</span></a>
                        @elseif(Auth::user()->remained_amount < 1500)
                            <a data-toggle="modal" data-target="#myModal" href="#"><span style="text-transform:none">mPlay</span> Subscribe <span id="subscribe_days" style="color:red;font-weight:bold">(Unactivated)</span></a>
                        @else
                            <a data-toggle="modal" data-target="#subscribeModal" href="#"><span style="text-transform:none">mPlay</span> Subscribe <span id="subscribe_days" style="color:red;font-weight:bold">(Unactivated)</span></a>
                        @endif
                    </li>
                    <li><a data-toggle="modal" data-target="#myQuality" href="#">Streaming Quality</a></li>
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>

            @elseguest
                <a href="{{ route('register') }}" class="ms_btn reg_btn"><span>register</span></a>
                <a href="{{ route('login') }}" class="ms_btn login_btn"><span>login</span></a>
            @endauth

        </div>
    </div>
</div>
