<!---Side Menu Start--->
<div class="ms_sidemenu_wrapper">
    <div class="ms_nav_close">
        <i class="fa fa-angle-right" aria-hidden="true"></i>
    </div>
    <div class="ms_sidemenu_inner">
        <div class="ms_logo_inner">
            <div class="ms_logo">
                <a href="/"><img src="/img/mmn_logo.png" alt="" class="img-fluid"/></a>
            </div>
            <div class="ms_logo_open">
                <a href="/"><img src="/img/open_mmn_logo.png" alt="" class="img-fluid"/></a>
            </div>
        </div>
        <div class="ms_nav_wrapper">
            <ul>
                <li><a class="{{ Request::is('/') ? 'active' : '' }}" href="/" title="Discover">
						<span class="nav_icon">
							<span class="icon icon_discover"></span>
						</span>
                        <span class="nav_text">
							discover
						</span>
                    </a>
                </li>
                <li><a class="{{ Request::is('albums') ? 'active' : '' }}" href="/albums" title="Albums">
						<span class="nav_icon">
							<span class="icon icon_albums"></span>
						</span>
                        <span class="nav_text">
							albums
						</span>
                    </a>
                </li>
                <li><a class="{{ Request::is('video/*') || Request::is('videos') ? 'active' : '' }}" href="/videos" title="Videos">
						<span class="nav_icon">
							<span class="icon icon_video"></span>
						</span>
                        <span class="nav_text">
							videos
						</span>
                    </a>
                </li>
                <li><a class="{{ Request::is('artist/*') || Request::is('artists') ? 'active' : '' }}" href="{{ url('artists') }}" title="Artists">
						<span class="nav_icon">
							<span class="icon icon_artists"></span>
						</span>
                        <span class="nav_text">
							artists
						</span>
                    </a>
                </li>
                <li><a class="{{ Request::is('genre/*') || Request::is('genres') ? 'active' : '' }}" href="{{ url('genres') }}" title="Genres">
						<span class="nav_icon">
							<span class="icon icon_genres"></span>
						</span>
                        <span class="nav_text">
							genres
						</span>
                    </a>
                </li>
                <li><a class="{{ Request::is('feeling/*') || Request::is('feelings') ? 'active' : '' }}" href="{{ url('feelings') }}" title="Feelings">
						<span class="nav_icon">
							<span class="icon icon_smile"></span>
						</span>
                        <span class="nav_text">
							feelings
						</span>
                    </a>
                </li>
                <li><a class="{{ Request::is('playlist') || Request::is('playlist/*') ? 'active' : '' }}" href="/playlist" title="Playlist">
						<span class="nav_icon">
							<span class="icon icon_fe_playlist"></span>
						</span>
                        <span class="nav_text">
							playlist
						</span>
                    </a>
                </li>
                <li><a class="{{ Request::is('event') || Request::is('event/*') ? 'active' : '' }}" href="/events" title="Events">
						<span class="nav_icon">
							<span class="icon icon_tracks"></span>
						</span>
                        <span class="nav_text">
							events
						</span>
                    </a>
                </li>

            </ul>
            <ul class="nav_downloads">
                <li><a class="{{ Request::is('user/purchased') ? 'active' : '' }}" href="{{ url('user/purchased') }}" title="Purchased">
						<span class="nav_icon">
							<span class="icon icon_purchased"></span>
						</span>
                        <span class="nav_text">
							purchased
						</span>
                    </a>
                </li>
                <li><a class="{{ Request::is('mymusic/favourite_songs') || Request::is('mymusic/favourite_albums') || Request::is('user/favourite') ? 'active' : '' }}" href="{{ url('/user/favourite') }}" title="Favourites">
						<span class="nav_icon">
							<span class="icon icon_favourite"></span>
						</span>
                        <span class="nav_text">
							favourites
						</span>
                    </a>
                </li>
                <li><a class="{{ Request::is('recent/*') || Request::is('recent') ? 'active' : '' }}" href="/recent" title="History">
						<span class="nav_icon">
							<span class="icon icon_history"></span>
						</span>
                        <span class="nav_text">
							history
						</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>