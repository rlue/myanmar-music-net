@extends('layouts.admin')

@section('content')
    
  <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Subscription Price</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li>Subscription Price</li>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <!-- data table start -->
                    <div class="col-8 mt-3" ></div>
                    <div class="col-4 mt-3">
                        @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-12 mt-5">
                        <div class="card">
                            <div class="card-body">
                            
                            <p class="pull-right"><a href="{{route('subprice.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a></p>
                            
                                <h4 class="header-title">Subscription Price List</h4>
                                
                                <div class="data-tables">
                                    <table id="subpriceTable" class="text-center">
                                        <thead class="bg-light text-capitalize">
                                            <tr>
                                                <th>No</th>
                                                <th>Day</th>
                                                <th>Amount</th>
                                               <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                       
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- data table end -->
                   
                </div>
            </div>

            
@endsection


@push('scripts')
<script>
$(function() {
    var baseUrl = '{!! URL::to("/") !!}';
  let me = this;
     this.tbl = $('#subpriceTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('subprice.getData') !!}',
        columns: [
            { data: 'no', orderable: false, bSearchable: false },
            { data: 'num_day', name: 'num_day' },
            { data: 'name', name: 'name' },
            {data: 'action', name: 'action', orderable: false, searchable: false},
            
        ],
        "fnRowCallback" : function(nRow, aData, iDisplayIndex){
          // For auto numbering at 'No' column
          var start = me.tbl.page.info().start;
          $('td:eq(0)',nRow).html(start + iDisplayIndex + 1);
      }
    });

  

   $('#subpriceTable').on('click', '.sub-delete', function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var checked = confirm("Sure You want to delete it");
    if(checked){
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type : "DELETE",
          url  : baseUrl + '/backend/subprice/' + id,
          data : {},
          success: function(data){
            var result = $.parseJSON(data);
            if(result['status'] == 'success'){
                me.tbl.ajax.reload( null, false );
            }else{
                alert(result['message']);        
            }
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert('Error in student view. Please contact to administrator');
          }
        });  
    }else{
        return false;
    }
        
  });
    
});
  

</script>
@endpush