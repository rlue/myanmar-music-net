@extends('layouts.admin')
@push('styles')

<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
@endpush
@section('content')
    

   <div class="page-title-area">
        <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Notifications</h4>
                           <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="{{route('notification.index')}}">Notifications</a></li>
                                <li><span> Create</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
        </div>
    </div>
            <!-- page title area end -->
    <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    
                                    <form method="POST" action="{{route('notification.store')}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Create Notifications </h4>
                                
                                
                                <div class="form-group">
                                 @if ($errors->has('title'))
                                          <div class="alert alert-danger">{{ $errors->first('title') }}</div>
                                      @endif
                                            <label for="example-text-input" class="col-form-label">Title</label>
                                            <input class="form-control" type="text"  name="title" placeholder="title" value="{{old('title')}}">
                                </div>
                              
                                <div class="form-group">
                                            <label for="description" class="col-form-label">Description</label>
                                            <textarea name="description" id="detail" class="form-control"></textarea>
                                </div>
                               
                                <div class="form-group">
                                    @if ($errors->has('noti_image'))
                                          <div class="alert alert-danger">{{ $errors->first('noti_image') }}</div>
                                      @endif
                                    <label for="Artist Image"> Image</label>
                                   
                                </div>
                        
                                <div class="input-group">
                                
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                     </a>
                                   </span>
                                   <input id="thumbnail" class="form-control" type="text" name="noti_image">
                                </div>
                                <div class="form-group">
                                 <img id="holder" style="margin-top:15px;max-height:100px;">
                                </div>
                                <div class="form-group" id="send_noti">
                                   <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="now_send" name="status" value="now" class="custom-control-input">
                                            <label class="custom-control-label" for="now_send">Now</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="schedule_send" name="status" value="schedule" class="custom-control-input">
                                            <label class="custom-control-label" for="schedule_send">Schedule</label>
                                        </div>
                                </div> 
                                <div class="form-group">
                                  <input class="form-control" type="text"  name="send_schedule" id="schedule_input" />
                                </div>
                                <div class="form-group">
                                            <a href="{{route('notification.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
    </div>
@endsection


@push('scripts')


  
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>    
     <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
      <script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $('#detail').ckeditor();
        // $('.textarea').ckeditor(); // if class is prefered.
        $(document).ready(function(){
          $('#schedule_input').hide();
          $('#send_noti input').on('change', function() {
              var checknoti =  $('input[name=status]:checked', '#send_noti').val(); 
              
              if(checknoti == 'now' ){
                $('#schedule_input').hide();
              }else{
                $('#schedule_input').show();
              }
            });
        });
    </script>
    <script>
    $(function() {
    $("#schedule_input").datetimepicker({
        format: 'Y-M-D h:m:s',
    });
    
  }); 

     
    
     

     
   </script>
   

@endpush