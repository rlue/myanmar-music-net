@extends('layouts.admin')
@push('styles')

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

@endpush
@section('content')
    

   <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Notifications </h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="{{route('events.index')}}">Notifications</a></li>
                                <li><span> Edit</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    @if (count($errors) > 0)
                                       <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                      @endif
                                    <form method="POST" action="{{route('notification.update',$noti->id)}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Update Notification</h4>
                                 <div class="form-group">
                                 @if ($errors->has('title'))
                                          <div class="alert alert-danger">{{ $errors->first('title') }}</div>
                                      @endif
                                            <label for="example-text-input" class="col-form-label">Title</label>
                                            <input class="form-control" type="text"  name="title" placeholder="title" value="{{$noti->title}}">
                                </div>
                        
                                <div class="form-group">
                                            <label for="description" class="col-form-label">Description</label>
                                            <textarea name="description" id="description" class="form-control">@if($noti->description) {{$noti->description}} @endif</textarea>
                                </div>
                                <div class="form-group">
                                    @if ($errors->has('noti_image'))
                                          <div class="alert alert-danger">{{ $errors->first('noti_image') }}</div>
                                      @endif
                                    <label for="Artist Image"> Image</label>
                                   
                                </div>
                        
                                <div class="input-group">
                                
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                     </a>
                                   </span>
                                   <input id="thumbnail" class="form-control" type="text" name="noti_image" value="{{$noti->noti_image}}">
                                 </div>
                                 <div class="form-group">
                                 @if($noti->noti_image)
                                    <img id="holder" style="margin-top:15px;max-height:100px;" src="{{ URL::to('/') }}{{$noti->noti_image}}" />
                                  @else
                                   <img id="holder" style="margin-top:15px;max-height:100px;">
                                  @endif
                                  </div>
                                <div class="form-group" id="send_noti">
                                   <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="now_send" name="status" value="now" class="custom-control-input" @if($noti->status == 'now') {{"checked"}} @endif>
                                            <label class="custom-control-label" for="now_send">Now</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="schedule_send" name="status" value="schedule" class="custom-control-input" @if($noti->status == 'schedule') {{"checked"}} @endif>
                                            <label class="custom-control-label" for="schedule_send">Schedule</label>
                                        </div>
                                </div> 
                                <div class="form-group">
                                  <input class="form-control" type="text" value="{{$noti->send_schedule}}" name="send_schedule" id="schedule_input" />
                                </div>
                                    
                                  <form action="{{ route('notification.store', $noti->id) }}"
                                    >
                                        {{ csrf_field() }}
                                        {{ method_field("patch") }}
                                         <div class="form-group">
                                            <a href="{{route('notification.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
            </div>
@endsection


@push('scripts')


<script src="{{ asset('js/select2.min.js') }}"></script>   
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>   
     <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
      <script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $('#description').ckeditor();
        // $('.textarea').ckeditor(); // if class is prefered.
        $(document).ready(function(){
          var check =  $('input[name=status]:checked', '#send_noti').val(); 
              
              if(check == 'now' ){
                $('#schedule_input').hide();
              }else{
                $('#schedule_input').show();
              }
          $('#send_noti input').on('change', function() {
              var checknoti =  $('input[name=status]:checked', '#send_noti').val(); 
              
              if(checknoti == 'now' ){
                $('#schedule_input').hide();
              }else{
                $('#schedule_input').show();
              }
            });
        });
    </script>
    <script>
    $(function() {
    $("#schedule_input").datetimepicker({
        format: 'Y-M-D h:m:s',
    });
    
  }); 

     
    
     
     
   </script>
   

@endpush