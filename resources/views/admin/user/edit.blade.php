@extends('layouts.admin')

@section('content')
    

   <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">User</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><span>Form</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    @if (count($errors) > 0)
                                       <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                      @endif
                                    <form method="POST" action="{{route('users.update',$user->id)}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Update User</h4>
                                        
                                        <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Name</label>
                                            <input class="form-control" type="text"  name="name" value="{{$user->name}}" placeholder="Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-search-input" class="col-form-label">Email</label>
                                            <input class="form-control" type="email"  name="email" value="{{$user->email}}" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <label for="example-email-input" class="col-form-label">Password</label>
                                            <input class="form-control" type="password"  name="password" placeholder="Password">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="inputPassword" class="">Confirm Password</label>
                                            <input type="password" class="form-control" name="password_confirmation"  placeholder="Confirm Password">
                                        </div>
                                        <div class="form-group">
                                        @foreach ($roles as $r)
                                            <label >
                                                <input type="checkbox"  name="roles[]" value="{{$r->id}}" {{ $user->roles->contains($r->id) ? 'checked' : '' }}> {{ucfirst($r->name)}}
                                            </label><br />
                                            @endforeach
                                        </div>
                                       <form action="{{ route('users.store', $user->id) }}"
                                    >
                                        {{ csrf_field() }}
                                        {{ method_field("patch") }}
                                         <div class="form-group">
                                            <a href="{{route('users.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
            </div>
@endsection


