@extends('layouts.admin')
@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@endpush
@section('content')
    

   <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Videos</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="{{route('videos.index')}}">Videos</a></li>
                                <li><span> Edit</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    @if (count($errors) > 0)
                                       <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                      @endif
                                    <form method="POST" action="{{route('videos.update',$video->id)}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Update Video</h4>
                                        
                                    
                                   <div class="form-group">
                                        <label for="example-text-input" class="col-form-label">Video Name (MM)</label>
                                        <input class="form-control" type="text"  name="name_mm" value="{{$video->name_mm}}" placeholder="Video Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="example-text-input" class="col-form-label">Video Name (ENG)</label>
                                        <input class="form-control" type="text"  name="name_eng" value="{{$video->name_eng}}" placeholder="Video Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="example-text-input" class="col-form-label">Video Description</label>
                                        <textarea name="video_desc" class="form-control">@if($video->video_desc)
                                        {{$video->video_desc}}@endif</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="video_category">Category</label>
                                        <select name="video_category" class="form-control" id="video_category">

                                        @foreach($videocat as $cat)
                                        @if($cat->id == $video->video_category)
                                          <option value="{{$cat->id}}" selected="selected">{{$cat->name}}</option>
                                        @else
                                        <option value="{{$cat->id}}" >{{$cat['name']}}</option>
                                        @endif
                                        @endforeach
                                        </select>
                                    </div>
                                     <div class="form-group">
                                        <label for="artist">Aritst</label>
                                        <div class="row">
                                        <div class="col-sm-11">
                                        <select name="artist_id[]" class="form-control" id="artist_id" multiple="multiple">

                                       @foreach($artist as $art)
                                        <option value="{{$art->id}}" @foreach($video->artists as $a) @if($a->id == $art->id)selected="selected"@endif @endforeach>{{$art->artist_name_eng}}</option>
                                        @endforeach
                                        </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="{{route('artists.create')}}" class="btn btn-primary" target="_blank"><i class="fa fa-plus"></i></a>
                                        </div>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                    @if ($errors->has('video_link'))
                                          <div class="alert alert-danger">{{ $errors->first('video_link') }}</div>
                                      @endif
                                            <label for="example-text-input" class="col-form-label">Video Link</label>
                                            <input class="form-control" type="text"  name="video_link" placeholder="Video Link" value="{{$video->video_link}}">
                                    </div>
                                    <form action="{{ route('videos.store', $video->id) }}"
                                    >
                                        {{ csrf_field() }}
                                        {{ method_field("patch") }}
                                         <div class="form-group">
                                            <a href="{{route('videos.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
            </div>
@endsection


@push('scripts')


<script src="{{ asset('js/select2.min.js') }}"></script>      
   
    <script>
    $('#video_category').select2();
     $('#artist_id').select2({
      multiple: true,
      placeholder: "Choose Artist",
     });

     
   </script>
   

@endpush