@extends('layouts.admin')
@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@endpush
@section('content')
    

   <div class="page-title-area">
        <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Video</h4>
                           <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="{{route('videos.index')}}">Video</a></li>
                                <li><span> Create</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
        </div>
    </div>
            <!-- page title area end -->
    <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    
                                    <form method="POST" action="{{route('videos.store')}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Create Video</h4>
                                    
                                   
                    
                                    <div class="form-group">
                                     @if ($errors->has('name_mm'))
                                          <div class="alert alert-danger">{{ $errors->first('name_mm') }}</div>
                                      @endif 
                                            <label for="example-text-input" class="col-form-label">Video Name (MM)</label>
                                            <input class="form-control" type="text"  name="name_mm" placeholder="Video Name (MM)" value="{{old('name_mm')}}">
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('name_eng'))
                                          <div class="alert alert-danger">{{ $errors->first('name_eng') }}</div>
                                      @endif
                                            <label for="example-text-input" class="col-form-label">Video Name (ENG)</label>
                                            <input class="form-control" type="text"  name="name_eng" placeholder="Video Name(ENG)" value="{{old('name_eng')}}">
                                    </div>
                                    <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Video Description</label>
                                            <textarea name="video_desc" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('video_category'))
                                          <div class="alert alert-danger">{{ $errors->first('video_category') }}</div>
                                    @endif
                                        <label for="product_category">Category</label>
                                        <select name="video_category" id="video_category" class="form-control">
                                        <option value>Select Category</option>
                                        @foreach($videocat as $cat)

                                          <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        
                                        @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('artist_id'))
                                          <div class="alert alert-danger">{{ $errors->first('artist_id') }}</div>
                                    @endif
                                        <label for="product_category">Artist</label>
                                        <select name="artist_id[]" id="artist_id" class="form-control" multiple="multiple">
                                        <option value>Select Artist</option>
                                        @foreach($artist as $a)

                                          <option value="{{$a->id}}">{{$a->artist_name_eng}}</option>
                                        
                                        @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('video_link'))
                                          <div class="alert alert-danger">{{ $errors->first('video_link') }}</div>
                                      @endif
                                            <label for="example-text-input" class="col-form-label">Video Link</label>
                                            <input class="form-control" type="text"  name="video_link" placeholder="Video Link" value="{{old('video_link')}}">
                                    </div>
                        
                                
                                 
                                        <div class="form-group">
                                            <a href="{{route('artists.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
    </div>
@endsection


@push('scripts')


<script src="{{ asset('js/select2.min.js') }}"></script>      
    
    <script>
     $('#video_category').select2();
    $('#artist_id').select2({
        multiple: true,
      placeholder: "Choose Artist",
    });
    
   </script>
   

@endpush