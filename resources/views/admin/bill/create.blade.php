@extends('layouts.admin')
@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">

@endpush
@section('content')
    

   <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Bill</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><span>Form</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                   
                                    <form method="POST" action="{{route('bills.store')}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Add Bill</h4>
                                        <div class="form-group">
                                    @if ($errors->has('user_id'))
                                          <div class="alert alert-danger">{{ $errors->first('user_id') }}</div>
                                    @endif
                                        <label for="user_id">User <span class="btn_require">*</span></label>
                                        <select name="user_id" class="form-control" id="user_id">
                                        <option value>Select User</option>
                                        @foreach($user as $p)

                                          <option value="{{$p->id}}">{{$p->name}}</option>
                                        
                                        @endforeach
                                        </select>
                                    </div>
                                        <div class="form-group">
                                         @if ($errors->has('amount'))
                                          <div class="alert alert-danger">{{ $errors->first('amount') }}</div>
                                    @endif
                                            <label for="example-text-input" class="col-form-label">Amount</label>
                                            <input class="form-control" type="text"  name="amount" placeholder="Amount">
                                        </div>
                                        
                                        
                                        
                                        <div class="form-group">
                                            <a href="{{route('bills.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
            </div>
@endsection

@push('scripts')


<script src="{{ asset('js/select2.min.js') }}"></script> 

     
    
    <script>
   
    
     $('#user_id').select2();
     
   </script>
   

@endpush

