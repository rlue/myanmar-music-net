@extends('layouts.admin')

@section('content')
    

   <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Feelings</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="{{route('feelings.index')}}">Feelings</a></li>
                                <li><span> Edit</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    @if (count($errors) > 0)
                                       <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                      @endif
                                    <form method="POST" action="{{route('feelings.update',$feeling->id)}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Update Feeling</h4>
                                        
                                    
                                    <div class="form-group">
                                        <label for="example-text-input" class="col-form-label">Feeling</label>
                                        <input class="form-control" type="text"  name="name" value="{{$feeling->name}}" placeholder="Feeling Name">
                                    </div>
                  
                                    <div class="form-group">
                                    <label for="ArtWork">Artwork</label>
                                   
                                </div>
                        
                                <div class="input-group">
                                
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                     </a>
                                   </span>
                                   <input id="thumbnail" class="form-control" type="text" name="feeling_image" value={{$feeling->feeling_image}}>
                                 </div>
                                 <div class="form-group">
                                 @if($feeling->feeling_image)
                                    <img id="holder" style="margin-top:15px;max-height:100px;" src="{{ URL::to('/') }}{{$feeling->feeling_image}}" />
                                  @else
                                   <img id="holder" style="margin-top:15px;max-height:100px;">
                                  @endif
                                  </div>
                                      
                                       <form action="{{ route('feelings.store', $feeling->id) }}"
                                    >
                                        {{ csrf_field() }}
                                        {{ method_field("patch") }}
                                         <div class="form-group">
                                            <a href="{{route('feelings.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
            </div>
@endsection


