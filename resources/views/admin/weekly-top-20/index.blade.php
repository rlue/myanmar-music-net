@extends('layouts.admin')

@section('content')

<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Weekly Top 20</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{route('dashboard')}}">Home</a></li>
                    <li><span>Weekly Top 20</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            <div class="user-profile pull-right">
                @include('layouts.partial.inc')
            </div>
        </div>
    </div>
</div>
<!-- page title area end -->
<div class="main-content-inner">
    <div class="row">
        <!-- data table start -->
        <div class="col-8 mt-3" ></div>
        <div class="col-4 mt-3">
            @if (Session::has('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ Session::get('message') }}
            </div>
            @endif
        </div>
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">

                    <h5>Date - {{ $top_20_ended_at }}</h5>
                    <br>

                    <form action="/backend/weekly-top" method="post">
                        @csrf
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                    </form>

                </div>
            </div>
        </div>
        <!-- data table end -->

    </div>
</div>


@endsection
