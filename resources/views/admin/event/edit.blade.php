@extends('layouts.admin')
@push('styles')

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

@endpush
@section('content')
    

   <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Events </h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="{{route('events.index')}}">Events</a></li>
                                <li><span> Edit</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    @if (count($errors) > 0)
                                       <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                      @endif
                                    <form method="POST" action="{{route('events.update',$event->id)}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Update Event</h4>
                                 <div class="form-group">
                                 @if ($errors->has('name'))
                                          <div class="alert alert-danger">{{ $errors->first('name') }}</div>
                                      @endif
                                            <label for="example-text-input" class="col-form-label">Name</label>
                                            <input class="form-control" type="text"  name="name" placeholder="name" value="{{$event->name}}">
                                </div>
                                <div class="form-group">
                                            <label for="start_date" class="col-form-label">Start Date</label>
                                            <input class="form-control" id="start_date" type="text"  name="start_date" placeholder="YYYY-MM-DD" value="{{$event->start_date}}">
                                    </div>
                                    <div class="form-group">
                                            <label for="end_date" class="col-form-label">End Date</label>
                                            <input class="form-control" id="end_date" type="text"  name="end_date" placeholder="YYYY-MM-DD" value="{{$event->end_date}}">
                                    </div>
                                <div class="form-group">
                                @if ($errors->has('location'))
                                          <div class="alert alert-danger">{{ $errors->first('location') }}</div>
                                      @endif
                                            <label for="location" class="col-form-label">Location</label>
                                            <textarea name="location" class="form-control">@if($event->location) {{$event->location}} @endif</textarea>
                                </div>
                                <div class="form-group">
                                            <label for="event_detail" class="col-form-label">Detail</label>
                                            <textarea name="event_detail" id="detail" class="form-control">@if($event->event_detail) {{$event->event_detail}} @endif</textarea>
                                </div>
                                <div class="form-group">
                                            <label for="co_host" class="col-form-label">Co Host</label>
                                            <input class="form-control" type="text"  name="co_host" placeholder="Co-Host" value="{{$event->co_host}}">
                                </div>
                                <div class="form-group">
                                    @if ($errors->has('event_image'))
                                          <div class="alert alert-danger">{{ $errors->first('event_image') }}</div>
                                      @endif
                                    <label for="Artist Image"> Image</label>
                                   
                                </div>
                        
                                <div class="input-group">
                                
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                     </a>
                                   </span>
                                   <input id="thumbnail" class="form-control" type="text" name="event_image" value="{{$event->event_image}}">
                                 </div>
                                 <div class="form-group">
                                 @if($event->event_image)
                                    <img id="holder" style="margin-top:15px;max-height:100px;" src="{{ URL::to('/') }}{{$event->event_image}}" />
                                  @else
                                   <img id="holder" style="margin-top:15px;max-height:100px;">
                                  @endif
                                  </div>
                                 
                                    
                                  <form action="{{ route('events.store', $event->id) }}"
                                    >
                                        {{ csrf_field() }}
                                        {{ method_field("patch") }}
                                         <div class="form-group">
                                            <a href="{{route('events.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
            </div>
@endsection


@push('scripts')


<script src="{{ asset('js/select2.min.js') }}"></script>   
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>   
     <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
      <script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $('#detail').ckeditor();
        // $('.textarea').ckeditor(); // if class is prefered.
    </script>
    <script>
    $(function() {
    $("#start_date").datepicker({
        format: 'yyyy-mm-dd',
    });
    $("#end_date").datepicker({
        format: 'yyyy-mm-dd',
    });
  }); 

     
    
     
     
   </script>
   

@endpush