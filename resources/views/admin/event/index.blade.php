@extends('layouts.admin')

@section('content')
    
  <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Events</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><span>Events</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <!-- data table start -->
                    <div class="col-8 mt-3" ></div>
                    <div class="col-4 mt-3">
                        @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-12 mt-5">
                        <div class="card">
                            <div class="card-body">
                            
                            <p class="pull-right"><a href="{{route('events.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a></p>
                            
                                <h4 class="header-title">Event List</h4>
                                
                                <div class="data-tables">
                                    <table id="eventTable" class="text-center">
                                        <thead class="bg-light text-capitalize">
                                            <tr>
                                                <th>Image</th>
                                                <th>Name</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th>Location</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                       
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- data table end -->
                   
                </div>
            </div>

            
@endsection


@push('scripts')
<script>
$(function() {
    var baseUrl = '{!! URL::to("/") !!}';
  let me = this;
     this.tbl = $('#eventTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('events.getData') !!}',
        columns: [
            { data: 'event_image', name: 'event_image', render: function( data, type, full, meta ) {
                        return "<img src=\"" + data + "\" width=\"50 \" height=\"30\"/>";
                    } 
            },
            { data: 'name', name: 'name' },
            { data: 'start_date', name: 'start_date' },
            { data: 'end_date', name: 'end_date' },
            { data: 'location', name: 'location' },
            {data: 'action', name: 'action', orderable: false, searchable: false},
            
        ]
    });

  

   $('#eventTable').on('click', '.sub-delete', function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var checked = confirm("Sure You want to delete it");
    if(checked){
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type : "DELETE",
          url  : baseUrl + '/backend/events/' + id,
          data : {},
          success: function(data){
            var result = $.parseJSON(data);
            if(result['status'] == 'success'){
                me.tbl.ajax.reload( null, false );
            }else{
                alert(result['message']);        
            }
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert('Error in student view. Please contact to administrator');
          }
        });  
    }else{
        return false;
    }
        
  });
    
});
  

</script>
@endpush