@extends('layouts.admin')

@section('content')
    
  <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Dashboard</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <!-- sales report area start -->
                <div class="sales-report-area mt-5 mb-5">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="single-report mb-xs-30">
                                <div class="s-report-inner pr--20 pt--30 mb-3">
                                    <div class="icon"><i class="fa fa-music"></i></div>
                                    <div class="s-report-title d-flex justify-content-between">
                                        <h4 class="header-title mb-0">Total Track</h4>
                                        <p></p>
                                    </div>
                                    <div class="d-flex justify-content-between pb-3">
                                        <h2>{{$total_song}}</h2>
                                        <span></span>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="single-report mb-xs-30">
                                <div class="s-report-inner pr--20 pt--30 mb-3">
                                    <div class="icon"><i class="fa fa-photo"></i></div>
                                    <div class="s-report-title d-flex justify-content-between">
                                        <h4 class="header-title mb-0">Total Album</h4>
                                        <p></p>
                                    </div>
                                    <div class="d-flex justify-content-between pb-2">
                                        <h2>{{$total_album}}</h2>
                                        <span></span>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="single-report">
                                <div class="s-report-inner pr--20 pt--30 mb-3">
                                    <div class="icon"><i class="fa fa-user"></i></div>
                                    <div class="s-report-title d-flex justify-content-between">
                                        <h4 class="header-title mb-0">Total Artist</h4>
                                        <p></p>
                                    </div>
                                    <div class="d-flex justify-content-between pb-2">
                                        <h2>{{$total_artist}}</h2>
                                        <span></span>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-5">
                        <div class="card">
                            <div class="card-body">
                            <h4 class="header-title">Last 10 Buy</h4>
                            <div class="data-tables">
                                    <table id="songTable" class="text-center">
                                        <thead class="bg-light text-capitalize">
                                            <tr>
                                                <th>ID</th>
                                                <th>User</th>
                                                <th>Name</th>
                                                <th>Amount</th>
                                                <th>Type</th>
                                               <th>Date</th>
                                              
                                            </tr>
                                        </thead>
                                        <tbody>
                                       
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <!-- sales report area end -->
                
               
            </div>
@endsection


@push('scripts')
<script>
$(function() {
    var baseUrl = '{!! URL::to("/") !!}';
  let me = this;
     this.tbl = $('#songTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('dashboard.getData') !!}',
        columns: [
            { data: 'no', orderable: false, bSearchable: false },
            { data: 'name', name: 'name' },
            { data: 'name_mm', name: 'name_mm' },
            { data: 'price', name: 'price', orderable: false, searchable: false },
            {data: 'type', name: 'type', orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at', orderable: false, searchable: false},
            
        ],
        "fnRowCallback" : function(nRow, aData, iDisplayIndex){
          // For auto numbering at 'No' column
          var start = me.tbl.page.info().start;
          $('td:eq(0)',nRow).html(start + iDisplayIndex + 1);
      }
        
    });

  

   $('#songTable').on('click', '.sub-delete', function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var checked = confirm("Sure You want to delete it");
    if(checked){
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type : "DELETE",
          url  : baseUrl + '/backend/songs/' + id,
          data : {},
          success: function(data){
            var result = $.parseJSON(data);
            if(result['status'] == 'success'){
                me.tbl.ajax.reload( null, false );
            }else{
                alert(result['message']);        
            }
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert('Error in student view. Please contact to administrator');
          }
        });  
    }else{
        return false;
    }
        
  });
    
});
  

</script>
@endpush