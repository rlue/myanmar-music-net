@extends('layouts.admin')
@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/multi.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
@endpush
@section('content')
    

   <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">CMS</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="{{route('cms.index')}}">CMS</a></li>
                                <li><span> Edit</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    @if (count($errors) > 0)
                                       <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                      @endif
                                    <form method="POST" action="{{route('cms.update',$cms->id)}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Update</h4>
                                        
                                    
                                      <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Title</label>
                                            <input class="form-control" type="text"  name="title" placeholder="Myanmar Name" value="{{$cms->title}}">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="product_category">Type</label>
                                       <select name="cms_type" class="form-control">
                                                <option >Select Type</option>
                                                @if($cms->cms_type == 'new')
                                                <option value="new" selected>New</option>
                                                @else
                                                <option value="new">New</option>
                                                @endif
                                                @if($cms->cms_type == 'playlist')
                                                <option value="playlist" selected>Playlist</option>
                                                @else
                                                <option value="playlist">Playlist</option>
                                                @endif
                                               @if($cms->cms_type == 'collection')
                                                <option value="collection" selected>Collection</option>
                                                @else
                                                <option value="collection">Collection</option>
                                                @endif
                                                
                                            </select>
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('song_id'))
                                          <div class="alert alert-danger">{{ $errors->first('song_id') }}</div>
                                    @endif
                                    <h4>Attracting Song * </h4>
                                        
                                     <div class="row">
                                    <div class="col-sm-5">
                                        <select  id="multiselect" class="form-control" size="8" multiple="multiple">
                                        @foreach($song as  $s)
                                            <option value="{{$s->id}}">{{$s->name_mm}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="col-sm-2">
                                        
                                        <button type="button" id="multiselect_rightSelected" class="btn btn-block"><i class="fa fa-arrow-right"></i> Add</button>
                                        <button type="button" id="multiselect_leftSelected" class="btn btn-block"><i class="fa fa-arrow-left"></i> Remove</button>
                                        
                                    </div>
                                    
                                    <div class="col-sm-5">
                                        <select name="song_id[]" id="multiselect_to" class="form-control" size="8" multiple="multiple">
                                            @foreach($cms->cms_songs as  $s)
                                            <option value="{{$s->id}}">{{$s->name_mm}}</option>
                                        @endforeach
                                        </select>
                                 
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <button type="button" id="multiselect_move_up" class="btn btn-block"><i class="fa fa-arrow-up"></i> Up</button>
                                            </div>
                                            <div class="col-sm-6">
                                                <button type="button" id="multiselect_move_down" class="btn btn-block"><i class="fa fa-arrow-down"></i> Down</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                                        
                                </div>
                                   <form action="{{ route('cms.store', $cms->id) }}"
                                    >
                                        {{ csrf_field() }}
                                        {{ method_field("patch") }}
                                         <div class="form-group">
                                            <a href="{{route('cms.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
            </div>
@endsection


@push('scripts')


<script src="{{ asset('js/multiselect.min.js') }}"></script>     
  
     <script>
            $(document).ready(function() {
              $('#multiselect').multiselect();  
            });
        </script>
   

@endpush