@extends('layouts.admin')
@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/multi.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
@endpush
@section('content')
    

   <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Exchange Rate</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="#">Exchange Rate</a></li>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                             <div class="col-8 mt-3" ></div>
                            <div class="col-4 mt-3">
                                @if (Session::has('message'))
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        {{ Session::get('message') }}
                                    </div>
                                @endif
                            </div>
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    @if (count($errors) > 0)
                                       <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                      @endif
                                    <form method="POST" action="{{route('currency.store')}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Exchange Rate</h4>
                                        
                                    <div class="form-group">
                                      <label for="example-text-input" class="col-form-label">Last Exchange Rate</label>
                                      <div>{{$last->name}}</div>
                                    </div>
                                     <div class="form-group">
                                    
                                    @if ($errors->has('name'))
                                          <div class="alert alert-danger">{{ $errors->first('name') }}</div>
                                    @endif
                                            <label for="example-text-input" class="col-form-label">Exchange Rate</label>
                                            <input class="form-control" type="text"  name="name" placeholder="Currency" value="{{$currency->name}}">
                                    </div>
                                    
                                    
                                   
                                         <div class="form-group">
                                            
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
            </div>
@endsection


@push('scripts')


<script src="{{ asset('js/multiselect.min.js') }}"></script>     
  
     <script>
            $(document).ready(function() {
              $('#multiselect').multiselect({
                search: {
    right: '<input type="text" name="q" class="form-control" placeholder="Search Album..." />',
    
},
                sort:false,
              });


            });
        </script>
   

@endpush