@extends('layouts.admin')

@section('content')
    
  <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Track Full Listen</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><span>Track Full Listen</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <!-- data table start -->
                    <div class="col-8 mt-3" ></div>
                    <div class="col-4 mt-3">
                        @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-12 mt-5">
                        <div class="card">
                            <div class="card-body">
                            
                            
                                <h4 class="header-title">Track Full Listen</h4>
                                
                                <div class="data-tables">
                                    <table id="songTable" class="text-center">
                                        <thead class="bg-light text-capitalize">
                                            <tr>
                                                <th>No</th>
                                                <th>Title</th>
                                                <!-- <th>Artists</th> -->
                                               <th>Count</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                       
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- data table end -->
                   
                </div>
            </div>

            
@endsection


@push('scripts')
<script>
$(function() {
    var baseUrl = '{!! URL::to("/") !!}';
  let me = this;
     this.tbl = $('#songTable').DataTable({
        processing: true,
        serverSide: false,
        ajax: '{!! route('tracklisten.getData') !!}',
        columns: [
            { data: 'no', orderable: false, bSearchable: false },
            { data: 'name_eng', name: 'name_eng' },
            // { data: 'artists', name: 'artists' },
            { data: 'count', name: 'count'},
            
        ],
        "fnRowCallback" : function(nRow, aData, iDisplayIndex){
          // For auto numbering at 'No' column
          var start = me.tbl.page.info().start;
          $('td:eq(0)',nRow).html(start + iDisplayIndex + 1);
      }
    });
    
});
  

</script>
@endpush