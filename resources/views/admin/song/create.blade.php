@extends('layouts.admin')
@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<style type="text/css">
    audio{
        display: none;
    }
</style>
@endpush
@section('content')
    

   <div class="page-title-area">
        <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Track</h4>
                           <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="{{route('songs.index')}}">Track</a></li>
                                <li><span> Create</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
        </div>
    </div>
            <!-- page title area end -->
    <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                              
                                    <form method="POST" action="{{route('songs.store')}}" enctype="multipart/form-data" >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Create Track</h4>
                                    
                                   
                                    <div class="form-group">
                                    @if ($errors->has('name_mm'))
                                          <div class="alert alert-danger">{{ $errors->first('name_mm') }}</div>
                                    @endif
                                            <label for="example-text-input" class="col-form-label">Myanmar Name <span class="btn_require">*</span></label>
                                            <input class="form-control" type="text"  name="name_mm" placeholder="Myanmar Name" value="{{old('name_mm')}}">
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('name_eng'))
                                          <div class="alert alert-danger">{{ $errors->first('name_eng') }}</div>
                                    @endif
                                            <label for="example-text-input" class="col-form-label">English Name <span class="btn_require">*</span></label>
                                            <input class="form-control" type="text"  name="name_eng" placeholder="English Name" value="{{old('name_eng')}}">
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('prices'))
                                          <div class="alert alert-danger">{{ $errors->first('prices') }}</div>
                                    @endif
                                        <label for="product_category">Price <span class="btn_require">*</span></label>
                                        <div class="row">
                                        <div class="col-sm-11">
                                        <select name="prices" class="form-control" id="price">
                                        <option value>Select Price</option>
                                        @foreach($price as $p)

                                          <option value="{{$p->id}}">{{$p->name}}</option>
                                        
                                        @endforeach
                                        </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="{{route('prices.create')}}" class="btn btn-primary" target="_blank"><i class="fa fa-plus"></i></a>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                     @if ($errors->has('feeling_id'))
                                          <div class="alert alert-danger">{{ $errors->first('feeling_id') }}</div>
                                    @endif
                                        <label for="product_category">Feeling</label>
                                        <div class="row">
                                        <div class="col-sm-11">
                                        <select name="feeling_id" class="form-control" id="feeling_id">
                                        <option value>Select Feeling</option>
                                        @foreach($feeling as $f)

                                          <option value="{{$f->id}}">{{$f->name}}</option>
                                        
                                        @endforeach
                                        </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="{{route('feelings.create')}}" class="btn btn-primary" target="_blank"><i class="fa fa-plus"></i></a>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('genre_id'))
                                          <div class="alert alert-danger">{{ $errors->first('genre_id') }}</div>
                                    @endif
                                        <label for="product_category">Genere <span class="btn_require">*</span></label>
                                        <div  class="row">
                                        <div class="col-sm-11">
                                        <select name="genre_id" id="genre_id" class="form-control">
                                        <option value>Select Gener</option>
                                        @foreach($songcat as $cat)

                                          <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        
                                        @endforeach
                                        </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="{{route('songcategory.create')}}" class="btn btn-primary" target="_blank"><i class="fa fa-plus"></i></a>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('artist_id'))
                                          <div class="alert alert-danger">{{ $errors->first('artist_id') }}</div>
                                    @endif
                                        <label for="artist">Main Aritst <span class="btn_require">*</span></label>
                                        <div class="row">
                                        <div class="col-sm-4">
                                    <input type="text" name="search_album" placeholder="Search Artist.." class="form-control" id="search_album" />
                                        <select  id="multiselect" class="form-control" size="8" multiple="multiple">
                                        @foreach($artist as  $a)
                                            <option value="{{$a->id}}">{{$a->artist_name_eng}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="col-sm-2">
                                        
                                        <button type="button" id="multiselect_rightSelected" class="btn btn-block"><i class="fa fa-arrow-right"></i> Add</button>
                                        <button type="button" id="multiselect_leftSelected" class="btn btn-block"><i class="fa fa-arrow-left"></i> Remove</button>
                                        
                                    </div>
                                    
                                    <div class="col-sm-5">
                                        <select name="artist_id[]" id="multiselect_to" class="form-control" size="8" multiple="multiple"></select>
                                 
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <button type="button" id="multiselect_move_up" class="btn btn-block"><i class="fa fa-arrow-up"></i> Up</button>
                                            </div>
                                            <div class="col-sm-6">
                                                <button type="button" id="multiselect_move_down" class="btn btn-block"><i class="fa fa-arrow-down"></i> Down</button>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="col-sm-1">
                                            <a href="{{route('artists.create')}}" class="btn btn-primary" target="_blank"><i class="fa fa-plus"></i></a>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('feat_id'))
                                          <div class="alert alert-danger">{{ $errors->first('feat_id') }}</div>
                                    @endif
                                        <label for="artist">Featuring Artist</label>
                                        <div class="row">
                                        <div class="col-sm-4">
                                    <input type="text" name="search_feature" placeholder="Search Featuring.." class="form-control" id="search_feature" />
                                        <select  id="featureArtist" class="form-control" size="8" multiple="multiple">
                                        @foreach($artist as  $a)
                                            <option value="{{$a->id}}">{{$a->artist_name_eng}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="col-sm-2">
                                        
                                        <button type="button" id="featureArtist_rightSelected" class="btn btn-block"><i class="fa fa-arrow-right"></i> Add</button>
                                        <button type="button" id="featureArtist_leftSelected" class="btn btn-block"><i class="fa fa-arrow-left"></i> Remove</button>
                                        
                                    </div>
                                    
                                    <div class="col-sm-5">
                                        <select name="feat_id[]" id="featureArtist_to" class="form-control" size="8" multiple="multiple"></select>
                                 
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <button type="button" id="featureArtist_move_up" class="btn btn-block"><i class="fa fa-arrow-up"></i> Up</button>
                                            </div>
                                            <div class="col-sm-6">
                                                <button type="button" id="featureArtist_move_down" class="btn btn-block"><i class="fa fa-arrow-down"></i> Down</button>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="col-sm-1">
                                            <a href="{{route('artists.create')}}" class="btn btn-primary" target="_blank"><i class="fa fa-plus"></i></a>
                                        </div>
                                        </div>
                                    </div>
                                   <!--  <div class="form-group">
                                        <label for="feat_id">Featuring Artist </label>
                                        <div class="row">
                                        <div class="col-sm-11">
                                        <select name="feat_id[]" class="form-control" id="feat_id" multiple="multiple">

                                        @foreach($artist as $art)

                                    <option value="{{$art->id}}">{{$art->artist_name_mm}}</option>
                                        
                                        @endforeach
                                        </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="{{route('artists.create')}}" class="btn btn-primary" target="_blank"><i class="fa fa-plus"></i></a>
                                        </div>
                                        </div>
                                    </div> -->
                                    <!-- <div class="form-group">
                                        
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadio12" name="add_btn" value="no" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio12">No</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadio10" name="add_btn" value="add_album" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio10">Add Album</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadio11" name="add_btn" value="add_playlist" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio11">Add Playlist</label>
                                        </div>
                                    </div> -->
                                    <hr/>
                                    <div class="form-group">
                                    @if ($errors->has('preview_file'))
                                          <div class="alert alert-danger">{{ $errors->first('preview_file') }}</div>
                                    @endif
                                    <h4>Preview File Uploading * </h4>
                                      <ul class="nav nav-tabs" id="previewFile" role="tablist">
                                        <li class="nav-item">
                                          <a class="nav-link active" id="home-tab" data-toggle="tab" href="#preview" role="tab" aria-controls="home" aria-selected="true">Upload File</a>
                                        </li>
                                        <li class="nav-item">
                                          <a class="nav-link" id="sound-tab" data-toggle="tab" href="#sound" role="tab" aria-controls="sound" aria-selected="false">Sound Cloud</a>
                                        </li>
                                        <li class="nav-item">
                                          <a class="nav-link" id="s3-tab" data-toggle="tab" href="#s3" role="tab" aria-controls="s3" aria-selected="false">S3</a>
                                        </li>
                                      </ul>
                                      <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="preview" role="tabpanel" aria-labelledby="preview-tab">
                                        <input type="hidden" name="preview_fileType" id="preview_fileType" value="preview_upload" />
                                          <input type="file" id="input-file"class="form-control" name="preview_file" >
                                        </div>
                                        <div class="tab-pane fade" id="sound" role="tabpanel" aria-labelledby="sound-tab">
                                          <input type="text" id="input-sound" class="form-control" name="preview_file" placeholder="www.soundcloud.com">
                                        </div>
                                        <div class="tab-pane fade" id="s3" role="tabpanel" aria-labelledby="s3-tab">
                                          <input type="text" id="input-s3"class="form-control" name="preview_file" placeholder="www.s3.com">
                                        </div>
                                      </div>
                                    </div>
                                    <hr/>
                                    <div class="form-group">
                                    @if ($errors->has('full_file'))
                                    <div class="alert alert-danger">{{ $errors->first('full_file') }}</div>
                                    @endif
                                    <h4>Full File Uploading * </h4>
                                    <div>
                                      <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                          <a class="nav-link active" id="fullFile-tab" data-toggle="tab" href="#fullFile" role="tab" aria-controls="fullFile" aria-selected="true">Upload File</a>
                                        </li>
                                        <li class="nav-item">
                                          <a class="nav-link" id="fullSound-tab" data-toggle="tab" href="#fullSound" role="tab" aria-controls="fullSound" aria-selected="false">Sound Cloud</a>
                                        </li>
                                        <li class="nav-item">
                                          <a class="nav-link" id="fullS3-tab" data-toggle="tab" href="#fullS3" role="tab" aria-controls="fullS3" aria-selected="false">S3</a>
                                        </li>
                                      </ul>
                                      <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="fullFile" role="tabpanel" aria-labelledby="fullFile-tab">
                                        <input type="hidden" name="full_fileType" id="full_fileType" value="full_upload" />
                                          <input type="file" id="input-fullFile" class="form-control" name="full_file">
                                        </div>
                                        <div class="tab-pane fade" id="fullSound" role="tabpanel" aria-labelledby="fullSound-tab">
                                          <input type="text" id="input-fullSound" class="form-control" name="full_file" placeholder="www.soundcloud.com">
                                        </div>
                                        <div class="tab-pane fade" id="fullS3" role="tabpanel" aria-labelledby="fullS3-tab">
                                          <input type="text" id="input-fullS3" class="form-control" name="full_file" placeholder="www.s3.com">
                                        </div>
                                        <audio id="audio" controls>
                                              <source src="horse.ogg" type="audio/ogg">
                                              <source src="" type="audio/mpeg">
                                            Your browser does not support the audio element.
                                        </audio>
                                      </div>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('duration'))
                                          <div class="alert alert-danger">{{ $errors->first('duration') }}</div>
                                    @endif
                                            <label for="example-text-input" class="col-form-label">Duration</label>
                                            <input class="form-control" type="text"  name="duration" id="duration" placeholder="Duration" value="{{old('duration')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="feat_id">Attrack Video </label>
                                        <div class="row">
                                        <div class="col-sm-11">
                                        <select name="video_id" class="form-control" id="video_id" >

                                        @foreach($video as $v)
                                    <option value="0">Select Video</option>
                                    <option value="{{$v->id}}">{{$v->name_mm}}</option>
                                        
                                        @endforeach
                                        </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="{{route('videos.create')}}" class="btn btn-primary" target="_blank"><i class="fa fa-plus"></i></a>
                                        </div>
                                        </div>
                                    </div> 
                                        <div class="form-group">
                                            <a href="{{route('songs.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
    </div>
@endsection

@push('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="{{ asset('js/select2.min.js') }}"></script> 
<script src="{{ asset('js/multiselect.min.js') }}"></script>
      <script>
            $(document).ready(function() {
              $('#multiselect').multiselect({
                search: {
    right: '<input type="text" name="q" class="form-control" placeholder="Search Artist..." />',
},
sort:false,
              });  
              $('#featureArtist').multiselect({
                search: {
    right: '<input type="text" name="q" class="form-control" placeholder="Search Artist..." />',
},
sort:false,
              });  
    
            });
         $(function () {
    opts = $('#featureArtist option').map(function () {
        return [[this.value, $(this).text()]];
    });
    

    
    var $elementList = $('#featureArtist').find('option');
  $('#search_feature').keyup(function(eve){
          searchString=$(this).val().toLowerCase();
  searchArray=searchString.split(' ');
  var len = searchArray.length;
  $elementList.each(function(index,elem){
        $eleli = $(elem)
        $eleli.removeClass('hideThisLine');
        var oneLine = $eleli.text().toLowerCase()
        match = true,
        sal = len;
        while(sal--){
          if( oneLine.indexOf( searchArray[sal] ) == -1 ){
            match = false;
          }
        }
        if(!match){
          //console.log('this one is gets hidden',searchString);
          $eleli.addClass('hideThisLine');
        }
      });
      $('.dontShow').removeClass('dontShow');
      $('.hideThisLine').addClass('dontShow');
    });
  $('#clearSearch').click(function (e){
    $('#cBuscador').val('').keyup();
  }); 
    


});
        </script>     
    <script type="text/javascript">
      $(document).ready(function(){
            
            $('#input-sound').prop('disabled', true);
            $('#input-s3').prop('disabled', true);
          $('#home-tab').click(function(){
            $('#input-file').removeAttr('disabled');
            $('#preview_fileType').val('preview_upload');
          });

          $('#sound-tab').click(function(){
            $('#input-sound').removeAttr('disabled');
            $('#input-file').prop('disabled', true);
            $('#input-s3').prop('disabled', true);
            $('#preview_fileType').val('preview_sound');
          });

          $('#s3-tab').click(function(){
            $('#input-s3').removeAttr('disabled');
            $('#input-file').prop('disabled', true);
            $('#input-sound').prop('disabled', true);
            $('#preview_fileType').val('preview_s3');
          });


            $('#input-fullSound').prop('disabled', true);
            $('#input-fullS3').prop('disabled', true);
          $('#fullFile-tab').click(function(){
            $('#input-fullFile').removeAttr('disabled');
            $('#input-fullSound').prop('disabled', true);
            $('#input-fullS3').prop('disabled', true);
            $('#full_fileType').val('full_upload');
          });

          $('#fullSound-tab').click(function(){
            $('#input-fullSound').removeAttr('disabled');
            $('#input-fullFile').prop('disabled', true);
            $('#input-fullS3').prop('disabled', true);
            $('#full_fileType').val('full_sound');
          });
          $('#fullS3-tab').click(function(){
            $('#input-fullS3').removeAttr('disabled');
            $('#input-fullSound').prop('disabled', true);
            $('#input-fullFile').prop('disabled', true);
            $('#full_fileType').val('full_s3');
          });
        //for get duration mp3 file
        var objectUrl;

    $("#audio").on("canplaythrough", function(e){
        var seconds = e.currentTarget.duration;
        var duration = moment.duration(seconds, "seconds");
        
        var time = "";
        var hours = duration.hours();
        if (hours > 0) { time = hours + ":" ; }
        
        time = time + duration.minutes() + ":" + duration.seconds();
        
        $("#duration").val(time);
        
        URL.revokeObjectURL(objectUrl);
    });

    $("#input-fullFile").change(function(e){
        var file = e.currentTarget.files[0];
        
        objectUrl = URL.createObjectURL(file);
        $("#audio").prop("src", objectUrl);
    });
    $("#input-fullSound").change(function(e){
        var file = $(this).val();
        
        $("#audio").prop("src", file);
    });
    $("#input-fullS3").change(function(e){
        var file = $(this).val();
        
        $("#audio").prop("src", file);
    });
      });
    </script>
    <script>
     $('#artist_id').select2({
      multiple: true,
      placeholder: "Choose Artist",
     });

     $('#feeling_id').select2();
     $('#genre_id').select2();
     $('#price').select2();
     $('#video_id').select2({
      placeholder: "Choose Feat",
      
     });
   </script>
   

@endpush
