@extends('layouts.admin')

@section('content')
    
  <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Track</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><span>Track</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <!-- data table start -->
                    <div class="col-8 mt-3" ></div>
                    <div class="col-4 mt-3">
                        @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-12 mt-5">
                        <div class="card">
                            <div class="card-body">
                            
                            <p class="pull-right"><a href="{{route('songs.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a></p>
                            
                                <h4 class="header-title">Track List</h4>
                                
                                <div class="data-tables">
                                    <table id="songTable" class="text-center">
                                        <thead class="bg-light text-capitalize">
                                            <tr>
                                                <th>Cover</th>
                                                <th>Title</th>
                                                <th>Artist</th>
                                                <th>Album</th>
                                                <th>Store/Streaming</th>
                                               <th>Genres</th>
                                               <th>Prices</th>
                                               <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                       
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- data table end -->
                   
                </div>
            </div>

            
@endsection


@push('scripts')
<script>
$(function() {
    
  let me = this;
     this.tbl = $('#songTable').DataTable({
        processing: true,
        //serverSide: true,
        ajax: '{!! route('songs.getData') !!}',
        columns: [
            { data: 'album_image', name: 'album_image', render: function( data, type, full, meta ) {
                        return "<img src=\"" + data + "\" width=\"100 \" height=\"50\"/>";
                    } ,orderable: false, searchable: false
            },
            { data: 'name_mm', name: 'name_mm' },
            { data: 'artists', name: 'artists'},
            { data: 'albums', name: 'albums'},
            { data: 'sub_menu', name: 'sub_menu'},
            { data: 'genre', name: 'genre'},
            { data: 'price', name: 'price'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
            
        ],
        
    });

  var baseUrl = '{!! URL::to("/") !!}';

   $('#songTable').on('click', '.sub-delete', function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var checked = confirm("Sure You want to delete it");
    if(checked){
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type : "DELETE",
          url  : baseUrl + '/backend/songs/' + id,
          data : {},
          success: function(data){
            var result = $.parseJSON(data);
            if(result['status'] == 'success'){
                me.tbl.ajax.reload( null, false );
            }else{
                alert(result['message']);        
            }
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert('Error in student view. Please contact to administrator');
          }
        });  
    }else{
        return false;
    }
        
  });
    
});
  

</script>
@endpush