@extends('layouts.admin')
@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
@endpush
@section('content')
    

   <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Track</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="{{route('songs.index')}}">Track</a></li>
                                <li><span> Edit</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    @if (count($errors) > 0)
                                       <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                      @endif
                                    <form method="POST" action="{{route('songs.update',$song->id)}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Update Track</h4>
                                        
                                    
                                      
                                    
                                    <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Myanmar Name <span class="btn_require">*</span></label>
                                            <input class="form-control" type="text"  name="name_mm" placeholder="Myanmar Name" value="{{$song->name_mm}}">
                                    </div>
                                    <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">English Name <span class="btn_require">*</span></label>
                                            <input class="form-control" type="text"  name="name_eng" placeholder="English Name" value="{{$song->name_eng}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="product_category">Price <span class="btn_require">*</span></label>
                                        <div class="row">
                                        <div class="col-sm-11">
                                        <select name="prices" class="form-control" id="price">
                                        <option value>Select Price</option>
                                        @foreach($price as $p)
                                        @if($p->id == $song->prices)
                                          <option value="{{$p->id}}" selected="selected">{{$p->name}}</option>
                                        @else
                                        <option value="{{$p->id}}">{{$p->name}}</option>
                                        @endif
                                        @endforeach
                                        </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="{{route('prices.create')}}" class="btn btn-primary" target="_blank"><i class="fa fa-plus"></i></a>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="product_category">Feeling</label>
                                        <div class="row">
                                        <div class="col-sm-11">
                                        <select name="feeling_id" class="form-control" id="feeling_id">
                                        <option value>Select Feeling</option>
                                        @foreach($feeling as $f)
                                        @if($f->id == $song->feeling_id)
                                        <option value="{{$f->id}}" selected="selected">{{$f->name}}</option>
                                        @else
                                        <option value="{{$f->id}}" >{{$f->name}}</option>
                                        @endif
                                        @endforeach
                                        </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="{{route('feelings.create')}}" class="btn btn-primary" target="_blank"><i class="fa fa-plus"></i></a>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="product_category">Genere <span class="btn_require">*</span></label>
                                        <div class="row">
                                        <div class="col-sm-11">
                                        <select name="genre_id" id="genre_id" class="form-control">
                                        <option value>Select Gener</option>
                                        @foreach($songcat as $cat)
                                        @if($cat->id == $song->genre_id)
                                          <option value="{{$cat->id}}" selected="selected">{{$cat->name}}</option>
                                        @else
                                        <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        @endif
                                        @endforeach
                                        </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="{{route('songcategory.create')}}" class="btn btn-primary" target="_blank"><i class="fa fa-plus"></i></a>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('artist_id'))
                                          <div class="alert alert-danger">{{ $errors->first('artist_id') }}</div>
                                    @endif
                                        <label for="artist">Main Aritst <span class="btn_require">*</span></label>
                                        <div class="row">
                                        <div class="col-sm-4">
                                    <input type="text" name="search_album" placeholder="Search Artist.." class="form-control" id="search_album" />
                                        <select  id="multiselect" class="form-control" size="8" multiple="multiple">
                                        @foreach($artist as  $a)
                                            <option value="{{$a->id}}">{{$a->artist_name_eng}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="col-sm-2">
                                        
                                        <button type="button" id="multiselect_rightSelected" class="btn btn-block"><i class="fa fa-arrow-right"></i> Add</button>
                                        <button type="button" id="multiselect_leftSelected" class="btn btn-block"><i class="fa fa-arrow-left"></i> Remove</button>
                                        
                                    </div>
                                    
                                    <div class="col-sm-5">
                                        <select name="artist_id[]" id="multiselect_to" class="form-control" size="8" multiple="multiple">
                                            @foreach($song->artists as  $s)
                                            <option value="{{$s->id}}">{{$s->artist_name_eng}}
                                            
                                            </option>
                                        @endforeach
                                        </select>
                                 
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <button type="button" id="multiselect_move_up" class="btn btn-block"><i class="fa fa-arrow-up"></i> Up</button>
                                            </div>
                                            <div class="col-sm-6">
                                                <button type="button" id="multiselect_move_down" class="btn btn-block"><i class="fa fa-arrow-down"></i> Down</button>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="col-sm-1">
                                            <a href="{{route('artists.create')}}" class="btn btn-primary" target="_blank"><i class="fa fa-plus"></i></a>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('feat_id'))
                                          <div class="alert alert-danger">{{ $errors->first('feat_id') }}</div>
                                    @endif
                                        <label for="artist">Featuring Artist</label>
                                        <div class="row">
                                        <div class="col-sm-4">
                                    <input type="text" name="search_feature" placeholder="Search Featuring.." class="form-control" id="search_feature" />
                                        <select  id="featureArtist" class="form-control" size="8" multiple="multiple">
                                        @foreach($artist as  $a)
                                            <option value="{{$a->id}}">{{$a->artist_name_eng}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="col-sm-2">
                                        
                                        <button type="button" id="featureArtist_rightSelected" class="btn btn-block"><i class="fa fa-arrow-right"></i> Add</button>
                                        <button type="button" id="featureArtist_leftSelected" class="btn btn-block"><i class="fa fa-arrow-left"></i> Remove</button>
                                        
                                    </div>
                                    
                                    <div class="col-sm-5">
                                        <select name="feat_id[]" id="featureArtist_to" class="form-control" size="8" multiple="multiple">
                                            @foreach($song->feats as  $s)
                                            <option value="{{$s->id}}">{{$s->artist_name_eng}}
                                            
                                            </option>
                                        @endforeach
                                        </select>
                                 
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <button type="button" id="featureArtist_move_up" class="btn btn-block"><i class="fa fa-arrow-up"></i> Up</button>
                                            </div>
                                            <div class="col-sm-6">
                                                <button type="button" id="featureArtist_move_down" class="btn btn-block"><i class="fa fa-arrow-down"></i> Down</button>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="col-sm-1">
                                            <a href="{{route('artists.create')}}" class="btn btn-primary" target="_blank"><i class="fa fa-plus"></i></a>
                                        </div>
                                        </div>
                                    </div>
                                    <!-- <div class="form-group">
                                        <label for="feat_id">Featuring Artist</label>
                                        <div class="row">
                                        <div class="col-sm-11">
                                        <select name="feat_id[]" class="form-control" id="feat_id" multiple="multiple">

                                        @foreach($artist as $art)
                                        <option value="{{$art->id}}" @foreach($song->feats as $a) @if($a->id == $art->id)selected="selected"@endif @endforeach>{{$art->artist_name_mm}}</option>
                                        @endforeach
                                        </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="{{route('artists.create')}}" class="btn btn-primary" target="_blank"><i class="fa fa-plus"></i></a>
                                        </div>
                                        </div>
                                    </div> -->
                                    <hr/>
                                    <div class="form-group">
                                    <h4>Preview File Uploading * </h4>
                                      <ul class="nav nav-tabs" id="previewFile" role="tablist">
                                        <li class="nav-item">
                                          <a class="nav-link @if($song->preview_fileType == 'preview_upload') {{'active'}} @endif" id="home-tab" data-toggle="tab" href="#preview" role="tab" aria-controls="home" aria-selected="true">Upload File</a>
                                        </li>
                                        <li class="nav-item">
                                          <a class="nav-link @if($song->preview_fileType == 'preview_sound') {{'active'}} @endif" id="sound-tab" data-toggle="tab" href="#sound" role="tab" aria-controls="sound" aria-selected="false">Sound Cloud</a>
                                        </li>
                                        <li class="nav-item">
                                          <a class="nav-link @if($song->preview_fileType == 'preview_s3') {{'active'}} @endif" id="s3-tab" data-toggle="tab" href="#s3" role="tab" aria-controls="s3" aria-selected="false">S3</a>
                                        </li>
                                      </ul>
                                      <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade @if($song->preview_fileType == 'preview_upload') {{'active show'}} @endif" id="preview" role="tabpanel" aria-labelledby="preview-tab">
                                        <input type="hidden" name="preview_fileType" id="preview_fileType" value="{{$song->preview_fileType}}" />
                                          <input type="file" id="input-file"class="form-control" name="preview_file" accept=".mp3,audio/*"  >
                                          
                                        </div>
                                        <div class="tab-pane fade @if($song->preview_fileType == 'preview_sound') {{'active show'}} @endif" id="sound" role="tabpanel" aria-labelledby="sound-tab">
                                          <input type="text" id="input-sound" class="form-control" name="preview_file" placeholder="www.soundcloud.com" value="{{$song->preview_file}}" >
                                        </div>
                                        <div class="tab-pane fade @if($song->preview_fileType == 'preview_s3') {{'active show'}} @endif" id="s3" role="tabpanel" aria-labelledby="s3-tab">
                                          <input type="text" id="input-s3"class="form-control" name="preview_file" placeholder="www.s3.com" value="{{$song->preview_file}}">
                                        </div>
                                        <audio controls>
                                              <source src="horse.ogg" type="audio/ogg">
                                              <source src="{{$song->preview_file}}" type="audio/mpeg">
                                            Your browser does not support the audio element.
                                        </audio>
                                      </div>
                                    </div>
                                    <hr/>
                                    <div class="form-group">
                                    <h4>Full File Uploading * </h4>
                                    <div>
                                      <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                          <a class="nav-link @if($song->full_fileType == 'full_upload') {{'active'}} @endif" id="fullFile-tab" data-toggle="tab" href="#fullFile" role="tab" aria-controls="fullFile" aria-selected="true">Upload File</a>
                                        </li>
                                        <li class="nav-item">
                                          <a class="nav-link @if($song->full_fileType == 'full_sound') {{'active'}} @endif" id="fullSound-tab" data-toggle="tab" href="#fullSound" role="tab" aria-controls="fullSound" aria-selected="false">Sound Cloud</a>
                                        </li>
                                        <li class="nav-item">
                                          <a class="nav-link @if($song->full_fileType == 'full_s3') {{'active'}} @endif" id="fullS3-tab" data-toggle="tab" href="#fullS3" role="tab" aria-controls="fullS3" aria-selected="false">S3</a>
                                        </li>
                                      </ul>
                                      <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade @if($song->full_fileType == 'full_upload') {{'active show'}} @endif" id="fullFile" role="tabpanel" aria-labelledby="fullFile-tab">
                                        <input type="hidden" name="full_fileType" id="full_fileType" value="{{$song->full_fileType}}" />
                                          <input type="file" id="input-fullFile" class="form-control" name="full_file">

                                        </div>
                                        <div class="tab-pane fade @if($song->full_fileType == 'full_sound') {{'active show'}} @endif"" id="fullSound" role="tabpanel" aria-labelledby="fullSound-tab">
                                          <input type="text" id="input-fullSound" class="form-control" name="full_file" placeholder="www.soundcloud.com" value="{{$song->full_file}}">
                                        </div>
                                        <div class="tab-pane fade @if($song->full_fileType == 'full_s3') {{'active show'}} @endif"" id="fullS3" role="tabpanel" aria-labelledby="fullS3-tab">
                                          <input type="text" id="input-fullS3" class="form-control" name="full_file" placeholder="www.s3.com" value="{{$song->full_file}}">
                                        </div>
                                        <audio id="audio" controls>
                                              <source src="horse.ogg" type="audio/ogg">
                                              <source src="{{$song->full_file}}" type="audio/mpeg">
                                            Your browser does not support the audio element.
                                        </audio>
                                      </div>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Duration</label>
                                            <input class="form-control" type="text"  name="duration" id="duration" placeholder="Duration" value="{{$song->duration}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="feat_id">Attrack Video </label>
                                        <div class="row">
                                        <div class="col-sm-11">
                                        <select name="video_id" class="form-control" id="video_id" >
                                        <option value="0">Select Video</option>
                                        @foreach($video as $v)
                                        @if($song->video_id == $v->id)
                                        <option value="{{$v->id}}" selected="selected">{{$v->name_mm}}</option>
                                        @else
                                        <option value="{{$v->id}}">{{$v->name_mm}}</option>
                                        @endif
                                    
                                        
                                        @endforeach
                                        </select>
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="{{route('videos.create')}}" class="btn btn-primary" target="_blank"><i class="fa fa-plus"></i></a>
                                        </div>
                                        </div>
                                    </div> 
                                       <form action="{{ route('songs.store', $song->id) }}"
                                    >
                                        {{ csrf_field() }}
                                        {{ method_field("patch") }}
                                         <div class="form-group">
                                            <a href="{{route('songs.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
            </div>
@endsection


@push('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="{{ asset('js/select2.min.js') }}"></script> 
<script src="{{ asset('js/multiselect.min.js') }}"></script>
      <script>
            $(document).ready(function() {
              $('#multiselect').multiselect({
                search: {
    right: '<input type="text" name="q" class="form-control" placeholder="Search Artist..." />',
},
sort:false,
              });  
             $('#featureArtist').multiselect({
                search: {
    right: '<input type="text" name="q" class="form-control" placeholder="Search Artist..." />',
},
sort:false,
              });   
    
            });
            $(function () {
    opts = $('#featureArtist option').map(function () {
        return [[this.value, $(this).text()]];
    });
    

    
    var $elementList = $('#featureArtist').find('option');
  $('#search_feature').keyup(function(eve){
          searchString=$(this).val().toLowerCase();
  searchArray=searchString.split(' ');
  var len = searchArray.length;
  $elementList.each(function(index,elem){
        $eleli = $(elem)
        $eleli.removeClass('hideThisLine');
        var oneLine = $eleli.text().toLowerCase()
        match = true,
        sal = len;
        while(sal--){
          if( oneLine.indexOf( searchArray[sal] ) == -1 ){
            match = false;
          }
        }
        if(!match){
          //console.log('this one is gets hidden',searchString);
          $eleli.addClass('hideThisLine');
        }
      });
      $('.dontShow').removeClass('dontShow');
      $('.hideThisLine').addClass('dontShow');
    });
  $('#clearSearch').click(function (e){
    $('#cBuscador').val('').keyup();
  }); 
    


});
        </script>         
   <script type="text/javascript">
      $(document).ready(function(){
        var previewType = "<?php echo $song->preview_fileType; ?>";
        var fullType = "<?php echo $song->full_fileType; ?>";
        if(previewType == 'preview_upload'){
          $('#input-sound').prop('disabled', true);
          $('#input-s3').prop('disabled', true);
        }
        if(previewType == 'preview_sound'){
          $('#input-file').prop('disabled', true);
          $('#input-s3').prop('disabled', true);
        }
        if(previewType == 'preview_s3'){
          $('#input-file').prop('disabled', true);
          $('#input-sound').prop('disabled', true);
        }

        if(fullType == 'full_upload'){
          $('#input-fullSound').prop('disabled', true);
          $('#input-fullS3').prop('disabled', true);
        }
        if(fullType == 'full_sound'){
          $('#input-fullFile').prop('disabled', true);
          $('#input-fullS3').prop('disabled', true);
        }
        if(fullType == 'full_s3'){
          $('#input-fullSound').prop('disabled', true);
          $('#input-fullFile').prop('disabled', true);
        }

          $('#home-tab').click(function(){
            $('#input-file').removeAttr('disabled');
            $('#input-sound').prop('disabled', true);
            $('#input-s3').prop('disabled', true);
            $('#preview_fileType').val('preview_upload');
          });
          $('#sound-tab').click(function(){
            $('#input-sound').removeAttr('disabled');
            $('#input-file').prop('disabled', true);
            $('#input-s3').prop('disabled', true);
            $('#preview_fileType').val('preview_sound');
          });
          $('#s3-tab').click(function(){
            $('#input-s3').removeAttr('disabled');
            $('#input-file').prop('disabled', true);
            $('#input-sound').prop('disabled', true);
            $('#preview_fileType').val('preview_s3');
          });
          $('#fullFile-tab').click(function(){
            $('#input-fullFile').removeAttr('disabled');
            $('#input-fullSound').prop('disabled', true);
            $('#input-fullS3').prop('disabled', true);
            $('#full_fileType').val('full_upload');
          });
          $('#fullSound-tab').click(function(){
            $('#input-fullSound').removeAttr('disabled');
            $('#input-fullFile').prop('disabled', true);
            $('#input-fullS3').prop('disabled', true);
            $('#full_fileType').val('full_sound');
          });
          $('#fullS3-tab').click(function(){
            $('#input-fullS3').removeAttr('disabled');
            $('#input-fullSound').prop('disabled', true);
            $('#input-fullFile').prop('disabled', true);
            $('#full_fileType').val('full_s3');
          });
        //for get duration song
        var objectUrl;

    $("#audio").on("canplaythrough", function(e){
        var seconds = e.currentTarget.duration;
        var duration = moment.duration(seconds, "seconds");
        
        var time = "";
        var hours = duration.hours();
        if (hours > 0) { time = hours + ":" ; }
        
        time = time + duration.minutes() + ":" + duration.seconds();
        
        $("#duration").val(time);
        
        URL.revokeObjectURL(objectUrl);
    });

    $("#input-fullFile").change(function(e){
        var file = e.currentTarget.files[0];
        
        objectUrl = URL.createObjectURL(file);
        $("#audio").prop("src", objectUrl);
    });
    $("#input-fullSound").change(function(e){
        var file = $(this).val();
        
        $("#audio").prop("src", file);
    });
    $("#input-fullS3").change(function(e){
        var file = $(this).val();
        
        $("#audio").prop("src", file);
    });
      });
    </script>
    <script>
     $('#artist_id').select2({
      multiple: true,
      placeholder: "Choose Artist",
     });

     $('#feeling_id').select2();
     $('#genre_id').select2();
     $('#price').select2();
     $('#feat_id').select2({
      multiple: true,
      placeholder: "Choose Feat",
      
     });
   </script>
   

@endpush