@extends('layouts.admin')

@section('content')
    
  <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Album</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><span>Album</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <!-- data table start -->
                    <div class="col-8 mt-3" ></div>
                    <div class="col-4 mt-3">
                        @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-12 mt-5">
                        <div class="card">
                            <div class="card-body">
                            
                            <p class="pull-right"><a href="{{route('albums.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a></p>
                            
                                <h4 class="header-title">Album List</h4>
                                
                                <div class="data-tables">
                                    <table id="albumTable" class="text-center">
                                        <thead class="bg-light text-capitalize">
                                            <tr>
                                                <th>Album Cover</th>
                                                <th>Album Title</th>
                                                <th>Artist</th>

                                                <th>Price</th>
                                                <th>Schedule Date</th>
                                                <th>Download / Streaming</th>
                                               <th>Status</th>
                                               <th>Date</th>
                                               <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                       
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- data table end -->
                   
                </div>
            </div>

            
@endsection


@push('scripts')
<script>
$(function() {
    var baseUrl = '{!! URL::to("/") !!}';
  let me = this;
     this.tbl = $('#albumTable').DataTable({
        processing: true,
        //serverSide: true,
        ajax: '{!! route('albums.getData') !!}',
        columns: [
            { data: 'album_image', name: 'album_image', render: function( data, type, full, meta ) {
                        return "<img src=\"" + data + "\" width=\"50 \" height=\"30\"/>";
                    } 
            },
            { data: 'album_name_mm', name: 'album_name_mm' },
            { data: 'artists', name: 'artists'},
            { data: 'price', name: 'price'},
            { data: 'scheduled_date', name: 'scheduled_date' },
            { data: 'sub_menu', name: 'sub_menu' },
            { data: 'status', name: 'status' },
            { data: 'created_at', name: 'created_at' },
            {data: 'action', name: 'action', orderable: false, searchable: false},
            
        ]
        
    });

  

   $('#albumTable').on('click', '.sub-delete', function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var checked = confirm("Sure You want to delete it");
    if(checked){
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type : "DELETE",
          url  : baseUrl + '/backend/albums/' + id,
          data : {},
          success: function(data){
            var result = $.parseJSON(data);
            if(result['status'] == 'success'){
                me.tbl.ajax.reload( null, false );
            }else{
                alert(result['message']);        
            }
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert('Error in student view. Please contact to administrator');
          }
        });  
    }else{
        return false;
    }
        
  });
    
});
  

</script>
@endpush