@extends('layouts.admin')
@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/multi.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
@endpush
@section('content')
    

   <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Album</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="{{route('albums.index')}}">Album</a></li>
                                <li><span> Edit</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner" id="app">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    @if (count($errors) > 0)
                                       <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                      @endif
                                    <form method="POST" action="{{route('albums.update',$album->id)}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Update Album</h4>
                                     <div class="form-group">
                                    @if ($errors->has('sub_menu'))
                                          <div class="alert alert-danger">{{ $errors->first('sub_menu') }}</div>
                                    @endif
                                      <label for="example-text-input" class="col-form-label">Sub Menu <span class="btn_require">*</span></label>
                                    </div>   
                                    <div class="form-group">
                                      <div class="custom-control custom-checkbox custom-control-inline">
                                          
                                        <input type="checkbox" name="sub_menu[store]"  class="custom-control-input" id="customCheck5" value="true" @if($album->sub_menu['store']) checked @endif>
                                          
                                                <label class="custom-control-label" for="customCheck5">Store </label>
                                            </div>
                                            <div class="custom-control custom-checkbox custom-control-inline">
                                            
                                                <input type="checkbox" name="sub_menu[streaming]"  class="custom-control-input" id="customCheck6" value="true" @if($album->sub_menu['streaming']) checked @endif>
                                            
                                                <label class="custom-control-label" for="customCheck6">Streaming</label>
                                            </div>

                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('status'))
                                          <div class="alert alert-danger">{{ $errors->first('status') }}</div>
                                    @endif
                                      <label for="example-text-input" class="col-form-label">Status <span class="btn_require">*</span></label>
                                    </div>
                                    <div class="form-group">
                                        
                                       
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadio22" name="status" value="publish" class="custom-control-input" @if($album->status == 'publish') {{"checked"}} @endif>
                                            <label class="custom-control-label" for="customRadio22">Publish</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadio33" name="status" value="unpublish" class="custom-control-input" @if($album->status == 'unpublish') {{"checked"}} @endif>
                                            <label class="custom-control-label" for="customRadio33">Unpublish</label>
                                        </div>
                                    </div>
                                      <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Myanmar Album Name <span class="btn_require">*</span></label>
                                            <input class="form-control" type="text"  name="album_name_mm" placeholder="Myanmar Name" value="{{$album->album_name_mm}}">
                                    </div>
                                    <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">English Album Name <span class="btn_require">*</span></label>
                                            <input class="form-control" type="text"  name="album_name_eng" placeholder="English Name" value="{{$album->album_name_eng}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="product_category">Price <span class="btn_require">*</span></label>
                                        <select name="album_prices" class="form-control" id="price">
                                        <option value>Select Price</option>
                                        @foreach($price as $p)
                                        @if($p->id == $album->album_prices)
                                          <option value="{{$p->id}}" selected="selected">{{$p->name}}</option>
                                        @else
                                          <option value="{{$p->id}}" >{{$p->name}}</option>
                                        @endif
                                        @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                            <label for="scheduled_date" class="col-form-label">Album Scheduled Date</label>
                                            <input class="form-control" id="datepicker" type="text"  name="scheduled_date" placeholder="YYYY-MM-DD" value="{{$album->scheduled_date}}">
                                    </div>
                                    <div class="form-group">
                                            <label for="release_date" class="col-form-label">Album Release Date <span class="btn_require">*</span></label>
                                            <input class="form-control" type="text"  name="release_date" id="release_date" placeholder="YYYY-MM-DD" value="{{$album->release_date}}">
                                    </div>
                                    <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Album CopyRight</label>
                                            <textarea name="copy_right" class="form-control">@if($album->copy_right){{$album->copy_right}} @endif</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="aggregator_id">Album Aggregator</label>
                                        <select name="aggregator_id" class="form-control">
                                         <option value>Select Aggregator</option>
                                        @foreach($aggregator as $a)
                                        @if($a->id == $album->aggregator_id)
                                          <option value="{{$a->id}}" selected="selected">{{$a->name}}</option>
                                        @else
                                        <option value="{{$a->id}}" >{{$a['name']}}</option>
                                        @endif
                                        @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                    <label for="Artwork">Album Cover Photo <span class="btn_require">*</span></label>
                                   
                                    </div>
                        
                                <div class="input-group">
                                
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                     </a>
                                   </span>
                                   <input id="thumbnail" class="form-control" type="text" name="album_image" value="{{$album->album_image}}">
                                </div>
                                <div class="form-group">
                                  @if($album->album_image)
                                    <img id="holder" style="margin-top:15px;max-height:100px;" src="{{ URL::to('/') }}{{$album->album_image}}" />
                                  @else
                                   <img id="holder" style="margin-top:15px;max-height:100px;">
                                  @endif
                                </div>
                                   <hr/>
                
                                 <div class="form-group">
                                    @if ($errors->has('song_id'))
                                          <div class="alert alert-danger">{{ $errors->first('song_id') }}</div>
                                    @endif
                                    <h4>Attracting Track * </h4>
                                        
                                     <div class="row">
                                    <div class="col-sm-5">
                                    <input type="text" name="search_album" placeholder="Search Song.." class="form-control" id="search_song" />
                                        <select  id="multiselect" class="form-control" size="8" multiple="multiple">
                                        @foreach($song as  $s)
                                            <option value="{{$s->id}}">{{$s->name_mm}} </option>
                                        @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="col-sm-2">
                                        
                                        <button type="button" id="multiselect_rightSelected" class="btn btn-block"><i class="fa fa-arrow-right"></i> Add</button>
                                        <button type="button" id="multiselect_leftSelected" class="btn btn-block"><i class="fa fa-arrow-left"></i> Remove</button>
                                        
                                    </div>
                                    
                                    <div class="col-sm-5">
                                    
                                        <select name="song_id[]" id="multiselect_to" class="form-control" size="8" multiple="multiple">
                                            @foreach($album->songs as  $s)
                                            <option value="{{$s->id}}">{{$s->name_mm}} - 
                                           
                                                @foreach($s->artists as $art)
                                                    {{$art->artist_name_eng}}
                                                @endforeach
                                            
                                            </option>
                                        @endforeach
                                        </select>
                                 
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <button type="button" id="multiselect_move_up" class="btn btn-block"><i class="fa fa-arrow-up"></i> Up</button>
                                            </div>
                                            <div class="col-sm-6">
                                                <button type="button" id="multiselect_move_down" class="btn btn-block"><i class="fa fa-arrow-down"></i> Down</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>    
                                    <hr/>
                                <div class="form-group">
                                    @if ($errors->has('artist_id'))
                                          <div class="alert alert-danger">{{ $errors->first('artist_id') }}</div>
                                    @endif
                                    <h4>Show Main Artist * </h4>
                                        
                                     <div class="row">
                                    <div class="col-sm-5">
                                    <input type="text" name="search_artist" placeholder="Search Artist.." class="form-control" id="search_artist" />
                                        <select  id="artist_multiselect" class="form-control" size="8" multiple="multiple">
                                        @foreach($artist as  $a)
                                            <option value="{{$a->id}}">{{$a->artist_name_eng}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="col-sm-2">
                                        
                                        <button type="button" id="artist_multiselect_rightSelected" class="btn btn-block"><i class="fa fa-arrow-right"></i> Add</button>
                                        <button type="button" id="artist_multiselect_leftSelected" class="btn btn-block"><i class="fa fa-arrow-left"></i> Remove</button>
                                        
                                    </div>
                                    
                                    <div class="col-sm-5">
                                        <select name="artist_id[]" id="artist_multiselect_to" class="form-control" size="8" multiple="multiple">
                                            @foreach($album->artists as  $s)
                                            <option value="{{$s->id}}">{{$s->artist_name_eng}}
                                            
                                            </option>
                                        @endforeach
                                        </select>
                                 
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <button type="button" id="artist_multiselect_move_up" class="btn btn-block"><i class="fa fa-arrow-up"></i> Up</button>
                                            </div>
                                            <div class="col-sm-6">
                                                <button type="button" id="artist_multiselect_move_down" class="btn btn-block"><i class="fa fa-arrow-down"></i> Down</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                                        
                                </div>
                                    <hr/>
                                <div class="form-group">
                                    @if ($errors->has('feat_id'))
                                          <div class="alert alert-danger">{{ $errors->first('feat_id') }}</div>
                                    @endif
                                    <h4>Show Featuring Artist  </h4>
                                        
                                     <div class="row">
                                    <div class="col-sm-5">
                                    <input type="text" name="search_feat" placeholder="Search Artist.." class="form-control" id="search_feat" />
                                        <select  id="feat_multiselect" class="form-control" size="8" multiple="multiple">
                                        @foreach($artist as  $a)
                                            <option value="{{$a->id}}">{{$a->artist_name_eng}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="col-sm-2">
                                        
                                        <button type="button" id="feat_multiselect_rightSelected" class="btn btn-block"><i class="fa fa-arrow-right"></i> Add</button>
                                        <button type="button" id="feat_multiselect_leftSelected" class="btn btn-block"><i class="fa fa-arrow-left"></i> Remove</button>
                                        
                                    </div>
                                    
                                    <div class="col-sm-5">
                                        <select name="feat_id[]" id="feat_multiselect_to" class="form-control" size="8" multiple="multiple">
                                        @foreach($album->feats as  $s)
                                            <option value="{{$s->id}}">{{$s->artist_name_eng}}
                                            
                                            </option>
                                        @endforeach    

                                        </select>
                                 
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <button type="button" id="feat_multiselect_move_up" class="btn btn-block"><i class="fa fa-arrow-up"></i> Up</button>
                                            </div>
                                            <div class="col-sm-6">
                                                <button type="button" id="feat_multiselect_move_down" class="btn btn-block"><i class="fa fa-arrow-down"></i> Down</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                                        
                                </div>
                                <!-- <div class="form-group">
                                        <h4>Show Featuring Artist  </h4>
                                        <select name="feat_id[]" class="form-control" id="feat_id" multiple="multiple">

                                        @foreach($artist as $art)

                                    <option value="{{$art->id}}" @foreach($album->feats as $f) @if($f->id == $art->id)selected="selected"@endif @endforeach>{{$art->artist_name_mm}}</option>
                                        
                                        @endforeach
                                        </select>
                                        
                                </div> -->
                                       <form action="{{ route('albums.store', $album->id) }}"
                                    >
                                        {{ csrf_field() }}
                                        {{ method_field("patch") }}
                                         <div class="form-group">
                                            <a href="{{route('albums.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
            </div>
@endsection


@push('scripts')


<script src="{{ asset('js/select2.min.js') }}"></script> 
<script src="{{ asset('js/multi.min.js') }}"></script>     
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="{{ asset('js/songsearch.js') }}"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
   <script src="{{ asset('js/multiselect.min.js') }}"></script> 
   
   <script>
            $(document).ready(function() {
              $('#multiselect').multiselect({
                search: {
    right: '<input type="text" name="q" class="form-control" placeholder="Search Song..." />',
},
sort:false,
              });  
               $('#artist_multiselect').multiselect({
                search: {
    right: '<input type="text" name="q" class="form-control" placeholder="Search Song..." />',
},
sort:false,
              }); 
               $('#feat_multiselect').multiselect({
                search: {
    right: '<input type="text" name="q" class="form-control" placeholder="Search Featuring..." />',
},
sort:false,
              });
    var $elementList = $('#artist_multiselect').find('option');
  $('#search_artist').keyup(function(eve){
          searchString=$(this).val().toLowerCase();
  searchArray=searchString.split(' ');
  var len = searchArray.length;
  $elementList.each(function(index,elem){
        $eleli = $(elem)
        $eleli.removeClass('hideThisLine');
        var oneLine = $eleli.text().toLowerCase()
        match = true,
        sal = len;
        while(sal--){
          if( oneLine.indexOf( searchArray[sal] ) == -1 ){
            match = false;
          }
        }
        if(!match){
          //console.log('this one is gets hidden',searchString);
          $eleli.addClass('hideThisLine');
        }
      });
      $('.dontShow').removeClass('dontShow');
      $('.hideThisLine').addClass('dontShow');
    });
  var $featList = $('#feat_multiselect').find('option');
  $('#search_feat').keyup(function(eve){
          searchString=$(this).val().toLowerCase();
  searchArray=searchString.split(' ');
  var len = searchArray.length;
  $featList.each(function(index,elem){
        $eleli = $(elem)
        $eleli.removeClass('hideThisLine');
        var oneLine = $eleli.text().toLowerCase()
        match = true,
        sal = len;
        while(sal--){
          if( oneLine.indexOf( searchArray[sal] ) == -1 ){
            match = false;
          }
        }
        if(!match){
          //console.log('this one is gets hidden',searchString);
          $eleli.addClass('hideThisLine');
        }
      });
      $('.dontShow').removeClass('dontShow');
      $('.hideThisLine').addClass('dontShow');
    });
            });
        </script>
     <script>
            $(document).ready(function() {
                
                $('#feat_id').multi({
                    search_placeholder: 'Search Artist...',
                });
            });
        </script>
    <script>
    $(function() {
    $("#datepicker").datetimepicker({
        format: 'Y-M-D h:m:s',
    });
    $("#release_date").datetimepicker({
        format: 'Y-M-D',
    });
  }); 

     
    
     $('#price').select2();
     
   </script>
   

@endpush