@extends('layouts.admin')
@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

@endpush
@section('content')
    

   <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Slider </h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="{{route('sliders.index')}}">Slider</a></li>
                                <li><span> Edit</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    @if (count($errors) > 0)
                                       <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                      @endif
                                    <form method="POST" action="{{route('sliders.update',$slider->id)}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Update Category</h4>
                                        
                                    
                                    <!--  <div class="form-group">
                                    @if ($errors->has('slider_type'))
                                          <div class="alert alert-danger">{{ $errors->first('slider_type') }}</div>
                                      @endif   
                                        <label for="example-text-input" class="col-form-label">Type</label>
                                    </div>
                                 <div class="form-group">
                                        
                                        
                                        <div class="custom-control custom-radio custom-control-inline">
                                        @if($slider->slider_type == 'music')
                                        <input type="radio" checked id="customRadio1" name="slider_type"  value="music" class="custom-control-input">
                                        @else
                                        <input type="radio" id="customRadio1" name="slider_type"  value="music" class="custom-control-input">
                                        @endif
                                            
                                            <label class="custom-control-label" for="customRadio1">Music</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                        @if($slider->slider_type == 'video')
                                        <input type="radio" checked id="customRadio2" name="slider_type"  value="video" class="custom-control-input">
                                        @else
                                        <input type="radio" id="customRadio2" name="slider_type"  value="video" class="custom-control-input">
                                        @endif
                                            
                                            <label class="custom-control-label" for="customRadio2">Video</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                        @if($slider->slider_type == 'news')
                                        <input type="radio" checked id="customRadio3" name="slider_type"  value="news" class="custom-control-input">
                                        @else
                                        <input type="radio" id="customRadio3" name="slider_type"  value="news" class="custom-control-input">
                                        @endif
                                    
                                            <label class="custom-control-label" for="customRadio3">News</label>
                                        </div>
                                </div>  -->
                                
                                <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Name</label>
                                            <input class="form-control" type="text"  name="name" placeholder="title" value="{{$slider->name}}">
                                </div>
                               <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Link</label>
                                            <input class="form-control" type="text"  name="slider_link" placeholder="Link" value="{{$slider->slider_link}}">
                                </div>
                                
                                <div class="form-group">
                                    @if ($errors->has('slider_image'))
                                          <div class="alert alert-danger">{{ $errors->first('slider_image') }}</div>
                                      @endif
                                    <label for="Artist Image">Slider Image</label>
                                   
                                </div>
                        
                                <div class="input-group">
                                
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                     </a>
                                   </span>
                                   <input id="thumbnail" class="form-control" type="text" name="slider_image" value="{{$slider->slider_image}}">
                                 </div>
                                 <div class="form-group">
                                 @if($slider->slider_image)
                                    <img id="holder" style="margin-top:15px;max-height:100px;" src="{{ URL::to('/') }}{{$slider->slider_image}}" />
                                  @else
                                   <img id="holder" style="margin-top:15px;max-height:100px;">
                                  @endif
                                  </div>
                                  
                                    
                                  <form action="{{ route('sliders.store', $slider->id) }}"
                                    >
                                        {{ csrf_field() }}
                                        {{ method_field("patch") }}
                                         <div class="form-group">
                                            <a href="{{route('sliders.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
            </div>
@endsection


@push('scripts')


<script src="{{ asset('js/select2.min.js') }}"></script>   
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>   
     <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
      <script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $('#news_desc').ckeditor();
        // $('.textarea').ckeditor(); // if class is prefered.
    </script>
    <script>
    $(function() {
    $("#datepicker").datepicker({
        format: 'yyyy-mm-dd',
    });
    $("#post_date").datepicker({
        format: 'yyyy-mm-dd',
    });
  }); 

     
    
     $('#news_category').select2();

     
   </script>
   

@endpush