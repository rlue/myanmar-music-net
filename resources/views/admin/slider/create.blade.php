@extends('layouts.admin')

@section('content')
    

   <div class="page-title-area">
        <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Slider</h4>
                           <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="{{route('sliders.index')}}">Slider</a></li>
                                <li><span> Create</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
        </div>
    </div>
            <!-- page title area end -->
    <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    
                                    <form method="POST" action="{{route('sliders.store')}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Create Slider </h4>
                                       <!--  <div class="form-group">
                                    @if ($errors->has('slider_type'))
                                          <div class="alert alert-danger">{{ $errors->first('slider_type') }}</div>
                                      @endif   
                                        <label for="example-text-input" class="col-form-label">Type</label>
                                    </div>
                                 <div class="form-group">
                                        
                                        
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadio1" name="slider_type" value="music" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio1">Music</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadio2" name="slider_type" value="video" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio2">Video</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio"  id="customRadio3" name="slider_type" value="news" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio3">News</label>
                                        </div>
                                </div>  -->
                                
                                <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Name</label>
                                            <input class="form-control" type="text"  name="name" placeholder="Name" value="{{old('name')}}">
                                </div>
                                <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Link</label>
                                            <input class="form-control" type="text"  name="slider_link" placeholder="Link" value="{{old('slider_link')}}">
                                </div>
                               
                                <div class="form-group">
                                    @if ($errors->has('slider_image'))
                                          <div class="alert alert-danger">{{ $errors->first('slider_image') }}</div>
                                      @endif
                                    <label for="Artist Image">Slider Image</label>
                                   
                                </div>
                        
                                <div class="input-group">
                                
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                     </a>
                                   </span>
                                   <input id="thumbnail" class="form-control" type="text" name="slider_image">
                                 </div>
                                 <div class="form-group">
                                 <img id="holder" style="margin-top:15px;max-height:100px;">
                                  </div>
                                  
                                    
                                <div class="form-group">
                                            <a href="{{route('sliders.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
    </div>
@endsection


