@extends('layouts.admin')
@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/multi.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

@endpush
@section('content')
    

   <div class="page-title-area">
        <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">{{$title}} CMS</h4>
                           <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="#">CMS</a></li>
                                <li><span> {{$title}} CMS</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
        </div>
    </div>
            <!-- page title area end -->
    <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    
                                    <form method="POST" action="{{route('cms.playlistcms.storecms')}}" enctype="multipart/form-data" >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">{{$title}} CMS</h4>
                                    
                                    
                                <input type="hidden" name="cms_type" value="{!! str_replace(' ', '_', $title) !!}">  
                                 <div class="form-group">
                                    @if ($errors->has('playlist_id'))
                                          <div class="alert alert-danger">{{ $errors->first('playlist_id') }}</div>
                                    @endif
                                    <h4>Attracting Playlists * </h4>
                                        
                                     <div class="row">
                                    <div class="col-sm-5">
                                    <input type="text" name="search_album" placeholder="Search Playlist.." class="form-control" id="search_album" />
                                        <select  id="multiselect" class="form-control" size="8" multiple="multiple">
                                        @foreach($playlist as  $s)
                                            <option value="{{$s->id}}">{{$s->playlist_name_mm}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="col-sm-2">
                                        
                                        <button type="button" id="multiselect_rightSelected" class="btn btn-block"><i class="fa fa-arrow-right"></i> Add</button>
                                        <button type="button" id="multiselect_leftSelected" class="btn btn-block"><i class="fa fa-arrow-left"></i> Remove</button>
                                        
                                    </div>
                                    
                                    <div class="col-sm-5">
                                        <select name="playlist_id[]" id="multiselect_to" class="form-control" size="8" multiple="multiple"></select>
                                 
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <button type="button" id="multiselect_move_up" class="btn btn-block"><i class="fa fa-arrow-up"></i> Up</button>
                                            </div>
                                            <div class="col-sm-6">
                                                <button type="button" id="multiselect_move_down" class="btn btn-block"><i class="fa fa-arrow-down"></i> Down</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                                        
                                </div>
                                   
                               
                                     <div class="form-group">
                                            
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
    </div>
@endsection

@push('scripts')



<script src="{{ asset('js/multiselect.min.js') }}"></script>     
  
     <script>
            $(document).ready(function() {
              $('#multiselect').multiselect({
                search: {
    
    right: '<input type="text" name="q" class="form-control" placeholder="Search Album..." />',

},
sort:false,
              });  
            });
        </script>
   
@endpush
