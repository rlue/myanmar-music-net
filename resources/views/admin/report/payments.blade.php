<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Date</th>
        <th>Amount</th>
    </tr>
    </thead>
    <tbody>
    @foreach($payment as $s)
        <tr>
            <td>{{$s->name}}</td>
            <td>{{$s->created_at}}</td>
             <td>{{ $s->amount }}</td>
        </tr>

    @endforeach
    <tr>
        <td></td>
        <td>Total</td>
        <td>{{$total}}</td>
    </tr>
    </tbody>
</table>