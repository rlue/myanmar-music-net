<table>
    <thead>
    <tr>
        <th>No</th>
        <th>Song Name</th>
        <th>Album Name</th>
        <th>Artists Name</th>
        <th>Count</th>
        <th>Date</th>
    </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach($songlisten as $s)
        <tr>
            <td>{{$i}}</td>
            <td>{{ $s->name_eng }}</td>
             <td>
            {{$s->albums->implode('album_name_mm', ', ')}}
            </td>
            <td>
            {{$s->artists->implode('artist_name_eng', ', ')}}
            </td>
            <td>{{$s->count}}</td>
            <td>{{$s->counted_at}}</td>
        </tr>
        <?php $i++; ?>
    @endforeach
    
    </tbody>
</table>