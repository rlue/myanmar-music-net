@extends('layouts.admin')
@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">

@endpush
@section('content')
    
  <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Order Song Report</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><span>Report</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <!-- data table start -->
                    <div class="col-8 mt-3" ></div>
                    <div class="col-4 mt-3">
                        @if (Session::has('message'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                    </div>
                    <div class="col-12 mt-5">
                        <div class="card">
                            <div class="card-body">
                            <form action="{{route('reports.songorder')}}" method="post">
                            {{ csrf_field() }}
                            <div class="export">
                                <div class="row">
                                    <div class="col-3">
                                        <div class="form-group">
                                        <label for="example-text-input" class="col-form-label">Name</label>
                                        
                                        <select name="user" id="user" class="form-control">
                                            <option value="0">All</option>
                                            @foreach($user as  $u)
                                            <option value="{{$u->id}}">{{$u->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group ">
                                        <label for="example-text-input" class="col-form-label">From Date</label>
                                        <input class="form-control" type="text" id="from_date"  name="from_date" placeholder="From date">
                                    </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group ">
                                        <label for="example-text-input" class="col-form-label">To Date</label>
                                        <input id="to_date" class="form-control" type="text"  name="to_date" placeholder="To Date">
                                    </div>
                                    </div>
                                    <div class="col-3">
                                        <!-- <div class="form-group ">
                                        <label for="example-text-input" class="col-form-label">Status</label>
                                        <select name="status" class="form-control">
                                        <option value="all">All</option>
                                            <option value="success">Success</option>
                                            <option value="pending">Pending</option>
                                        </select>
                                    </div> -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3"></div>
                                    <div class="col-3"></div>
                                    <div class="col-4"></div>
                                    <div class="col-2">
                                        <div class="form-group">
                                        <a href="{{route('reports.getsongorder')}}" class="btn btn-default">Cancel</a>
                                            <input type="submit" value="Export" class="btn btn-info">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                            
                                <div class="data-tables">
                                    <table id="albumTable" class="text-center">
                                        <thead class="bg-light text-capitalize">
                                            <tr>
                                                <th>No</th>
                                                <th>User Name</th>
                                                <th>Title</th>
                                                <th>Artist</th>
                                                <th>Price</th>
                                                <th>Date</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                       
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- data table end -->
                   
                </div>
            </div>

            
@endsection


@push('scripts')
<script src="{{ asset('js/select2.min.js') }}"></script>    
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script> 
<script>
$(function() {
    var baseUrl = '{!! URL::to("/") !!}';
  let me = this;
     this.tbl = $('#albumTable').DataTable({
        processing: true,
        serverSide: false,
        ajax: '{!! route('reports.getSongOrderData') !!}',
        columns: [
            { data: 'no', orderable: false, bSearchable: false },
            { data: 'name', name: 'name' },
            { data: 'name_eng', name: 'name_eng' },
            { data: 'artists', name: 'artists' },
            { data: 'price', name: 'price' },
            { data: 'created_at', name: 'created_at' },
            
        ],
        "fnRowCallback" : function(nRow, aData, iDisplayIndex){
          // For auto numbering at 'No' column
          var start = me.tbl.page.info().start;
          $('td:eq(0)',nRow).html(start + iDisplayIndex + 1);
      }
        
    });


});
  $(function() {
    $('#user').select2();
    $("#from_date").datetimepicker({
        format: 'Y-M-D',
    });
    $("#to_date").datetimepicker({
        format: 'Y-M-D',
    });
});
</script>
@endpush