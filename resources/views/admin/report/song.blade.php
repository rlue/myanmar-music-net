<table>
    <thead>
    <tr>
        <th>No</th>
        <th>User Name</th>
        <th>Song Name</th>
        <th>Album Name</th>
        <th>Artist Name</th>
        <th>Price</th>
        <th>Date</th>
    </tr>
    </thead>
    <tbody>
    <?php $i = 1; ?>
    @foreach($songs as $s)
        <tr>
        <td>{{$i}}</td>
        <td>{{$s->name}}</td>
            <td>{{ $s->name_eng }}</td>
            <td>
            {{$s->albums->implode('album_name_mm', ', ')}}
            </td>
            <td>
            {{$s->artists->implode('artist_name_eng', ', ')}}
            </td>
            
            <td>{{ $s->prices }}</td>
            <td>{{$s->created_at}}</td>
        </tr>
        <?php $i++; ?>
    @endforeach
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>Total</td>
        <td>{{$total}}</td>
        <td></td>
    </tr>
    </tbody>
</table>