<table>
    <thead>
    <tr>
        <th>No</th>
        <th>User Name</th>
        <th>Album Name</th>
        <th>Artist Name</th>
        <th>Price</th>
        <th>Date</th>
    </tr>
    </thead>
    <tbody>
    <?php $i=1; ?>
    @foreach($album as $s)
        <tr>
        <td>{{$i}}</td>
        <td>{{$s->name}}</td>
            <td>{{ $s->album_name_eng }}</td>
            <td>
            {{$s->artists->implode('artist_name_eng', ', ')}}
            </td>
            
            <td>{{ $s->prices }}</td>
            <td>{{$s->created_at}}</td>
        </tr>
        <?php $i++; ?>
    @endforeach
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td>Total</td>
        <td>{{$total}}</td>
        <td></td>
    </tr>
    </tbody>
</table>