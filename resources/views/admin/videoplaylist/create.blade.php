@extends('layouts.admin')
@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">

@endpush
@section('content')
    

   <div class="page-title-area">
        <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Video PlayList</h4>
                           <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="{{route('videoplaylists.index')}}">PlayList</a></li>
                                <li><span> Create</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
        </div>
    </div>
            <!-- page title area end -->
    <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    
                                    <form method="POST" action="{{route('videoplaylists.store')}}" enctype="multipart/form-data" >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Create Playlist</h4>
                                     <div class="form-group">
                                    @if ($errors->has('playlist_status'))
                                          <div class="alert alert-danger">{{ $errors->first('playlist_status') }}</div>
                                    @endif
                                      <label for="example-text-input" class="col-form-label">Status</label>
                                    </div>
                                    <div class="form-group">
                                        
                                       
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadio22" name="playlist_status" value="publish" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio22">Publish</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadio33" name="playlist_status" value="unpublish" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio33">Unpublish</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('playlist_name_mm'))
                                          <div class="alert alert-danger">{{ $errors->first('playlist_name_mm') }}</div>
                                    @endif
                                            <label for="example-text-input" class="col-form-label">PlayList Name(MM)</label>
                                            <input class="form-control" type="text"  name="playlist_name_mm" placeholder="Myanmar Playlist Name" value="{{old('playlist_name_mm')}}">
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('playlist_name_eng'))
                                          <div class="alert alert-danger">{{ $errors->first('playlist_name_eng') }}</div>
                                    @endif
                                            <label for="example-text-input" class="col-form-label">English PlayList Name</label>
                                            <input class="form-control" type="text"  name="playlist_name_eng" placeholder="English Playlist Name" value="{{old('playlist_name_eng')}}">
                                    </div>
                                    
                                    <div class="form-group">
                                            <label for="scheduled_date" class="col-form-label">Scheduled Date</label>
                                            <input class="form-control" id="datepicker" type="text"  name="playlist_scheduled_date" placeholder="YYYY-MM-DD" value="{{old('playlist_scheduled_date')}}">
                                    </div>
                                    <div class="form-group">
                                            <label for="release_date" class="col-form-label">Release Date</label>
                                            <input class="form-control" type="text"  name="playlist_release_date" id="release_date" placeholder="YYYY-MM-DD" value="{{old('playlist_release_date')}}">
                                    </div>
                                    <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">PlayList Description</label>
                                            <textarea name="playlist_desc" class="form-control"></textarea>
                                    </div>
                                    
                                    <div class="form-group">
                                    @if ($errors->has('playlist_image'))
                                          <div class="alert alert-danger">{{ $errors->first('playlist_image') }}</div>
                                    @endif
                                    <label for="Artwork">Cover Photo</label>
                                   
                                    </div>
                        
                                <div class="input-group">
                                
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                     </a>
                                   </span>
                                   <input id="thumbnail" class="form-control" type="text" name="playlist_image">
                                </div>
                                <div class="form-group">
                                 <img id="holder" style="margin-top:15px;max-height:100px;">
                                </div>
                                   <hr/>
                
                                <div class="form-group">
                                    @if ($errors->has('video_id'))
                                          <div class="alert alert-danger">{{ $errors->first('video_id') }}</div>
                                    @endif
                                    <h4>Attracting Video * </h4>
                                        
                                     <div class="row">
                                    <div class="col-sm-5">
                                    <input type="text" name="search_album" placeholder="Search Video.." class="form-control" id="search_album" />
                                        <select  id="multiselect" class="form-control" size="8" multiple="multiple">
                                        @foreach($video as  $s)
                                            <option value="{{$s->id}}">{{$s->name_mm}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="col-sm-2">
                                        
                                        <button type="button" id="multiselect_rightSelected" class="btn btn-block"><i class="fa fa-arrow-right"></i> Add</button>
                                        <button type="button" id="multiselect_leftSelected" class="btn btn-block"><i class="fa fa-arrow-left"></i> Remove</button>
                                        
                                    </div>
                                    
                                    <div class="col-sm-5">
                                        <select name="video_id[]" id="multiselect_to" class="form-control" size="8" multiple="multiple"></select>
                                 
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <button type="button" id="multiselect_move_up" class="btn btn-block"><i class="fa fa-arrow-up"></i> Up</button>
                                            </div>
                                            <div class="col-sm-6">
                                                <button type="button" id="multiselect_move_down" class="btn btn-block"><i class="fa fa-arrow-down"></i> Down</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>   
                                        
                                </div>
                                    <hr/>
                                
                                
                                     <div class="form-group">
                                            <a href="{{route('videoplaylists.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
    </div>
@endsection

@push('scripts')


<script src="{{ asset('js/select2.min.js') }}"></script> 
<script src="{{ asset('js/multiselect.min.js') }}"></script>   
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.min.js" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>   
     <script>
            $(document).ready(function() {
                $('#multiselect').multiselect({
                search: {
    
    right: '<input type="text" name="q" class="form-control" placeholder="Search Song..." />',
},
sort:false,
              });  
            });
        </script>
    <script>
    $(function() {
    $("#datepicker").datetimepicker({
        format: 'Y-M-D h:m:s',
    });
    $("#release_date").datetimepicker({
        format: 'Y-M-D h:m:s',
    });
  }); 

     
    
     
     
   </script>
   

@endpush
