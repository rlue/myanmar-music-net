@extends('layouts.admin')
@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

@endpush
@section('content')
    

   <div class="page-title-area">
        <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">News</h4>
                           <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="{{route('news.index')}}">News</a></li>
                                <li><span> Create</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
        </div>
    </div>
            <!-- page title area end -->
    <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    
                                    <form method="POST" action="{{route('news.store')}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Create News </h4>
                                        <div class="form-group">
                                    @if ($errors->has('news_type'))
                                          <div class="alert alert-danger">{{ $errors->first('news_type') }}</div>
                                      @endif   
                                        <label for="example-text-input" class="col-form-label">Type</label>
                                    </div>
                                 <div class="form-group">
                                        
                                        
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadio1" name="news_type" value="local_news" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio1">Local News</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadio2" name="news_type" value="intenational_news" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio2">International News</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio"  id="customRadio3" name="news_type" value="event_news" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio3">Event News</label>
                                        </div>
                                </div> 
                                <div class="form-group">
                                    @if ($errors->has('news_category'))
                                          <div class="alert alert-danger">{{ $errors->first('news_category') }}</div>
                                      @endif
                                        <label for="product_category">Genre</label>
                                        <select name="news_category" class="form-control" id="news_category">
                                        <option value>Select Genre</option>
                                        @foreach($newcat as $cat)

                                          <option value="{{$cat->id}}">{{$cat->name}}</option>
                                        }
                                        @endforeach
                                        </select>
                                    </div>  
                                <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Title</label>
                                            <input class="form-control" type="text"  name="title" placeholder="title" value="{{old('title')}}">
                                </div>
                                <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Brief Description</label>
                                            <textarea name="brief_desc" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                @if ($errors->has('news_desc'))
                                          <div class="alert alert-danger">{{ $errors->first('news_desc') }}</div>
                                      @endif
                                            <label for="example-text-input" class="col-form-label"> Description</label>
                                            <textarea name="news_desc" id="news_desc" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    @if ($errors->has('news_image'))
                                          <div class="alert alert-danger">{{ $errors->first('news_image') }}</div>
                                      @endif
                                    <label for="Artist Image">Cover Image</label>
                                   
                                </div>
                        
                                <div class="input-group">
                                
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                     </a>
                                   </span>
                                   <input id="thumbnail" class="form-control" type="text" name="news_image">
                                 </div>
                                 <div class="form-group">
                                 <img id="holder" style="margin-top:15px;max-height:100px;">
                                  </div>
                                  <div class="form-group">
                                            <label for="event_date" class="col-form-label">Event Date</label>
                                            <input class="form-control" id="datepicker" type="text"  name="event_date" placeholder="YYYY-MM-DD" value="{{old('event_date')}}">
                                    </div>
                                    <div class="form-group">
                                            <label for="post_date" class="col-form-label">Post Date</label>
                                            <input class="form-control" id="post_date" type="text"  name="post_date" placeholder="YYYY-MM-DD" value="{{old('post_date')}}">
                                    </div>
                                <div class="form-group">
                                            <a href="{{route('news.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
    </div>
@endsection


@push('scripts')


<script src="{{ asset('js/select2.min.js') }}"></script>   
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>   
     <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
      <script src="{{ asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $('#news_desc').ckeditor();
        // $('.textarea').ckeditor(); // if class is prefered.
    </script>
    <script>
    $(function() {
    $("#datepicker").datepicker({
        format: 'yyyy-mm-dd',
    });
    $("#post_date").datepicker({
        format: 'yyyy-mm-dd',
    });
  }); 

     
    
     $('#news_category').select2();

     
   </script>
   

@endpush