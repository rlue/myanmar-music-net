@extends('layouts.admin')

@section('content')
    

   <div class="page-title-area">
        <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Artist</h4>
                           <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="{{route('artists.index')}}">Artist</a></li>
                                <li><span> Create</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
        </div>
    </div>
            <!-- page title area end -->
    <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    
                                    <form method="POST" action="{{route('artists.store')}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Create Artist</h4>
                                   <!--  <div class="form-group">
                                    @if ($errors->has('artistType_id'))
                                          <div class="alert alert-danger">{{ $errors->first('artistType_id') }}</div>
                                      @endif   
                                        <label for="example-text-input" class="col-form-label">Type</label>
                                    </div> -->
                                    <!-- <div class="form-group">
                                        
                                        @foreach($type as $t)
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadio{{$t['id']}}" name="artistType_id" value="{{$t['id']}}" class="custom-control-input">
                                            <label class="custom-control-label" for="customRadio{{$t['id']}}">{{$t['name']}}</label>
                                        </div>
                                        @endforeach
                                    </div> -->
                                    <div class="form-group">
                                    @if ($errors->has('artistCategory_id'))
                                          <div class="alert alert-danger">{{ $errors->first('artistCategory_id') }}</div>
                                      @endif
                                        <label for="product_category">Artist Category <span class="btn_require">*</span></label>
                                        <select name="artistCategory_id" class="form-control">
                                        <option value>Select Artist Category</option>
                                        @foreach($category as $cat)

                                          <option value="{{$cat['id']}}">{{$cat['name']}}</option>
                                        }
                                        @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                     @if ($errors->has('artist_name_mm'))
                                          <div class="alert alert-danger">{{ $errors->first('artist_name_mm') }}</div>
                                      @endif 
                                            <label for="example-text-input" class="col-form-label">Artist Name (MM)</label>
                                            <input class="form-control" type="text"  name="artist_name_mm" placeholder="Artist Name (MM)" value="{{old('artist_name_mm')}}">
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('artist_name_eng'))
                                          <div class="alert alert-danger">{{ $errors->first('artist_name_eng') }}</div>
                                      @endif
                                            <label for="example-text-input" class="col-form-label">Artist Name (ENG) <span class="btn_require">*</span></label>
                                            <input class="form-control" type="text"  name="artist_name_eng" placeholder="Artist Name(ENG)" value="{{old('artist_name_eng')}}">
                                    </div>
                                    <div class="form-group">
                                            <label for="example-text-input" class="col-form-label">Artist Description</label>
                                            <textarea name="artist_description" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('artist_image'))
                                          <div class="alert alert-danger">{{ $errors->first('artist_image') }}</div>
                                      @endif
                                    <label for="Artist Image">Artist Image <span class="btn_require">*</span></label>
                                   
                                </div>
                        
                                <div class="input-group">
                                
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                     </a>
                                   </span>
                                   <input id="thumbnail" class="form-control" type="text" name="artist_image" value="/images/artist/artist.jpg">
                                 </div>
                                 <div class="form-group">
                                 <img id="holder" style="margin-top:15px;max-height:100px;">
                                  </div>
                                        <div class="form-group">
                                            <a href="{{route('artists.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
    </div>
@endsection


