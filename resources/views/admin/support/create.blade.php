@extends('layouts.admin')
@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/multi.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">

@endpush
@section('content')
    

   <div class="page-title-area">
        <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Support</h4>
                           <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li>Support</li>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
        </div>
    </div>
            <!-- page title area end -->
    <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    
                                    <form method="POST" action="{{route('support.store')}}" enctype="multipart/form-data" >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Supporte</h4>
                                    
                                    
                                 
                                 <div class="form-group">
                                    @if ($errors->has('description'))
                                          <div class="alert alert-danger">{{ $errors->first('description') }}</div>
                                    @endif
                                            <label for="example-text-input" class="col-form-label">Exchange Rate</label>
                                            <textarea name="description" class="form-control" id="description"></textarea>
                                    </div>
                                   
                               
                                     <div class="form-group">
                                            
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
    </div>
@endsection

@push('scripts')


<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
  
</script>
    <script>
    var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager?type=Images',
    qtBorder: '0',
    startupShowBorders: false,
    outline: false
  };
        CKEDITOR.replace('description', options);
       
        // $('.textarea').ckeditor(); // if class is prefered.
        CKEDITOR.on('dialogDefinition', function( ev )
{
   var dialogName = ev.data.name;  
   var dialogDefinition = ev.data.definition;
         
   switch (dialogName) {  
   case 'image': //Image Properties dialog      
   dialogDefinition.removeContents('Link');
   dialogDefinition.removeContents('Upload');
   dialogDefinition.removeContents('advanced');
  var infoTab = dialogDefinition.getContents( 'info' );
 
        infoTab.remove( 'txtBorder' ); //Remove Element Border From Tab Info
        infoTab.remove( 'txtHSpace' ); //Remove Element Horizontal Space From Tab Info
        infoTab.remove( 'txtVSpace' ); //Remove Element Vertical Space From Tab Info
        infoTab.remove( 'txtWidth' ); //Remove Element Width From Tab Info
        infoTab.remove( 'txtHeight' ); //Remove Element Height From Tab Info
        infoTab.remove( 'txtAlt' ); //Remove Element Height From Tab Info
        infoTab.remove( 'cmbAlign' ); //Remove Element Height From Tab Info
        infoTab.remove('ratioLock');
        infoTab.remove('previewText');
        infoTab.remove('htmlPreview');
   break;      
   case 'link': //image Properties dialog          
   dialogDefinition.removeContents('advanced');   
   break;
   }
});

    </script>
   


@endpush
