@extends('layouts.admin')
@push('styles')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css">
@endpush
@section('content')
    

   <div class="page-title-area">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Promotion</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="{{route('dashboard')}}">Home</a></li>
                                <li><a href="{{route('promotions.index')}}">Prmotion</a></li>
                                <li><span> Edit</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix">
                        <div class="user-profile pull-right">
                            @include('layouts.partial.inc')
                        </div>
                    </div>
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="row">
                    <div class="col-lg-6 col-ml-12">
                        <div class="row">
                            <!-- Textual inputs start -->
                            <div class="col-12 mt-5">
                                <div class="card">
                                    <div class="card-body">
                                    @if (count($errors) > 0)
                                       <div class="alert alert-danger">
                                        <ul>
                                          @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                          @endforeach
                                        </ul>
                                      </div>
                                      @endif
                                    <form method="POST" action="{{route('promotions.update',$promotion->id)}}"  >
                                     {{ csrf_field() }}
                                        <h4 class="header-title">Update Promotion</h4>
                                     <div class="form-group">
                                    @if ($errors->has('name'))
                                          <div class="alert alert-danger">{{ $errors->first('name') }}</div>
                                    @endif
                                            <label for="example-text-input" class="col-form-label">Name</label>
                                            <input class="form-control" type="text"  name="name" placeholder="Enter Name" value="{{$promotion->name}}">
                                    </div>
                                   <div class="form-group">
                                    @if ($errors->has('amount'))
                                          <div class="alert alert-danger">{{ $errors->first('amount') }}</div>
                                    @endif
                                            <label for="example-text-input" class="col-form-label">Amount</label>
                                            <input class="form-control" type="text"  name="amount" placeholder="Enter Amount" value="{{$promotion->amount}}">
                                    </div>
                                    <div class="form-group">
                                    @if ($errors->has('type'))
                                          <div class="alert alert-danger">{{ $errors->first('type') }}</div>
                                    @endif
                                        <label for="product_category">Type</label>
                                        <select name="type" class="form-control" id="type">
                                        <option value>Select Type</option>
                                        <option value="song" @if($promotion->type == 'song') {{'selected'}} @endif>Song</option>
                                        <option value="album" @if($promotion->type == 'album') {{'selected'}} @endif>Album</option>
                                        
                                        </select>
                                    </div>
                                    <div class="form-group">
                                            <label for="start_date" class="col-form-label">Start Date</label>
                                            <input class="form-control" id="datepicker" type="text"  name="start_date" placeholder="YYYY-MM-DD" value="{{$promotion->start_date}}">
                                    </div>
                                    <div class="form-group">
                                            <label for="end_date" class="col-form-label">End Date</label>
                                            <input class="form-control" type="text"  name="end_date" id="end_date" placeholder="YYYY-MM-DD" value="{{$promotion->end_date}}">
                                    </div>
                                     <div class="form-group">
                                    @if ($errors->has('status'))
                                          <div class="alert alert-danger">{{ $errors->first('status') }}</div>
                                    @endif
                                        <label for="product_category">Status</label>
                                        <select name="status" class="form-control" id="status">
                                        <option value>Select Status</option>
                                        <option value="active" @if($promotion->status == 'active') {{'selected'}} @endif>Active</option>
                                        <option value="inactive" @if($promotion->status == 'inactive') {{'selected'}} @endif>Inactive</option>
                                        
                                        </select>
                                    </div>
                                    <form action="{{ route('promotions.store', $promotion->id) }}"
                                    >
                                        {{ csrf_field() }}
                                        {{ method_field("patch") }}
                                         <div class="form-group">
                                            <a href="{{route('promotions.index')}}" class="btn btn-danger"><i class="fa fa-remove"></i> Cancel</a>
                                           <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </form>
                                    </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Textual inputs end -->
                           
                        </div>
                    </div>
                   
                </div>
            </div>
@endsection


@push('scripts')


<script src="{{ asset('js/select2.min.js') }}"></script> 
<script src="{{ asset('js/multi.min.js') }}"></script>     
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>   
     <script>
            $(document).ready(function() {
                $('select').multi({
                    search_placeholder: 'Search Artist...',
                });
            });
        </script>
    <script>
    $(function() {
    $("#datepicker").datepicker({
        format: 'yyyy-mm-dd',
    });
    $("#release_date").datepicker({
        format: 'yyyy-mm-dd',
    });
  }); 

     
    
     $('#price').select2();
     
   </script>
   

@endpush