
@if (Auth::guest())
                  <h2>Login</h2>
@else

    <h4 class="user-name dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->name }}<i class="fa fa-angle-down"></i></h4>
    <div class="dropdown-menu">
        <a class="dropdown-item" href="{{ route('logout') }}">Log Out</a>
    </div>
@endif