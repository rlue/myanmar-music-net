<script src="{{ asset('js/vendor/jquery-2.2.4.min.js') }}"></script>
    <!-- bootstrap 4 js -->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/metisMenu.min.js') }}"></script>
    <script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('js/jquery.slicknav.min.js')}}"></script>
   
   
    
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
    <!-- others plugins -->
    <script src="{{ asset('js/plugins.js') }}"></script>
    <script src="{{asset('js/scripts.js')}}"></script>

    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script type="text/javascript">
  

  $(document).ready(function(){
    $('#lfm').filemanager('image');
     $(function () {
    opts = $('#multiselect option').map(function () {
        return [[this.value, $(this).text()]];
    });
    
    // $('#search_album').keyup(function () {
        
    //     var rxp = new RegExp($('#search_album').val(), 'i');
    //     var optlist = $('#multiselect').empty();
    //     opts.each(function () {
    //         if (rxp.test(this[1])) {
    //             optlist.append($('<option/>').attr('value', this[0]).text(this[1]));
    //         } else{
    //             optlist.append($('<option/>').attr('value', this[0]).text(this[1]).addClass("hidden"));
    //         }
    //     });
    //     $(".hidden").toggleOption(false);
    
    // });
    
    var $elementList = $('#multiselect').find('option');
  $('#search_album').keyup(function(eve){
          searchString=$(this).val().toLowerCase();
  searchArray=searchString.split(' ');
  var len = searchArray.length;
  $elementList.each(function(index,elem){
        $eleli = $(elem)
        $eleli.removeClass('hideThisLine');
        var oneLine = $eleli.text().toLowerCase()
        match = true,
        sal = len;
        while(sal--){
          if( oneLine.indexOf( searchArray[sal] ) == -1 ){
            match = false;
          }
        }
        if(!match){
          //console.log('this one is gets hidden',searchString);
          $eleli.addClass('hideThisLine');
        }
      });
      $('.dontShow').removeClass('dontShow');
      $('.hideThisLine').addClass('dontShow');
    });
  $('#clearSearch').click(function (e){
    $('#cBuscador').val('').keyup();
  }); 
    


});

jQuery.fn.toggleOption = function( show ) {
    jQuery( this ).toggle( show );
    if( show ) {
        if( jQuery( this ).parent( 'span.toggleOption' ).length )
            jQuery( this ).unwrap( );
    } else {
        if( jQuery( this ).parent( 'span.toggleOption' ).length == 0 )
            jQuery( this ).wrap( '<span class="toggleOption" style="display: none;" />' );
    }
};    
  });
</script>