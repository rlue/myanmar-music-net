 <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo">
                    <a href="index.html">MM Musice Net</a>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">

                            <li class="@if(Request::segment(2) == 'dashboard') {{'Active'}}@endif">
                                <a href="javascript:void(0)" aria-expanded="true"><i class="ti-dashboard"></i><span>dashboard</span></a>
                                <ul class="collapse">
                                    <li class="active"><a href="{{route('dashboard')}}">  Dashboard</a></li>

                                </ul>
                            </li>
                           <li class="@if(Request::segment(2) == 'laravel-filemanager') {{'Active'}}@endif">
                                <a href="/laravel-filemanager?type=image" aria-expanded="true"><i class="ti-dashboard"></i><span>Media</span></a>
                               <!--  <ul class="collapse">
                                    <li class="active"><a href="/laravel-filemanager?type=image">  Media</a></li>

                                </ul> -->
                            </li>
                            <li class="@if(Request::segment(2) == 'users' || Request::segment(2) == 'members') {{'active'}}@endif">
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-user"></i><span>User Manage</span></a>
                                <ul class="collapse">
                                    <li class="@if(Request::segment(2) == 'users') {{'active'}}@endif"><a href="{{route('users.index')}}">User List</a></li>
                                    <li class="@if(Request::segment(2) == 'members') {{'active'}}@endif"><a href="{{route('members.index')}}">Member User List</a></li>
                                    <li class="@if(Request::segment(2) == 'bills') {{'active'}}@endif"><a href="{{route('bills.index')}}">Bill</a></li>

                                </ul>
                            </li>

                            <li class="@if(Request::segment(2) == 'artists' || Request::segment(2) == 'artistcategory' || Request::segment(2) == 'artisttype') {{'active'}}@endif">
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-group"></i><span>Artist</span></a>
                                <ul class="collapse">
                                    <li class="@if(Request::segment(2) == 'artists') {{'active'}}@endif"><a href="{{route('artists.index')}}">Artist List</a></li>
                                    <li class="@if(Request::segment(2) == 'artistcategory') {{'active'}}@endif"><a href="{{route('artistcategory.index')}}">Artist Category</a></li>
                                   <!--  <li class="@if(Request::segment(2) == 'artisttype') {{'active'}}@endif"><a href="{{route('artisttype.index')}}">Artist Type</a></li> -->

                                </ul>
                            </li>
                            <li class="@if(Request::segment(2) == 'songs' || Request::segment(2) == 'feelings' || Request::segment(2) == 'songcategory' || Request::segment(2) == 'albums' || Request::segment(2) == 'playlists' || Request::segment(2) == 'albumlists' || Request::segment(2) == 'aggregator') {{'active'}}@endif">
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-music"></i><span>Music</span></a>
                                <ul class="collapse">
                                <li class="@if(Request::segment(2) == 'songs') {{'active'}}@endif"><a href="{{route('songs.index')}}">Track</a></li>
                                <!-- <li class="@if(Request::segment(2) == 'tracklisten') {{'active'}}@endif"><a href="{{route('tracklisten')}}">Track Full Listen</a></li> -->
                                <li class="@if(Request::segment(2) == 'albums') {{'active'}}@endif"><a href="{{route('albums.index')}}">Album</a></li>
                                <li class="@if(Request::segment(2) == 'playlists') {{'active'}}@endif"><a href="{{route('playlists.index')}}">PlayList</a></li>
                                <li class="@if(Request::segment(2) == 'albumlists') {{'active'}}@endif"><a href="{{route('albumlists.index')}}">Collection Album</a></li>
                                <li class="@if(Request::segment(2) == 'songcategory') {{'active'}}@endif"><a href="{{route('songcategory.index')}}">Genre / Category</a></li>
                                <li class="@if(Request::segment(2) == 'feelings') {{'active'}}@endif"><a href="{{route('feelings.index')}}">Feelings List</a></li>
                                  <li class="@if(Request::segment(2) == 'aggregator') {{'active'}}@endif"><a href="{{route('aggregator.index')}}">Aggregator</a></li>

                                </ul>
                            </li>
                            <li class="@if(Request::segment(2) == 'videocategory' || Request::segment(2) == 'videos' || Request::segment(2) == 'videoplaylists') {{'active'}}@endif">
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-video-camera"></i><span>Video</span></a>
                                <ul class="collapse">
                                <li class="@if(Request::segment(2) == 'videos') {{'active'}}@endif"><a href="{{route('videos.index')}}">Video List</a></li>
                                <li class="@if(Request::segment(2) == 'videoplaylists') {{'active'}}@endif"><a href="{{route('videoplaylists.index')}}">Video PlayList</a></li>
                                    <li class="@if(Request::segment(2) == 'videocategory') {{'active'}}@endif"><a href="{{route('videocategory.index')}}">Video Category</a></li>



                                </ul>
                            </li>
                             <li class="@if(Request::segment(2) == 'events') {{'active'}}@endif">
                                <a href="{{route('events.index')}}" aria-expanded="true"><i class="fa fa-newspaper-o"></i><span>Events</span></a>
                                <!-- <ul class="collapse">

                                    <li class="@if(Request::segment(2) == 'events') {{'active'}}@endif"><a href="{{route('events.index')}}">Events List</a></li>


                                </ul> -->
                            </li>
                            <li class="@if(Request::segment(2) == 'support') {{'active'}}@endif">
                                <a href="{{route('support.create')}}" aria-expanded="true"><i class="fa fa-newspaper-o"></i><span>Support</span></a>
                                <!-- <ul class="collapse">

                                    <li class="@if(Request::segment(2) == 'notification') {{'active'}}@endif"><a href="{{route('notification.index')}}">Notification List</a></li>


                                </ul> -->
                            </li>
                            <li class="@if(Request::segment(2) == 'notification') {{'active'}}@endif">
                                <a href="{{route('notification.index')}}" aria-expanded="true"><i class="fa fa-newspaper-o"></i><span>Notification</span></a>
                                <!-- <ul class="collapse">

                                    <li class="@if(Request::segment(2) == 'notification') {{'active'}}@endif"><a href="{{route('notification.index')}}">Notification List</a></li>


                                </ul> -->
                            </li>
                            <li class="@if(Request::segment(2) == 'sliders') {{'active'}}@endif">
                                <a href="{{route('sliders.index')}}" aria-expanded="true"><i class="fa fa-sliders"></i><span>Slider</span></a>
                                <!-- <ul class="collapse">
                                    <li class="@if(Request::segment(2) == 'sliders') {{'active'}}@endif"><a href="{{route('sliders.index')}}">Slider List</a></li>



                                </ul> -->
                            </li>
                            <li class="@if(Request::segment(2) == 'reports') {{'active'}}@endif">
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-sliders"></i><span>Report</span></a>
                                <ul class="collapse">
                                    <li class="@if(Request::segment(2) == 'sliders') {{'active'}}@endif"><a href="{{route('reports.payment')}}">Payment Report</a></li>
                                    <li class="@if(Request::segment(2) == 'getsongorder') {{'active'}}@endif"><a href="{{route('reports.getsongorder')}}">Song Order</a></li>
                                    <li class="@if(Request::segment(2) == 'getalbumorder') {{'active'}}@endif"><a href="{{route('reports.getalbumorder')}}">Album Order</a></li>
                                     <li class="@if(Request::segment(2) == 'getsonglinsten') {{'active'}}@endif"><a href="{{route('reports.getsonglisten')}}">Song Listen</a></li>
                                    <li class="@if(Request::segment(2) == 'getsubscribe') {{'active'}}@endif"><a href="{{route('reports.getsubscribe')}}">Subscribe Report</a></li>

                                </ul>
                            </li>
                            <li class="@if(Request::segment(2) == 'cms' || Request::segment(3) == 'top' || Request::segment(3) == 'new_release' || Request::is('backend/weekly-top')) {{'active'}}@endif">
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-bars"></i><span>CMS</span></a>
                                <ul class="collapse">
                                    <!-- <li class="@if(Request::segment(2) == 'cms') {{'active'}}@endif"><a href="{{route('cms.index')}}">CMS for Song</a></li> -->
                                    <li class="@if(Request::segment(3) == 'new_release') {{'active'}}@endif"><a href="{{route('cms.albumcms','new_release')}}">New Release Album</a></li>
                                    <!-- <li class="@if(Request::segment(3) == 'top') {{'active'}}@endif"><a href="{{route('cms.albumcms','top')}}">Top Album</a></li> -->
                                    <li class="@if(Request::segment(2) == 'cms') {{'active'}}@endif"><a href="{{route('cms.playlistcms','playlists')}}">Cms for Playlist</a></li>
                                    <li class="@if(Request::segment(2) == 'cms') {{'active'}}@endif"><a href="{{route('cms.albumlistcms','collection')}}">Cms for Collection</a></li>
                                    <li class="@if(Request::segment(2) == 'cms') {{'active'}}@endif"><a href="{{route('video.videocms','new_release')}}">New Release Video</a></li>
                                    <li class="@if(Request::segment(2) == 'cms') {{'active'}}@endif"><a href="{{route('video.videocms','playlists')}}">Playlists Video</a></li>
                                        <li class="{{ Request::is('backend/weekly-top') ? 'active' : '' }}"><a href="{{route('weekly.show')}}">Weekly Top 20</a></li>
                                    <!-- <li class="@if(Request::segment(2) == 'videocms') {{'active'}}@endif"><a href="{{route('videocms.index')}}">CMS for Video</a></li> -->


                                </ul>
                            </li>
                            <li class="@if(Request::segment(2) == 'prices' || Request::segment(2) == 'albumprices' || Request::segment(2) == 'subprice'|| Request::segment(2) == 'currency') {{'active'}}@endif">
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-dollar"></i><span>Prices</span></a>
                                <ul class="collapse">
                                <li class="@if(Request::segment(2) == 'currency') {{'active'}}@endif"><a href="{{route('currency.create')}}">Exchange Rate</a></li>
                                    <li class="@if(Request::segment(2) == 'prices') {{'active'}}@endif"><a href="{{route('prices.index')}}">Price for Song</a></li>
                                    <li class="@if(Request::segment(2) == 'albumprices') {{'active'}}@endif"><a href="{{route('albumprices.index')}}">Price for Album</a></li>
                                    <li class="@if(Request::segment(2) == 'subprice') {{'active'}}@endif"><a href="{{route('subprice.index')}}">Price for Subscription</a></li>
                                    <li class="@if(Request::segment(2) == 'promotions') {{'active'}}@endif">
                                <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-paperclip"></i><span>Promotion</span></a>
                                <ul class="collapse">
                                    <li class="@if(Request::segment(2) == 'sliders') {{'active'}}@endif"><a href="{{route('promotions.index')}}">Promotion List</a></li>



                                </ul>
                            </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
