<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>MMusicNet | Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('layouts.partial.header')
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    
    <!-- preloader area end -->
    <!-- login area start -->
    <div class="login-area">
        <div class="container">
            <div class="login-box ptb--100">
                <form class="form-horizontal" method="POST" action="{{ route('backend.login') }}">
                        {{ csrf_field() }}
                    <div class="login-form-head">
                        <h4>Sign In</h4>
                        
                    </div>
                    <div class="login-form-body">
                    @if (isset($ret['msg']))
                                    <div class="alert alert-danger">
                                        <strong>{{ $ret['msg'] }}</strong>
                                    </div>
                                @endif
                        <div class="form-gp">
                         
                            
                            <input type="email" id="exampleInputEmail1" placeholder="Enter Email" name="email">
                            <i class="ti-email"></i>
                        </div>
                        <div class="form-gp">
                        
                            
                            <input type="password" id="exampleInputPassword1" placeholder="Enter Password" name="password">
                            <i class="ti-lock"></i>
                        </div>
                        <div class="row mb-4 rmber-area">
                            <div class="col-6">
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                                    <label class="custom-control-label" for="customControlAutosizing">Remember Me</label>
                                </div>
                            </div>
                            <div class="col-6 text-right">
                                <a href="#">Forgot Password?</a>
                            </div>
                        </div>
                        <div class="submit-btn-area">
                            <button id="form_submit" type="submit">Submit <i class="ti-arrow-right"></i></button>
                           <!--  <div class="login-other row mt-4">
                                <div class="col-6">
                                    <a class="fb-login" href="#">Log in with <i class="fa fa-facebook"></i></a>
                                </div>
                                <div class="col-6">
                                    <a class="google-login" href="#">Log in with <i class="fa fa-google"></i></a>
                                </div>
                            </div> -->
                        </div>
                        
                        <!-- <div class="form-footer text-center mt-5">
                            <p class="text-muted">Don't have an account? <a href="register.html">Sign up</a></p>
                        </div> -->
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- login area end -->

    @include('layouts.partial.header')
</body>

</html>