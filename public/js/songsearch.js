var baseUrl = 'https://mmusicnet.com';
  $('#search_song').keyup(function(eve){
          searchString=$(this).val().toLowerCase();

          $.ajax({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          type : "POST",
          url  : baseUrl + '/backend/songs/getsearchsong',
          data : {q:searchString},
          success: function(data){
            var result = $.parseJSON(data);
            var songlist = result['songs'];
            if(result['status'] == 'success'){
                var songa='';
              
                $.each(songlist, function(index, item) {
                    songa += "<option value='"+item.id+"'>"+item.name_mm + " (" + item.artist_name +" ) </option>";
                   
                });
                // console.log(songa);
                $('#multiselect').empty();
                $('#multiselect').append(songa);
            }else{
                $('#multiselect').empty();
                $('#multiselect').append("<option value=''>Search not found</option>");        
            }
        },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert('Error in student view. Please contact to administrator');
          }
        });

  
    });