/*--------------------- Copyright (c) 2018 -----------------------
[Master Javascript]
Project: Miraculous - Online Music Store Html Template
Version: 1.0.0
Assigned to: Theme Forest
-------------------------------------------------------------------*/
(function($) {
    "use strict";
    var music = {
        initialised: false,
        version: 1.0,
        mobile: false,
        init: function() {
            if (!this.initialised) {
                this.initialised = true;
            } else {
                return;
            }
            /*-------------- Music Functions Calling ---------------------------------------------------
            ------------------------------------------------------------------------------------------------*/
            this.RTL();
            this.Menu();
            this.Player_close();
            this.Popup();
            this.Slider();
            this.More();
            this.Nice_select();
            this.showPlayList();
            this.volume();
        },
        /*-------------- Music Functions definition ---------------------------------------------------
        ---------------------------------------------------------------------------------------------------*/
        RTL: function() {
            var rtl_attr = $("html").attr('dir');
            if (rtl_attr) {
                $('html').find('body').addClass("rtl");
            }
        },
        // Toggle Menu
        Menu: function() {
            $(".ms_nav_close").on('click', function() {
                $(".ms_sidemenu_wrapper").toggleClass('open_menu');
            });
            // on click player list
            $(".play-left-arrow").on('click', function() {
                $(".player_left").toggleClass('open_list');
            });
            //On Click Profile
            $(".ms_admin_name").on('click', function() {
                $(".pro_dropdown_menu").toggleClass("open_dropdown");
            });
        },
        // Player Close On Click
        Player_close: function() {
            $(".ms_player_close").on('click', function() {
                $(".ms_player_wrapper").toggleClass("close_player");
                $("body").toggleClass("main_class")
            })
        },
        // Pop Up
        Popup: function() {
            $('.clr_modal_btn a').on('click', function() {
                $('#clear_modal').hide();
                $('.modal-backdrop').hide();
                $('body').removeClass("modal-open").css("padding-right", "0px");
            })
            $('.hideCurrentModel').on('click', function() {
                $(this).closest('.modal-content').find('.form_close').trigger('click');
            });
            // Language Popup
            $('.lang_list').find("input[type=checkbox]").on('change', function() {
                if ($('.lang_list').find("input[type=checkbox]:checked").length) {
                    $('.ms_lang_popup .modal-content').addClass('add_lang');
                } else {
                    $('.ms_lang_popup .modal-content').removeClass('add_lang');
                }
            });
        },
        // Slider
        Slider: function() {
            var swiper = new Swiper('.swiper-container', {
                slidesPerView: 6,
                spaceBetween: 25,
                loop: false,
                speed: 1500,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                breakpoints: {
                    1800: {
                        slidesPerView: 6,
                    },
                    1400: {
                        slidesPerView: 6,
                    },
                    992: {
                        slidesPerView: 3,
                    },
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    640: {
                        slidesPerView: 2,
                        spaceBetween: 15,
                    },
                    480: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    },
                    375: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    }
                },
            });

            // Recent Slider
            var swiper = new Swiper('.ms_rcnt_slider .swiper-container', {
                slidesPerView: 6,
                spaceBetween: 25,
                loop: false,
                speed: 1500,
                navigation: {
                    nextEl: '.swiper-button-next5',
                    prevEl: '.swiper-button-prev5',
                },
                breakpoints: {
                    1800: {
                        slidesPerView: 6,
                    },
                    1400: {
                        slidesPerView: 6,
                    },
                    992: {
                        slidesPerView: 3,
                    },
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    640: {
                        slidesPerView: 2,
                        spaceBetween: 15,
                    },
                    480: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    },
                    375: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    }
                },
            });
            // Featured Slider
            var swiper = new Swiper('.ms_feature_slider.swiper-container', {
                slidesPerView: 6,
                spaceBetween: 25,
                loop: false,
                speed: 1500,
                navigation: {
                    nextEl: '.swiper-button-next1',
                    prevEl: '.swiper-button-prev1',
                },
                breakpoints: {
                    1800: {
                        slidesPerView: 6,
                    },
                    1400: {
                        slidesPerView: 6,
                    },
                    992: {
                        slidesPerView: 3,
                    },
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    640: {
                        slidesPerView: 1,
                        spaceBetween: 15,
                    },
                    480: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    },
                    375: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    }
                },
            });
            // New released Slider
            var swiper = new Swiper('.ms_release_slider.swiper-container', {
                slidesPerView: 4,
                spaceBetween: 25,
                speed: 1500,
                loop: false,
                navigation: {
                    nextEl: '.swiper-button-next2',
                    prevEl: '.swiper-button-prev2',
                },
                breakpoints: {
                    1800: {
                        slidesPerView: 6,
                    },
                    1400: {
                        slidesPerView: 6,
                    },
                    992: {
                        slidesPerView: 3,
                    },
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    640: {
                        slidesPerView: 2,
                        spaceBetween: 15,
                    },
                    480: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    },
                    375: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    }
                },
            });
            // Featured Album Slider
            var swiper = new Swiper('.ms_album_slider.swiper-container', {
                slidesPerView: 'auto',
                slidesPerColumn: 1,
                spaceBetween: 25,
                loop: false,
                speed: 1500,
                navigation: {
                    nextEl: '.swiper-button-next3',
                    prevEl: '.swiper-button-prev3',
                },
                breakpoints: {
                    1800: {
                        slidesPerView: 6,
                    },
                    1400: {
                        slidesPerView: 6,
                    },
                    992: {
                        slidesPerView: 2,
                    },
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    640: {
                        slidesPerView: 2,
                        spaceBetween: 15,
                    },
                    480: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    },
                    375: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    }
                },
            });
            // Radio Slider
            var swiper = new Swiper('.ms_radio_slider.swiper-container', {
                slidesPerView: 6,
                spaceBetween: 25,
                loop: false,
                speed: 1500,
                navigation: {
                    nextEl: '.swiper-button-next4',
                    prevEl: '.swiper-button-prev4',
                },
                breakpoints: {
                    1800: {
                        slidesPerView: 6,
                    },
                    1400: {
                        slidesPerView: 6,
                    },
                    992: {
                        slidesPerView: 2,
                    },
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    640: {
                        slidesPerView: 2,
                        spaceBetween: 15,
                    },
                    480: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    },
                    375: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    }
                },
            });
            // Collection Slider
            var swiper = new Swiper('.ms_collection_slider.swiper-container', {
                slidesPerView: 6,
                spaceBetween: 25,
                loop: false,
                speed: 1500,
                navigation: {
                    nextEl: '.swiper-button-next-collection',
                    prevEl: '.swiper-button-prev-collection',
                },
                breakpoints: {
                    1800: {
                        slidesPerView: 4,
                    },
                    1400: {
                        slidesPerView: 4,
                    },
                    992: {
                        slidesPerView: 2,
                    },
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    640: {
                        slidesPerView: 1,
                        spaceBetween: 15,
                    },
                    480: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    },
                    375: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    }
                },
            });
            // Featured Album Slider
            var swiper = new Swiper('.ms_related_album_slider.swiper-container', {
                slidesPerView: 'auto',
                slidesPerColumn: 1,
                spaceBetween: 25,
                loop: false,
                speed: 1500,
                navigation: {
                    nextEl: '.swiper-button-next-related',
                    prevEl: '.swiper-button-prev-related',
                },
                breakpoints: {
                    1800: {
                        slidesPerView: 8,
                    },
                    1400: {
                        slidesPerView: 8,
                    },
                    992: {
                        slidesPerView: 2,
                    },
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    640: {
                        slidesPerView: 2,
                        spaceBetween: 15,
                    },
                    480: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    },
                    375: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    }
                },
            });
            // Testimonial Slider
            var swiper = new Swiper('.ms_test_slider.swiper-container', {
                slidesPerView: 4,
                spaceBetween: 25,
                slidesPerColumn: 1,
                loop: false,
                speed: 1500,
                navigation: {
                    nextEl: '.swiper-button-next1',
                    prevEl: '.swiper-button-prev1',
                },
                breakpoints: {
                    1400: {
                        slidesPerView: 3,
                    },
                    992: {
                        slidesPerView: 2,
                    },
                    767: {
                        slidesPerView: 2,
                    },
                },
            });
        },
        // More
        More: function() {
            $(document).on('click', '.ms_more_icon', function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                if (typeof $(this).attr('data-other') != 'undefined') {
                    var target = $(this).parent().parent();
                } else {
                    var target = $(this).parent();
                }
                if (target.find("ul.more_option").hasClass('open_option')) {
                    target.find("ul.more_option").removeClass('open_option');
                } else {
                    $("ul.more_option.open_option").removeClass('open_option');
                    target.find("ul.more_option").addClass('open_option');
                }
            });
            $(document).on("click", function(e) {
                $("ul.more_option.open_option").removeClass("open_option");
            })
            // On Button Click
            $(".ms_btn.play_btn").on('click', function() {
                $('.ms_btn.play_btn').toggleClass('btn_pause');
            });
            $(document).on('click', '#playlist-wrap ul li .action .que_more', function(e) {
				e.preventDefault();
                e.stopImmediatePropagation();
                $('#playlist-wrap ul li .action .que_more').not($(this)).closest('li').find('.more_option').removeClass('open_option');
                $(this).closest('li').find('.more_option').toggleClass('open_option');
            });
            // $('.jp-playlist').on('click', function(){
            // $('#playlist-wrap ul li .more_option').removeClass('open_option');
            // });

            $(document).on('click', function(e) {
                if (!$(e.target).closest('.more_option').length && !$(e.target).closest('.action').length) {
                    $('#playlist-wrap .more_option').removeClass('open_option');
                }
                if (!$(e.target).closest('#playlist-wrap').length && !$(e.target).closest('.jp_queue_wrapper').length && !$(e.target).closest('.player_left').length) {
                    $('#playlist-wrap').hide();
                }
            });
            //
            $('.jp_queue_cls').on('click', function(e) {
                $('#playlist-wrap').hide();
            });

        },
        // Nice Select
        Nice_select: function() {
            if ($('.custom_select').length > 0) {
                $('.custom_select select').niceSelect();
            }
        },
        showPlayList: function() {
            $(document).on('click', '#myPlaylistQueue', function() {
                $('#playlist-wrap').fadeToggle();
            });
            $('#playlist-wrap').on('click', '#myPlaylistQueue', function(event) {
                event.stopPropagation();
            });
        },

        // Volume
        volume: function() {
            $(".knob-mask .knob").css("transform", "rotateZ(270deg)");
            $(".knob-mask .handle").css("transform", "rotateZ(270deg)");

        }

    };
    $(document).ready(function() {
        music.init();

		// Scrollbar
		$(".ms_nav_wrapper").mCustomScrollbar({
			theme:"minimal"
		});

		// Queue Scrollbar
		$(".jp_queue_list_inner").mCustomScrollbar({
			theme:"minimal",
			setHeight:345
		});
    });
    // Preloader Js
    jQuery(window).on('load', function() {
        setTimeout(function() {
            $('body').addClass('loaded');
        }, 500);
        // Li Lenght
        if ($('.jp-playlist ul li').length > 3) {
            $('.jp-playlist').addClass('find_li');
        }
    });
    // Window Scroll
    $(window).scroll(function() {
        var wh = window.innerWidth;
        //Go to top
        if ($(this).scrollTop() > 100) {
            $('.gotop').addClass('goto');
        } else {
            $('.gotop').removeClass('goto');
        }
    });
    $(".gotop").on("click", function() {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false
    });


    var song = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/search-by-song?q=%QUERY%',
            wildcard: '%QUERY%'
        },
    });

    var artist = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/search-by-artist?q=%QUERY%',
            wildcard: '%QUERY%'
        },
    });

    var album = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/search-by-album?q=%QUERY%',
            wildcard: '%QUERY%'
        },
    });

    var playlist = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/search-by-playlist?q=%QUERY%',
            wildcard: '%QUERY%'
        },
    });

    $('#search').typeahead({
            highlight: true
        },
        {
            name: 'artist',
            display: function (data) {
                if (data.artist_name_eng === null) {
                    return data.artist_name_mm  //Input value to be set when you select a suggestion.
                }

                return data.artist_name_eng  //Input value to be set when you select a suggestion.
            },
            source: artist,
            templates: {
                empty:'<h3 class="league-name">Artist</h3><div style="font-weight:normal; margin-top:10px ! important;border:0;margin-left:18px;" class="list-group-item"> Not Found</div></div>',
                header: '<h3 class="league-name">Artist</h3>',
                suggestion: function (data) {
                    if (data.artist_name_eng === null) {
                        return '<a href="/search?q=' + data.artist_name_mm + '" style="font-weight:normal; margin-top:10px ! important;border:0;" class="list-group-item"><div>' + data.artist_name_mm + '</div></div></a>'
                    }

                    return '<a href="/search?q=' + data.artist_name_eng + '" style="font-weight:normal; margin-top:10px ! important;border:0;" class="list-group-item"><div>' + data.artist_name_eng + '</div></div></a>'
                }
            }
        },
        {
            name: 'song',
            display: function (data) {
                if (data.name_eng === null) {
                    return data.name_mm  //Input value to be set when you select a suggestion.
                }

                return data.name_eng  //Input value to be set when you select a suggestion.

            },
            source: song,
            templates: {
                empty:'<h3 class="league-name">Song</h3><div style="font-weight:normal; margin-top:10px ! important;border:0;margin-left:18px;" class="list-group-item"> Not Found</div></div>',
                header: '<h3 class="league-name">Song</h3>',
                suggestion: function (data) {
                    if (data.name_eng === null) {
                        return '<a href="/search?q=' + data.name_mm + '" style="font-weight:normal; margin-top:10px ! important;border:0;" class="list-group-item"><div>' + data.name_mm + '</div></div></a>'
                    }

                    return '<a href="/search?q=' + data.name_eng + '" style="font-weight:normal; margin-top:10px ! important;border:0;" class="list-group-item"><div>' + data.name_eng + '</div></div></a>'

                }
            }
        },
        {
            name: 'album',
            display: function (data) {
                if (data.album_name_eng === null) {
                    return data.album_name_eng  //Input value to be set when you select a suggestion.
                } else {
                    return data.album_name_mm  //Input value to be set when you select a suggestion.
                }
            },
            source: album,
            templates: {
                empty:'<h3 class="league-name">Album</h3><div style="font-weight:normal; margin-top:10px ! important;border:0;margin-left:18px;" class="list-group-item"> Not Found</div></div>',
                header: '<h3 class="league-name">Album</h3>',
                suggestion: function (data) {
                    if (data.album_name_eng === null) {
                        return '<a href="/search?q=' + data.album_name_mm + '" style="font-weight:normal; margin-top:10px ! important;border:0;" class="list-group-item"><div>' + data.album_name_mm + '</div></div></a>'
                    }

                    return '<a href="/search?q=' + data.album_name_eng + '" style="font-weight:normal; margin-top:10px ! important;border:0;" class="list-group-item"><div>' + data.album_name_eng + '</div></div></a>'

                }
            }
        },
        {
        name: 'playlist',
            display: function (data) {
            if (data.playlist_name_eng === null) {
                return data.playlist_name_eng  //Input value to be set when you select a suggestion.
            } else {
                return data.playlist_name_mm  //Input value to be set when you select a suggestion.
            }
        },
        source: playlist,
            templates: {
            empty:'<h3 class="league-name">Playlist</h3><div style="font-weight:normal; margin-top:10px ! important;border:0;margin-left:18px;" class="list-group-item"> Not Found</div></div>',
                header: '<h3 class="league-name">Playlist</h3>',
                suggestion: function (data) {
                if (data.playlist_name_eng === null) {
                    return '<a href="/search?q=' + data.playlist_name_mm + '" style="font-weight:normal; margin-top:10px ! important;border:0;" class="list-group-item"><div>' + data.playlist_name_mm + '</div></div></a>'
                }

                return '<a href="/search?q=' + data.playlist_name_eng + '" style="font-weight:normal; margin-top:10px ! important;border:0;" class="list-group-item"><div>' + data.playlist_name_eng + '</div></div></a>'

            }
        }
    });

})(jQuery);

function sendFav(url, elem) {
    $.ajax({
        type: 'POST',
        url: url,
        success:function(response){
            alert(response.message);

            if (response.status === 'favoured') {
                elem.classList.add('text-danger');
            } else if(response.status === 'unfavoured') {
                elem.classList.remove('text-danger')
            }
        }
    });
}

function sendLike(url) {
    $.ajax({
        type: 'POST',
        url: url,
        success:function(response){
            like = document.getElementById("album-like");
            like.innerHTML = '<span class="fa fa-thumbs-up"></span> ' + response.status;
        }
    });
}

$buy = $('#buy');

function showBuySongModal(id) {
    $buy.data('id', id);
    $buy.attr('href', '/user/buy_song');
    $('#musicModal').modal('show');
}

function showBuyAlbumModal(id) {
    $buy.data('id', id);
    $buy.attr('href', '/user/buy_album');
    $('#musicModal').modal('show');
}

$('#download_now').click(function (e) {
    e.preventDefault();

    window.open($(this).attr('href'),"_blank");
    $('#downloadModal').modal('hide');
});

$('.song_download').on('click', function (e) {
    e.preventDefault();
    window.open($(this).attr('href'),"_blank");
});

$buy.on('click', function (e) {
    e.preventDefault();

    var id = $(this).data('id');
    var data = { id : id };
    var url = $(this).attr('href');

    var $modalUrl = $('#modalUrl');

    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success:function(response){

            $('#musicModal').modal('hide');

            if (response.status === '000') {
                $('#download_now').attr('href', '/user/download_song/' + id);
                $('#downloadModal h1').text('Buying successful, are you sure you want to download now ?');
                $('#downloadModal').modal('show');
                $('#redeem_btn').text('Redeem (Top Up) - ' + response.remained_amount + ' (MMK)')

            }else if (response.status === '100') {
                $('#download_album_btn').attr('href', response.path);
                $('#downloadAlbumModal h1').text('Buying successful, do you want to go Album Download page now ?');
                $('#downloadAlbumModal').modal('show');
                $('#redeem_btn').text('Redeem (Top Up) - ' + response.remained_amount + ' (MMK)')
            } else {
                $('#modalHeader').text(response.message);
                $modalUrl.hide();
                $('#mmModal').modal('show');
            }

        },
        error:function (xhr, textStatus, errorThrown) {

            $('#musicModal').modal('hide');

            if(xhr.responseJSON.status === '003') {
                $('#modalHeader').text(xhr.responseJSON.message);
                $modalUrl.attr('href', '/user/payment');
                $modalUrl.show();
                $('#mmModal').modal('show');
            } else if(xhr.responseJSON.status === '002')  {
                $('#downloadModal h1').text(xhr.responseJSON.message);
                $('#download_now').attr('href', '/user/download_song/' + id);
                $('#downloadModal').modal('show');
            } else if(xhr.responseJSON.status === '102')  {
                $('#downloadAlbumModal h1').text(xhr.responseJSON.message);
                $('#download_album_btn').attr('href', xhr.responseJSON.path);
                $('#downloadAlbumModal').modal('show');
            } else {
                $('#modalHeader').text(xhr.responseJSON.message);
                $modalUrl.hide();
                $('#mmModal').modal('show');
            }
        }
    })
});

if (navigator.userAgent.match(/android/i)) {
    document.getElementById('play-store-link').href = 'market://details?id=com.mmusicnet.mmusicnet';
}
