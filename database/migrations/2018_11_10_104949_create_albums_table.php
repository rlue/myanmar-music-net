<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlbumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('albums', function (Blueprint $table) {
            $table->increments('id');
            $table->string('album_name_mm')->nullable();
            $table->string('album_name_eng')->nullable();
            $table->string('album_prices');
            $table->dateTime('scheduled_date')->nullable();
            $table->dateTime('release_date')->nullable();
            $table->longText('copy_right')->nullable();
            $table->string('album_image');
            $table->timestamps();
        });

        Schema::create('album_songs', function (Blueprint $table) {
            $table->integer('album_id');
            $table->integer('song_id');
        });
        Schema::create('album_artists', function (Blueprint $table) {
            $table->integer('album_id');
            $table->integer('artist_id');
        });
        Schema::create('album_feat', function (Blueprint $table) {
            $table->integer('album_id');
            $table->integer('artist_id');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('albums');
        Schema::dropIfExists('album_songs');
        Schema::dropIfExists('album_artists');
        Schema::dropIfExists('album_feat');
    }
}
