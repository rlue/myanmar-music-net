<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlbumlistCmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('albumlist_cms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cms_type');
            $table->timestamps();
        });
        Schema::create('cms_albumlists', function (Blueprint $table) {
            $table->integer('cms_id');
            $table->integer('albumlist_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('albumlist_cms');
        Schema::dropIfExists('cms_albumlists');
    }
}
