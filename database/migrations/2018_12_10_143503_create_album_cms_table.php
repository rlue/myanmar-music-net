<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlbumCmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('album_cms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cms_type');
            $table->timestamps();
        });
        Schema::create('cms_albums', function (Blueprint $table) {
            $table->integer('cms_id');
            $table->integer('album_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('album_cms');
        Schema::dropIfExists('cms_albums');
    }
}
