<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlbumListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('album_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('albumlist_name_mm')->nullable();
            $table->string('albumlist_name_eng')->nullable();
            $table->dateTime('albumlist_scheduled_date')->nullable();
            $table->dateTime('albumlist_release_date')->nullable();
            $table->longText('albumlist_desc')->nullable();
            $table->string('albumlist_image');
            $table->timestamps();
        });

        Schema::create('albumlist_albums', function (Blueprint $table) {
            $table->integer('albumlist_id');
            $table->integer('album_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('album_lists');
        Schema::dropIfExists('albumlist_albums');
    }
}
