<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('songs', function (Blueprint $table) {
            $table->increments('id');
            $table->jsonb('sub_menu');
            $table->string('name_mm');
            $table->string('name_eng');
            $table->string('prices');
            $table->integer('genre_id');
            $table->integer('feeling_id');
            $table->string('preview_fileType');
            $table->longText('preview_file')->nullable();
            $table->string('full_fileType');
            $table->longText('full_file')->nullable();
            $table->timestamps();
        });
        

        Schema::create('song_artists', function (Blueprint $table) {
            $table->integer('song_id');
            $table->integer('artist_id');
        });
        Schema::create('song_feat', function (Blueprint $table) {
            $table->integer('song_id');
            $table->integer('artist_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('songs');
        Schema::dropIfExists('song_artists');
        Schema::dropIfExists('song_feat');
    }
}
