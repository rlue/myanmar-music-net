<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaylistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('playlists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('playlist_name_mm')->nullable();
            $table->string('playlist_name_eng')->nullable();
            $table->dateTime('playlist_scheduled_date')->nullable();
            $table->dateTime('playlist_release_date')->nullable();
            $table->longText('playlist_desc')->nullable();
            $table->string('playlist_image');
            $table->timestamps();
        });
        Schema::create('playlist_songs', function (Blueprint $table) {
            $table->integer('playlist_id');
            $table->integer('song_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('playlists');
        Schema::dropIfExists('playlist_songs');
    }
}
