<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddedColumnToSongTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('songs', function (Blueprint $table) {
            $table->string('name_eng')->nullable()->change();
            $table->string('sub_menu')->nullable()->change();
            $table->string('feeling_id')->nullable()->change();
            $table->string('duration')->nullable()->change();
            $table->string('preview_fileType')->nullable()->change();
            $table->text('preview_file')->nullable()->change();
            $table->string('full_fileType')->nullable()->change();
            $table->text('full_file')->nullable()->change();
            $table->text('full_lowFile')->nullable()->after('full_file');
            $table->string('full_lowFileType')->nullable()->after('full_file');
        });

        Schema::table('albums', function (Blueprint $table) {
            $table->string('name_eng')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('songs', function (Blueprint $table) {
            $table->string('name_eng')->change();
            $table->dropColumn('full_lowFileType');
            $table->dropColumn('full_lowFile');
        });
    }
}
