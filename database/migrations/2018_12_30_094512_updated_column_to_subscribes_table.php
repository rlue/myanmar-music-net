<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatedColumnToSubscribesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::table('subscribes', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');

            $table->timestamp('ends_at')->nullable()->after('subscribe_price_id');
            $table->timestamp('trial_ends_at')->nullable()->after('subscribe_price_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscribes', function (Blueprint $table) {
            //
        });
    }
}
