<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoPlaylistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_playlists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('playlist_status')->nullable();
            $table->string('playlist_name_mm')->nullable();
            $table->string('playlist_name_eng')->nullable();
            $table->dateTime('playlist_scheduled_date')->nullable();
            $table->dateTime('playlist_release_date')->nullable();
            $table->longText('playlist_desc')->nullable();
            $table->string('playlist_image')->nullable();
            $table->timestamps();
        });
        Schema::create('playlist_videos', function (Blueprint $table) {
            $table->integer('playlist_id');
            $table->integer('video_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_playlists');
        Schema::dropIfExists('playlist_videos');
    }
}
