<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaylistCmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('playlist_cms', function (Blueprint $table) {
             $table->increments('id');
            $table->string('cms_type');
            $table->timestamps();
        });
        Schema::create('cms_playlists', function (Blueprint $table) {
            $table->integer('cms_id');
            $table->integer('playlist_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('playlist_cms');
        Schema::dropIfExists('cms_playlists');
    }
}
