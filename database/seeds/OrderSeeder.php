<?php

use Illuminate\Database\Seeder;
use App\Order;
use Carbon\Carbon;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order_json = File::get(public_path('json/order.json'));

        $orders = json_decode($order_json);

        foreach ($orders as $old_order) {

            $order = Order::firstOrNew(['id' => $old_order->id]);

            $item_id = $old_order->item_id;

            if ($old_order->type === 'album') {
                $album = \App\OldAlbum::where('id',$old_order->item_id)->first();

                if (is_null($album)) {
                    $this->command->info($old_order->id);
                    continue;
                }

                $item_id = $album->main_id;
            }

            $order->user_id = $old_order->user_id;
            $order->item_id = $item_id;
            $order->type = $old_order->type;
            $order->created_at = Carbon::parse($old_order->bought_at);
            $order->updated_at = Carbon::parse($old_order->bought_at);
            $order->save();
        }
    }
}
