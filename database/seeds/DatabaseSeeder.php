<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SliderSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(PriceSeeder::class);
        $this->call(ArtistSeeder::class);
        $this->call(FeelingSeeder::class);
        $this->call(SongGenreSeeder::class);

        if (env('APP_ENV') !== 'production') {
            $this->call(DemoSeeder::class);
        }
    }
}
