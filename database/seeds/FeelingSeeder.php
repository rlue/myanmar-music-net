<?php

use Illuminate\Database\Seeder;
use App\Feeling;

class FeelingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $feeling_json        = File::get(public_path('json/feelings.json'));

        $feelings = json_decode($feeling_json);

        foreach ($feelings as $old_feeling) {

            $file_name = uploadImageFromUrl($old_feeling->art_work, '/photos/1/feelings/');

            $feeling = Feeling::firstOrNew(['id' => $old_feeling->id]);

            $feeling->name = $old_feeling->feeling;
            $feeling->feeling_image = $file_name;
            $feeling->save();
        }

    }
}
