<?php

use Illuminate\Database\Seeder;
use App\Artist;
use App\ArtistType;
use App\ArtistCategory;

class ArtistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $artist_types = [
            ['name' => 'Artist'],
            ['name' => 'Actor']
        ];

        foreach ($artist_types as $artist_type) {
            $type = ArtistType::firstOrNew(['name' => $artist_type['name']]);

            if ($type->wasRecentlyCreated) {
                continue;
            }

            $type->save();
        }

        $artist_categories = [
            ['name' => 'Male Artist'],
            ['name' => 'Female Artist']
        ];

        foreach ($artist_categories as $artist_category) {
            $category = ArtistCategory::firstOrNew(['name' => $artist_category['name']]);

            if ($category->wasRecentlyCreated) {
                continue;
            }

            $category->save();
        }


//        $artist_json        = File::get(public_path('json/artist.json'));
//
//        $artists = json_decode($artist_json);
//
//        foreach ($artists as $old_artist) {
//
//            $artist = Artist::firstOrNew(['id' => $old_artist->id]);
//
//            $type_id = $old_artist->status === 'artist' ? 1 : 2;
//
//            if ($old_artist->genre_id == 38) {
//                $category_id = 1;
//                $default_image = '/images/artist/male_artist.jpg';
//            }else {
//                $category_id = 2;
//                $default_image = '/images/artist/female_artist.jpg';
//            }
//
//            $file_name = uploadImageFromUrl($old_artist->photo, '/photos/1/artists/', $default_image);
//
//            $artist->fill([
//                'artist_name_mm' => $old_artist->name,
//                'artist_name_eng' => $old_artist->ename,
//                'artist_description' => null,
//                'artistType_id' => $type_id,
//                'artistCategory_id' => $category_id,
//                'artist_image' => $file_name,
//            ]);
//
//            $artist->save();
//
//            $this->command->info($old_artist->id);
//        }

    }
}
