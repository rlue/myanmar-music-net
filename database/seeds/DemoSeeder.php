<?php

use Illuminate\Database\Seeder;
use App\Song;
use App\Album;
use App\AlbumCms;
use App\Aggregator;

class DemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $song_data = [
			"name_mm"=> "ေမွ်ာ္ႏိုင္သမွ်",
			"name_eng"=> "Myaw Naing Tha Mya",
			"prices"=> "3",
			"genre_id"=> 1,
			"feeling_id"=> 10,
			"preview_fileType"=> "preview_s3",
			"preview_file"=> "https://s3-ap-southeast-1.amazonaws.com/mmn01/Music/Phyoe+Pyae+Sone_%E1%80%B1%E1%80%99%E1%80%BD%E1%80%BA%E1%80%AC%E1%80%B9%E1%82%8F%E1%80%AD%E1%80%AF%E1%80%84%E1%80%B9%E1%80%9E%E1%80%99%E1%80%BD%E1%80%BA/01+Myaw+Naing+Tha+Mya+(%E1%80%B1%E1%80%99%E1%80%BD%E1%80%BA%E1%80%AC%E1%80%B9%E1%82%8F%E1%80%AD%E1%80%AF%E1%80%84%E1%80%B9%E1%80%9E%E1%80%99%E1%80%BD%E1%80%BA)+Pre.mp3",
			"full_fileType"=> "full_s3",
			"full_file"=> "https://s3-ap-southeast-1.amazonaws.com/mmn01/Music/Phyoe+Pyae+Sone_%E1%80%B1%E1%80%99%E1%80%BD%E1%80%BA%E1%80%AC%E1%80%B9%E1%82%8F%E1%80%AD%E1%80%AF%E1%80%84%E1%80%B9%E1%80%9E%E1%80%99%E1%80%BD%E1%80%BA/01+Myaw+Naing+Tha+Mya+(%E1%80%B1%E1%80%99%E1%80%BD%E1%80%BA%E1%80%AC%E1%80%B9%E1%82%8F%E1%80%AD%E1%80%AF%E1%80%84%E1%80%B9%E1%80%9E%E1%80%99%E1%80%BD%E1%80%BA)+Pre.mp3",
			"duration"=> "1:00",
        ];

        $song = Song::create($song_data);

        $song->artists()->sync([4]);

        $aggreator = Aggregator::create([ 'name' => 'Testing' ]);

        $album_data = [
			"album_name_mm" => "ေမွ်ာ္ႏိုင္သမွ်",
			"album_name_eng" => "Myaw Naing Tha Mya",
			"album_prices" => "17",
			"scheduled_date" => "2018-12-01",
			"release_date" => "2018-12-01",
			"copy_right" => null,
			"album_image" => "https://mmusicnet.com/album_photos/1151105554PhyoPyaeSone500x500(1).jpg",
			"aggregator_id" => $aggreator->id,
			"sub_menu" => [ 'store' => true, 'streaming' => true],
        ];

        $album = Album::create($album_data);

        $album->songs()->sync([$song->id]);

        $album->artists()->sync([4]);

        $cms = [
            [ 'title' => 'New Album', 'cms_type' => 'new'],
            [ 'title' => 'Top Playlist', 'cms_type' => 'playlist'],
            [ 'title' => 'Top Album', 'cms_type' => 'collection'],
        ];

        foreach ($cms as $item) {
            $album_cms = AlbumCms::create($item);
            $album_cms->cms_albums()->sync([$album->id]);
        }


    }
}
