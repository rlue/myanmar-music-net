<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;
use App\OldSubscribe;
use App\Subscribe;
use Carbon\Carbon;
use App\Social;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

//        $role = Role::firstOrNew(['name' => 'admin']);
//
//        $role->display_name = 'super admin';
//        $role->description = 'Normal User';
//        $role->save();
//
//
//        $role = Role::firstOrNew(['name' => 'normal']);
//
//        $role->display_name = 'normal user';
//        $role->description = 'description';
//        $role->save();

        $user_json        = File::get(public_path('json/user.json'));

        $users = json_decode($user_json);

        foreach ($users as $old_user) {

            if (is_null($old_user->facebook_id)) {

                $userCheck = User::where('email', $old_user->email)->first();

                if (is_null($userCheck)) {
                    $newSocialUser                    = new User;
                    $newSocialUser->email             = is_null($old_user->email) ? null : $old_user->email;;
                    $newSocialUser->name              = $old_user->name;
                    $newSocialUser->phone             = is_null($old_user->ph) ? null : $old_user->ph;
                    $newSocialUser->email_verified_at = is_null($old_user->created_at) ? Carbon::now() : $old_user->created_at;;
                    $newSocialUser->password          = $old_user->password;
                    $newSocialUser->remained_amount   = $old_user->remained_amount;
                    $newSocialUser->save();

                    $newSocialUser->attachRole('normal');

                    $socialUser = $newSocialUser;
                }else {
                    $socialUser = $userCheck;
                }



            }else {
                $socialUser = null;

                // check is this email present
                if($old_user->email == null){
                    $userCheck = null;
                }else{
                    $userCheck = User::where('email', $old_user->email)->first();
                }

                $email     = $old_user->email;

                if (!$old_user->email) {
                    $email = null;
                }

                $sameSocialId = Social::where('social_id', '=', $old_user->facebook_id)->where('provider', 'facebook' )->first();

                if (!empty($userCheck)) {

                    $socialUser = $userCheck;

                    if (empty($sameSocialId)) {
                        $socialData = new Social;
                        $socialData->user_id   = $socialUser->id;
                        $socialData->social_id = $old_user->facebook_id;
                        $socialData->provider  = '';
                        $socialData->save();
                    }

                } else {
                    // new user
                    if (empty($sameSocialId)) {

                        $newSocialUser                    = new User;
                        $newSocialUser->email             = $email;
                        $newSocialUser->name              = $old_user->name;
                        $newSocialUser->phone             = is_null($old_user->ph) ? null : $old_user->ph;
                        $newSocialUser->email_verified_at = is_null($old_user->created_at) ? Carbon::now() : $old_user->created_at;;
                        $newSocialUser->password          = $old_user->password;
                        $newSocialUser->remained_amount = $old_user->remained_amount;
                        $newSocialUser->save();

                        $newSocialUser->attachRole('normal');

                        $socialData            = new Social;
                        $socialData->user_id   = $newSocialUser->id;
                        $socialData->social_id = $old_user->facebook_id;
                        $socialData->provider  = 'facebook';
                        $socialData->save();

                        $socialUser = $newSocialUser;

                        $newSocialUser->addTrialSubscribe();

                    } else {

                        //Load this existing social user
                        $socialUser = $sameSocialId->user;
                        $socialUser->email = $email;
                        $socialUser->save();
                    }

                }
            }

            $days = Carbon::parse($socialUser->email_verified_at)->addDays(30);

            $subscribe = Subscribe::where('user_id', $socialUser->id)->first();

            if (is_null($subscribe)) {
                Subscribe::create([
                    'user_id' => $socialUser->id,
                    'subscribe_price_id' => 1,
                    'trial_ends_at' => $days,
                ]);
            }

            $this->command->info($old_user->id);
        }


//        $user = User::firstOrNew(['email' => 'admin@admin.com']);
//
//        if (!$user->wasRecentlyCreated) {
//            $user->name = 'admin';
//            $user->email_verified_at = Carbon::now();
//            $user->password = bcrypt('password');
//            $user->save();
//
//            $user->roles()->sync(1);
//        }
//
//
//        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->command->info('done users');

    }
}
