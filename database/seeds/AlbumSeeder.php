<?php

use Illuminate\Database\Seeder;
use App\Album;

class AlbumSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $album_json        = File::get(public_path('json/albums.json'));

        $albums = json_decode($album_json);

        foreach ($albums as $old_album) {

            $album = Album::firstOrNew(['id' => $old_album->main_id]);

            $sub_menu = [ 'store' => true, 'streaming' =>  true , 'free' => false];

            if ($old_album->price_id === 4 || $old_album->price_id === 8) {
                $sub_menu = [ 'streaming' => false, 'store' => false, 'free' => true ];
            }

            if ($old_album->price_id === 12) {
                $sub_menu = [ 'streaming' => true, 'store' => false , 'free' => false];
            }


            $album->sub_menu = $sub_menu;
            $album->save();

            $this->command->info($old_album->main_id);
        }
    }
}
