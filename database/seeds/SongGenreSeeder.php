<?php

use Illuminate\Database\Seeder;
use App\SongCategory;

class SongGenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genre_json = File::get(public_path('json/genre.json'));

        $genres = json_decode($genre_json);

        foreach ($genres as $old_genre) {

            $file_name = uploadImageFromUrl($old_genre->art_work, '/photos/1/genres/');

            $genre = SongCategory::firstOrNew(['id' => $old_genre->id]);
            $genre->name = $old_genre->information;
            $genre->songCategory_image = $file_name;
            $genre->save();

        }
    }
}
