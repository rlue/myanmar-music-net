<?php

use Illuminate\Database\Seeder;
use App\Slider;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $slider_json = File::get(public_path('json/slider.json'));

        $sliders = json_decode($slider_json);

        foreach ($sliders as $old_slider) {
            if ($old_slider->category !== 'movie') {

                $file_name = uploadImageFromUrl($old_slider->image, '/photos/1/slider/');

                $slider = Slider::firstOrNew(['id' => $old_slider->id]);

                $slider->name = "";
                $slider->slider_type = $old_slider->category;
                $slider->slider_image = $file_name;
                $slider->save();
            }
        }
    }
}
