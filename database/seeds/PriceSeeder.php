<?php

use Illuminate\Database\Seeder;
use App\Price;
use App\AlbumPrice;

class PriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $price_json        = File::get(public_path('json/price.json'));

        $prices = json_decode($price_json);

        foreach ($prices as $price) {
            Price::create([
                'id' => $price->id,
                'name' => $price->price,
            ]);

            AlbumPrice::create([
                'id' => $price->id,
                'name' => $price->price,
            ]);

        }
    }
}
