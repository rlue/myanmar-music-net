<?php

use Illuminate\Database\Seeder;
use App\Song;
use App\SongFile;

class SongSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $albums = \App\Album::all();

        foreach ($albums as $album) {
            $sub_menu = $album->sub_menu;
            foreach ($album->songs as $song) {
                $song->sub_menu = $sub_menu;
                $song->save();

                $this->command->info($song->id);
            }
        }

//        $songs = Song::whereNull('sub_menu')->get();
//
//        foreach ($songs as $song) {
//            $song->sub_menu = [ 'streaming' => false, 'store' => false, 'free' => true ];
//            $song->save();
//        }


    }


    private function save($json) {
        $songs = json_decode($json);

        foreach ($songs as $old_song) {

            $this->command->info($old_song->id);

            $song = Song::firstOrNew(['id' => $old_song->id]);

            $song->name_mm  = $old_song->name;
            $song->name_eng = $old_song->ename;
            $song->prices   = $old_song->price;
            $song->genre_id = $old_song->genre_id;
            $song->feeling_id = $old_song->feeling_id;
            $song->duration = $old_song->duration;

            if ($old_song->trailer_status !== 'upload') {
                $song->preview_fileType = "preview_" . $old_song->trailer_status;
                $song->preview_file = $old_song->trailer;
            }

            $files = SongFile::where('item_id', $old_song->id)->get();

            $store = $files->where('ext', 'store')->first();

            if (!is_null($store) && $store->status !== 'upload') {
                $song->full_fileType = "full_" . $store->status;
                $song->full_file = $store->file_name;
            }

            $low_file = $files->where('file_quality', 96)->first();

            if (!is_null($low_file) && $store->status !== 'upload') {
                $song->full_lowFileType = "low_" . $store->status;
                $song->full_lowFile = $store->file_name;
            }

            $song->save();

            $artist_ids = \App\OldSongArtist::where('song_id', $old_song->id)->pluck('artist_id');

            $song->artists()->sync($artist_ids);

            $album_ids = \App\OldSongAlbum::where('song_id', $old_song->id)->pluck('album_id');

            $song->albums()->sync($album_ids);
        }
    }
}
