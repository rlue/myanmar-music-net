<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Staudenmeir\EloquentEagerLimit\HasEagerLimit;

class VideoCategory extends Model
{
    use HasEagerLimit;

     protected $fillable = [
        'name'
    ];

     public function videos () {
         return $this->hasMany(Video::class,'video_category','id');
     }
}
