<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Request;
use Session;

class ArtistView extends Model
{
    protected $fillable = [
        'artist_id', 'view'
    ];

    public $timestamps = false;

    public $incrementing = false;

    public $primaryKey = 'artist_id';

    public static function track($artist_id){
        if (!Session::has('artist_track') || Session::get('artist_track') !== Request::path()) {

            ArtistView::firstOrCreate(['artist_id' => $artist_id])->increment('view');

            Session::put('artist_track', Request::path());

            return ['message' => 'saved'];

        }else {
            return ['message' => Session::get('artist_track')];
        }

    }
}
