<?php

namespace App;

use App\Payment\Subscribable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use HasApiTokens;
    use LaratrustUserTrait;
    use Subscribable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'email_verified_at', 'remained_amount', 'streaming_quality'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function recent_songs() {
        return $this->belongsToMany(Song::class, 'recents',
            'user_id', 'song_id');
    }

    public function songs() {
        return $this->belongsToMany(Song::class, 'orders','user_id', 'item_id')
                    ->where('type', 'song')->withTimestamps();
    }

    public function albums() {
        return $this->belongsToMany(Album::class, 'orders','user_id', 'item_id')
            ->where('type', 'album')->withTimestamps();
    }

    public function favourite_songs() {
        return $this->belongsToMany(Song::class, 'favourites',
            'user_id', 'item_id')->where('type', 'song')->withTimestamps();
    }

    public function favourite_albums() {
        return $this->belongsToMany(Album::class, 'favourites',
            'user_id', 'item_id')->where('type', 'album')->withTimestamps();
    }

    public function like_albums() {
        return $this->belongsToMany(Album::class, 'album_likes',
            'user_id', 'album_id')->withTimestamps();
    }


    public function comments() 
    {
        return $this->hasMany(Comment::class);
    }
}
