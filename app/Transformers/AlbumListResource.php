<?php

namespace App\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class AlbumListResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name_mm' => $this->albumlist_name_mm,
            'name_eng' => $this->albumlist_name_eng,
            'image' => $this->albumlist_image,
            'album' => AlbumResource::collection($this->albums),
            'scheduled_date' => $this->albumlist_scheduled_date,
            'release_date' => $this->albumlist_release_date,
            'description' => $this->albumlist_desc,
            
           
        ];
    }
}
