<?php

namespace App\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class SubPriceResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'num_day'   => $this->num_day,
            
           
        ];
    }
}
