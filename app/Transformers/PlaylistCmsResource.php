<?php

namespace App\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class PlaylistCmsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->cms_type,
            'playlist' => PlayListResource::collection($this->cms_playlists),
           
        ];
    }
}
