<?php

namespace App\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class EventResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->event_image,
            'location' => $this->location,
            'co_host' => $this->co_host,
            'event_detail' => $this->event_detail,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            
           
        ];
    }
}
