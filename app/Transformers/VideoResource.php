<?php

namespace App\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class VideoResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name_mm' => $this->name_mm,
            'name_eng' => $this->name_eng,
            'description' => $this->video_desc,
            'category' => ($this->video_category) ? ['id' => $this->category->id,'name' => $this->category->name]:null,
            'video_link' => $this->video_link,
            'artist' => ArtistResource::collection($this->artists),
           
        ];
    }
}
