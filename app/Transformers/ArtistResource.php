<?php

namespace App\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class ArtistResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'artist_name_mm' => $this->artist_name_mm,
            'artist_name_eng' => $this->artist_name_eng,
            'artist_description' => $this->artist_description,
            'artist_image' => $this->artist_image,
            'category' => ($this->category) ? ['id' => $this->category->id,'name' => $this->category->name]:null,
            'type' => ($this->type) ? ['id' => $this->type->id,'name' => $this->type->name]:null,
           
        ];
    }
}
