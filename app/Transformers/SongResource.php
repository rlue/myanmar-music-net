<?php

namespace App\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\PlayCount;
class SongResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $count =PlayCount::select('click_count')->where('song_id','=',$this->id)->first();
        if(!$count){
            $count['click_count'] = 0;
        }
        $album = $this->albums->first();
        if(isset($album->id)){
            $albumid = $album->id;
            $albumimage =$album->album_image;
        }else{
            $albumid = null;
            $albumimage = null;
        }
        return [
            'id' => $this->id,
            'name_mm' => $this->name_mm,
            'name_eng' => $this->name_eng,
            'prices' => ($this->price) ? ['id' => $this->price->id,'name' => $this->price->name]:null,
            'category' => ($this->genre) ? ['id' => $this->genre->id,'name' => $this->genre->name]:null,
            'feeling' => ($this->feeling) ? ['id' => $this->feeling->id,'name' => $this->feeling->name]:null,
            'artist' => ArtistResource::collection($this->artists),
            'feat' => ArtistResource::collection($this->feats),
            'preview_fileType' => $this->preview_fileType,
            'preview_file' => $this->preview_file,
            'full_fileType' => $this->full_fileType,
            'full_file' => $this->full_file,
            'full_lowFileType' => $this->full_lowFileType,
            'full_lowFile'  => $this->full_lowFile,
            'duration' => $this->duration,
            'click_count' =>$this->getPlayCount(),
            'album_id'  => $albumid,
            'album_image' =>$albumimage
            
           
        ];
    }
}
