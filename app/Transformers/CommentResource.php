<?php

namespace App\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class CommentResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'comment_body' => $this->comment_body,
            'comment_parent' => $this->comment_parent,
            'user' => UserResource::collection($this->user_id),
            'song' => $this->song_id
            
           
        ];
    }
}
