<?php

namespace App\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class PlayListResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name_mm' => $this->playlist_name_mm,
            'name_eng' => $this->playlist_name_eng,
            'image' => $this->playlist_image,
            'song' => SongResource::collection($this->songs),
            'scheduled_date' => $this->playlist_scheduled_date,
            'release_date' => $this->playlist_release_date,
            'description' => $this->playlist_desc,
            'status'    => $this->status,
            
           
        ];
    }
}
