<?php

namespace App\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class SliderResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
             'link' => $this->slider_link,
            'image' => $this->slider_image,
           
        ];
    }
}
