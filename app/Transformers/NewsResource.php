<?php

namespace App\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class NewsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'category' => ($this->category) ? ['id' => $this->category->id,'name' => $this->category->name]:null,
            'type' => $this->news_type,
            'image' => $this->news_image,
            'brief_desc' => $this->brief_desc,
            'news_desc' => $this->news_desc,
            'event_date' => $this->event_date,
            'post_date' => $this->post_date,
            
           
        ];
    }
}
