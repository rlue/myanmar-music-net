<?php

namespace App\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class AlbumlistCmsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->cms_type,
            'collection' => AlbumListResource::collection($this->cms_albumlists),
           
        ];
    }
}
