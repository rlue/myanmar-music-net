<?php

namespace App\Transformers;

use Illuminate\Http\Resources\Json\Resource;

class AlbumResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name_mm' => $this->album_name_mm,
            'name_eng' => $this->album_name_eng,
            'prices' => $this->album_prices,
            'image' => $this->album_image,
            'song' => SongResource::collection($this->songs),
            'artist' => ArtistResource::collection($this->artists),
            'feat' => ArtistResource::collection($this->feats),
            'scheduled_date' => $this->scheduled_date,
            'release_date' => $this->release_date,
            'copy_right' => $this->copy_right,
            'status'    => $this->status,
            
           
        ];
    }
}
