<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoCms extends Model
{
    //
    protected $table = 'video_cms';

    protected $fillable = [
        'cms_type'
    ];

    public function cms_videos()
    {
      return $this->belongsToMany(Video::class,'cms_videos','cms_id','video_id');  
    }
}
