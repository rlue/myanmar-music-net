<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feeling extends Model
{
    //
    protected $fillable = [
        'name','feeling_image'
    ];
}
