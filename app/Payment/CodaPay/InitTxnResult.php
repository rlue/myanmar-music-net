<?php

namespace App\Payment\CodaPay;

/**
 * Class InitTxnResult
 *
 *
 * @property string txnId
 * @property string resultDesc
 * @property string resultCode
 * @property string profile
 */
class InitTxnResult {
    /**
     * @return mixed
     */
    public function getTxnId()
    {
        return $this->txnId;
    }

    /**
     * @param mixed $txnId
     * @return InitTxnResult
     */
    public function setTxnId($txnId)
    {
        $this->txnId = $txnId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResultDesc()
    {
        return $this->resultDesc;
    }

    /**
     * @param mixed $resultDesc
     * @return InitTxnResult
     */
    public function setResultDesc($resultDesc)
    {
        $this->resultDesc = $resultDesc;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResultCode()
    {
        return $this->resultCode;
    }

    /**
     * @param mixed $resultCode
     * @return InitTxnResult
     */
    public function setResultCode($resultCode)
    {
        $this->resultCode = $resultCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param mixed $profile
     * @return InitTxnResult
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
        return $this;
    }


}

?>
