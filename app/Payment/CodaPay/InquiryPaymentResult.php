<?php

namespace App\Payment\CodaPay;

/**
 * Class InitTxnRequest
 *
 *
 * @property string orderId
 * @property string resultCode
 * @property string resultDesc
 * @property string totalPrice
 * @property string txnId
 */
class InquiryPaymentResult {

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     * @return InquiryPaymentResult
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResultCode()
    {
        return $this->resultCode;
    }

    /**
     * @param mixed $resultCode
     * @return InquiryPaymentResult
     */
    public function setResultCode($resultCode)
    {
        $this->resultCode = $resultCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResultDesc()
    {
        return $this->resultDesc;
    }

    /**
     * @param mixed $resultDesc
     * @return InquiryPaymentResult
     */
    public function setResultDesc($resultDesc)
    {
        $this->resultDesc = $resultDesc;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @param mixed $totalPrice
     * @return InquiryPaymentResult
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTxnId()
    {
        return $this->txnId;
    }

    /**
     * @param mixed $txnId
     * @return InquiryPaymentResult
     */
    public function setTxnId($txnId)
    {
        $this->txnId = $txnId;
        return $this;
    }
}

?>