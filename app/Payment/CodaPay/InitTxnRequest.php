<?php

namespace App\Payment\CodaPay;

/**
 * Class InitTxnRequest
 *
 *
 * @property string apiKey
 * @property string txnId
 * @property string country
 * @property string currency
 * @property Item items
 * @property string orderId
 * @property string payType
 * @property string profile
 */
class InitTxnRequest {

    /**
     * InitTxnRequest constructor.
     */
    public function __construct()
    {
        $this->setCountry(config('codapay.country'))
            ->setCurrency(config('codapay.currency'))
            ->setApiKey(config('codapay.apikey'))
            ->setPayType(config('codapay.txnType'));
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param mixed $apiKey
     * @return InitTxnRequest
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     * @return InitTxnRequest
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     * @return InitTxnRequest
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param mixed $items
     * @return InitTxnRequest
     */
    public function setItems($items)
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     * @return InitTxnRequest
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPayType()
    {
        return $this->payType;
    }

    /**
     * @param mixed $payType
     * @return InitTxnRequest
     */
    public function setPayType($payType)
    {
        $this->payType = $payType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param mixed $profile
     * @return InitTxnRequest
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
        return $this;
    }
}

?>
