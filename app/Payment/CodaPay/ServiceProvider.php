<?php

namespace App\Payment\CodaPay;

class ServiceProvider extends \Illuminate\Support\ServiceProvider {

    public function register()
    {
        $this->app->singleton('CodaPay', function () {
            return CodaPay::class;
        });
    }

}