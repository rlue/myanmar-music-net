<?php

namespace App\Payment\CodaPay;

/**
 * Class InquiryPaymentRequest
 *
 * Item details.
 *
 * @package PayPal\Api
 *
 * @property string apiKey
 * @property string txnId
 */
class InquiryPaymentRequest {

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTxnId()
    {
        return $this->txnId;
    }

    /**
     * @param mixed $txnId
     */
    public function setTxnId($txnId)
    {
        $this->txnId = $txnId;
        return $this;
    }
}