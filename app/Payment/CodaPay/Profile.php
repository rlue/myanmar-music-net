<?php

namespace App\Payment\CodaPay;

use phpDocumentor\Reflection\Types\Integer;

/**
 * Class Profile
 *
 *
 *
 * @property integer user_id
 * @property string email
 */

class Profile {

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Profile
     */
    public function setEmail(string $email): Profile
    {
        $this->email = $email;
        return $this;
    }


    /**
     * @return string
     */
    public function getUserId(): Integer
    {
        return $this->user_id;
    }

    /**
     * @param string $user_id
     */
    public function setUserId(string $user_id): Profile
    {
        $this->user_id = $user_id;
        return $this;
    }
}

?>