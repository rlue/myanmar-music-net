<?php

namespace App\Payment\CodaPay;

class Obj2xml {

    var $xmlResult;

    /**
     * Obj2xml constructor.
     * @param $rootNode
     */
    function __construct($rootNode){
        $this->xmlResult = new SimpleXMLElement("<$rootNode></$rootNode>");
    }
    
    private function iteratechildren($object,$xml){
        foreach ($object as $name=>$value) {
        	if ($value != null) {
	            if (is_string($value) || is_numeric($value)) {
	                $xml->$name=$value;
	            } else {
	                $xml->$name=null;
	                $this->iteratechildren($value,$xml->$name);
	            }
        	}
        }
    }

    /**
     * @param $object
     * @return mixed
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    function toXml($object) {
        $this->iteratechildren($object,$this->xmlResult);
        return $this->xmlResult->asXML();
    }
}
?>