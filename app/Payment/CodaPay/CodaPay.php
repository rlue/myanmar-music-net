<?php

namespace App\Payment\CodaPay;

use App\Payment\PaymentException;

class  CodaPay {

    /**
     * CodaPay airtime url - to be provided during on boarding
     *
     * @var string
     */
    protected $airtimeURL;

    /**
     * CodaPay secret key - to be provided during on boarding
     *
     * @var string
     */
    protected $airtimeRestURL;

    /**
     * CodaPay airtime rest url - to be provided during on boarding
     *
     * @var string
     */
    protected $apiKey;

    /**
     * CodaPay constructor.
     * @throws PaymentException
     */
    public function __construct() {

        $this->airtimeURL = config('codapay.airtimeURL');
        $this->airtimeRestURL = config('codapay.airtimeRestURL');
        $this->apiKey = config('codapay.apikey');

        if(empty($this->airtimeURL))
            throw new PaymentException("Airtime URL can't be empty.",E_USER_WARNING);
        if(empty($this->airtimeRestURL))
            throw new PaymentException("Airtime Rest URL can't be empty.",E_USER_WARNING);
        if(empty($this->apiKey))
            throw new PaymentException("Api KEY can't be empty.",E_USER_WARNING);
    }

    public function airtimeURL() {
        return $this->airtimeURL;
    }

    public function getAirtimeRestURL() {
        return $this->airtimeRestURL;
    }

    public function getApiKey() {
        return $this->apiKey;
    }

    public static function country() {
        return config('codapay.country');
    }

    public static function currency() {
        return config('codapay.currency');
    }

    public static function payType() {
        return config('codapay.txnType');
    }

    public static function requestType() {
        return config('codapay.requestType');
    }

    public static function startsWith($haystack, $needle) {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public static function getItems($httpRequest) {
        $itemList = [];

        foreach ($_REQUEST as $key=>$value) {

            if ( self::startsWith( $key, "Item") ) {
                $vals = explode ("_", $value);

                $code = $vals[0];
                $price = $vals[1];

                $item = new Item();
                $item->setName($key)
                    ->setCode($code)
                    ->setPrice((double) $price);

                array_push ($itemList, $item);
            }
        }

        return $itemList;
    }

    public function inquiryPayment ($txnId) {
        if ($this->requestType() == 'xml') {
            return $this->inquiryPaymentXML($txnId);
        } else {
            return $this->inquiryPaymentJSON($txnId);
        }
    }

    public function inquiryPaymentJSON ($txnId) {
        $WebServiceURL = $this->airtimeRestURL . "/inquiryPaymentResult/";
        $headers = array("Content-Type: application/json","Accept: application/json");

        $request = new InquiryPaymentRequest();
        $request->setTxnId($txnId)
            ->setApiKey($this->apiKey);

        $json = json_encode([ "inquiryPaymentRequest"=> $request ]);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $WebServiceURL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

        $responseText = curl_exec($ch);
        $reader = json_decode($responseText, false, 512, JSON_BIGINT_AS_STRING);

        if (is_null($reader)) {
            return $reader;
        }

        $response = new InquiryPaymentResult;

        $response->setOrderId($reader->paymentResult->{'orderId'})
            ->setTxnId($reader->paymentResult->{'txnId'})
            ->setResultCode($reader->paymentResult->{'resultCode'})
            ->setResultDesc($reader->paymentResult->{'resultDesc'})
            ->setTotalPrice($reader->paymentResult->{'totalPrice'});

        return $response;
    }

    public function inquiryPaymentXML ($txnId) {
        $WebServiceURL = $this->airtimeRestURL . "/inquiryPaymentResult/";
        $headers = array("Content-Type: application/xml","Accept: application/xml");

        $request = new InquiryPaymentRequest;
        $request->setTxnId($txnId)
            ->setApiKey($this->apiKey);

        $converter = new Obj2xml("inquiryPaymentRequest");
        $xml = $converter->toXml($request);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $WebServiceURL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $responseText = curl_exec($ch);

        $xml = simplexml_load_string($responseText);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);

        $response = new InquiryPaymentResult;
        $response->setOrderId($array['orderId'])
            ->setTxnId($array['txnId'])
            ->setResultCode($array['resultCode'])
            ->setResultDesc($array['resultDesc'])
            ->setTotalPrice($array['totalPrice']);

        return $response;
    }

    public function initTxn ($txnId) {
        if ($this->requestType() == 'xml') {
            return  $this->initTxnXML($txnId);
        } else {
            return $this->initTxnJSON($txnId);
        }
    }

    public function initTxnJSON ($request) {
        $WebServiceURL = $this->airtimeRestURL . "/init/";
        $headers = array("Content-Type: application/json","Accept: application/json");

        $json = json_encode([ "initRequest" => $request ]);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $WebServiceURL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

        $responseText = curl_exec($ch);
        $reader = json_decode($responseText, false, 512, JSON_BIGINT_AS_STRING);

        $response = new InitTxnResult;
        $response->setResultCode(0)
            ->setTxnId($reader->initResult->{'txnId'});

        return $response;
    }

    public function validateChecksum ($httpRequest) {
        try {
            $txnId = $httpRequest["TxnId"];
            $apiKey = $this->apiKey; // Merchant APIKey
            $orderId = $httpRequest["OrderId"];
            $resultCode = $httpRequest["ResultCode"];
            $checksum = $httpRequest["Checksum"];

            $values = $txnId . $apiKey . $orderId . $resultCode;

            $sum = md5($values);

            return ($sum == $checksum);
        } catch (PaymentException $e) {
            echo 'Message: ' .$e->getMessage();
        }

        return false;
    }

    public static function strToHex($string)
    {
        $hex='';
        for ($i=0; $i < strlen($string); $i++)
        {
            $hex .= dechex(ord($string[$i]));
        }
        return $hex;
    }

    public function initTxnXML ($request) {
        $WebServiceURL = $this->airtimeRestURL . "/init/";
        $headers = array("Content-Type: application/xml","Accept: application/xml");

        $converter=new Obj2xml("initRequest");
        $xml = $converter->toXml($request);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $WebServiceURL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $responseText = curl_exec($ch);

        $xml = simplexml_load_string($responseText);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);

        $response = new InitTxnResult;
        $response->setResultCode($array['txnId'])
            ->setTxnId($array['resultCode']);

        if ( (int) $response->resultCode > 0) {
            $response->setResultDesc($array['resultDesc']);
        }

        return $response;
    }
}

