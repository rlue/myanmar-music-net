<?php namespace App\Payment\CodaPay;

class Facade extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return CodaPay::class;
    }
}
