<?php

namespace App\Payment;

use App\Subscribe;
use Carbon\Carbon;

trait Subscribable
{
    /**
     * For new user
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function addTrialSubscribe() {
        $days = Carbon::now()->addDays(30);

        Subscribe::create([
            'user_id' => $this->id,
            'subscribe_price_id' => 1,
            'trial_ends_at' => $days,
        ]);
    }

    /**
     * Update the user's subscribe.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function addedSubscribe() {

        $days = Carbon::now()->addDays(30);

        $subscribe = [
            'user_id' => $this->id,
            'subscribe_price_id' => 1,
            'trial_ends_at' => null,
            'ends_at' => $days
        ];

        $subscription = $this->subscription();

        if ($this->subscriptions->isEmpty()) {
            return Subscribe::create($subscribe);
        }

        if ($subscription->onTrial()) {
            $subscribe['ends_at'] = Carbon::instance($subscription->trial_ends_at)->addDays(30) ;
        }

        if ($subscription->onGracePeriod()) {
            $subscribe['ends_at'] = Carbon::instance($subscription->ends_at)->addDays(30) ;
        }

        return Subscribe::create($subscribe);
    }

    public function getLeftDays() {
        $subscription = $this->subscription();

        if (is_null($subscription)) {
            return 0;
        }

        if (!is_null($subscription->trial_ends_at)) {
            return Carbon::now()->diffInDays($subscription->trial_ends_at);
        }

        if (!is_null($subscription->ends_at)) {
            return Carbon::now()->diffInDays($subscription->ends_at);
        }

        return 0;
    }

    /**
     * Determine if the model is on trial.
     *
     * @param  string  $subscription
     * @param  string|null  $plan
     * @return bool
     */
    public function onTrial()
    {
        if (func_num_args() === 0) {
            return true;
        }

        $subscription = $this->subscription();

        return $subscription && $subscription->onTrial();
    }

    /**
     * Determine if the model has a given subscription.
     *
     * @param  string  $subscription
     * @param  string|null  $plan
     * @return bool
     */
    public function subscribed()
    {
        $subscription = $this->subscription();

        if (is_null($subscription)) {
            return false;
        }

        return $subscription->valid();
    }

    /**
     * Get a subscription instance by name.
     *
     * @param  string  $subscription
     * @return \App\Subscribe
     */
    public function subscription()
    {
        return $this->subscriptions->sortByDesc(function ($value) {
            return $value->created_at->getTimestamp();
        })->first();
    }

    /**
     * Get all of the subscriptions for the model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions()
    {
        return $this->hasMany(Subscribe::class)->orderBy('created_at', 'desc');
    }
}