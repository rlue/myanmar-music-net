<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Request;
use Session;

class PlaylistView extends Model
{
    protected $fillable = [
        'playlist_id', 'view'
    ];

    public $timestamps = false;

    public $incrementing = false;

    public $primaryKey = 'playlist_id';

    public static function track($playlist_id){
        if (!Session::has('playlist_track') || Session::get('playlist_track') !== Request::path()) {

            PlaylistView::firstOrCreate(['playlist_id' => $playlist_id])->increment('view');

            Session::put('artist_track', Request::path());

            return ['message' => 'saved'];

        }else {
            return ['message' => Session::get('playlist_track')];
        }

    }
}
