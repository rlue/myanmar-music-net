<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlbumlistCms extends Model
{
    //
    protected $table = 'albumlist_cms';

    protected $fillable = [
        'cms_type'
    ];

    public function cms_albumlists()
    {
      return $this->belongsToMany(AlbumList::class,'cms_albumlists','cms_id','albumlist_id');  
    }
}
