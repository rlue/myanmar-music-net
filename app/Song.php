<?php

namespace App;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Log;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use DB;
class Song extends Model implements HasMedia
{
	use HasMediaTrait;
    //
    protected $fillable = [
        'id','name_mm','name_eng','prices','genre_id','feeling_id','preview_fileType','preview_file','full_fileType','full_file','full_lowFileType','full_lowFile','duration','video_id'
    ];

    protected $casts = [
        'sub_menu' => 'array'
    ];

    public function artists()
    {
      return $this->belongsToMany(Artist::class,'song_artists','song_id','artist_id');  
    }
    public function feeling()
    {
        return $this->belongsTo(Feeling::class,'feeling_id');
    }
    public function feats()
    {
      return $this->belongsToMany(Artist::class,'song_feat','song_id','artist_id');  
    }

     public function genre()
    {
        return $this->belongsTo(SongCategory::class,'genre_id');
    }
     public function video()
    {
        return $this->belongsTo(Video::class,'video_id');
    }
    public function albums() {
        return $this->belongsToMany(Album::class,'album_songs','song_id','album_id');
    }

    public function favourite()
    {
        return $this->belongsToMany(User::class,'favourites','item_id','user_id')->withTimestamps();
    }

    public function orders() {
        return $this->belongsToMany(User::class,'orders','item_id','user_id');
    }

    public function auth_user_favourite() {
        return $this->favourite()->where('favourites.type', 'song')
            ->where('favourites.user_id', 1);
    }

    public function getIsFavourite() {
        if (auth()->check()) {
            return $this->auth_user_favourite->isNotEmpty();
        }

        return false;
    }

    public function auth_user_order() {
        return $this->orders()->where('orders.type', 'song')
            ->where('orders.user_id', 1);
    }

    public function getIsBought() {
        if (auth()->check()) {
            return $this->auth_user_order->isNotEmpty();
        }

        return false;
    }

    public function price() {
        return $this->hasOne(Price::class, 'id', 'prices');
    }

    public function play_counts() {
        return $this->hasOne(PlayCount::class);
    }

    public function getAudioFile() {

        if ($this->sub_menu['free']) {
            if (!auth()->check()) {
                return $this->full_file;
            }

            if (auth()->user()->streaming_quality === 256) {
                return $this->full_file;
            }

            return $this->full_lowFile;
        }

        if (!auth()->check()) {
            return $this->preview_file;
        }

        if ($this->sub_menu['streaming']) {
            if (auth()->user()->subscribed()) {
                if (auth()->user()->streaming_quality === 256) {
                    return $this->full_file;
                }

                return $this->full_lowFile;
            }
        }

        if ($this->sub_menu['store']) {
            return $this->preview_file;
        }

        return $this->preview_file;

    }

    public function getIsFullSong() {
        if ($this->sub_menu['free']) {
            return true;
        }

        if (!auth()->check()) {
            return false;
        }

        if ($this->sub_menu['streaming']) {
            return auth()->user()->subscribed();
        }

        return false;
    }

    public function getAudioLowFile() {

        if ($this->sub_menu['free']) {
            return $this->full_lowFile;
        }

        if (!auth()->check()) {
            return $this->preview_file;
        }

        if ($this->sub_menu['streaming']) {
            if (auth()->user()->subscribed()) {
                return $this->full_lowFile;
            }
        }

        if ($this->sub_menu['store']) {
            return $this->preview_file;
        }

        return $this->preview_file;

    }

    public function getPlayCount() {
        return is_null(optional($this->play_counts)->click_count) ? 0 : $this->play_counts->click_count;
    }

    public function getSoundCloudFileName() {
        $url = parse_url($this->full_file);

        $path = dirname($url['path']);

        $api = $url['scheme'] . '://' . $url['host'] . $path . '?' . $url['query'];

        $client = new Client();

        try {
            $response = $client->get($api);

            Log::info(json_decode($response->getBody(), true));

        } catch (RequestException $exception) {

            if ($exception->hasResponse()) {
                Log::warning(json_decode($exception->getResponse()->getBody()->getContents(), true));
            }

            return str_random(12) . '.mp3';

        } catch (Exception $e) {
            return str_random(12) . '.mp3';
        }

        try {
            $soundcloud = json_decode($response->getBody(), true);

            $filename = $soundcloud['permalink'] . '.' . $soundcloud['original_format'];

        }catch (Exception $exception) {
            $filename = str_random(12) . '.mp3';
        }

        return $filename;
    }

    public function getAlbumImage() {
        $album = $this->albums->first();

        if (is_null($album)) {
            return '/images/album/album.jpg';
        }
        return $album->album_image;
    }
    
    public function getPreviewAudio($id)
    {
        $audio = Song::where('id','=',$id)->first();
        $file =  $audio->getMedia('SongPreview');
        return $file[0]->getUrl();
    }
    public function getFullAudio($id)
    {
        $audio = Song::where('id','=',$id)->first();
        $file =  $audio->getMedia('SongFull');
        return $file[0]->getUrl();
    }

    public function checkFavourite($item,$id)
    {   
        $song = DB::table('favourites')
        ->where('item_id','=',$item)
        ->where('user_id','=',$id)
        ->where('type','=','song')->first();
        if(isset($song)){
            return 1;
        }else{
            return 0;
        }
    }

    public function checkOrder($item,$id)
    {
        $song = DB::table('orders')
        ->where('item_id','=',$item)
        ->where('user_id','=',$id)
        ->where('type','=','song')->first();
        if(isset($song)){
            return 1;
        }else{
            return 0;
        }
    }
    public function comments() 
    {
        return $this->hasMany(Comment::class);
    }
}
