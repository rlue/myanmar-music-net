<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //

    protected $fillable = [
        'comment_body','comment_parent','user_id','song_id'
    ];

    function song()
    {
    	return $this->belongsTo(Song::class,'song_id');
    }
}
