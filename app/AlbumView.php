<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Request;
use Session;

class AlbumView extends Model
{

    protected $fillable = [
        'album_id', 'view'
    ];

    public $timestamps = false;

    public $incrementing = false;

    public $primaryKey = 'album_id';

    public static function track($album_id){
        if (!Session::has('album_track') || Session::get('album_track') !== Request::path()) {

            AlbumView::firstOrCreate(['album_id' => $album_id])->increment('view');

            Session::put('album_track', Request::path());

            return ['message' => 'saved'];

        }else {
            return ['message' => Session::get('album_track')];
        }
    }
}
