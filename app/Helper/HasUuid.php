<?php

namespace App\Helpers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

trait HasUuid
{
    public static function boot()
    {
        parent::boot();
        self::creating(function (Model $model) {
            $model->uuid = (string) Str::uuid();
        });
    }
}