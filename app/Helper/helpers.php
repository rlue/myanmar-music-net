<?php
/**
 * Created by PhpStorm.
 * User: tintnaingwin
 * Date: 12/13/18
 * Time: 1:02 AM
 */

use Illuminate\Support\Carbon;

function youtube_id($url)
{
    if(strlen($url) > 11)
    {
        if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match))
        {
            return $match[1];
        }
        else
            return false;
    }

    return $url;
}

function make_slug($string) {
    return preg_replace('/\s+/u', '-', trim($string));
}


function uploadImageFromUrl($url, $file_path = '/photos/1/', $default_image = '') {

    $filename   = basename($url);

//    $default = preg_replace('/[0-9]+/', '', $filename);
//
//    if ($default === 'default_artist.jpg') {
//        if (File::exists(public_path($file_path . $filename))) {
//            File::delete(public_path($file_path . $filename));
//        }
//
//        return $default_image;
//    }

    if ($url === '' || $url === null) {
        return $default_image;
    }

    $new_name = preg_replace('/[\s\+]/', '-', $filename);

    if (File::exists(public_path($file_path . $filename))) {
        File::move(public_path($file_path . $filename), public_path($file_path . $new_name));
    }

    if (!File::exists(public_path($file_path . $new_name))) {

        createFolderByPath(public_path($file_path));

        try {
            $img = Image::make($url)
                ->orientate() //Apply orientation from exif data
                ->save(public_path($file_path . $new_name));

            $img->destroy();

        } catch (\Intervention\Image\Exception\NotReadableException $exception) {
            return $default_image;
        }

        createFolderByPath(public_path($file_path . 'thumbs'));

        $img = Image::make(public_path($file_path . $new_name))
            ->fit(config('lfm.thumb_img_width', 200), config('lfm.thumb_img_height', 200))
            ->save(public_path($file_path . 'thumbs/' . $new_name));

        $img->destroy();
    }

    return $file_path . $new_name;

}

function createFolderByPath($path)
{

    if (! File::exists($path)) {
        File::makeDirectory($path, config('lfm.create_folder_mode', 0755), true, true);
    }
}

function carbon_parse($tz = null)
{
    return Carbon::parse($tz);
}