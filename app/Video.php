<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Staudenmeir\EloquentEagerLimit\HasEagerLimit;

class Video extends Model
{
    use HasEagerLimit;

    protected $fillable = [
        'name_mm','name_eng','video_category','video_desc','video_link'
    ];

    protected $appends = ['youtube_id'];

    public function category()
    {
    	return $this->belongsTo(VideoCategory::class,'video_category');
    }
   	public function artists()
    {
      return $this->belongsToMany(Artist::class,'video_artists','video_id','artist_id');  
    }

    public function getYoutubeIdAttribute()
    {
        return youtube_id($this->video_link);
    }
}
