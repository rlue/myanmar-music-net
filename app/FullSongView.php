<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Request;
use Session;

class FullSongView extends Model
{
    protected $fillable = [
        'song_id', 'count', 'counted_at'
    ];

    public $timestamps = false;

    public static function track($song_id){
        if (!Session::has('full_song_view_track') || Session::get('full_song_view_track') !== Request::path()) {

            $song = Song::find($song_id);

            if ($song->getIsFullSong()) {
                FullSongView::firstOrCreate(['song_id' => $song->id, 'counted_at' => date('Y-m-d')])->increment('count');

                Session::put('full_song_view_track', Request::path());

                return ['message' => 'saved'];
            }

            return ['message' => 'This song is not full'];

        }else {
            return ['message' => Session::get('full_song_view_track')];
        }
    }

}
