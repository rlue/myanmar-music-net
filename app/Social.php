<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $fillable = [
        'user_id','provider','social_id'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author TintNaingWin
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
