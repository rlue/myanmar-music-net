<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistType extends Model
{
    protected $fillable = [
        'name'
    ];

    public function artists() {
        return $this->hasMany(Artist::class, 'artistType_id', 'id');
    }
}
