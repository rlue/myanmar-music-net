<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Staudenmeir\EloquentEagerLimit\HasEagerLimit;

class ArtistCategory extends Model
{
    use HasEagerLimit;

    protected $fillable = [
        'name'
    ];

    public function artists() {
        return $this->hasMany(Artist::class, 'artistCategory_id', 'id');
    }
}
