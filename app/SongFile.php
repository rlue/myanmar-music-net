<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongFile extends Model
{

    protected $connection = 'mysql_devmm';

    protected $table = 'files';

    protected $fillable = [
        'user_id', 'song_id', 'updated_at'
    ];


}
