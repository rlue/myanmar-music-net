<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Staudenmeir\EloquentEagerLimit\HasEagerLimit;

class Artist extends Model
{
    use HasEagerLimit;

    protected $fillable = [
        'artist_name_mm','artist_name_eng','artistType_id','artistCategory_id','artist_description','artist_image'
    ];

    public function category()
    {
    	return $this->belongsTo(ArtistCategory::class,'artistCategory_id');
    }
     public function type()
    {
    	return $this->belongsTo(ArtistType::class,'artistType_id');
    }

    public function songs()
    {
        return $this->belongsToMany(Song::class,'song_artists','artist_id','song_id');
    }


    public function albums()
    {
        return $this->belongsToMany(Album::class,'album_artists','artist_id','album_id');
    }
}
