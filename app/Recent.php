<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recent extends Model
{
    protected $fillable = [
        'user_id', 'song_id'
    ];


    public static function track($song_id) {
        return Recent::updateOrCreate(['song_id' => $song_id, 'user_id' => auth()->id()])->touch();
    }
}
