<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentOrder extends Model
{
    protected $fillable = [
        'user_id', 'transaction_id', 'payment_service', 'amount', 'status', 'client',
    ];


    public function user() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
