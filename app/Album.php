<?php

namespace App;

use App\Helpers\HasUuid;
use Illuminate\Database\Eloquent\Model;
use DB;

class Album extends Model
{
    use HasUuid;

    protected $fillable = [
        'album_name_mm','album_name_eng','album_prices','scheduled_date','release_date','copy_right','album_image','aggregator_id','sub_menu','status','uuid'
    ];

    protected $casts = [
        'sub_menu' => 'array'
    ];

    public function songs()
    {
      return $this->belongsToMany(Song::class,'album_songs','album_id','song_id');  
    }

    public function artists()
    {
      return $this->belongsToMany(Artist::class,'album_artists','album_id','artist_id');  
    }

    public function feats()
    {
      return $this->belongsToMany(Artist::class,'album_feat','album_id','artist_id');  
    }
    public function aggregator()
    {
      return $this->belongsTo(Aggregator::class,'aggregator_id');  
    }
    public function prices()
    {
    	return $this->belongsTo(AlbumPrice::class,'album_prices');
    }
    public function favourite()
    {
        return $this->belongsToMany(User::class,'favourites','item_id','user_id')->withTimestamps();
    }

    public function likes() {
        return $this->belongsToMany(User::class, 'album_likes', 'album_id', 'user_id')->withTimestamps();
    }

    public function view() {
        return $this->hasOne(AlbumView::class, 'album_id', 'id');
    }

    public function orders() {
        return $this->belongsToMany(User::class,'orders','item_id','user_id');
    }

    public function auth_user_favourite() {
        return $this->favourite()->where('favourites.type', 'album')
            ->where('favourites.user_id', 1);
    }

    public function getIsFavourite() {
        if (auth()->check()) {
            return $this->auth_user_favourite->isNotEmpty();
        }

        return false;
    }

    public function auth_user_order() {
        return $this->orders()->where('orders.type', 'album')
            ->where('orders.user_id', 1);
    }

    public function getIsBought() {
        if (auth()->check()) {
            return $this->auth_user_order->isNotEmpty();
        }

        return false;
    }

    public function auth_user_like() {
        return $this->likes()->where('album_likes.user_id', auth()->id());
    }

    public function getLiked() {
        if (auth()->check()) {
            return $this->auth_user_like->isNotEmpty();
        }

        return false;
    }

    public function checkFavourite($item,$id)
    {   
        $album = DB::table('favourites')
        ->where('item_id','=',$item)
        ->where('user_id','=',$id)
        ->where('type','=','album')->first();
        if(isset($album)){
            return 1;
        }else{
            return 0;
        }
    }

    public function checkOrder($item,$id)
    {
        $album = DB::table('orders')
        ->where('item_id','=',$item)
        ->where('user_id','=',$id)
        ->where('type','=','album')->first();
        if(isset($album)){
            return 1;
        }else{
            return 0;
        }
    } 
    
}
