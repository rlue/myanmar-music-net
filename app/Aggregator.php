<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aggregator extends Model
{
    //
    protected $fillable = [
        'name'
    ];
}
