<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayCount extends Model
{
    protected $fillable = [
        'song_id', 'click_count'
    ];

    public $timestamps = false;

    public $incrementing = false;

    public $primaryKey = 'song_id';

    public static function track($song_id){
        PlayCount::firstOrCreate(['song_id' => $song_id])->increment('click_count');
    }
}
