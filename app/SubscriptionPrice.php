<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPrice extends Model
{
    //
    protected $fillable = [
        'name','num_day'
    ];
}
