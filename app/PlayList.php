<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayList extends Model
{
    //
    protected $table = 'playlists';

    protected $fillable = [
        'playlist_name_mm','playlist_name_eng','playlist_scheduled_date','playlist_release_date','playlist_desc','playlist_image','status'
    ];

     public function songs()
    {
      return $this->belongsToMany(Song::class,'playlist_songs','playlist_id','song_id');  
    }

    public function checkFavourite($item,$id)
    {   
        $song = DB::table('favourites')
        ->where('item_id','=',$item)
        ->where('user_id','=',$id)
        ->where('type','=','song')->first();
        if(isset($song)){
            return 1;
        }else{
            return 0;
        }
    }

    public function checkOrder($item,$id)
    {
        $song = DB::table('orders')
        ->where('item_id','=',$item)
        ->where('user_id','=',$id)
        ->where('type','=','song')->first();
        if(isset($song)){
            return 1;
        }else{
            return 0;
        }
    }
}
