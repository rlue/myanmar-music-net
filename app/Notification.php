<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    //

    protected $table = 'notification';
    protected $fillable = [
        'title','description','noti_image','status','send_schedule'
    ];
}
