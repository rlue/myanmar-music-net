<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlbumCms extends Model
{
    //
     protected $table = 'album_cms';

    protected $fillable = [
        'cms_type'
    ];

    public function cms_albums()
    {
      return $this->belongsToMany(Album::class,'cms_albums','cms_id','album_id');  
    }
}
