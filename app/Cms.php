<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cms extends Model
{
    //
    protected $table = 'cms';

    protected $fillable = [
        'title','cms_type'
    ];

    public function cms_songs()
    {
      return $this->belongsToMany(Song::class,'cms_songs','cms_id','song_id');  
    }
}
