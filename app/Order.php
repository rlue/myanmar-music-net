<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id', 'item_id', 'type'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function song() {
        return $this->belongsTo(Song::class, 'id', 'item_id');
    }

    public function album() {
        return $this->belongsTo(Album::class, 'id', 'item_id');
    }
}
