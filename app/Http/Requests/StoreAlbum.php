<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAlbum extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'album_name_mm'  => 'required',
            'album_name_eng'  => 'required',
            'song_id'  => 'required',
            'artist_id'  => 'required',
            'album_image' => 'required',
            'album_prices' => 'required',
            'release_date' => 'required',
            'status'    => 'required'

        ];
    }
}
