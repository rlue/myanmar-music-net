<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSong extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name_mm'  => 'required',
            'name_eng'  => 'required',
            'artist_id'  => 'required',
            'prices'  => 'required',
            'genre_id' => 'required',
            'preview_file' => 'required',
            'full_file' => 'required',
            

        ];
    }
}
