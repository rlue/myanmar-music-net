<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePlayList extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'playlist_name_mm'  => 'required',
            'playlist_name_eng'  => 'required',
            'song_id'  => 'required',
            'playlist_image' => 'required',
            
        ];
    }
}
