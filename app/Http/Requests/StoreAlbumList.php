<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAlbumList extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'albumlist_name_mm'  => 'required',
            'albumlist_name_eng'  => 'required',
            'album_id'  => 'required',
            'albumlist_image' => 'required',
        ];
    }
}
