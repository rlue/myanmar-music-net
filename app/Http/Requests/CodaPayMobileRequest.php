<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CodaPayMobileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'required|numeric|in:1000,3000,5000',
            'type'  => 'required',
            'user_id' => 'required',
        ];
    }
}
