<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required|password_hash_check',
            'password' => 'required|confirmed|min:6|max:50|different:old_password',
            'password_confirmation' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'old_password.password_hash_check' => 'Current password does not match!',
            'old_password.required' => 'Need Your Old Current'
        ];
    }

}
