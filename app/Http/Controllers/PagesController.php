<?php

namespace App\Http\Controllers;

use App\Album;
use App\AlbumCms;
use App\AlbumList;
use App\AlbumlistCms;
use App\AlbumView;
use App\Artist;
use App\ArtistCategory;
use App\ArtistView;
use App\Event;
use App\Feeling;
use App\PlayList;
use App\PlaylistView;
use App\Slider;
use App\SongCategory;
use App\Support;
use App\Video;
use App\Song;
use App\PlaylistCms;
use App\VideoCategory;
use Carbon\Carbon;
use Request;
use DB;
use Cache;

class PagesController extends Controller
{

    /**
     * Display the home page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function index()
    {
        $top_playlists = null;
        $new_albums    = null;

        $sliders       = Slider::all();

        $top_20_ended_at = Cache::get('top_20_ended_at');

        $songs         = Song::join('full_song_views', function ($join) use ($top_20_ended_at){
                            $join->on('songs.id', '=', 'full_song_views.song_id')
                                ->where('full_song_views.counted_at', '<', $top_20_ended_at);
                            })
                        ->groupBy('songs.id')
                        ->select('songs.*', DB::raw('SUM(full_song_views.count) as count'))
                        ->with(['artists','feats','albums','price', 'auth_user_order'])
                        ->orderByDesc('count')
                        ->limit(15)->get();

        $genres        = SongCategory::limit(6)->inRandomOrder()->get();

        $artists       = Artist::join('artist_views', 'artists.id', '=', 'artist_views.artist_id')
                                ->orderBy('view', 'desc')
                                ->limit(10)
                                ->get();

        $top_albums    = Album::join('album_views', 'albums.id', '=', 'album_views.album_id')
            ->where('scheduled_date', '<', date('Y-m-d H:i:s'))
            ->with(['artists','prices'])
            ->orderBy('view', 'desc')
            ->limit(10)
            ->get();

        $top_playlists    = Playlist::join('playlist_views', 'playlists.id', '=', 'playlist_views.playlist_id')
            ->where('playlist_scheduled_date', '<', date('Y-m-d H:i:s'))
            ->orderBy('view', 'desc')
            ->limit(10)
            ->get();

        $new           = AlbumCms::where('cms_type', 'new_release')->first();

        if (!is_null($new)) {
            $new_albums = $new->cms_albums()->where('scheduled_date', '<', date('Y-m-d H:i:s'))->where('status','publish')->with(['artists', 'prices'])->limit(9)->get();
        }

        $playlist  = PlaylistCms::where('cms_type','playlists')->first();

        if (!is_null($playlist)) {
            $recommended_playlists = $playlist->cms_playlists()->where('playlist_scheduled_date', '<', date('Y-m-d H:i:s'))->limit(9)->get();
        }

        $free_albums = Album::where('sub_menu->free', true)->where('scheduled_date', '<', date('Y-m-d H:i:s'))->where('status','publish')->inRandomOrder()->limit(9)->with('artists')->get();

        $collections = AlbumlistCms::join('cms_albumlists', 'cms_albumlists.cms_id', '=', 'albumlist_cms.id')
            ->join('album_lists', 'album_lists.id', 'cms_albumlists.albumlist_id')
            ->where('albumlist_scheduled_date', '<', date('Y-m-d H:i:s'))
            ->where('cms_type', 'collection')
            ->limit(8)
            ->get();

        return view('pages.home', compact('songs', 'new_albums','top_albums', 'top_playlists', 'sliders', 'genres', 'artists', 'free_albums', 'collections', 'recommended_playlists'));
    }

    /**
     * Display the recommended playlist page
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function recommendedPlaylist()
    {
        $playlist  = PlaylistCms::where('cms_type','playlists')->first();

        if (!is_null($playlist)) {
            $recommended_playlists = $playlist->cms_playlists()->get();
        }

        return view('pages.playlist.recommended', compact('recommended_playlists'));
    }

    /**
     * Display the top albums page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function topAlbums()
    {
        $top_albums    = Album::join('album_views', 'albums.id', '=', 'album_views.album_id')
            ->where('scheduled_date', '<', date('Y-m-d H:i:s'))
            ->with(['artists','prices'])
            ->orderBy('view', 'desc')
            ->limit(12)
            ->get();

        return view('pages.album.top-albums', compact('top_albums'));
    }

    /**
     * Display the new releases page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function newReleases()
    {
        $new          = AlbumCms::where('cms_type', 'new_release')->first();
        $new_releases = $new->cms_albums()->with('artists')->get();

        return view('pages.album.new-releases', compact('new_releases'));
    }


    /**
     * Display the all of albums.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function albums()
    {
        $albums = Album::where('status', 'publish')->where('scheduled_date', '<', date('Y-m-d H:i:s'))->with(['artists', 'prices'])->orderByDesc('created_at')->paginate(18);

        if (Request::ajax()) {
            return response()->json($albums);
        }

        return view('pages.album.index', compact('albums'));
    }


    /**
     * Display the album detail page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function album($uuid)
    {
        $album = Album::with(['songs', 'artists','feats','songs.artists', 'songs.feats', 'songs.auth_user_order', 'songs.price', 'songs.auth_user_favourite', 'prices'])
            ->where('scheduled_date', '<', date('Y-m-d H:i:s'))
            ->where('status', 'publish')->where('uuid', $uuid)->firstOrFail();

        AlbumView::track($album->id);

        $artists = $album->artists;
        $related_albums = $artists->first()->albums()->where('status','publish')
            ->where('scheduled_date', '<', date('Y-m-d H:i:s'))
            ->whereNotIn('id',[$album->id])
            ->with(['artists','prices'])->get();

        return view('pages.album.show', compact('album', 'related_albums'));
    }

    /**
     * Display the free music page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function freeMusic()
    {
        $albums = Album::where('sub_menu->free', true)->where('scheduled_date', '<', date('Y-m-d H:i:s'))
            ->where('status', 'publish')
            ->with('artists')
            ->limit(6)
            ->paginate(18);

        if (Request::ajax()) {
            return response()->json($albums);
        }

        return view('pages.all-free-music', compact('albums'));
    }

    /**
     * Download the free song.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function downloadFreeSong($id)
    {
        $song = Song::where('sub_menu->free', true)->findOrFail($id);

        if (is_null($song)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Unauthorized action'
            ], 413);
        }

        $filename = str_random(12) . '.mp3';

        if ($song->full_fileType === 'full_s3')
        {
            $filename   = basename($song->full_file);
        }elseif ($song->full_fileType === 'full_soundcloud')
        {
            $filename = $song->getSoundCloudFileName($song->full_file);
        }

        return response()->streamDownload(function () use($song){
            echo file_get_contents($song->full_file);
        }, $filename,[
            'Pragma' => 'public',
            'Expires' => 0,
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Cache-Control' => 'public',
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment; filename="'.$filename.'"',
            'Content-Transfer-Encoding' => 'binary'
        ]);

    }

    /**
     * Display the free music page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function topSongs()
    {
        $songs         = Song::join('full_song_views', function ($join) {
            $join->on('songs.id', '=', 'full_song_views.song_id')
                ->where('full_song_views.counted_at', '>=', Carbon::today()->subWeek())
                ->where('full_song_views.counted_at', '<=', Carbon::today());
            })
            ->groupBy('songs.id')
            ->select('songs.*', DB::raw('SUM(full_song_views.count) as count'))
            ->with(['artists','feats','albums','price', 'auth_user_order'])
            ->orderByDesc('count')
            ->limit(20)->get();

        return view('pages.top-songs', compact('songs'));
    }


    /**
     * Display all of collection.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function showCollectionList()
    {
        $collections = AlbumList::where('albumlist_scheduled_date', '<', date('Y-m-d H:i:s'))->paginate(12);

        if (Request::ajax()) {
            return response()->json($collections);
        }

        return view('pages.collection.index', compact('collections'));
    }


    /**
     * Display the collection's albums page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function showCollection($id)
    {
        $collection = AlbumList::with('albums', 'albums.artists', 'albums.prices')
            ->where('albumlist_scheduled_date', '<', date('Y-m-d H:i:s'))
            ->findOrFail($id);

        return view('pages.collection.show', compact('collection'));
    }


    /**
     * Display the list of artists page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function artists()
    {
        $categories = ArtistCategory::with(['artists' => function($query){
            $query->limit(6);
        }])->get();

        return view('pages.artists', compact('categories'));
    }


    /**
     * Display the artist by category page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function artistsByCategory($id, $slug)
    {
        $category = ArtistCategory::findOrFail($id);

        $artists = Artist::where('artistCategory_id', $id)->paginate(18);

        if (Request::ajax()) {
            return response()->json($artists);
        }

        return view('pages.artists-category', compact('category', 'artists'));
    }

    /**
     * Display the artist detail page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function artist($id)
    {
        $artist = Artist::findOrFail($id);

        ArtistView::track($id);

        $albums = Album::join('album_artists', 'album_artists.album_id', '=', 'albums.id')
                ->join('artists', 'artists.id', 'album_artists.artist_id')
                ->select('albums.*','artists.id as artist_id')
                ->where('albums.status','publish')
                ->where('albums.scheduled_date', '<', date('Y-m-d H:i:s'))
                ->where('album_artists.artist_id', $id)->get();

        return view('pages.artist', compact('artist', 'albums'));
    }


    /**
     * Display the listing of genres page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function genres()
    {
        $genres = SongCategory::all();

        return view('pages.genres', compact('genres'));
    }


    /**
     * Display the genre detail page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function genre($id)
    {
        $genre = SongCategory::findOrFail($id);
        $songs = Song::join('album_songs', 'album_songs.song_id', '=', 'songs.id')
            ->join('albums', 'albums.id', '=', 'album_songs.album_id')
            ->where('songs.genre_id', $genre->id)
            ->where('albums.scheduled_date', '<', date('Y-m-d H:i:s'))
            ->paginate(18);

        if (Request::ajax()) {
            return response()->json($songs);
        }

        return view('pages.genre', compact('genre', 'songs'));
    }


    /**
     * Display the listing of playlist page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function playlist()
    {
        $playlist = PlayList::where('playlist_scheduled_date', '<', date('Y-m-d H:i:s'))->get();

        return view('pages.playlist.index', compact('playlist'));
    }


    /**
     * Display the playlist detail page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function playlistDetail($id)
    {
        $playlist = PlayList::with(['songs', 'songs.price', 'songs.play_counts', 'songs.auth_user_favourite', 'songs.auth_user_order','songs.artists', 'songs.feats', 'songs.albums'])
                ->where('playlist_scheduled_date', '<', date('Y-m-d H:i:s'))->findOrFail($id);

        PlaylistView::track($id);

        return view('pages.playlist.show', compact('playlist'));
    }


    /**
     * Display the listing of feelings page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function feelings()
    {
        $feelings = Feeling::all();

        return view('pages.feelings', compact('feelings'));
    }


    /**
     * Display the feeling detail page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.comm>
     */
    public function feeling($id)
    {
        $feeling = Feeling::findOrFail($id);
        $songs = Song::where('feeling_id', $feeling->id)->whereHas('albums',function ($query) {
                        $query->where('scheduled_date', '<', date('Y-m-d H:i:s'));
                })->with(['albums' => function ($query) {
                        $query->where('scheduled_date', '<', date('Y-m-d H:i:s'));
                }, 'artists', 'price'])
            ->paginate(20);

        return view('pages.feeling', compact('feeling', 'songs'));
    }


    /**
     * Display the listing of videos page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.comm>
     */
    public function videos()
    {
        $video_categories = VideoCategory::with(['videos' => function ($query) {
            $query->limit(6);
        }])->get();

        return view('pages.videos.index', compact('video_categories'));
    }


    /**
     * Display the video detail page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function video($id)
    {
        $video = Video::findOrFail($id);
        $other_videos = Video::limit(3)->inRandomOrder()->get();

        return view('pages.videos.show', compact('video', 'other_videos'));
    }


    /**
     * Display the videos by category page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function videosByCategory($id, $slug)
    {
        $category = VideoCategory::findOrFail($id);
        $videos = Video::where('video_category', $id)->paginate(18);

        if (Request::ajax()) {
            return response()->json($videos);
        }

        return view('pages.videos.category', compact('category', 'videos'));
    }


    /**
     * Display listing of events page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function events()
    {
        $events = Event::all();

        return view('pages.events.index', compact('events'));
    }


    /**
     * Display the event detail page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function event($id)
    {
        $event = Event::findOrFail($id);

        $latest_events = Event::orderBy('created_at', 'desc')->limit(3)->get();

        return view('pages.events.show', compact('event', 'latest_events'));
    }


    /**
     * Display the privacy page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function privacy()
    {
        return view('pages.privacy');
    }


    /**
     * Display the support guide page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function supportGuide()
    {
        $support_guide = optional(Support::first())->description;
        return view('pages.support-guide', compact('support_guide'));
    }
}
