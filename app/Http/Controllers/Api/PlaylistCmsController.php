<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\PlaylistCmsResource;
use App\PlaylistCms;
use App\Repositories\PlayListRepo;
class PlaylistCmsController extends Controller
{
    //
    protected $playListRepo;
    


    public function __construct(PlayListRepo $playListRepo) {
       
        $this->playListRepo = $playListRepo;
        
    }
    public function index(Request $request)
    {
    	$userid = $request->get('user_id');
    	$playlists = PlaylistCms::where('cms_type','=','playlists')->get();
    	$playlist=[];
      
      foreach($playlists as $list)
        {
        $playlist[] =  [
            'id' => $list->id,
            'type' => $list->cms_type,
            'playlist' => $this->playListRepo->getPlaylistApi($list->cms_playlists,$userid),
              
             
          ];
      }
    	
    	$result = [
                
                'data' => $playlist
            ];
        return response()->json($result);
    	
    }
}
