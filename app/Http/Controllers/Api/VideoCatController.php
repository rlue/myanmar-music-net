<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\VideoCatResource;
use App\VideoCategory;
class VideoCatController extends Controller
{
    //
    public function index()
    {
    	return  VideoCatResource::collection(VideoCategory::get());
    }
}
