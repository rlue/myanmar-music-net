<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\SongCatResource;
use App\SongCategory;
class SongCatController extends Controller
{
    //
    public function index()
    {
    	return  SongCatResource::collection(SongCategory::get());
    }
}
