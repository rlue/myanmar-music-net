<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\AlbumlistCmsResource;
use App\AlbumlistCms;
use App\Repositories\AlbumListRepo;
class AlbumlistCmsController extends Controller
{
    //
    protected $albumListRepo;
    


    public function __construct(AlbumListRepo $albumListRepo) {
       
        $this->albumListRepo = $albumListRepo;
        
    }

    public function index(Request $request)
    {
    	
    	$userid = $request->get('user_id');
    	$playlists = AlbumlistCms::where('cms_type','=','collection')->get();
    	$playlist=[];
      
      foreach($playlists as $list)
        {
        $playlist[] =  [
            'id' => $list->id,
            'type' => $list->cms_type,
            'playlist' => $this->albumListRepo->getAlbumListApi($list->cms_albumlists,$userid),
              
             
          ];
      }
    	
    	$result = [
                
                'data' => $playlist
            ];
        return response()->json($result);
    	
    
    }
}
