<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\NewsCatResource;
use App\NewCategory;
class NewsCatController extends Controller
{
    //
    public function index()
    {
    	return  NewsCatResource::collection(NewCategory::get());
    }
}
