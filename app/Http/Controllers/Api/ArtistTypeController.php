<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\ArtistTypeResource;
use App\ArtistType;
class ArtistTypeController extends Controller
{
    //
    public function index()
    {
    	return  ArtistTypeResource::collection(ArtistType::get());
    }
}
