<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\VideoCmsResource;
use App\VideoCms;
class VideoCmsController extends Controller
{
    //
    public function getNewRelease()
    {
    	return  VideoCmsResource::collection(VideoCms::where('cms_type','=','new_release')->get());
    }

    public function getPlaylist()
    {
    	return  VideoCmsResource::collection(VideoCms::where('cms_type','=','playlists')->get());
    }
}
