<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\AlbumResource;
use App\Repositories\AlbumRepo;
use App\Repositories\SongRepo;
use App\AlbumView;
use App\Album;
use App\Transformers\ArtistResource;
class AlbumController extends Controller
{
    //
    protected $albumRepo;
    protected $songRepo;
    

    public function __construct(AlbumRepo $albumRepo,SongRepo $songRepo) {
       
        $this->songRepo = $songRepo;
        $this->albumRepo = $albumRepo;
        
    }

    public function index(Request $request)
    {
    	$id = $request->get('user_id');
        $albumlist =Album::where('status','=','publish')->orderBy('id','desc')->paginate(10);
        $albums = $this->albumRepo->getAlbumApi($albumlist,$id);
        $result = [
                'numberPage' => $albumlist->lastPage(),
                'data' => $albums
            ];
        return response()->json($result);
    }
    public  function albumDetail(Request $request)
    {
        $id = $request->get('album_id');
        $userid = $request->get('user_id');
        $list = Album::find($id);
       
        $count =AlbumView::select('view')->where('album_id','=',$id)->first();
        if(!$count){
            $count['view'] = 0;
        }  
        $albums  =  [
            'id' => $id,
            'sub_menu' => $list->sub_menu,
            'name_mm' => $list->album_name_mm,
            'name_eng' => $list->album_name_eng,
            'prices' => $list->album_prices,
            'image' => $list->album_image,
            'song' => $this->songRepo->getSongSortApi($list->songs,$id),
            'artist' => ArtistResource::collection($list->artists),
            'feat' => $list->feats,
            'scheduled_date' => $list->scheduled_date,
            'release_date' => $list->release_date,
            'copy_right' => $list->copy_right,
            'status'    => $list->status,
            'favourites' => $list->checkFavourite($list->id,$userid),
            'bought' => $list->checkOrder($list->id,$userid),
            'view_count' => $count['view']
           
        ];
        $result = [
            
                'data' => $albums
            ];
        return response()->json($result);
    }

    public function getwithAggregator(Request $request)
    {
    	$id = $request->get('aggregator_id');
    	
        $userid = $request->get('user_id');
        $albumlist =Album::where('status','=','publish')->where('aggregator_id','=',$id)->orderBy('id','desc')->paginate(20);
        $albums = $this->albumRepo->getAlbumApi($albumlist,$userid);
        $result = [
                'numberPage' => $albumlist->lastPage(),
                'data' => $albums
            ];
        return response()->json($result);
    }

    public function playCount(Request $request)
    {
        $this->validate($request, [
            'album_id' => 'required'
            
        ]);
        $id = $request->get('album_id');
        AlbumView::track($id);
        
            $data['data'] = [
            'status' => 200,
            'message' => 'Success'

            ];
        

        return response()->json($data);
    }
}
