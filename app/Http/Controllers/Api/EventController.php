<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\EventResource;
use App\Event;
class EventController extends Controller
{
    //
    public function index()
    {
    	return  EventResource::collection(Event::get());
    }
}
