<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\SliderResource;
use App\Slider;

class SliderController extends Controller
{
    //

    public function index()
    {
    	return  SliderResource::collection(Slider::get());
    }
}
