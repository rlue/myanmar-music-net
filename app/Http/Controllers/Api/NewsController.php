<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\NewsResource;
use App\News;
class NewsController extends Controller
{
    //
    public function index()
    {
    	return  NewsResource::collection(News::get());
    }
}
