<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\AlbumResource;
use App\Album;
use App\Transformers\SongResource;
use App\Song;
use App\Transformers\ArtistResource;
use App\Artist;
use App\Repositories\SongRepo;
use App\Repositories\AlbumRepo;
use App\Transformers\VideoResource;
use App\Video;
class SearchController extends Controller
{
    //
    protected $songRepo;
    protected $albumRepo;
    

    public function __construct(SongRepo $songRepo,AlbumRepo $albumRepo) {
       
        $this->songRepo = $songRepo;
        $this->albumRepo = $albumRepo;
        
    }

    public function index(Request $request)
    {
    	$search = $request->get('search');
        $userid = $request->get('user_id');
    	$songlist = Song::where('name_eng', 'LIKE', '%'. $search .'%')
            ->orWhere('name_mm', 'LIKE', '%'. $search .'%')
            ->limit(10)
            ->get();
        $songs = $this->songRepo->getSongApi($songlist,$userid);
        $artists = Artist::where('artist_name_mm', 'like', '%'. $search .'%')
            ->orWhere('artist_name_eng', 'like', '%'. $search .'%')
            ->limit(10)
            ->get();

        $albumlist = Album::where('album_name_mm', 'LIKE', '%'. $search .'%')
            ->orWhere('album_name_eng', 'LIKE', '%'. $search .'%')
            ->limit(10)
            ->get();
        $albums = $this->albumRepo->getAlbumApi($albumlist,$userid); 
        $videolist = Video::where('name_mm', 'LIKE', '%'. $search .'%')
            ->orWhere('name_eng', 'LIKE', '%'. $search .'%')
            ->limit(10)
            ->get();  
        $data['data'] = [
        'song' => $songs,
        'artist' => ArtistResource::collection($artists),
        'album' => $albums,
        'video' => VideoResource::collection($videolist),
        ];

       return response()->json($data);
    }
}
