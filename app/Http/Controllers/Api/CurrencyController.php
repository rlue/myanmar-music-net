<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Currency;
class CurrencyController extends Controller
{
    //
    
      public function index()
      {
      	$currency = Currency::orderBy('created_at', 'desc')->first();
        $last = Currency::orderBy('created_at', 'desc')->skip(1)->take(1)->first();
        
    	$result = [
    			'last_currency' => $last,
                'data' => $currency
            ];
        return response()->json($result);
      }
}
