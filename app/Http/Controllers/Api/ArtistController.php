<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\ArtistResource;
use App\Artist;
use App\ArtistCategory;
class ArtistController extends Controller
{
    //
    public function index()
    {
    	return  ArtistResource::collection(Artist::get());
    }

    public function getWithCategory(Request $request)
    {
    	$id = $request->get('category_id');
    	$artist = Artist::where('artistCategory_id','=',$id)->paginate(10);
    	return  ArtistResource::collection($artist);
    }
}
