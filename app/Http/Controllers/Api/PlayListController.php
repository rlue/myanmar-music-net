<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\PlayListResource;
use App\PlaylistView;
use App\PlayList;
use App\Repositories\PlayListRepo;
class PlayListController extends Controller
{
    //
    protected $playListRepo;
    


    public function __construct(PlayListRepo $playListRepo) {
       
        $this->playListRepo = $playListRepo;
        
    }
    public function index(Request $request)
    {
    	$userid = $request->get('user_id');
    	$playlists = PlayList::where('status','=','publish')->get();
    	$playlist = $this->playListRepo->getPlaylistApi($playlists,$userid);
    	$result = [
                
                'data' => $playlist
            ];
        return response()->json($result);
    }

    public function playCount(Request $request)
    {
        $this->validate($request, [
            'playlist_id' => 'required'
            
        ]);
        $id = $request->get('playlist_id');
        PlaylistView::track($id);
        
            $data['data'] = [
            'status' => 200,
            'message' => 'Success'

            ];
        

        return response()->json($data);
    }
}
