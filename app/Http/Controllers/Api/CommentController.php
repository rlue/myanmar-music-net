<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CommentRepo;
use App\Transformers\CommentResource;
class CommentController extends Controller
{
    //
    protected $commentRepo;

    public function __construct(CommentRepo $commentRepo) {
       
        $this->commentRepo = $commentRepo;
    }

    public function index()
    {
    	$comment = $this->commentRepo->getComment();
    	$data['data'] = $comment;
    	return response()->json($data);
    }

    public function save(Request $request)
    {	
    	$this->validate($request, [
            'comment_body' => 'required'
            
        ]);
    	$input['comment_body'] = $request->get('comment_body');
    	$input['comment_parent'] = $request->get('comment_parent');
    	$input['user_id'] = $request->get('user_id');
    	$input['song_id'] = $request->get('song_id');
    	$comment = $this->commentRepo->saveComment($input);
    	if($comment){
    		$data['data'] = [
                        'status' => '200',
                        'message' => 'Success ,comment success'
                    ];
    	}else{
    		$data['data'] = [
                        'status' => '400',
                        'message' => 'Cannot comment'
                    ];
    	}	
    	
        return response()->json($data);
    }

    public function edit(Request $request)
    {
    	$comment = $this->commentRepo->editComment($request);
    	return response()->json($comment);
    }

    public function update(Request $request)
    {	
    	$id = $request->get('comment_id');
    	$input['comment_body'] = $request->get('comment_body');
    	$input['comment_parent'] = $request->get('comment_parent');
    	$input['user_id'] = $request->get('user_id');
    	$input['song_id'] = $request->get('song_id');
    	$comment = $this->commentRepo->saveComment($input,$id);
    	if($comment){
    		$data['data'] = [
                        'status' => '200',
                        'message' => 'Success ,comment success'
                    ];
    	}else{
    		$data['data'] = [
                        'status' => '400',
                        'message' => 'Cannot comment'
                    ];
    	}	
    	
        return response()->json($data);
    }
    public function destory(Request $request)
    {

    	$comment = $this->commentRepo->deleteComment($request);
    	if($comment){
    		$data['data'] = [
                        'status' => '200',
                        'message' => 'Success ,delete success'
                    ];
    	}else{
    		$data['data'] = [
                        'status' => '400',
                        'message' => 'Fail ,delete comment'
                    ];
    	}
    	
        return response()->json($data);
    }
}
