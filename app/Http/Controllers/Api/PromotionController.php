<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\PromotionResource;
use App\Promotion;
class PromotionController extends Controller
{
    //
    public function index()
    {
    	return  PromotionResource::collection(Promotion::get());
    }
}
