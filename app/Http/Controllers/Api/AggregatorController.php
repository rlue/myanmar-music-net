<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\AggregatorResource;
use App\Aggregator;
class AggregatorController extends Controller
{
    //

    public function index()
    {
    	return  AggregatorResource::collection(Aggregator::get());
    }
}
