<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VersionController extends Controller
{
    //

    public function index()
    {
    	$data['data'] = [
            'version_no' => '2.2.4',
            'force_download' => 0

            ];
        

        return response()->json($data);
    }
}
