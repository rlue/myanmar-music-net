<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\VideoResource;
use App\Video;
class VideoController extends Controller
{
    //
    public function index()
    {
    	return  VideoResource::collection(Video::orderBy('id','desc')->paginate(20));
    }

    public function getwithCategory(Request $request)
    {
    	$id = $request->get('category_id');
    	return  VideoResource::collection(Video::where('video_category','=',$id)->orderBy('id','desc')->paginate(20));
    }
}
