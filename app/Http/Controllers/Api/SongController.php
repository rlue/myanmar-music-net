<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\SongResource;
use App\Song;
use App\Transformers\AlbumResource;
use App\Album;
use App\PlayCount;
use App\Recent;
use App\Order;
use App\Artist;
use App\Repositories\SongRepo;
use App\Repositories\AlbumRepo;
use DB;
class SongController extends Controller
{
    //
    protected $songRepo;
    protected $albumRepo;


    public function __construct(SongRepo $songRepo,AlbumRepo $albumRepo) {
       
        $this->songRepo = $songRepo;
        $this->albumRepo = $albumRepo;
    }
    public function index(Request $request)
    {
        $id = $request->get('user_id');
        $songlist =Song::orderBy('id','desc')->paginate(10);
        $songs = $this->songRepo->getSongApi($songlist,$id);
    	$result = [
                'numberPage' => $songlist->lastPage(),
                'data' => $songs
            ];
        return response()->json($result);
    }

    public function getwithFeeling(Request $request)
    {
    	$id = $request->get('feeling_id');
        $userid = $request->get('user_id');
    	$songlist = Song::where('feeling_id','=',$id)->orderBy('id','desc')->paginate(20);
        $songs = $this->songRepo->getSongApi($songlist,$userid);
        $result = [
                'numberPage' => $songlist->lastPage(),
                'data' => $songs
            ];
        return response()->json($result);
    }

    public function getwithGenre(Request $request)
    {
    	$id = $request->get('genre_id');
        $userid = $request->get('user_id');
    	$songlist = Song::where('genre_id','=',$id)->orderBy('id','desc')->paginate(20);
        $songs = $this->songRepo->getSongApi($songlist,$userid);
        $result = [
                'numberPage' => $songlist->lastPage(),
                'data' => $songs
            ];
        return response()->json($result);
    }

    public function getwithArtist(Request $request)
    {
        $id = $request->get('artist_id');
        $userid = $request->get('user_id');
        $songlist = Song::join('song_artists', 'song_artists.song_id', '=', 'songs.id')->where('song_artists.artist_id','=',$id)->orderBy('id','desc')->paginate(20);
        
        $songs = $this->songRepo->getSongApi($songlist,$userid);
        $result = [
                'numberPage' => $songlist->lastPage(),
                'data' => $songs
            ];
        return response()->json($result);
    }

    public function getwithRecent(Request $request)
    {
        $id = $request->get('user_id');
        $recent = DB::table('recents')->where('user_id','=',$id)->get();
        if($recent->isEmpty()){
            $songs = [];
        }else{
          //$songid[]='';
        foreach($recent as $c)
        {
            $songid[] = $c->song_id;
        }        
        $songlist = Song::whereIn('id',$songid)
        ->orderBy('songs.id','desc')->limit(20)->get();
        $songs = $this->songRepo->getSongApi($songlist,$id);  
        }
        
        $result = [
                'data' => $songs
            ];
        return response()->json($result);  
    }

    public function getwithFavourite(Request $request)
    {
        $id = $request->get('user_id');
        $recent = DB::table('favourites')
        ->where('type','=','song')
        ->where('user_id','=',$id)->get();
        if($recent->isEmpty()){
            $songlist = [];
        }else{
            foreach($recent as $c)
            {
                $songid[] = $c->item_id;
            }        
            $song = Song::whereIn('id',$songid)
            ->orderBy('songs.id','desc')->paginate(10);
            $songlist = $this->songRepo->getSongApi($song,$id);
        }
        
        $falbum = DB::table('favourites')
        ->where('type','=','album')
        ->where('user_id','=',$id)->get();
        if($falbum->isEmpty()){
            $albumlist = [];
        }else{
           foreach($falbum as $f)
            {
                $albumid[] = $f->item_id;
            }        
            $album = Album::where('status','=','publish')->whereIn('id',$albumid)
            ->orderBy('albums.id','desc')->paginate(10); 
            $albumlist = $this->albumRepo->getAlbumApi($album,$id);
        }
        
        $data['data'] = [
        'song_totalpage' => $song->lastPage(),
        'songs' => $songlist,
        'album_totalpage' => $album->lastPage(),
        'albums' => $albumlist
        ];
        return response()->json($data);
    }

    public function recentSong(Request $request)
    {
        $this->validate($request, [
            'song_id' => 'required',
            'user_id' => 'required',
            
        ]);
        $songid = $request->get('song_id');
        $userid = $request->get('user_id');
        $recent = Recent::updateOrCreate(['song_id' => $songid, 'user_id' => $userid])->touch();
        if($recent){
            $data['data'] = [
            'status' => 200,
            'message' => 'Success'
            ];
        }else{
            $data['data'] = [
            'status' => 400,
            'message' => 'fail'
            ];
        }

        return response()->json($data);
    }

    public function removeFavouriteSong(Request $request)
    {
        $this->validate($request, [
            'item_id' => 'required',
            'user_id' => 'required',
            'type' => 'required',
            
        ]);
        $input['item_id'] = $request->get('item_id');
        $input['user_id'] = $request->get('user_id');
        $input['type'] = $request->get('type');
        $recent = DB::table('favourites')->where($input)->delete();
        if($recent){
            $data['data'] = [
            'status' => 200,
            'message' => 'Success'
            ];
        }else{
            $data['data'] = [
            'status' => 400,
            'message' => 'fail'
            ];
        }

        return response()->json($data);
    }
    public function favouriteSong(Request $request)
    {
        $this->validate($request, [
            'item_id' => 'required',
            'user_id' => 'required',
            'type' => 'required',
            
        ]);
        $input['item_id'] = $request->get('item_id');
        $input['user_id'] = $request->get('user_id');
        $input['type'] = $request->get('type');
        $input['created_at'] = date('Y-m-d H:i:s');
        $input['updated_at'] = date('Y-m-d H:i:s');
        $recent = DB::table('favourites')->insert($input);
        if($recent){
            $data['data'] = [
            'status' => 200,
            'message' => 'Success'
            ];
        }else{
            $data['data'] = [
            'status' => 400,
            'message' => 'fail'
            ];
        }

        return response()->json($data);
    }

     public function getwithGender(Request $request)
    {
        $userid = $request->get('user_id');
        $genderid = $request->get('gender_id');
        $genderlist = Artist::where('artistCategory_id','=',$genderid)->get();
        if($genderlist->isEmpty()){
            $songlist = [];
            $albumlist = [];
        }else{
            foreach($genderlist as $c)
            {
                $songid[] = $c->id;
            }        
            $song = Song::whereIn('id',$songid)
            ->orderBy('songs.id','desc')->paginate(10);
            $songlist = $this->songRepo->getSongApi($song,$userid);

            $album = Album::where('status','=','publish')->whereIn('id',$songid)
            ->orderBy('albums.id','desc')->paginate(10); 
            $albumlist = $this->albumRepo->getAlbumApi($album,$userid);
        }
        
              
            
        
        
        $data['data'] = [
        'song_totalpage' => $song->lastPage(),
        'songs' => $songlist,
        'album_totalpage' => $album->lastPage(),
        'albums' => $albumlist
        ];
        return response()->json($data);
    }

    public function playCount(Request $request)
    {
        $this->validate($request, [
            'song_id' => 'required'
            
        ]);
        $id = $request->get('song_id');
        PlayCount::track($id);
        
            $data['data'] = [
            'status' => 200,
            'message' => 'Success'
            ];
        

        return response()->json($data);
    }

    public  function getOrder(Request $request)
    {
        $id = $request->get('user_id');
        $order = Order::where('user_id','=',$id)->where('type','song')->get();
        if($order->isEmpty()){
            $songlist=[];
        }else{
            foreach($order as $song)
            {
                $songid[] = $song->item_id;
            }
            $song = Song::whereIn('id',$songid)
            ->orderBy('songs.id','desc')->paginate(20);
            $songlist = $this->songRepo->getSongApi($song,$id);
        }
        
        $albumorder = Order::where('user_id','=',$id)->where('type','album')->get();
        if($albumorder->isEmpty()){
            $albumlist = [];
        }else{
            foreach($albumorder as $album)
            {
                $albumid[] = $album->item_id;
            }
            $album = Album::where('status','=','publish')->whereIn('id',$albumid)
            ->orderBy('albums.id','desc')->paginate(20);
            $albumlist = $this->albumRepo->getAlbumApi($album,$id);
        }
        
        $data['data'] = [
        'songs' => $songlist,
        'albums' => $albumlist
        ];
        return response()->json($data);
    }

    public function getFreeSong(Request $request)
    {
        $id = $request->get('user_id');
        $albums = Album::where('sub_menu->free', true)->where('status', 'publish')->paginate(10);
        $albumlist = $this->albumRepo->getAlbumApi($albums,$id);
         
        $data['data'] = [
        'total_page' => $albums->lastPage(),
        'albums' => $albumlist
        ];
        return response()->json($data);
    }

    public function getWeeklyTop(Request $request)
    {
        $userid = $request->get('user_id');
        $song = Song::join('play_counts', 'songs.id', '=', 'play_counts.song_id')
            ->orderBy('click_count', 'desc')
            ->limit(20)->get();

        $song = $song->sortBy('click_count');

        $songlist = $this->songRepo->getSongApi($song,$userid);
         $data['data'] = [
        'songs' => $songlist
        ];
        return response()->json($data);
    }
 }
