<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\AlbumCmsResource;
use App\AlbumCms;
use App\Repositories\AlbumRepo;
use App\AlbumView;
use App\Album;
class AlbumCmsController extends Controller
{
    //
    protected $albumRepo;
    


    public function __construct(AlbumRepo $albumRepo) {
       
        $this->albumRepo = $albumRepo;
        
    }

    public function getNewRelease(Request $request)
    {
    	
    	$userid = $request->get('user_id');
    	$playlists = AlbumCms::where('cms_type','=','new_release')->get();
    	$playlist=[];
      
      foreach($playlists as $list)
        {
        $playlist[] =  [
            'id' => $list->id,
            'type' => $list->cms_type,
            'album' => $this->albumRepo->getAlbumApi($list->cms_albums,$userid),
              
             
          ];
      }
    	
    	$result = [
                
                'data' => $playlist
            ];
        return response()->json($result);
    }

    public function getTopAlbum(Request $request)
    {
        $userid = $request->get('user_id');
        
        $albumlist =Album::join('album_views', 'albums.id', '=', 'album_views.album_id')
            ->where('scheduled_date', '<', date('Y-m-d H:i:s'))
            ->where('status','=','publish')
            ->orderBy('view', 'desc')
            ->limit(20)
            ->get();
        $albums = $this->albumRepo->getAlbumApi($albumlist,$userid);
        $result = [
                'data' => $albums
            ];
        return response()->json($result);
    }
}
