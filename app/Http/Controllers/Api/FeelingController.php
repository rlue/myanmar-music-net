<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\FeelingResource;
use App\Feeling;
class FeelingController extends Controller
{
    //

    public function index()
    {
    	return  FeelingResource::collection(Feeling::get());
    }

    
}
