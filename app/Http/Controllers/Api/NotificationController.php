<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\NotificationResource;
use App\Notification;
class NotificationController extends Controller
{
    //
    public function index()
    {
    	return  NotificationResource::collection(Notification::get());
    }
}
