<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\AlbumListResource;
use App\AlbumList;
use App\Repositories\AlbumListRepo;
class AlbumListController extends Controller
{
    //
    protected $albumListRepo;
    


    public function __construct(AlbumListRepo $albumListRepo) {
       
        $this->albumListRepo = $albumListRepo;
        
    }
    public function index(Request $request)
    {
    	$userid = $request->get('user_id');
    	$playlists = AlbumList::get();
    	$playlist = $this->albumListRepo->getAlbumlistApi($playlists,$userid);
    	$result = [
                
                'data' => $playlist
            ];
        return response()->json($result);
    	
    }
}
