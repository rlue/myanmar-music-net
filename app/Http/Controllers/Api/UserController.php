<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Transformers\UserResource;
use App\Transformers\PaymentResource;
use App\User;
use App\PaymentOrder;
use App\Song;
use App\Album;
use App\Order;
use App\Subscribe;
use App\Social;
use App\SubscriptionPrice;
use Carbon\Carbon;
class UserController extends Controller
{
    //

    public function index()
    {

    }

    public function register(Request $request)
    {
    	$this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
            
        ]);
        
        $input['password'] = bcrypt($request->get('password'));
        $input['name'] = $request->get('name');
        $input['email'] = $request->get('email');
        $user = User::create($input);
        $user->addTrialSubscribe();
        $input['roles'] = ['2'];
        
            $user->roles()->attach($input['roles']);
        
        return new UserResource($user);
    }

    public function login(Request $request)
    {
    	$this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
            
        ]);
    	$input= $request->all();
        if (Auth::guard('web')->attempt(['email' => $input['email'], 'password' => $input['password']])) {
                if(Auth::user()){
                    return new UserResource(User::where('email','=',$input['email'])->first());
                }else{
                    $data = [
                    'status' => 0,
                    'message' => 'Login fail , email or password incorrect'
                ];
                return response()->json($data);
                }
            }else{
                $data = [
                    'status' => 0,
                    'message' => 'Login fail , email or password incorrect'
                ];
                return response()->json($data);   
            }
    }

    public function getPayment(Request $request)
    {
        $id = $request->get('user_id');
        $payment = PaymentOrder::where('user_id','=',$id)->get();
        return  PaymentResource::collection($payment);
    }
    
    public function buySong(Request $request)
    {
        $this->validate($request, [
            'song_id' => 'required',
            'user_id' => 'required',
            
        ]);
        $songid = $request->get('song_id');
        $userid = $request->get('user_id');

        $song = Song::find($songid);
        $price = $song->price;
        //return response()->json($price);
        $user = User::find($userid);
        if(isset($song) && isset($user)){
            $checkorder = $this->checkOrder($userid,$songid,'song');
            $checkAlbum = $this->checkOrder($userid,$song->albums->first()->id,'album');
            if(isset($checkorder) || isset($checkAlbum)){
                $data['data'] = [
                'status' => 401,
                'message' => "This song have been buy please check list"
                ];
            }else{
                if (empty($user->remained_amount)) {
                    $user->remained_amount = 0;
                }

                if ($price->name > $user->remained_amount) {
                    $data['data'] = [
                        'status' => '003',
                        'message' => 'You have insufficient amount to buy!'
                    ];
                }else{
                    $user->remained_amount = $user->remained_amount - $price->name;
                    $user->save();

                    $user->songs()->attach($songid, ['type' => 'song']);
                                
                    $data['data'] = [
                        'status' => '200',
                        'message' => 'Success ,thank you for buy'
                    ];
                }
            }
            
        }else{
            $data['data'] = [
                'status' => 400,
                'message' => "Sorry song not match"
            ];
        }
        return response()->json($data);
    }

    private function checkOrder($userid,$songid,$type)
    {
        $order = Order::where('item_id', $songid)->where('user_id',$userid )->where('type', $type)->first();
        return $order;
    }

    public function buyAlbum(Request $request)
    {
        $this->validate($request, [
            'album_id' => 'required',
            'user_id' => 'required',
            
        ]);
        $albumid = $request->get('album_id');
        $userid = $request->get('user_id');

        $album = Album::find($albumid);
        $prices = $album->prices;
        //return response()->json($prices);
        $user = User::find($userid);
        if(isset($album) && isset($user)){
            $checkorder = $this->checkOrder($userid,$albumid,'album');
            
            if(isset($checkorder)){
                $data['data'] = [
                'status' => 401,
                'message' => "This song have been buy please check list"
                ];
            }else{
                if (empty($user->remained_amount)) {
                    $user->remained_amount = 0;
                }

                if ($prices->name > $user->remained_amount) {
                    $data['data'] = [
                        'status' => '003',
                        'message' => 'You have insufficient amount to buy!'
                    ];
                }else{
                    $user->remained_amount = $user->remained_amount - $prices->name;
                    $user->save();

                    $user->albums()->attach($albumid, ['type' => 'album']);
                                
                    $data['data'] = [
                        'status' => '200',
                        'message' => 'Success ,thank you for buy'
                    ];
                }
            }
            
        }else{
            $data['data'] = [
                'status' => 400,
                'message' => "Sorry song not match"
            ];
        }
        return response()->json($data);
    }

    public function editUser(Request $request)
    {
        $id = $request->get('user_id');
        $user = User::find($id);
        return new UserResource($user);
    }

    public function updateUser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required',
            'streaming_quality' => 'required|integer',
            
        ]);
        $id = $request->get('user_id');
        $input['name'] = $request->get('name');
        $input['phone'] = $request->get('phone');
        $input['streaming_quality'] = $request->get('streaming_quality');
        
        $user = User::where('id','=',$id)->update($input);
        if(isset($user)){
            $data = [
                    'status' => 200,
                    'message' => 'Update Success'
                ];
        }else{
            $data = [
                    'status' => 400,
                    'message' => 'Update Fial'
                ];
        }
        return response()->json($data); 
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'old_password' => 'required|password_hash_check',
            'password' => 'required|confirmed|min:6|max:50|different:old_password',
            'password_confirmation' => 'required',
            
        ]);

        $id = $request->get('user_id');
        $user = User::find($id);
        $user->password = bcrypt($request->get('password'));
        $user->save();
        if($user){
            $data = [
                    'status' => 200,
                    'message' => 'Success password change'
                ];
        }else{
            $data = [
                    'status' => 400,
                    'message' => 'Fail password change'
                ];
        }
        return response()->json($data);
    }

    public function subscribe(Request $request){
        $id = $request->get('user_id');
        $user = User::find($id);
        $price = SubscriptionPrice::orderBy('id','asc')->first();
        if(isset($user)){
            if ($user->remained_amount >= $price->name) {
                // $checkSubscribe = Subscribe::find($id);
                // if(!$checkSubscribe){
                //     $user->addTrialSubscribe();
                // }

            $user->addedSubscribe();

            $user->remained_amount = $user->remained_amount - $price->name;
            $user->save();
            $data = [
                    'status' => 200,
                    'message' => 'Subscribe Success'
                ];
            
        }else{
            $data = [
                    'status' => 400,
                    'message' => 'You have insufficient amount to subscribe!'
                ];
        }
        }else{
            $data = [
                    'status' => 400,
                    'message' => 'This user not found'
                ];
        }   
        
        return response()->json($data);
    }

    public function subscribeDetail(Request $request)
    {
        $id = $request->get('user_id');
        $user = User::find($id);
        $sub = $user->subscription();
        if(!empty($sub)){
            $price = SubscriptionPrice::find($sub->subscribe_price_id);
        foreach($sub->ends_at as $d){
                $da[] = $d;
        }
        $data['data'] = [
                    'price' => $price->name,
                    'expired_date' => $da[0]
                ];
        }else{
            $data['data'] = [
                    'price' => null,
                    'expired_date' => null
                ];
        }
        
        return response()->json($data);
    }
    public function newsubscribe(Request $request){
        $id = $request->get('user_id');
        $sub = $request->get('subscribe_id');
        $user = User::find($id);
        $price = SubscriptionPrice::find($sub);
        if(isset($user)){
            if ($user->remained_amount >= $price->name) {
                // $checkSubscribe = Subscribe::find($id);
                // if(!$checkSubscribe){
                //     $user->addTrialSubscribe();
                // }

            $user->addedSubscribe();

            $user->remained_amount = $user->remained_amount - $price->name;
            $user->save();
            $data = [
                    'status' => 200,
                    'message' => 'Subscribe Success'
                ];
            
        }else{
            $data = [
                    'status' => 400,
                    'message' => 'You have insufficient amount to subscribe!'
                ];
        }
        }else{
            $data = [
                    'status' => 400,
                    'message' => 'This user not found'
                ];
        }   
        
        return response()->json($data);
    }
    public function social(Request $request)
    {
        $id = $request->get('social_id');
        $email = $request->get('social_email');
        $name = $request->get('social_name');
        $provider = 'facebook';
        $socialUser = null;

        // check is this email present
        if($email == null){
            $userCheck = null;
        }else{
            $userCheck = User::where('email', '=', $email)->first();
        }

        

        $sameSocialId = Social::where('social_id', '=', $id)->where('provider', $provider )->first();

        if (!empty($userCheck)) {

            $socialUser = $userCheck;

            if (empty($sameSocialId)) {
                $socialData = new Social;
                $socialData->user_id   = $socialUser->id;
                $socialData->social_id = $id;
                $socialData->provider  = $provider;
                $socialData->save();
            }

        } else {
            // new user
            if (empty($sameSocialId)) {

                $newSocialUser                    = new User;
                $newSocialUser->email             = $email;
                $newSocialUser->name              = $name;
                $newSocialUser->email_verified_at = Carbon::now();
                $newSocialUser->password          = null;
                $newSocialUser->save();

                $newSocialUser->attachRole('normal');

                $socialData            = new Social;
                $socialData->user_id   = $newSocialUser->id;
                $socialData->social_id = $id;
                $socialData->provider  = $provider;
                $socialData->save();

                $socialUser = $newSocialUser;

                $newSocialUser->addTrialSubscribe();

            } else {

                //Load this existing social user
                $socialUser = $sameSocialId->user;
                $socialUser->email = $email;
                $socialUser->save();
            }

        }

       
        return new UserResource($socialUser);
    }
}
