<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\VideoPlaylistResource;
use App\VideoPlaylist;
class VideoPlaylistController extends Controller
{
    //
    public function index()
    {
    	return  VideoPlaylistResource::collection(VideoPlaylist::get());
    }
}
