<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\ArtistCatResource;
use App\ArtistCategory;
class ArtistCatController extends Controller
{
    //

    public function index()
    {
    	return  ArtistCatResource::collection(ArtistCategory::get());
    }
}
