<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\AlbumPriceResource;
use App\Transformers\SongPriceResource;
use App\Transformers\SubPriceResource;
use App\AlbumPrice;
use App\Price;
use App\SubscriptionPrice;
class PriceController extends Controller
{
    //

	public function albumprice()
    {
    	return  AlbumPriceResource::collection(AlbumPrice::get());
    }

    public function songprice()
    {
    	return  SongPriceResource::collection(Price::get());
    }

    public function subscriptionprice()
    {
    	return  SubPriceResource::collection(SubscriptionPrice::get());
    }
}
