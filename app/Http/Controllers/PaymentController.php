<?php

namespace App\Http\Controllers;

use App\Http\Requests\CodaPayRequest;
use App\Http\Requests\MpuPayRequest;
use App\Payment\CodaPay\InitTxnRequest;
use App\Payment\CodaPay\ItemInfo;
use App\Payment\CodaPay\Profile;
use App\PaymentOrder;
use App\Payment\CodaPay\Item as CodaItem;
use CodaPay;
use Session;

class PaymentController extends Controller
{

    public function showPayment() {
        return view('pages.payment');
    }


    public function payWithCodaPay(CodaPayRequest $request) {

        $itemList = [];
        $item = new CodaItem();
        $item->setName('m-'. $request->get('amount'))
            ->setCode('m-coda')
            ->setPrice((double) $request->get('amount'));

        $profile = new Profile();
        $profile->setUserId(auth()->id())->setEmail(auth()->user()->email);

        array_push ($itemList, $item);

        $request = new InitTxnRequest();
        $request->setOrderId((string) round(microtime(true) * 1000))
            ->setItems($itemList)
            ->setProfile($profile);

        $result = CodaPay::initTxn($request);

        PaymentOrder::create([
            'user_id' => auth()->id(),
            'transaction_id' => $result->txnId,
            'payment_service' => 'codapay',
            'amount' => request('amount'),
        ]);

        /** add payment ID to session **/
        Session::put('codapay_txn_id', $result->txnId);

        return $result->txnId;
    }

    protected function create_signature_string($input_fields_array)
    {
        sort($input_fields_array, SORT_STRING);

        $signature_string = "";
        foreach($input_fields_array as $value)
        {
            if ($value != "")
            {
                $signature_string .= $value;
            }
        }

        return $signature_string;
    }


    public function payWithMPU(MpuPayRequest $request) {

        $merchant_id 		= config('mpu.merchant_id');
        $product_desc       = 'mmn';
        $order_id           = uniqid();
        $invoice_no 		= $order_id;
        $currency 			= config('mpu.currency');

        $amount             = str_pad($request->get('amount') . '00', 12, '0', STR_PAD_LEFT);
        $user_defined_1 	= auth()->id();
        $user_defined_2 	= '';
        $user_defined_3 	= '';
        $secret_key 		= config('mpu.secret');

        $input_fields_array = [
            $merchant_id , $product_desc , $invoice_no , $currency , $amount , $user_defined_1 , $user_defined_2 , $user_defined_3
        ];

        $signature_string = $this->create_signature_string($input_fields_array);


        $HashValue = hash_hmac('sha1', $signature_string, $secret_key, false);
        $HashValue = strtoupper($HashValue);

        $data = [
            'url'                   => config('mpu.url'),
            'merchant_id' 			=> $merchant_id,
            'product_desc' 	        => $product_desc,
            'invoice_no' 			=> $order_id,
            'currency' 				=> $currency,
            'amount' 				=> $amount,
            'user_defined_1' 		=> $user_defined_1,
            'user_defined_2' 		=> $user_defined_2,
            'user_defined_3' 		=> $user_defined_3,
            'HashValue' 			=> $HashValue
        ];

        PaymentOrder::create([
            'user_id' => auth()->id(),
            'transaction_id' => $order_id,
            'payment_service' => 'mpu',
            'amount' => request('amount'),
        ]);

        return response()->json($data);

    }
}
