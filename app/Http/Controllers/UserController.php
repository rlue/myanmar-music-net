<?php

namespace App\Http\Controllers;

use App\Album;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UpdateAccountInfo;
use App\Http\Requests\UpdateStreamingQuality;
use App\Order;
use Auth;
use GuzzleHttp\Exception\RequestException;
use Log;
use Exception;
use App\Song;
use GuzzleHttp\Client;

class UserController extends Controller
{

    /**
     * Display the user's info page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function profile()
    {
        return view('pages.profile');
    }


    /**
     * Display the user's profile edit page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function showProfileEditPage()
    {
        return view('pages.edit-profile');
    }


    /**
     * Update the user's info
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function editProfile(UpdateAccountInfo $request)
    {
        Auth::user()->update($request->validated());

        return redirect('user/profile')->with('status', 'success')->with('message', 'Account Information updated!');
    }


    /**
     * Display the change password page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function showChangePasswordPage()
    {
        return view('pages.change-password');
    }


    /**
     * Change the auth user's password.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function passwordChange(ChangePasswordRequest $request)
    {
        $user = Auth::user();
        $user->password = bcrypt($request->get('password'));
        $user->save();

        return redirect('user/profile')->with('status', 'success')->with('message', 'Password updated!');
    }


    /**
     * User subscribe
     * @return \Illuminate\Http\RedirectResponse
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function subscribe()
    {
        $user = Auth::user();

        if ($user->remained_amount >= 1500) {
            $user->addedSubscribe();

            $user->remained_amount = $user->remained_amount - 1500;
            $user->save();

            return redirect('user/profile')->with('status', 'success')->with('message', 'Subscribe!');
        }

        return redirect('user/profile')->with('status', 'error')->with('message', 'You have insufficient amount to subscribe!');
    }


    /**
     * Update the user's streaming quality
     *
     * @param UpdateStreamingQuality $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function updateStreamingQuality(UpdateStreamingQuality $request)
    {
        Auth::user()->update($request->validated());

        return redirect()->back()->with('status', 'success')->with('message', 'Streaming Quality updated!');
    }


    /**
     * User favourite song.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function favouriteSong($id)
    {
        $song = Song::findOrFail($id);
        $has = $song->favourite()->where('user_id', Auth::id())->where('type', 'song')->exists();

        if ($has) {
            $song->favourite()->detach(Auth::id(), ['type' => 'song']);
            return response()->json([
                'message' => 'This song is removed to your favourite song list',
                'status' => 'unfavoured',
            ]);
        }

        $song->favourite()->attach(Auth::id(), ['type' => 'song']);

        return response()->json([
            'message' => 'This song is added to your favourite song list. You can check it in your favourite list.',
            'status' => 'favoured',
        ]);
    }


    /**
     * User favourite album.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function favouriteAlbum($uuid)
    {
        $album = Album::where('uuid', $uuid)->firstOrFail();
        $has = $album->favourite()->where('user_id', Auth::id())->where('type', 'album')->exists();

        if ($has) {
            return response()->json([ 'message' => 'This album is already added to your favourite list. You can check it in your favourite list.' ]);
        }
        $album->favourite()->attach(Auth::id(), ['type' => 'album']);

        return response()->json([ 'message' => 'This album is added to your favourite list. You can check it in your favourite list.' ]);
    }


    /**
     * Remove the user's favourite song.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function deleteFavouriteSong($id)
    {
        $song = Song::findOrFail($id);
        $doesntExist = $song->favourite()->where('user_id', Auth::id())->where('type', 'song')->doesntExist();

        if ($doesntExist) {
            return response()->json([ 'status' => false , 'message' => 'This song is already removed to your favourite list.' ]);
        }

        $song->favourite()->detach(Auth::id(), ['type' => 'song']);

        return response()->json([ 'status' => true ,'message' => 'This song is removed to your favourite list.' ]);
    }


    /**
     * Remove the user's favourite album.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function deleteFavouriteAlbum($uuid)
    {
        $album = Album::where('uuid', $uuid)->firstOrFail();
        $doesntExist = $album->favourite()->where('user_id', Auth::id())->where('type', 'album')->doesntExist();

        if ($doesntExist) {
            return response()->json([ 'status' => false ,'message' => 'This album is already removed to your favourite list.' ]);
        }
        $album->favourite()->detach(Auth::id(), ['type' => 'album']);

        return response()->json([ 'status' => true , 'message' => 'This album is removed to your favourite list' ]);
    }


    /**
     * Display the user's favourite page ( Songs And Albums).
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function showUserFav()
    {
        $songs = Auth::user()->favourite_songs()->with('albums','artists','play_counts')->limit(5)->get();
        $albums = Auth::user()->favourite_albums()->with('songs', 'artists','prices')->limit(5)->get();

        return view('pages.favourite', compact('songs', 'albums'));
    }


    /**
     * Display the user's favourite songs page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function showUserFavSongs()
    {
        $songs = Auth::user()->favourite_songs()->with('albums','artists','play_counts')->get();

        return view('pages.favourite-songs', compact('songs'));
    }


    /**
     * Display the user's favourite albums page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function showUserFavAlbums()
    {
        $albums = Auth::user()->favourite_albums()->with('artists','songs','prices')->get();

        return view('pages.favourite-albums', compact('albums'));
    }


    /**
     * User like album.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function likeAlbum($uuid)
    {
        $album = Album::where('uuid', $uuid)->firstOrFail();
        $has = $album->likes()->where('user_id', Auth::id())->exists();

        if ($has) {
            $album->likes()->detach(Auth::id());
            return response()->json([
                'status' => 'Like',
            ]);
        }

        $album->likes()->attach(Auth::id());

        return response()->json([
            'status' => 'Liked',
        ]);
    }


    /**
     * Display the user's recent page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function showRecentPage()
    {
        $recent_songs = Auth::user()->recent_songs()->orderByDesc('created_at')->limit(100)
            ->with(['artists','feats','auth_user_favourite','albums','price', 'auth_user_order'])->limit(100)->get();

        return view('pages.recent', compact('recent_songs'));
    }


    /**
     * Display the user's purchased page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function purchasedPage()
    {
        $songs = Auth::user()->songs()
            ->with(['albums','artists','auth_user_favourite', 'auth_user_order'])
            ->limit(5)->orderByDesc('created_at')->get();

        $albums = Auth::user()->albums()
            ->with('artists')
            ->limit(5)->orderByDesc('created_at')->get();


        return view('pages.purchased.index', compact('songs', 'albums'));
    }


    /**
     * Display the user's purchased songs page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function showUserPurchasedSongs()
    {
        $songs = Auth::user()->songs()->with(['albums','artists','auth_user_favourite'])->orderByDesc('created_at')->get();

        return view('pages.purchased.songs', compact('songs'));
    }


    /**
     * Display the user's purchased albums page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function showUserPurchasedAlbums()
    {
        $albums = Auth::user()->albums()->with('artists')->orderByDesc('created_at')->get();

        return view('pages.purchased.albums', compact('albums'));
    }


    /**
     * Display the user's purchased albums detail page.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function showUserPurchasedAlbum($id)
    {
        $album = Auth::user()->albums()
            ->with(['songs', 'songs.artists', 'artists', 'songs.auth_user_favourite', 'songs.auth_user_order'])
            ->where('albums.id', $id)->first();

        $songs = $album->songs;

        return view('pages.purchased.album', compact('album', 'songs'));
    }


    /**
     * Buy the song.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function buySong() {
        $song = Song::with('price')->findOrFail(request('id'));

        $price = $song->price;

        $user = Auth::user();

        if (is_null($price) && $price->name < 0) {
            return response()->json([
                'status' => '001',
                'message' => 'Unauthorized action'
            ], 403);
        }

        $order = Order::where('item_id', $song->id)->where('user_id', Auth::id())->where('type', 'song')->first();

        $album_order = Order::where('item_id', $song->albums->first()->id)->where('user_id', Auth::id())->where('type', 'album')->first();

        $filename   = basename($song->full_file);

        if (!is_null($order) || !is_null($album_order)) {
            return response()->json([
                'status' => '002',
                'message' => 'You have already bought this music. You can download it from your purchased list.',
                'path' => $song->full_file,
                'filename' => $filename
            ], 413);
        }

        if (empty($user->remained_amount)) {
            $user->remained_amount = 0;
        }

        if ($price->name > $user->remained_amount) {
            return response()->json([
                'status' => '003',
                'message' => 'You have insufficient amount to buy!'
            ], 403);
        }

        $user->remained_amount = $user->remained_amount - $price->name;
        $user->save();

        $user->songs()->attach($song->id, ['type' => 'song']);

        return response()->json([
            'status' => '000',
            'message' => 'Buying Successful',
            'remained_amount' => $user->remained_amount,
            'id' => $song->id
        ]);
    }


    /**
     * Buy the album.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function buyAlbum() {
        $album = Album::with('prices')->where('uuid', request('id'))->firstOrFail();

        $price = $album->prices;

        $user = Auth::user();

        if (is_null($price) && $price->name > 0) {
            return response()->json([
                'status' => 'error',
                'message' => 'Unauthorized action'
            ], 403);
        }

        if (empty($user->remained_amount)) {
            $user->remained_amount = 0;
        }

        $order = Order::where('item_id', $album->id)->where('user_id', Auth::id())->where('type', 'album')->first();

        if (!is_null($order)) {
            return response()->json([
                'status' => '102',
                'message' => 'You have already bought this album. You can download it from your purchased list.',
                'path' => '/mymusic/purchased_album/' . $album->id,
            ], 413);
        }

        if ($price->name > $user->remained_amount) {
            return response()->json([
                'status' => '003',
                'message' => 'You have insufficient amount to subscribe!'
            ], 403);
        }

        $user->remained_amount = $user->remained_amount - $price->name;

        $user->albums()->attach($album->id, ['type' => 'album']);

        return response()->json([
            'status' => '100',
            'message' => 'Buying Successful',
            'remained_amount' => $user->remained_amount,
            'path' => '/mymusic/purchased_album/' . $album->id,
            'id' => $album->id
        ]);
    }


    /**
     * Download the user purchased song.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function downloadSong($id)
    {
        $song = Song::findOrFail($id);

        $order = Order::where('item_id', $id)->where('user_id', Auth::id())->where('type', 'song')->first();

        $album_order = Order::where('item_id', $song->albums->first()->id)->where('user_id', Auth::id())->where('type', 'album')->first();

        if (is_null($order) && is_null($album_order)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Unauthorized action'
            ], 413);
        }

        $filename = str_random(12) . '.mp3';

        if ($song->full_fileType === 'full_s3')
        {
            $filename   = basename($song->full_file);
        }elseif ($song->full_fileType === 'full_soundcloud')
        {
            $filename = $this->getSoundCloudFileName($song->full_file);
        }

        return response()->streamDownload(function () use($song){
            echo file_get_contents($song->full_file);
        }, $filename,[
            'Pragma' => 'public',
            'Expires' => 0,
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Cache-Control' => 'public',
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment; filename="'.$filename.'"',
            'Content-Transfer-Encoding' => 'binary'
        ]);

    }


    /**
     * Download the user purchased song by album.
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function downloadSongByAlbum($album_id, $song_id) {
        $order = Order::where('item_id', $album_id)->where('user_id', Auth::id())->where('type', 'album')->first();

        if (is_null($order)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Unauthorized action'
            ], 413);
        }

        $song = Song::findOrFail($song_id);

        $filename = str_random(12) . '.mp3';

        if ($song->full_fileType === 'full_s3')
        {
            $filename   = basename($song->full_file);
        }elseif ($song->full_fileType === 'full_soundcloud')
        {
            $filename = $song->getSoundCloudFileName();
        }

        return response()->streamDownload(function () use($song){
            echo file_get_contents($song->full_file);
        }, $filename,[
            'Pragma' => 'public',
            'Expires' => 0,
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Cache-Control' => 'public',
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment; filename="'.$filename.'"',
            'Content-Transfer-Encoding' => 'binary'
        ]);
    }


    private function getSoundCloudFileName($url) {
        $url = parse_url($url);

        $path = dirname($url['path']);

        $api = $url['scheme'] . '://' . $url['host'] . $path . '?' . $url['query'];

        $client = new Client();

        try {
            $response = $client->get($api);

        } catch (RequestException $exception) {

            if ($exception->hasResponse()) {
                Log::warning(json_decode($exception->getResponse()->getBody()->getContents(), true));
            }

            return str_random(12) . '.mp3';

        } catch (Exception $e) {

        }

        try {
            $soundcloud = json_decode($response->getBody(), true);

            $filename = $soundcloud['permalink'] . '.' . $soundcloud['original_format'];

        }catch (Exception $exception) {
            $filename = str_random(12) . '.mp3';
        }

        return $filename;
    }

}
