<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Song;
use App\Artist;
use App\Http\Requests\StoreSong;
use App\Repositories\SongRepo;
use App\Repositories\PriceRepo;
use App\Repositories\ArtistRepo;
use App\Repositories\FeelingRepo;
use App\Repositories\SongCatRepo;
use App\Repositories\VideoRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use File;

class SongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $songRepo;
    protected $songCatRepo;
    protected $feelingRepo;
    protected $artistRepo;
    protected $priceRepo;
    protected $videoRepo;

    public function __construct(SongRepo $songRepo,SongCatRepo $songCatRepo,ArtistRepo $artistRepo,FeelingRepo $feelingRepo,PriceRepo $priceRepo,VideoRepo $videoRepo) {
       
        $this->songRepo = $songRepo;
        $this->songCatRepo = $songCatRepo;
        $this->feelingRepo = $feelingRepo;
        $this->artistRepo = $artistRepo;
        $this->priceRepo = $priceRepo;
        $this->videoRepo = $videoRepo;
    }

    public function index()
    {
        //
       
        return view('admin.song.index');
    }

    public function getData()
    {
       
        return $this->songRepo->getSong();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $feeling = $this->feelingRepo->getFeelingList();
        $artist = $this->artistRepo->getArtistList();
        $songcat = $this->songCatRepo->getSongCategoryList();
        $price = $this->priceRepo->getPriceList();
        $video = $this->videoRepo->getVideoList();
        return view('admin.song.create',compact('feeling','artist','songcat','price','video'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSong $request)
    {
        //
        $input = $request->all();
        $song = $this->songRepo->saveSong($request);
        // if($input['add_btn'] == 'add_album'){
        //     return redirect()->route('albums.create'); 
        // }else if($input['add_btn'] == 'add_playlist'){
        //     return redirect()->route('playlists.create'); 
        // }else{
            Session::flash('message', 'You have successfully Insert Song .');
        return redirect()->route('songs.index'); 
        //}   
        
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $feeling = $this->feelingRepo->getFeelingList();
        $artist = $this->artistRepo->getArtistList();
        $songcat = $this->songCatRepo->getSongCategoryList();
        $price = $this->priceRepo->getPriceList();
        $song = $this->songRepo->getSongName($id);
        $video = $this->videoRepo->getVideoList();
        return view('admin.song.edit',compact('song','media','feeling','artist','price','songcat','video'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        $song = $this->songRepo->saveSong($request, $id);
        Session::flash('message', 'You have successfully Update Song .');
        return redirect()->route('songs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $feeling = $this->songRepo->deleteSong($id);
        
        if($feeling){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }

    public function getSearchSong(Request $request)
    {
        $songs =  $this->songRepo->getSearchSong($request);

        foreach($songs as $s){

            $datas[]= array(
                'id' => $s->id,
                'name_mm' => $s->name_mm,
                'artist_name' => $s->artists->implode('artist_name_eng', ', ')
                );
        }
        if(!$songs->isEmpty()){
            $data = [
            'status' => "success",
            'songs' => $datas
            ];
        }else{
            $data = [
            'status' => "faile",
            'songs' => ''
            ];
        }
        
        
        return json_encode($data);
    }   
}
