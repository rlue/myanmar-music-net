<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\NewCategory;
use App\Http\Requests\StoreNewCategory;
use App\Repositories\NewCatRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class NewCatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $newCatRepo;


    public function __construct(NewCatRepo $newCatRepo) {
       
        $this->newCatRepo = $newCatRepo;
    }

    public function index()
    {
        //
        return view('admin.newcategory.index');
    }

    public function getData()
    {
       
        return $this->newCatRepo->getNewCategory();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.newcategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNewCategory $request)
    {
        //
        $input = $request->all();

        $category = $this->newCatRepo->saveNewCategory($input);
          
        Session::flash('message', 'You have successfully Insert New Category.');
        return redirect()->route('newcategory.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $newcat = $this->newCatRepo->getNewCategoryName($id);
        return view('admin.newcategory.edit',compact('newcat'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        $category = $this->newCatRepo->saveNewCategory($input, $id);
        Session::flash('message', 'You have successfully Update New Category.');
        return redirect()->route('newcategory.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $category = $this->newCatRepo->deleteNewCategory($id);
        
        if($category){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
