<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\VideoPlaylist;
use App\Video;
use App\Artist;
use App\Repositories\VideoPlaylistRepo;
use App\Repositories\VideoRepo;
use App\Repositories\ArtistRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use File;

class VideoPlaylistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $videoPlaylistRepo;
    protected $videoRepo;
    protected $artistRepo;



    public function __construct(VideoRepo $videoRepo,VideoPlaylistRepo $videoPlaylistRepo,ArtistRepo $artistRepo) {
       
        $this->videoRepo = $videoRepo;
        $this->videoPlaylistRepo = $videoPlaylistRepo;
        $this->artistRepo = $artistRepo;
        
    }

    public function index()
    {
        //
        return view('admin.videoplaylist.index');
    }

    public function getData()
    {
       
        return $this->videoPlaylistRepo->getVideoPlaylist();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        
        $video = $this->videoRepo->getVideoList();
 
        return view('admin.videoplaylist.create',compact('video'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        $playlist = $this->videoPlaylistRepo->saveVideoPlaylist($request);
          
        Session::flash('message', 'You have successfully Insert Video PlayList .');
        return redirect()->route('videoplaylists.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        
        $video = $this->videoRepo->getVideoList();
        $playlist = $this->videoPlaylistRepo->getVideoPlaylistName($id);
        
        return view('admin.videoplaylist.edit',compact('playlist','video'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        $playlist = $this->videoPlaylistRepo->saveVideoPlaylist($request, $id);
        Session::flash('message', 'You have successfully Update Video PlayList .');
        return redirect()->route('videoplaylists.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $playlist = $this->videoPlaylistRepo->deleteVideoPlaylist($id);
        
        if($playlist){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
