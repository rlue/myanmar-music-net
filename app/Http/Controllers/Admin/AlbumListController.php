<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\AlbumList;
use App\Album;
use App\Http\Requests\StoreAlbumList;
use App\Repositories\AlbumListRepo;
use App\Repositories\AlbumRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use File;

class AlbumListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $albumListRepo;
    protected $albumRepo;



    public function __construct(AlbumRepo $albumRepo,AlbumListRepo $albumListRepo) {
       
        $this->albumRepo = $albumRepo;
        $this->albumListRepo = $albumListRepo;
        
    }

    public function index()
    {
        //
        return view('admin.albumlist.index');
    }

    public function getData()
    {
       
        return $this->albumListRepo->getAlbumList();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        
        $album = $this->albumRepo->getAlbumList();
 
        return view('admin.albumlist.create',compact('album'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAlbumList $request)
    {
        //
        
        $albumlist = $this->albumListRepo->saveAlbumList($request);
          
        Session::flash('message', 'You have successfully Insert AlbumList .');
        return redirect()->route('albumlists.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        
        $album = $this->albumRepo->getAlbumList();
        $albumlist = $this->albumListRepo->getAlbumListName($id);
        
        return view('admin.albumlist.edit',compact('albumlist','album'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreAlbumList $request, $id)
    {
        //
        
        $albumlist = $this->albumListRepo->saveAlbumList($request, $id);
        Session::flash('message', 'You have successfully Update AlbumList .');
        return redirect()->route('albumlists.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $albumlist = $this->albumListRepo->deleteAlbumList($id);
        
        if($albumlist){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
