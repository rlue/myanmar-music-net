<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Slider;
use App\Http\Requests\StoreSlider;
use App\Repositories\SliderRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $sliderRepo;
    


    public function __construct(SliderRepo $sliderRepo) {
       
        $this->sliderRepo = $sliderRepo;
       
    }

    public function index()
    {
        //
        return view('admin.slider.index');
    }

    public function getData()
    {
       
        return $this->sliderRepo->getSlider();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        
        return view('admin.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSlider $request)
    {
        //
        
        $slider = $this->sliderRepo->saveSlider($request);
          
        Session::flash('message', 'You have successfully Insert slider .');
        return redirect()->route('sliders.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
       
        $slider = $this->sliderRepo->getSliderName($id);
       
        return view('admin.slider.edit',compact('slider'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreSlider $request, $id)
    {
        //
        
        $new = $this->sliderRepo->saveSlider($request, $id);
        Session::flash('message', 'You have successfully Update Slider .');
        return redirect()->route('sliders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $feeling = $this->sliderRepo->deleteSlider($id);
        
        if($feeling){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
