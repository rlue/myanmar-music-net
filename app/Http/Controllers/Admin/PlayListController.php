<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\PlayList;
use App\Song;
use App\Artist;
use App\Http\Requests\StorePlayList;
use App\Repositories\PlayListRepo;
use App\Repositories\SongRepo;
use App\Repositories\ArtistRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use File;

class PlayListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $playListRepo;
    protected $songRepo;
    protected $artistRepo;



    public function __construct(SongRepo $songRepo,PlayListRepo $playListRepo,ArtistRepo $artistRepo) {
       
        $this->songRepo = $songRepo;
        $this->playListRepo = $playListRepo;
        $this->artistRepo = $artistRepo;
        
    }

    public function index()
    {
        //
        return view('admin.playlist.index');
    }

    public function getData()
    {
       
        return $this->playListRepo->getPlayList();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        $artist = $this->artistRepo->getArtistList();
        $song = $this->songRepo->getSongList();
 
        return view('admin.playlist.create',compact('artist','song'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePlayList $request)
    {
        //
        
        $playlist = $this->playListRepo->savePlayList($request);
          
        Session::flash('message', 'You have successfully Insert PlayList .');
        return redirect()->route('playlists.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        $artist = $this->artistRepo->getArtistList();
        $song = $this->songRepo->getSongList();
        $playlist = $this->playListRepo->getPlayListName($id);
        
        return view('admin.playlist.edit',compact('playlist','artist','song'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePlayList $request, $id)
    {
        //
        
        $playlist = $this->playListRepo->savePlayList($request, $id);
        Session::flash('message', 'You have successfully Update PlayList .');
        return redirect()->route('playlists.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $playlist = $this->playListRepo->deletePlayList($id);
        
        if($playlist){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
