<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Feeling;
use App\Http\Requests\StoreFeeling;
use App\Repositories\FeelingRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class FeelingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $feelingRepo;


    public function __construct(FeelingRepo $feelingRepo) {
       
        $this->feelingRepo = $feelingRepo;
    }

    public function index()
    {
        //
        return view('admin.feeling.index');
    }

    public function getData()
    {
       
        return $this->feelingRepo->getFeeling();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.feeling.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFeeling $request)
    {
        //
        $input = $request->all();

        $type = $this->feelingRepo->saveFeeling($input);
          
        Session::flash('message', 'You have successfully Insert Feelings .');
        return redirect()->route('feelings.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $feeling = $this->feelingRepo->getFeelingName($id);
        return view('admin.feeling.edit',compact('feeling'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreFeeling $request, $id)
    {
        //
        $input = $request->all();
        $feeling = $this->feelingRepo->saveFeeling($input, $id);
        Session::flash('message', 'You have successfully Update Feeling .');
        return redirect()->route('feelings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $feeling = $this->feelingRepo->deleteFeeling($id);
        
        if($feeling){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
