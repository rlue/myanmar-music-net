<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Artist;
use App\Http\Requests\StoreArtist;
use App\Repositories\ArtistTypeRepo;
use App\Repositories\ArtistCatRepo;
use App\Repositories\ArtistRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $artistRepo;
    protected $artisttypeRepo;
    protected $artistcatRepo;


    public function __construct(ArtistRepo $artistRepo,ArtistTypeRepo $artisttypeRepo,ArtistCatRepo $artistcatRepo) {
       
        $this->artistRepo = $artistRepo;
        $this->artisttypeRepo = $artisttypeRepo;
        $this->artistcatRepo = $artistcatRepo;
    }

    public function index()
    {
        //
        return view('admin.artist.index');
    }

    public function getData()
    {
       
        return $this->artistRepo->getArtist();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        $type = $this->artisttypeRepo->getArtistTypeList();
        $category = $this->artistcatRepo->getArtistCategoryList();
        

        return view('admin.artist.create',compact('type','category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArtist $request)
    {
        //
        $input = $request->all();

        $type = $this->artistRepo->saveArtist($input);
          
        Session::flash('message', 'You have successfully Insert Artist .');
        return redirect()->route('artists.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $type = $this->artisttypeRepo->getArtistTypeList();
        $category = $this->artistcatRepo->getArtistCategoryList();
        $artist = $this->artistRepo->getArtistName($id);
        return view('admin.artist.edit',compact('artist','type','category'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        $artist = $this->artistRepo->saveArtist($input, $id);
        Session::flash('message', 'You have successfully Update Artist .');
        return redirect()->route('artists.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $artist = $this->artistRepo->deleteArtist($id);
        
        if($artist){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
