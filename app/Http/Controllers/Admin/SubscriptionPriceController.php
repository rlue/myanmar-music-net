<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\SubscriptionPrice;
use App\Http\Requests\StoreSubPrice;
use App\Repositories\SubPriceRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class SubscriptionPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $subPriceRepo;


    public function __construct(SubPriceRepo $subPriceRepo) {
       
        $this->subPriceRepo = $subPriceRepo;
    }

    public function index()
    {
        //
        return view('admin.subprice.index');
    }

    public function getData()
    {
       
        return $this->subPriceRepo->getSubPrice();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.subprice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubPrice $request)
    {
        //
        $input = $request->all();

        $type = $this->subPriceRepo->saveSubPrice($input);
          
        Session::flash('message', 'You have successfully Insert Price.');
        return redirect()->route('subprice.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $subprice = $this->subPriceRepo->getSubPriceName($id);
        return view('admin.subprice.edit',compact('subprice'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        $price = $this->subPriceRepo->saveSubPrice($input, $id);
        Session::flash('message', 'You have successfully Update Price.');
        return redirect()->route('subprice.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $price = $this->subPriceRepo->deleteSubPrice($id);
        
        if($price){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
