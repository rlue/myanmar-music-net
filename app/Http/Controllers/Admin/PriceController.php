<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Price;
use App\Http\Requests\StorePrice;
use App\Repositories\PriceRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class PriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $priceRepo;


    public function __construct(PriceRepo $priceRepo) {
       
        $this->priceRepo = $priceRepo;
    }

    public function index()
    {
        //
        return view('admin.price.index');
    }

    public function getData()
    {
       
        return $this->priceRepo->getPrice();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.price.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePrice $request)
    {
        //
        $input = $request->all();

        $type = $this->priceRepo->savePrice($input);
          
        Session::flash('message', 'You have successfully Insert Price.');
        return redirect()->route('prices.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $price = $this->priceRepo->getPriceName($id);
        return view('admin.price.edit',compact('price'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        $price = $this->priceRepo->savePrice($input, $id);
        Session::flash('message', 'You have successfully Update Price.');
        return redirect()->route('prices.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $price = $this->priceRepo->deletePrice($id);
        
        if($price){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
