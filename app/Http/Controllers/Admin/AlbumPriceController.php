<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\AlbumPrice;
use App\Http\Requests\StoreAlbumPrice;
use App\Repositories\AlbumPriceRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class AlbumPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $albumPriceRepo;


    public function __construct(AlbumPriceRepo $albumPriceRepo) {
       
        $this->albumPriceRepo = $albumPriceRepo;
    }

    public function index()
    {
        //
        return view('admin.albumprice.index');
    }

    public function getData()
    {
       
        return $this->albumPriceRepo->getAlbumPrice();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.albumprice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAlbumPrice $request)
    {
        //
        $input = $request->all();

        $type = $this->albumPriceRepo->saveAlbumPrice($input);
          
        Session::flash('message', 'You have successfully Insert Album Price.');
        return redirect()->route('albumprices.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $albumprice = $this->albumPriceRepo->getAlbumPriceName($id);
        return view('admin.albumprice.edit',compact('albumprice'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        $price = $this->albumPriceRepo->saveAlbumPrice($input, $id);
        Session::flash('message', 'You have successfully Update Album Price.');
        return redirect()->route('albumprices.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $price = $this->albumPriceRepo->deleteAlbumPrice($id);
        
        if($price){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
