<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use Validator;
use Auth;
use Session;
use Socialite;


class AuthController extends Controller
{
    public function index()
    {   
        if (!Auth::check()) {   
            return view('layouts.login');
        }
        else { 
            return redirect()->route('dashboard');
        }
    }

    public function loginValidate(Request $request) 
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'email'   => 'required|email',
            'password' => 'required',
        ]);
        
        if ($validator->fails()) {
            $messages = $validator->errors();
            $ret = array(
                'status' => 'fail',
                'msg'    => 'Field is  required',
            );
            return view('layouts.login',compact('ret'));
        }
        else {
            if (Auth::attempt(['email' => $input['email'], 'password' => $input['password']])) {
                if(Auth::user()->hasRole('admin')){
                    return redirect()->route('dashboard');
                }else{
                    Auth::logout();
                    return redirect()->route('backend.login');
                }
            }else{
                $ret = array(
                    'status' => 'fail',
                    'msg'    => 'Invalid Email or Password'
                );  
                return view('layouts.login',compact('ret'));   
            }
        }
        //echo json_encode($ret);
    }

    private function loginSuccess() {
        $ret = array(
            'status' => 'success',
        );     
        return $ret; 
    }

    private function loginFail() {
        Auth::logout();
        Session::flush();
        $ret = array(
            'status' => 'fail',
            'msg'    => "Sorry, currently you can't login as admin..."
        );  
        return $ret;             
    }

    public function forgotPassword(Request $request) 
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'email'         => 'required|email',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            $ret = array(
                'status' => 'fail',
                'msg'    => implode('<br/>', $messages->all()),
            );
        }
        else {
            //to update password
            if(true) {
                $ret = array(
                    'status' => 'success',
                    'msg'    => "Password successfully reset. Please check your email.",
                );
            }
        }
        echo json_encode($ret);
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect()->route('backend.login');
    }

}