<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Bill;
use App\User;
use App\Http\Requests\StoreBill;
use App\Repositories\UserRepo;
use App\Repositories\BillRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use File;

class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $userRepo;
    protected $billRepo;

    public function __construct(UserRepo $userRepo,BillRepo $billRepo) {
       
        $this->userRepo = $userRepo;
        $this->billRepo = $billRepo;
    }

    public function index()
    {
        //
        return view('admin.bill.index');
    }

    public function getData()
    {
       
        return $this->billRepo->getBill();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        
        $user = $this->userRepo->getUserList();
        return view('admin.bill.create',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBill $request)
    {
        //
        
        $bill = $this->billRepo->saveBill($request);
          
        Session::flash('message', 'You have successfully Insert Bill .');
        return redirect()->route('bills.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        $bill = $this->billRepo->getBillList();
        $user = $this->userRepo->getUserList();
        return view('admin.bill.edit',compact('bill','user'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBill $request, $id)
    {
        //
        
        $bill = $this->billRepo->saveBill($request, $id);
        Session::flash('message', 'You have successfully Update Bill .');
        return redirect()->route('bills.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $bill = $this->billRepo->deleteBill($id);
        
        if($bill){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }

    
}
