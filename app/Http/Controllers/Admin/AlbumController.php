<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Album;
use App\Song;
use App\Artist;
use App\Http\Requests\StoreAlbum;
use App\Repositories\AlbumRepo;
use App\Repositories\SongRepo;
use App\Repositories\AlbumPriceRepo;
use App\Repositories\ArtistRepo;
use App\Aggregator;
use App\Repositories\AggregatorRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use File;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $albumRepo;
    protected $songRepo;
    protected $artistRepo;
    protected $albumPriceRepo;
    protected $aggregatorRepo;

    public function __construct(SongRepo $songRepo,AlbumRepo $albumRepo,ArtistRepo $artistRepo,AlbumPriceRepo $albumPriceRepo,AggregatorRepo $aggregatorRepo) {
       
        $this->songRepo = $songRepo;
        $this->albumRepo = $albumRepo;
        $this->artistRepo = $artistRepo;
        $this->albumPriceRepo = $albumPriceRepo;
        $this->aggregatorRepo = $aggregatorRepo;
    }

    public function index()
    {
        //
        return view('admin.album.index');
    }

    public function getData()
    {
       
        return $this->albumRepo->getAlbum();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        $artist = $this->artistRepo->getArtistList();
        $song = $this->songRepo->getSongList();
        $price = $this->albumPriceRepo->getAlbumPriceList();
        $aggregator = $this->aggregatorRepo->getAggregatorList();
        return view('admin.album.create',compact('artist','song','price','aggregator'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAlbum $request)
    {
        //
        
        $song = $this->albumRepo->saveAlbum($request);
          
        Session::flash('message', 'You have successfully Insert Album .');
        return redirect()->route('albums.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        $artist = $this->artistRepo->getArtistList();
        $song = $this->songRepo->getSongList();
        $price = $this->albumPriceRepo->getAlbumPriceList();
        $album = $this->albumRepo->getAlbumName($id);
        $aggregator = $this->aggregatorRepo->getAggregatorList();
        return view('admin.album.edit',compact('album','artist','price','song','aggregator'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreAlbum $request, $id)
    {
        //
        
        $album = $this->albumRepo->saveAlbum($request, $id);
        Session::flash('message', 'You have successfully Update Album .');
        return redirect()->route('albums.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $album = $this->albumRepo->deleteAlbum($id);
        
        if($album){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }

    public function transformItem($item)
    {
       
        //$name =  $item['name'];
        return [
            'id'         => $item['id'] ,
            'name_mm'         => $item['name_mm'],
            'order'         =>  true,
            'listfixed'     =>  false
            ];
    }
}
