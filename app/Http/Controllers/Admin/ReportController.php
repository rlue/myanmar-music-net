<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReportRepo;
use Yajra\Datatables\Datatables;
use App\Exports\PaymentExport;
use App\Exports\SongListenExport;
use App\Exports\SubscribeExport;
use App\Exports\SongOrderExport;
use App\Exports\AlbumOrderExport;
use App\User;
use App\Song;
use App\PlayCount;
use DB;
use Session;
use Excel;
class ReportController extends Controller
{
    //
	protected $reportRepo;

    public function __construct(ReportRepo $reportRepo) {
       
        $this->reportRepo = $reportRepo;
        
    }
    public function payment()
    {
        $user = User::whereHas('roles', function($q){
            $q->where('name', 'normal');
        })->get();
    	return view('admin.report.transaction',compact('user'));
    }

    public function getPaymentData()
    {
    	return $this->reportRepo->getPayment();
    }
    public function wihtPayment(Request $request)
    {   
        //return (new PaymentExport($request))->download('invoices.xlsx');
        return Excel::download(new PaymentExport($request), 'payment.xlsx');
        
        
   }
   //for song order report
    public function getSongOrder()
    {
         $user = User::whereHas('roles', function($q){
            $q->where('name', 'normal');
        })->get();
        return view('admin.report.ordersong',compact('user'));
    }

    public function getSongOrderData()
    {
        return $this->reportRepo->getSongOrder();
    }
    public function songOrder(Request $request)
    {
        //return (new SongOrderExport($request))->download('songorder.xlsx');
        return Excel::download(new SongOrderExport($request), 'songorder.xlsx');
    }
    //end song order report

    //for album order report
    public function getAlbumOrder()
    {
         $user = User::whereHas('roles', function($q){
            $q->where('name', 'normal');
        })->get();
        return view('admin.report.orderalbum',compact('user'));
    }

    public function getAlbumOrderData()
    {
        return $this->reportRepo->getAlbumOrder();
    }
    public function albumOrder(Request $request)
    {
        //return (new SongOrderExport($request))->download('songorder.xlsx');
        return Excel::download(new AlbumOrderExport($request), 'albumorder.xlsx');
    }
    // end album order report

    public function getSongListen()
    {
        
        return view('admin.report.tracklisten');
    }

    public function getSongListenData()
    {
        return $this->reportRepo->getSongListen();
    }

    public function songlisten(Request $request)
    {
        
        //return (new SongListenExport($request))->download('songlisten.xlsx');
         return Excel::download(new SongListenExport($request), 'songlisten.xlsx');
    }

    public function getSubscriptionData()
    {
        return $this->reportRepo->getSubscription();
    }

    public function getSubscription()
    {
         $user = User::whereHas('roles', function($q){
            $q->where('name', 'normal');
        })->get();
        return view('admin.report.subscribe',compact('user'));
    }

     public function subscribe(Request $request)
    {
        
        return (new SubscribeExport($request))->download('subscribe.xlsx');
    }
}
