<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\VideoCms;
use App\Video;
use App\Http\Requests\StoreVideoCms;
use App\Repositories\VideoCmsRepo;
use App\Repositories\VideoRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use File;

class VideoCmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $videocmsRepo;
    protected $videoRepo;


    public function __construct(VideoRepo $videoRepo,VideoCmsRepo $videocmsRepo) {
       
        $this->videoRepo = $videoRepo;
        $this->videocmsRepo = $videocmsRepo;
    }

    public function index()
    {
        //
        return view('admin.videocms.index');
    }

    public function getData()
    {
       
        return $this->videocmsRepo->getVideoCms();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        $video = $this->videoRepo->getVideoList();
        return view('admin.videocms.create',compact('video'));
    }

    public function createCms($name)
    {
        $videocms = $this->videocmsRepo->createCms($name);
        $title = str_replace('_',' ',$name);
        if(empty($videocms)){
            $video = $this->videoRepo->getVideoList();
            return view('admin.videocms.create',compact('video','title'));
        }else{
            $video = $this->videoRepo->getVideoList();
            return view('admin.videocms.edit',compact('video','videocms','title'));
        }
        
    }

    public function storeCms(Request $request)
    {
        $videocms = $this->videocmsRepo->storeCms($request);
        $title = str_replace('_',' ',$videocms->cms_type);
        $video = $this->videoRepo->getVideoList();
        Session::flash('message', 'You have successfully Insert .');
            return view('admin.videocms.edit',compact('video','videocms','title'));
    }    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVideoCms $request)
    {
        //
        
        $videocms = $this->videocmsRepo->saveVideoCms($request);
          
        Session::flash('message', 'You have successfully Insert .');
        return redirect()->route('videocms.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        
        $video = $this->videoRepo->getVideoList();
       
        $videocms = $this->videocmsRepo->getVideoCmsName($id);
        
        return view('admin.videocms.edit',compact('videocms','video'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        $videocms = $this->videocmsRepo->saveVideoCms($request, $id);
        Session::flash('message', 'You have successfully Update .');
        return redirect()->route('videocms.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $cms = $this->videocmsRepo->deleteVideoCms($id);
        
        if($cms){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
