<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\AlbumList;
use App\AlbumlistCms;
use App\Repositories\AlbumlistCmsRepo;
use App\Repositories\AlbumListRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class AlbumlistCmsController extends Controller
{
    //
     protected $albumlistcmsRepo;
    protected $albumlistRepo;


    public function __construct(AlbumListRepo $albumlistRepo,AlbumlistCmsRepo $albumlistcmsRepo) {
       
        $this->albumlistRepo = $albumlistRepo;
        $this->albumlistcmsRepo = $albumlistcmsRepo;
    }

    public function createCms($name)
    {
        $albumlistcms = $this->albumlistcmsRepo->createCms($name);
        $albumlist = $this->albumlistRepo->getAlbumListList();
        $title = str_replace('_',' ',$name);
        if(empty($albumlistcms)){
            return view('admin.albumlistcms.create',compact('albumlist','title'));
        }else{
            return view('admin.albumlistcms.edit',compact('albumlist','albumlistcms','title'));
        }
    }

    public function storeCms(Request $request)
    {
        $albumlistcms = $this->albumlistcmsRepo->storeCms($request);
        $title = str_replace('_',' ',$albumlistcms->cms_type);
        $albumlist = $this->albumlistRepo->getAlbumListList();
        Session::flash('message', 'You have successfully Insert .');
            return view('admin.albumlistcms.edit',compact('albumlist','albumlistcms','title'));
    }
}
