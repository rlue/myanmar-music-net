<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Promotion;
use App\Http\Requests\StorePromotion;
use App\Repositories\PromotionRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;


class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $promotionRepo;


    public function __construct(PromotionRepo $promotionRepo) {
       
        
        $this->promotionRepo = $promotionRepo;
    }

    public function index()
    {
        //
        return view('admin.promotion.index');
    }

    public function getData()
    {
       
        return $this->promotionRepo->getPromotion();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        
        return view('admin.promotion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePromotion $request)
    {
        //
        
        $promotion = $this->promotionRepo->savePromotion($request);
          
        Session::flash('message', 'You have successfully Insert Promotion .');
        return redirect()->route('promotions.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        $promotion = $this->promotionRepo->getPromotionName($id);
        
        return view('admin.promotion.edit',compact('promotion'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        $promotion = $this->promotionRepo->savePromotion($request, $id);
        Session::flash('message', 'You have successfully Update promotion .');
        return redirect()->route('promotions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $promotion = $this->promotionRepo->deletePromotion($id);
        
        if($promotion){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
