<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Permission;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class MemberController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware("auth");
       
    }
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        return view('admin.member.index');
    }

    public function getData()
    {
        $user = User::whereHas('roles', function($q){
            $q->where('name', 'normal');
        })->get();
        $table = DataTables::of($user)
            ->escapeColumns([])
            ->addColumn('no', function ($user) {
                return '';
            })
            
            ->addColumn('action', function ($user) {
               

                $btn = '<a href="'. route('members.edit', $user) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                $btn .= '<a href="#" data-id="'.$user->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
               
                return "<div class='action-column'>" . $btn . "</div>";
            });

        return $table->make(true);
    }


    public function create()
    {

        $roles = Role::get();        
        return view('admin.member.create',compact('roles'));
    }
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
            'roles' => 'required'
        ]);

        $input['password'] = bcrypt($request->get('password'));
        $input['name'] = $request->get('name');
        $input['email'] = $request->get('email');
        $user = User::create($input);
        
        if($request->roles <> ''){
            $user->roles()->attach($request->roles);
        }
        Session::flash('message', 'You have successfully Insert User.');
        return redirect()->route('members.index');            
        
    }
    public function edit($id) {
        $member = User::findOrFail($id);
       
        $roles = Role::get(); 
        return view('admin.member.edit', compact('member', 'roles','userRole')); 
    }
    public function update(UpdateUser $request, $id) {
        $user = User::findOrFail($id);   
        $this->validate($request, [
            
            'password'=>'confirmed'
        ]);
        $input = $request->except('roles');
        
         $name  = $request->input("name");
        $email     = $request->input('email');
        $password  = $request->input('password');
        
        if(isset($password) && !empty($password)){
            $userupdate = ['name' => $name,'email' => $email,'password'=>bcrypt($password)];

        }else{
             $userupdate = ['name' => $name,'email' => $email];
        }
       
       DB::table('users')
            ->where('id', $id)
            ->update($userupdate);
        if ($request->roles <> '') {
            $user->roles()->sync($request->roles);        
        }        
        else {
            $user->roles()->detach(); 
        }
        Session::flash('message', 'You have successfully Update User.');
        return redirect()->route('members.index');
    }
    public function destroy($id) {
        $user = User::findOrFail($id); 
        $user->delete();
        if($user){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
