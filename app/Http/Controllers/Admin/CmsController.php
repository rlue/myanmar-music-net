<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Cms;
use App\Song;
use App\Http\Requests\StoreCms;
use App\Repositories\CmsRepo;
use App\Repositories\SongRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use File;

class CmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $cmsRepo;
    protected $songRepo;


    public function __construct(SongRepo $songRepo,CmsRepo $cmsRepo) {
       
        $this->songRepo = $songRepo;
        $this->cmsRepo = $cmsRepo;
    }

    public function index()
    {
        //
        return view('admin.cms.index');
    }

    public function getData()
    {
       
        return $this->cmsRepo->getCms();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        $song = $this->songRepo->getSongList();
        return view('admin.cms.create',compact('song'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCms $request)
    {
        //
        
        $cms = $this->cmsRepo->saveCms($request);
          
        Session::flash('message', 'You have successfully Insert .');
        return redirect()->route('cms.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        
        $song = $this->songRepo->getSongList();
       
        $cms = $this->cmsRepo->getCmsName($id);
        
        return view('admin.cms.edit',compact('cms','song'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCms $request, $id)
    {
        //
        
        $cms = $this->cmsRepo->saveCms($request, $id);
        Session::flash('message', 'You have successfully Update .');
        return redirect()->route('cms.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $cms = $this->cmsRepo->deleteCms($id);
        
        if($cms){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
