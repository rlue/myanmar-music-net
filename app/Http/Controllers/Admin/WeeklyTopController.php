<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Cache;

class WeeklyTopController extends Controller
{

    public function show()
    {
        $top_20_ended_at = Cache::get('top_20_ended_at');

        return view('admin.weekly-top-20.index', compact('top_20_started_at', 'top_20_ended_at'));
    }

    public function update()
    {
        $top_20_ended_at = today();

        Cache::forever('top_20_ended_at', $top_20_ended_at);

        return redirect()->back();
    }


}
