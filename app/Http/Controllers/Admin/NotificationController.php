<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Notification;
use App\Http\Requests\StoreNotification;
use App\Repositories\NotificationRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $notiRepo;
    


    public function __construct(NotificationRepo $notiRepo) {
       
        $this->notiRepo = $notiRepo;
       
    }

    public function index()
    {
        //
        return view('admin.notification.index');
    }

    public function getData()
    {
       
        return $this->notiRepo->getNoti();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
       
        return view('admin.notification.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNotification $request)
    {
        //
        
        $noti = $this->notiRepo->saveNoti($request);
          
        Session::flash('message', 'You have successfully Insert Notification .');
        return redirect()->route('notification.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        
        $noti = $this->notiRepo->getNotiName($id);
       
        return view('admin.notification.edit',compact('noti'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreNotification $request, $id)
    {
        //
        
        $noti = $this->notiRepo->saveNoti($request, $id);
        Session::flash('message', 'You have successfully Update Notification .');
        return redirect()->route('notification.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $noti = $this->notiRepo->deleteNoti($id);
        
        if($noti){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
