<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\VideoCategory;
use App\Http\Requests\StoreVideoCategory;
use App\Repositories\VideoCatRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class VideoCatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $videoCatRepo;


    public function __construct(VideoCatRepo $videoCatRepo) {
       
        $this->videoCatRepo = $videoCatRepo;
    }

    public function index()
    {
        //
        return view('admin.videocategory.index');
    }

    public function getData()
    {
       
        return $this->videoCatRepo->getVideoCategory();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.videocategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVideoCategory $request)
    {
        //
        $input = $request->all();

        $category = $this->videoCatRepo->saveVideoCategory($input);
          
        Session::flash('message', 'You have successfully Insert Video Category.');
        return redirect()->route('videocategory.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $videocat = $this->videoCatRepo->getVideoCategoryName($id);
        return view('admin.videocategory.edit',compact('videocat'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        $category = $this->videoCatRepo->saveVideoCategory($input, $id);
        Session::flash('message', 'You have successfully Update Video Category.');
        return redirect()->route('videocategory.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $category = $this->videoCatRepo->deleteVideoCategory($id);
        
        if($category){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
