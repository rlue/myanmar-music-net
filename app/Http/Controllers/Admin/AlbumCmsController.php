<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\AlbumCms;
use App\Album;
use App\Http\Requests\StoreAlbumCms;
use App\Repositories\AlbumCmsRepo;
use App\Repositories\AlbumRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use File;

class AlbumCmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $albumcmsRepo;
    protected $albumRepo;


    public function __construct(AlbumRepo $albumRepo,AlbumCmsRepo $albumcmsRepo) {
       
        $this->albumRepo = $albumRepo;
        $this->albumcmsRepo = $albumcmsRepo;
    }

    public function index()
    {
        //
        return view('admin.albumcms.index');
    }

    public function getData()
    {
       
        return $this->albumcmsRepo->getAlbumCms();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        $album = $this->albumRepo->getAlbumList();
        return view('admin.albumcms.create',compact('album'));
    }

    public function createCms($name)
    {
        $albumcms = $this->albumcmsRepo->createCms($name);
        $album = $this->albumRepo->getAlbumList();
        $title = str_replace('_',' ',$name);
        if(empty($albumcms)){
            return view('admin.albumcms.create',compact('album','title'));
        }else{
            return view('admin.albumcms.edit',compact('album','albumcms','title'));
        }
    }

    public function storeCms(Request $request)
    {
        $albumcms = $this->albumcmsRepo->storeCms($request);
        $title = str_replace('_',' ',$albumcms->cms_type);
        $album = $this->albumRepo->getAlbumList();
        Session::flash('message', 'You have successfully Insert .');
            return view('admin.albumcms.edit',compact('album','albumcms','title'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAlbumCms $request)
    {
        //
        
        $albumcms = $this->albumcmsRepo->saveAlbumCms($request);
          
        Session::flash('message', 'You have successfully Insert .');
        return redirect()->route('albumcms.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        
        $album = $this->albumRepo->getAlbumList();
       
        $albumcms = $this->albumcmsRepo->getAlbumCmsName($id);
        
        return view('admin.albumcms.edit',compact('albumcms','album'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        $albumcms = $this->albumcmsRepo->saveAlbumCms($request, $id);
        Session::flash('message', 'You have successfully Update .');
        return redirect()->route('albumcms.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $cms = $this->albumcmsRepo->deleteAlbumCms($id);
        
        if($cms){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
