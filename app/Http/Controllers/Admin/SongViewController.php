<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Song;
use App\FullSongView;
use App\Repositories\SongRepo;
use App\Repositories\SongViewRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class SongViewController extends Controller
{
    //

    protected $songRepo;
    protected $songViewRepo;
   

    public function __construct(SongRepo $songRepo,SongViewRepo $songViewRepo) {
       
        $this->songRepo = $songRepo;
        $this->songViewRepo = $songViewRepo;
    }

    public function index()
    {
    	return view('admin.songview.index');
    }

    public function getData()
    {
    	return $this->songViewRepo->getSongView();
    }
}
