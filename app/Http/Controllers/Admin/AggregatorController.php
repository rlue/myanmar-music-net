<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Aggregate;
use App\Http\Requests\StoreAggregator;
use App\Repositories\AggregatorRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class AggregatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $aggregatorRepo;


    public function __construct(AggregatorRepo $aggregatorRepo) {
       
        $this->aggregatorRepo = $aggregatorRepo;
    }

    public function index()
    {
        //
        return view('admin.aggregator.index');
    }

    public function getData()
    {
       
        return $this->aggregatorRepo->getAggregator();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.aggregator.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAggregator $request)
    {
        //
        $input = $request->all();

        $aggregator = $this->aggregatorRepo->saveAggregator($input);
          
        Session::flash('message', 'You have successfully Insert Aggregator Category.');
        return redirect()->route('aggregator.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $aggregator = $this->aggregatorRepo->getAggregatorName($id);
        return view('admin.aggregator.edit',compact('aggregator'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        $aggregator = $this->aggregatorRepo->saveAggregator($input, $id);
        Session::flash('message', 'You have successfully Update Aggregator.');
        return redirect()->route('aggregator.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $aggregator = $this->aggregatorRepo->deleteAggregator($id);
        
        if($aggregator){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
