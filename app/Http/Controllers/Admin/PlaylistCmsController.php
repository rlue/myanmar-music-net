<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\PlayList;
use App\PlaylistCms;
use App\Repositories\PlaylistCmsRepo;
use App\Repositories\PlayListRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use File;
class PlaylistCmsController extends Controller
{
    //

    protected $playlistcmsRepo;
    protected $playlistRepo;


    public function __construct(PlayListRepo $playlistRepo,PlaylistCmsRepo $playlistcmsRepo) {
       
        $this->playlistRepo = $playlistRepo;
        $this->playlistcmsRepo = $playlistcmsRepo;
    }

    public function createCms($name)
    {
        $playlistcms = $this->playlistcmsRepo->createCms($name);
        $playlist = $this->playlistRepo->getPlayListList();
        $title = str_replace('_',' ',$name);
        if(empty($playlistcms)){
            return view('admin.playlistcms.create',compact('playlist','title'));
        }else{
            return view('admin.playlistcms.edit',compact('playlist','playlistcms','title'));
        }
    }

    public function storeCms(Request $request)
    {
        $playlistcms = $this->playlistcmsRepo->storeCms($request);
        $title = str_replace('_',' ',$playlistcms->cms_type);
        $playlist = $this->playlistRepo->getPlayListList();
        Session::flash('message', 'You have successfully Insert .');
            return view('admin.playlistcms.edit',compact('playlist','playlistcms','title'));
    }
}
