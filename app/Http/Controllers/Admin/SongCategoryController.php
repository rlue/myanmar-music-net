<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\SongCategory;
use App\Http\Requests\StoreSongCategory;
use App\Repositories\SongCatRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class SongCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $songCatRepo;


    public function __construct(SongCatRepo $songCatRepo) {
       
        $this->songCatRepo = $songCatRepo;
    }

    public function index()
    {
        //
        return view('admin.songcategory.index');
    }

    public function getData()
    {
       
        return $this->songCatRepo->getSongCategory();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.songcategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSongCategory $request)
    {
        //
        $input = $request->all();

        $song = $this->songCatRepo->saveSongCategory($input);
          
        Session::flash('message', 'You have successfully Insert Category .');
        return redirect()->route('songcategory.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $songcat = $this->songCatRepo->getSongCategoryName($id);
        return view('admin.songcategory.edit',compact('songcat'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreSongCategory $request, $id)
    {
        //
        $input = $request->all();
        $song = $this->songCatRepo->saveSongCategory($input, $id);
        Session::flash('message', 'You have successfully Update Category .');
        return redirect()->route('songcategory.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $feeling = $this->songCatRepo->deleteSongCategory($id);
        
        if($feeling){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
