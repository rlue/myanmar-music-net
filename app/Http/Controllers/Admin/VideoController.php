<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Video;
use App\VideoCategory;
use App\Http\Requests\StoreVideo;
use App\Repositories\ArtistRepo;
use App\Repositories\VideoRepo;
use App\Repositories\VideoCatRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use File;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $videoRepo;
    protected $videoCatRepo;
    protected $artistRepo;


    public function __construct(VideoRepo $videoRepo,VideoCatRepo $videoCatRepo,ArtistRepo $artistRepo) {
       
        $this->videoRepo = $videoRepo;
        $this->videoCatRepo = $videoCatRepo;
        $this->artistRepo = $artistRepo;
        
    }

    public function index()
    {
        //
        return view('admin.video.index');
    }

    public function getData()
    {
       
        return $this->videoRepo->getVideo();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        $videocat = $this->videoCatRepo->getVideoCategoryList();
        $artist = $this->artistRepo->getArtistList();
        return view('admin.video.create',compact('videocat','artist'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVideo $request)
    {
        //
        
        $video = $this->videoRepo->saveVideo($request);
          
        Session::flash('message', 'You have successfully Insert Video .');
        return redirect()->route('videos.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        $videocat = $this->videoCatRepo->getVideoCategoryList();
       	 $video = $this->videoRepo->getVideoName($id);
        $artist = $this->artistRepo->getArtistList();
        return view('admin.video.edit',compact('videocat','video','artist'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreVideo $request, $id)
    {
        //
        
        $video = $this->videoRepo->saveVideo($request, $id);
        Session::flash('message', 'You have successfully Update Video .');
        return redirect()->route('videos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $feeling = $this->videoRepo->deleteVideo($id);
        
        if($feeling){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
