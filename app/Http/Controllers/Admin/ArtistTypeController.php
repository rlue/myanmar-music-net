<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ArtistType;
use App\Http\Requests\StoreArtistType;
use App\Repositories\ArtistTypeRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class ArtistTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $artisttypeRepo;


    public function __construct(ArtistTypeRepo $artisttypeRepo) {
       
        $this->artisttypeRepo = $artisttypeRepo;
    }

    public function index()
    {
        //
        return view('admin.artisttype.index');
    }

    public function getData()
    {
       
        return $this->artisttypeRepo->getArtistType();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.artisttype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArtistType $request)
    {
        //
        $input = $request->all();

        $type = $this->artisttypeRepo->saveArtistType($input);
          
        Session::flash('message', 'You have successfully Insert Artist Type.');
        return redirect()->route('artisttype.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $artisttype = $this->artisttypeRepo->getArtistTypeName($id);
        return view('admin.artisttype.edit',compact('artisttype'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        $category = $this->artisttypeRepo->saveArtistType($input, $id);
        Session::flash('message', 'You have successfully Update Artist Type.');
        return redirect()->route('artisttype.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $category = $this->artisttypeRepo->deleteArtistType($id);
        
        if($category){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
