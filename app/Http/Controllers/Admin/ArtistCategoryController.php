<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ArtistCategory;
use App\Http\Requests\StoreArtistCategory;
use App\Repositories\ArtistCatRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
class ArtistCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $artistcatRepo;


    public function __construct(ArtistCatRepo $artistcatRepo) {
       
        $this->artistcatRepo = $artistcatRepo;
    }

    public function index()
    {
        //
        return view('admin.artistcategory.index');
    }

    public function getData()
    {
       
        return $this->artistcatRepo->getArtistCategory();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.artistcategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArtistCategory $request)
    {
        //
        $input = $request->all();

        $category = $this->artistcatRepo->saveArtistCategory($input);
          
        Session::flash('message', 'You have successfully Insert Artist Category.');
        return redirect()->route('artistcategory.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $artistcat = $this->artistcatRepo->getArtistCategoryName($id);
        return view('admin.artistcategory.edit',compact('artistcat'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $input = $request->all();
        $category = $this->artistcatRepo->saveArtistCategory($input, $id);
        Session::flash('message', 'You have successfully Update Artist Category.');
        return redirect()->route('artistcategory.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $category = $this->artistcatRepo->deleteArtistCategory($id);
        
        if($category){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
