<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\News;
use App\NewCategory;
use App\Http\Requests\StoreNews;
use App\Repositories\NewCatRepo;
use App\Repositories\NewsRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use File;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $newRepo;
    protected $newCatRepo;
    


    public function __construct(NewsRepo $newRepo,NewCatRepo $newCatRepo) {
       
        $this->newRepo = $newRepo;
        $this->newCatRepo = $newCatRepo;
       
    }

    public function index()
    {
        //
        return view('admin.news.index');
    }

    public function getData()
    {
       
        return $this->newRepo->getNew();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        $newcat = $this->newCatRepo->getNewCategoryList();
        return view('admin.news.create',compact('newcat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNews $request)
    {
        //
        
        $news = $this->newRepo->saveNew($request);
          
        Session::flash('message', 'You have successfully Insert New .');
        return redirect()->route('news.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        $newcat = $this->newCatRepo->getNewCategoryList();
        $new = $this->newRepo->getNewName($id);
       
        return view('admin.news.edit',compact('new','newcat'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreNews $request, $id)
    {
        //
        
        $new = $this->newRepo->saveNew($request, $id);
        Session::flash('message', 'You have successfully Update News .');
        return redirect()->route('news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $feeling = $this->newRepo->deleteNew($id);
        
        if($feeling){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
