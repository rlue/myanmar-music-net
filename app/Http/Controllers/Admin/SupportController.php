<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Support;
use App\Repositories\SupportRepo;
use Session;
class SupportController extends Controller
{
    //
	protected $supportRepo;


    public function __construct(SupportRepo $supportRepo) {
       
        $this->supportRepo = $supportRepo;
     
    }
    public function create()
    {
        
        $support = Support::first();
       
        if(!$support){
            return view('admin.support.create');
        }else{
            return view('admin.support.edit',compact('support'));
        }
    }

    public function store(Request $request)
    {
        $supportlist = $this->supportRepo->save($request);
        
        Session::flash('message', 'You have successfully Insert .');
            return redirect()->route('support.create'); 
    }
}
