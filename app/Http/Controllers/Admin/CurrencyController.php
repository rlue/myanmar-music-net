<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Currency;
use App\Repositories\CurrencyRepo;
use Session;
class CurrencyController extends Controller
{
    //
	protected $currencyRepo;


    public function __construct(CurrencyRepo $currencyRepo) {
       
        $this->currencyRepo = $currencyRepo;
     
    }
    public function create()
    {
        
        $currency = Currency::orderBy('created_at', 'desc')->first();
        $last = Currency::orderBy('created_at', 'desc')->skip(1)->take(1)->first();
        if(!$currency){
            return view('admin.currency.create');
        }else{
            return view('admin.currency.edit',compact('currency','last'));
        }
    }

    public function store(Request $request)
    {
        $currencylist = $this->currencyRepo->save($request);
        
        Session::flash('message', 'You have successfully Insert .');
            return redirect()->route('currency.create'); 
    }
}
