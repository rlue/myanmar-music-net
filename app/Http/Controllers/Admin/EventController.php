<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Event;
use App\Http\Requests\StoreEvent;
use App\Repositories\EventRepo;
use Yajra\Datatables\Datatables;
use DB;
use Session;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $eventRepo;
    


    public function __construct(EventRepo $eventRepo) {
       
        $this->eventRepo = $eventRepo;
       
    }

    public function index()
    {
        //
        return view('admin.event.index');
    }

    public function getData()
    {
       
        return $this->eventRepo->getEvent();
    }
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
       
        return view('admin.event.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEvent $request)
    {
        //
        
        $event = $this->eventRepo->saveEvent($request);
          
        Session::flash('message', 'You have successfully Insert Event .');
        return redirect()->route('events.index'); 
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        
        
        $event = $this->eventRepo->getEventName($id);
       
        return view('admin.event.edit',compact('event'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreEvent $request, $id)
    {
        //
        
        $event = $this->eventRepo->saveEvent($request, $id);
        Session::flash('message', 'You have successfully Update Event .');
        return redirect()->route('events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $event = $this->eventRepo->deleteEvent($id);
        
        if($event){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
