<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;
use Yajra\Datatables\Datatables;
use Session;
class RoleController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        

        return view('admin.role.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function getData()
    {
           $role = Role::get();
        $table = DataTables::of($role)
            ->escapeColumns([])
            ->addColumn('no', function ($role) {
                return '';
            })
            
            ->addColumn('action', function ($role) {
               

                $btn = '<a href="'. route('roles.edit', $role) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                $btn .= '<a href="#" data-id="'.$role->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
               
                return "<div class='action-column'>" . $btn . "</div>";
            });

        return $table->make(true);
    }

    public function create()
    {
        
        return view('admin.role.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required',
            
            ]
        );
        
        $role = new Role();
        $role->name = $request->name;
        $role->save();
        // if($request->permissions <> ''){
        //     $role->permissions()->attach($request->permissions);
        // }
        Session::flash('message', 'You have successfully Insert Role.');
        return redirect()->route('roles.index');
    }
   
     public function edit($id) {
        $role = Role::findOrFail($id);
       
        // $permissions = Permission::all();
        return view('admin.role.edit', compact('role'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $role = Role::findOrFail($id);//Get role with the given id
    //Validate name and permission fields
        $this->validate($request, [
            'name'=>'required|max:10|unique:roles,name,'.$id,
            
        ]);
        $input = $request->except(['permissions']);
        $role->fill($input)->save();
        if($request->permissions <> ''){
            $role->permissions()->sync($request->permissions);
        }
        Session::flash('message', 'You have successfully Update Role.');
        return redirect()->route('roles.index')->with('success','Roles updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();
        if($role){
            $data = [
            'status' => "success",
            'message' => "Delete Success"
            ];
        }else{
            $data = [
            'status' => "fail",
            'message' => "Sorry can't delete "
            ];
        }
        return json_encode($data);
    }
}
