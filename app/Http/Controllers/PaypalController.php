<?php

namespace App\Http\Controllers;


use App\Currency;
use App\PaymentOrder;
use Illuminate\Http\Request;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;
use Log;
use Redirect;
use Session;
use Config;

class PaypalController extends Controller
{

    private $_api_context;

    public function __construct()
    {
        $paypal_conf = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
                $paypal_conf['client_id'],
                $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }


    public function payWithPaypal(Request $request)
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $exchange_rate = Currency::first();
        $request_amount = $request->get('amount') / $exchange_rate->name;

        $item_1 = new Item();
        $item_1->setName('m-'. $request->get('amount')) /** item name **/
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($request_amount); /** unit price **/

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($request_amount);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(url('payment/paypal-response')) /** Specify return URL **/
        ->setCancelUrl(url('payment/paypal-response'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        /** dd($payment->create($this->_api_context));exit; **/
        try {
            $payment->create($this->_api_context);
        } catch (PayPalConnectionException $ex) {
            Log::error($ex);
            if (config('app.debug')) {
                return redirect('/user/payment')->with('status', 'danger')->with('message', 'Connection timeout');
            } else {
                return redirect('/user/payment')->with('status', 'danger')->with('message', 'Some error occur, sorry for inconvenient');
            }
        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());

        PaymentOrder::create([
            'user_id' => auth()->id(),
            'transaction_id' => $payment->getId(),
            'payment_service' => 'paypal',
            'amount' => request('amount'),
        ]);

        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }

        Session::put('error', 'Unknown error occurred');
        return redirect('/user/payment');
    }


    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');

        if (empty(request('PayerID')) || empty(request('token'))) {
            return redirect('/')->with('status', 'danger')->with('message', 'Payment failed');
        }

        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(request('PayerID'));

        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        $payment_order = PaymentOrder::where('transaction_id', $payment_id)
            ->where('payment_service', 'paypal')
            ->where('status', 'pending')
            ->first();

        if ($result->getState() == 'approved') {

            $user = $payment_order->user;

            if (is_null($payment_order)) {
                return redirect('/')->with('status', 'danger')->with('message', 'unauthorized transaction');
            }

            $user->remained_amount += (int) $payment_order->amount;
            $user->save();

            $payment_order->status = 'success';
            $payment_order->save();

            if ($payment_order->client === 'mobile') {
                return redirect('/transaction_success');
            }

            return redirect('/user/profile')->with('status', 'success')->with('message', 'Payment success');
        }

        $payment_order->status = 'failed';
        $payment_order->save();

        if ($payment_order->client === 'mobile') {
            return redirect('/transaction_failed');
        }

        return redirect('/')->with('status', 'danger')->with('message', 'Payment failed');

    }
}
