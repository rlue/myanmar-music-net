<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Socialite;
use App\User;
use App\Social;
use Config;
use Redirect;

class SocialController extends Controller
{
    /**
     * SocialController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @param $provider
     * @return $this
     * @author TintNaingWin
     */
    public function getSocialRedirect($provider)
    {
        $providerKey = Config::get('services.' . $provider);

        if (empty($providerKey)) {
            return view('errors.503')->with('error','No such provider');
        }

        return Socialite::driver($provider)->redirect();
    }

    /**
     * @param $provider
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @author TintNaingWin
     */
    public function getSocialHandle($provider)
    {

        if (request('denied') != '') {
            return redirect()->to('/login')
                ->with('status', 'danger')
                ->with('message', 'You did not share your profile data with our social app.');
        }

        $user = Socialite::driver($provider)->user();

        $socialUser = $this->findOrCreateUser($user,$provider);

        auth()->login($socialUser);

        if ($socialUser->email_verified_at !== null) {

            return Redirect::intended('/');

        } elseif ($socialUser->email_verified_at === null) {

            $socialUser->email_verified_at = Carbon::now();
            $socialUser->save();

            return Redirect::intended('/');

        }

    }

    /**
     * @param $user
     * @param $provider
     * @return User|null
     * @author TintNaingWin
     */
    private function findOrCreateUser($user, $provider)
    {

        $socialUser = null;

        // check is this email present
        if($user->email == null){
            $userCheck = null;
        }else{
            $userCheck = User::where('email', '=', $user->email)->first();
        }

        $email     = $user->email;

        if (!$user->email) {
            $email = null;
        }

        $sameSocialId = Social::where('social_id', '=', $user->id)->where('provider', $provider )->first();

        if (!empty($userCheck)) {

            $socialUser = $userCheck;

            if (empty($sameSocialId)) {
                $socialData = new Social;
                $socialData->user_id   = $socialUser->id;
                $socialData->social_id = $user->id;
                $socialData->provider  = $provider;
                $socialData->save();
            }

        } else {
            // new user
            if (empty($sameSocialId)) {

                $newSocialUser                    = new User;
                $newSocialUser->email             = $email;
                $newSocialUser->name              = $user->name;
                $newSocialUser->email_verified_at = Carbon::now();
                $newSocialUser->password          = null;
                $newSocialUser->save();

                $newSocialUser->attachRole('normal');

                $socialData            = new Social;
                $socialData->user_id   = $newSocialUser->id;
                $socialData->social_id = $user->id;
                $socialData->provider  = $provider;
                $socialData->save();

                $socialUser = $newSocialUser;

                $user->attachRole('normal');

                $newSocialUser->addTrialSubscribe();

            } else {

                //Load this existing social user
                $socialUser = $sameSocialId->user;
                $socialUser->email = $email;
                $socialUser->save();
            }

        }

        return $socialUser;

    }
}
