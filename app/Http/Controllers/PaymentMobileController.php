<?php

namespace App\Http\Controllers;

use App\Http\Requests\MobilePaymentRequest;
use App\Payment\CodaPay\InitTxnRequest;
use App\Payment\CodaPay\ItemInfo;
use App\Payment\CodaPay\Profile;
use App\PaymentOrder;
use Illuminate\Http\Request;
use App\Payment\CodaPay\Item as CodaItem;
use CodaPay;
use Session;
use Validator;

class PaymentMobileController extends Controller
{

    public function showCodaPayPayment(Request $request) {

        $data = $request->all();

        $validator = $this->validateData($data);

        if ($validator->fails()) {
            abort(403);
        }

        $txtId = $this->payWithCodaPay($data);

        return view('pages.codapay-mobile', compact('txtId'));
    }

    public function showMpuPayPayment(Request $request) {

        $data = $request->all();

        $validator = $this->validateData($data);

        if ($validator->fails()) {
            abort(403);
        }

        return view('pages.mpu-mobile', compact('data'));
    }

    public function showPayPal(Request $request) {

        $data = $request->all();

        $validator = $this->validateData($data);

        if ($validator->fails()) {
            abort(403);
        }

        return view('pages.paypal-mobile', compact('data'));
    }


    public function validateData($data) {
        $validator = Validator::make($data, [
            'amount' => 'required|numeric|in:1000,3000,5000',
            'user_id' => 'required',
        ]);

        return $validator;
    }

    public function transactionSuccess() {
        return view('pages.transaction-success-mobile');
    }

    public function transactionFailed() {
        return view('pages.transaction-failed-mobile');
    }

    protected function create_signature_string($input_fields_array)
    {
        sort($input_fields_array, SORT_STRING);

        $signature_string = "";
        foreach($input_fields_array as $value)
        {
            if ($value != "")
            {
                $signature_string .= $value;
            }
        }

        return $signature_string;
    }

    public function payWithMPU(MobilePaymentRequest $request) {

        $merchant_id 		= config('mpu.merchant_id');
        $product_desc       = 'mmn';
        $order_id           = uniqid();
        $invoice_no 		= $order_id;
        $currency 			= config('mpu.currency');

        $amount             = str_pad($request->get('amount') . '00', 12, '0', STR_PAD_LEFT);
        $user_defined_1 	= $request->get('user_id');
        $user_defined_2 	= '';
        $user_defined_3 	= '';
        $secret_key 		= config('mpu.secret');

        $input_fields_array = [
            $merchant_id , $product_desc , $invoice_no , $currency , $amount , $user_defined_1 , $user_defined_2 , $user_defined_3
        ];

        $signature_string = $this->create_signature_string($input_fields_array);


        $HashValue = hash_hmac('sha1', $signature_string, $secret_key, false);
        $HashValue = strtoupper($HashValue);

        $data = [
            'url'                   => config('mpu.url'),
            'merchant_id' 			=> $merchant_id,
            'product_desc' 	        => $product_desc,
            'invoice_no' 			=> $order_id,
            'currency' 				=> $currency,
            'amount' 				=> $amount,
            'user_defined_1' 		=> $user_defined_1,
            'user_defined_2' 		=> $user_defined_2,
            'user_defined_3' 		=> $user_defined_3,
            'HashValue' 			=> $HashValue
        ];

        PaymentOrder::create([
            'user_id' => $request->get('user_id'),
            'transaction_id' => $order_id,
            'payment_service' => 'mpu',
            'amount' => request('amount'),
            'client' => 'mobile',
        ]);

        return response()->json($data);

    }

    public function payWithCodaPay($data) {

        $itemList = [];
        $item = new CodaItem();
        $item->setName('m-'. $data['amount'])
            ->setCode('m-coda')
            ->setPrice((double) $data['amount']);

        $profile = new Profile();
        $profile->setUserId($data['user_id']);

        array_push ($itemList, $item);

        $request = new InitTxnRequest();
        $request->setOrderId((string) round(microtime(true) * 1000))
            ->setItems($itemList)
            ->setProfile($profile);

        $result = CodaPay::initTxn($request);

        PaymentOrder::create([
            'user_id' => request('user_id'),
            'transaction_id' => $result->txnId,
            'payment_service' => 'codapay',
            'amount' => $data['amount'],
            'client' => 'mobile',
        ]);

        /** add payment ID to session **/
        Session::put('codapay_txn_id', $result->txnId);

        return $result->txnId;
    }
}
