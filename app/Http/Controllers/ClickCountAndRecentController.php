<?php

namespace App\Http\Controllers;

use App\FullSongView;
use App\PlayCount;
use App\Recent;
use Request;
use Session;
use Auth;

class ClickCountAndRecentController extends Controller
{
    public function create($id) {

        FullSongView::track($id);

        if (!Session::has('song_track') || Session::get('song_track') !== Request::path()) {

            PlayCount::track($id);

            if (Auth::check())
                Recent::track($id);

            Session::put('song_track', Request::path());

            return response()->json(['message' => 'saved']);

        }else {
            return response()->json(['message' => Session::get('song_track')]);
        }
    }
}
