<?php

namespace App\Http\Controllers;

use App\PlayList;
use MyanFont;
use App\Album;
use App\Artist;
use App\Song;

class SearchController extends Controller
{
    private $query;

    public function __construct()
    {
        $this->query = MyanFont::zg2uni(request('q'));
    }

    public function bySong() {
        return Song::where('name_eng', 'LIKE', '%'. $this->query .'%')
            ->orWhere('name_mm', 'LIKE', '%'. $this->query .'%')
            ->has('albums')
            ->limit(5)
            ->get();
    }

    public function byArtist() {
        return Artist::where('artist_name_mm', 'like', '%'. $this->query .'%')
            ->orWhere('artist_name_eng', 'like', '%'. $this->query .'%')
            ->limit(5)
            ->get();
    }

    public function byAlbum() {
        return Album::where('album_name_mm', 'LIKE', '%'. $this->query .'%')
            ->orWhere('album_name_eng', 'LIKE', '%'. $this->query .'%')
            ->where('scheduled_date', '<', date('Y-m-d H:i:s'))
            ->limit(5)
            ->get();
    }

    public function byPlaylist() {
        return PlayList::where('playlist_name_mm', 'LIKE', '%'. $this->query .'%')
            ->orWhere('playlist_name_eng', 'LIKE', '%'. $this->query .'%')
            ->where('playlist_scheduled_date', '<', date('Y-m-d H:i:s'))
            ->limit(5)
            ->get();
    }

    public function showAll() {

        $songs = Song::where('name_eng', 'LIKE', '%'. $this->query .'%')
            ->orWhere('name_mm', 'LIKE', '%'. $this->query .'%')
            ->with(['artists','feats','albums','price', 'auth_user_order'])
            ->has('albums')
            ->limit(5)
            ->get();

        $artists = Artist::where('artist_name_mm', 'like', '%'. $this->query .'%')
            ->orWhere('artist_name_eng', 'like', '%'. $this->query .'%')
            ->limit(5)
            ->get();

        $albums = Album::where('album_name_mm', 'LIKE', '%'. $this->query .'%')
            ->orWhere('album_name_eng', 'LIKE', '%'. $this->query .'%')
            ->where('scheduled_date', '<', date('Y-m-d H:i:s'))
            ->with(['artists'])
            ->limit(5)
            ->get();

        $playlist = PlayList::where('playlist_name_mm', 'LIKE', '%'. $this->query .'%')
            ->orWhere('playlist_name_eng', 'LIKE', '%'. $this->query .'%')
            ->where('playlist_scheduled_date', '<', date('Y-m-d H:i:s'))
            ->limit(5)
            ->get();

        $query = $this->query;

        return view('pages.search', compact('songs', 'artists', 'albums', 'playlist', 'query'));
    }

}
