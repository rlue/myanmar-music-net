<?php

namespace App\Http\Controllers;

use App\PaymentOrder;
use Illuminate\Http\Request;
use Session;
use Redirect;
use CodaPay;
use Log;

class HandelPaymentRespondController extends Controller
{
    public function codaResponse() {

        if (empty(request('TxnId')) || empty(request('OrderId'))) {
            Session::put('error', 'Payment failed');
            return Redirect::route('/payment');
        }

        $result = null;
        $errorCode = 0;
        $errMsg = null;
        $txnId = null;
        $orderId = null;
        $totalPrice = 0.0;

        if ( strlen (request('TxnId')) == 0 ) {
            $errMsg = "Incomplete transaction error.";
        } else {
            /*
             *  Optional: for double check the transaction result
             */

            $txnId = request('TxnId');
            $result = CodaPay::inquiryPayment($txnId);

            if (is_null($result)) {
                Session::put('error', "Incomplete transaction error.");
                return redirect('/');
            }

            $errorCode = $result->resultCode;
            $errMsg = $result->resultDesc;
        }

        $payment_order = PaymentOrder::where('transaction_id', $txnId)
            ->where('payment_service', 'codapay')
            ->where('status', 'pending')
            ->first();

        $user = $payment_order->user;

        if($errorCode > 0) {
            $payment_order->status = 'failed';
            $payment_order->save();

            if ($payment_order->client === 'mobile') {
                return redirect('/transaction_failed');
            }

            Session::put('error', $errMsg);
            return redirect('/');
        }

        if (is_null($payment_order)) {
            return redirect('/')->with('status', 'danger')->with('message', 'unauthorized transaction');
        }


        $user->remained_amount += (int) $payment_order->amount;
        $user->save();

        $payment_order->status = 'success';
        $payment_order->save();

        if ($payment_order->client === 'mobile') {
            return redirect('/transaction_success');
        }

        return redirect('/user/profile')->with('status', 'success')->with('message', 'Payment success');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function getMPUFrondEndRespond(Request $request) {

        $inputs = $request->all();

        Log::info('MPU frond end respond');
        Log::info($inputs);

        $payment_order = PaymentOrder::where('transaction_id', $inputs['invoiceNo'])
            ->where('payment_service', 'mpu')
            ->first();

        if (is_null($payment_order)) {
            Session::put('error', 'unauthorized transaction');
            return redirect('/');
        }

        if ($inputs['respCode'] != '00') {

            if ($payment_order->client === 'mobile') {
                return redirect('/transaction_failed');
            }

            return redirect('/');
        }

        if ($payment_order->status === 'success') {
            if ($payment_order->client === 'mobile') {
                return redirect('/transaction_success');
            }

            Session::put('success', 'Payment success');

            return redirect('/user/profile');
        }

        Session::put('error', 'failed');

        if ($payment_order->client === 'mobile') {
            return redirect('/transaction_failed');
        }

        return redirect('/');

    }

    /**
     * @param Request $request
     * @return string|void
     *
     * @author Tint Naing Win <tnwdeveloper@gmail.com>
     */
    public function getMPUBackEndRespond(Request $request) {

        $inputs = $request->all();

        Log::info('MPU Back end respond');
        Log::info($inputs);

        $payment_order = PaymentOrder::where('transaction_id', $inputs['invoiceNo'])
            ->where('payment_service', 'mpu')
            ->where('status', 'pending')
            ->first();

        if (is_null($payment_order)) {
            return;
        }

        if ($inputs['respCode'] != '00') {

            $payment_order->status = 'failed';
            $payment_order->save();

            return;
        }

        $user = $payment_order->user;


        $user->remained_amount += (int) $payment_order->amount;
        $user->save();

        $payment_order->status = 'success';
        $payment_order->save();

    }
}
