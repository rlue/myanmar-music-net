<?php

namespace App\Repositories;



use App\Bill;
use App\User;
use Yajra\Datatables\Datatables;
use DB;
use Carbon\Carbon;
class BillRepo
{
    public function getBill() {
        $cat = $this->getBillDT();
        $datatables = Datatables::of($cat) 
                        ->addColumn('no', function ($cat) { return ''; })
                        ->editColumn('created_at', function ($cat) {
                                return $cat->created_at ? with(new Carbon($cat->created_at))->format('d-m-Y') : '';
                            })
                        ->addColumn('action', function ($cat) {
                          // $btn = '<a href="'. route('subprice.edit', $cat) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                          $btn = '<a href="#" data-id="'.$cat->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getBillDT() {
      $price = Bill::select('users.name as name','bills.*')
      ->join('users','users.id','=','bills.user_id')
      ->get();
      return $price;
    }

    public function getBillName($id) {
        $price = Bill::find($id);    
        return $price;
    }  

    public function saveBill($request, $id = NULL) {
     
        $input = $request->all();
        if ($id === NULL) {
          $price = new Bill();
        }
        else {
          $price = Bill::find($id);
        }

         $saved = $price->fill($input)->save();
         if($id === NULL){
            $user = User::find($input['user_id']);
            $user->increment('remained_amount', $input['amount']);
         }

        return ($saved) ? $price : FALSE;
    }

    public function deleteBill($id) {
      return Bill::find($id)->delete();
    }

    public function getBillList()
    {
        
      $bill = Bill::get();      
      return $bill;
    }
}