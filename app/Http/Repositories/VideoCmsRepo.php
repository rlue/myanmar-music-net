<?php

namespace App\Repositories;

use App\VideoCms;
use App\Video;
use Yajra\Datatables\Datatables;
use DB;
class VideoCmsRepo
{
    public function getVideoCms() {
        $cat = $this->getVideoCmsDT();
        $datatables = Datatables::of($cat) 
                        
                        ->addColumn('action', function ($cat) {
                          $btn = '<a href="'. route('videocms.edit', $cat) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                          $btn .= '<a href="#" data-id="'.$cat->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getVideoCmsDT() {
      $cms = VideoCms::get();
      return $cms;
    }

    public function getVideoCmsName($id) {
        $cms = VideoCms::find($id);    
        return $cms;
    } 

    public function createCms($name)
    {
        $cms = VideoCms::where('cms_type','=',$name)->first();
        return $cms;
    }

    public function storeCms($request)
    {
      $input = $request->all();
      $check = VideoCms::where('cms_type','=',$input['cms_type'])->first();

      if (empty($check)) {
          $cms = new VideoCms();
        }
        else {
          $cms = VideoCms::find($check->id);
        }

         $saved = $cms->fill($input)->save();
       
         if(isset($input['video_id'])){
            
            DB::table('cms_videos')->where('cms_id','=',$cms->id)->delete();
            $cms->cms_videos()->sync($input['video_id']);
         } 
         

         return $cms;
    }
    
    public function saveVideoCms($request, $id = NULL) {
     
      $input = $request->all();
        if ($id === NULL) {
          $cms = new VideoCms();
        }
        else {
          $cms = VideoCms::find($id);
        }

         $saved = $cms->fill($input)->save();
       
         
         $cms->cms_videos()->sync($input['video_id']);
         
         
        return ($saved) ? $cms : FALSE;
    }

    public function deleteVideoCms($id) {
      $cms =VideoCms::find($id); 
      $cms->cms_videos()->detach();  
      $cms->delete();
      
      return $cms;
    }

    public function getVideoCmsList()
    {
        
      $cms = VideoCms::get();      
      return $cms;
    }
}