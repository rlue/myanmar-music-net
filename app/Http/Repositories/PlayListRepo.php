<?php

namespace App\Repositories;

use App\PlayList;
use App\PlaylistView;
use App\Song;
use App\Feeling;
use App\SongCategory;
use Yajra\Datatables\Datatables;
use App\Repositories\SongRepo;
use DB;
class PlayListRepo
{
    protected $songRepo;
    


    public function __construct(SongRepo $songRepo) {
       
        $this->songRepo = $songRepo;
        
    }
    public function getPlayList() {
        $cat = $this->getPlayListDT();
        $datatables = Datatables::of($cat) 
                        
                        ->addColumn('action', function ($cat) {
                          $btn = '<a href="'. route('playlists.edit', $cat) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                          $btn .= '<a href="#" data-id="'.$cat->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getPlayListDT() {
      $playlist = PlayList::get();
      return $playlist;
    }

    public function getPlayListName($id) {
        $playlist = PlayList::find($id);    
        return $playlist;
    }  

    public function savePlayList($request, $id = NULL) {
     
      $input = $request->all();
        if ($id === NULL) {
          $playlist = new PlayList();
        }
        else {
          $playlist = PlayList::find($id);
        }

         $saved = $playlist->fill($input)->save();
       
        if(isset($input['song_id'])){
            
            DB::table('playlist_songs')->where('playlist_id','=',$playlist->id)->delete();
            $playlist->songs()->sync($input['song_id']);
         } 
         
         
        
        return ($saved) ? $playlist : FALSE;
    }

    public function deletePlayList($id) {
      $playlist =PlayList::find($id); 
      $playlist->songs()->detach(); 
      $playlist->delete();
      
      return $playlist;
    }

    public function getPlayListList()
    {
        
      $playlist = PlayList::get();      
      return $playlist;
    }

    public function getPlaylistApi($playlists,$userid)
    {
        $playlist=[];
      
      foreach($playlists as $list)
        {
          $count =PlaylistView::select('view')->where('playlist_id','=',$list->id)->first();
        if(!$count){
            $count['view'] = 0;
        }  
        $playlist[] =  [
              'id' => $list->id,
              'name_mm' => $list->playlist_name_mm,
              'name_eng' => $list->playlist_name_eng,
              'image' => $list->playlist_image,
              'song' => $this->songRepo->getSongApi($list->songs,$userid),
              'scheduled_date' => $list->playlist_scheduled_date,
              'release_date' => $list->playlist_release_date,
              'description' => $list->playlist_desc,
              'status'    => $list->status,
              'view_count' => $count['view']
              
             
          ];
      }
      return $playlist;
    }
}