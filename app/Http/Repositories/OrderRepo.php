<?php

namespace App\Repositories;



use App\Order;
use Yajra\Datatables\Datatables;
use DB;
class OrderRepo
{
    public function getOrderSong() {
        $cat = $this->getOrderSongDT();
        $datatables = Datatables::of($cat) 
                        ->addColumn('no', function ($cat) { return ''; });

        
        return $datatables->make(true);
    }

    private function getOrderSongDT() {
      $category = DB::table('orders')
      ->select('users.name as name','songs.name_mm as name_mm','orders.*','prices.name as price')
        ->join('users','users.id','=','orders.user_id')
        ->join('songs','songs.id','=','orders.item_id')
        ->join('prices','prices.id','=','songs.prices')
        ->where('type','song')
        ->orderBy('id','desc')
        ->limit(10);
      return $category;
    }

   
    
}