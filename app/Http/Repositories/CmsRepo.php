<?php

namespace App\Repositories;

use App\Cms;
use App\Song;
use Yajra\Datatables\Datatables;
class CmsRepo
{
    public function getCms() {
        $cat = $this->getCmsDT();
        $datatables = Datatables::of($cat) 
                        
                        ->addColumn('action', function ($cat) {
                          $btn = '<a href="'. route('cms.edit', $cat) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                          $btn .= '<a href="#" data-id="'.$cat->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getCmsDT() {
      $cms = Cms::get();
      return $cms;
    }

    public function getCmsName($id) {
        $cms = Cms::find($id);    
        return $cms;
    }  

    public function saveCms($request, $id = NULL) {
     
      $input = $request->all();
        if ($id === NULL) {
          $cms = new Cms();
        }
        else {
          $cms = Cms::find($id);
        }

         $saved = $cms->fill($input)->save();
       
         
         $cms->cms_songs()->sync($input['song_id']);
         
         
        return ($saved) ? $cms : FALSE;
    }

    public function deleteCms($id) {
      $cms =Cms::find($id); 
      $cms->cms_songs()->detach();  
      $cms->delete();
      
      return $cms;
    }

    public function getCmsList()
    {
        
      $cms = Cms::get();      
      return $cms;
    }
}