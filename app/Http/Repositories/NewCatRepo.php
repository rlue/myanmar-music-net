<?php

namespace App\Repositories;



use App\NewCategory;
use Yajra\Datatables\Datatables;
class NewCatRepo
{
    public function getNewCategory() {
        $category = $this->getNewCategoryDT();
        $datatables = Datatables::of($category) 
                        ->addColumn('no', function ($category) { return ''; })
                        ->addColumn('action', function ($category) {
                             
                              
                              
                               $btn = '<a href="'. route('newcategory.edit', $category) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                              $btn .= '<a href="#" data-id="'.$category->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getNewCategoryDT() {
      $category = NewCategory::get();
      return $category;
    }

    public function getNewCategoryName($id) {
        $category = NewCategory::find($id);    
        return $category;
    }  

    public function saveNewCategory($input, $id = NULL) {
     
        if ($id === NULL) {
          $category = new NewCategory();
        }
        else {
          $category = NewCategory::find($id);
        }

         $saved = $category->fill($input)->save();

        return ($saved) ? $category : FALSE;
    }

    public function deleteNewCategory($id) {
        return NewCategory::find($id)->delete();
    }

    public function getNewCategoryList()
    {
        
        $categories = NewCategory::get();      
       
        return $categories;
    }
}