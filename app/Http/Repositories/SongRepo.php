<?php

namespace App\Repositories;


use App\Song;
use App\Feeling;
use App\SongCategory;
use Yajra\Datatables\Datatables;
use App\Transformers\ArtistResource;
use DB;
class SongRepo
{

    public function getSongListing()
    {
      $song = DB::table('songs as s')
      ->join('song_artists as sa', 'sa.song_id', '=', 's.id')
      ->join('artists as a', 'a.id', '=', 'sa.artist_id')
      ->join('song_categories as sc', 'sc.id', '=', 's.genre_id')
      ->join('prices as p', 'p.id', '=', 's.prices')
      ->join('album_songs as al', 'al.song_id', '=', 's.id')
      ->join('albums as alb', 'alb.id', '=', 'al.album_id')
      ->select('s.id as id','s.name_mm','a.artist_name_mm as artists','sc.name as catname','p.name as price','alb.album_image','alb.album_name_mm as albums','alb.sub_menu','p.name as prices','sc.name as genre');
      return $song;
    }

    public function getSong() {
        $cat = $this->getSongDT();
        $datatables = Datatables::of($cat) 
                        ->editColumn('album_image', function ($cat) {
                            foreach($cat->albums as $a){
                                return $a->album_image;
                            }
                          })
                        ->editColumn('albums', function ($cat) {
                            foreach($cat->albums as $a){
                                return $a->album_name_mm;
                            }
                          })
                        ->editColumn('artists', function ($cat) {
                            return $cat->artists->implode('artist_name_eng', ', ');
                          })
                        ->editColumn('sub_menu', function ($cat) {
                          $show =[];
                            foreach($cat->albums as $a){
                              foreach($a->sub_menu as $key=>$val){
                                $show[] = $key;
                              } 
                              return implode($show , ', ');
                            }
                          })
                        ->addColumn('action', function ($cat) {
                          $btn = '<a href="'. route('songs.edit', $cat) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                          $btn .= '<a href="#" data-id="'.$cat->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        })
                        ->filterColumn('genre', function($query, $keyword) {
                    
                    $query->whereRaw('song_categories.name like ?', ["%{$keyword}%"]);
                        })
                        
                        ->filterColumn('price', function($query, $keyword) {
                    
                    $query->whereRaw('prices.name like ?', ["%{$keyword}%"]);
                        });

        
        return $datatables->make(true);
    }

    private function getSongDT() {
      $song = Song::select('songs.*','prices.name as price','song_categories.name as genre')
      ->with(['artists' => function ($query) {
          $query->select('id', 'artist_name_eng');
      },'albums' => function($query){
          $query->select('id','album_name_mm','sub_menu');
      }])
      ->join('song_categories','song_categories.id','=','songs.genre_id')
      ->join('prices','prices.id','=','songs.prices');
      return $song;
    }

    public function getSongName($id) {
        $song = Song::find($id);    
        return $song;
    }  

    public function saveSong($request, $id = NULL) {
     
      $input = $request->all();
        if ($id === NULL) {
            $song = new Song();
            $song->sub_menu = [ 'streaming' => false, 'store' => false, 'free' => true ];
        }
        else {
          $song = Song::find($id);
          $song->artists()->detach();
          $song->feats()->detach();
          if ($song->sub_menu === null) {
              $song->sub_menu = [ 'streaming' => false, 'store' => false, 'free' => true ];
          }
        }


         $saved = $song->fill($input)->save();
       
         $song->artists()->sync($input['artist_id']);
         if(isset($input['feat_id'])){
            $song->feats()->sync($input['feat_id']);
             
         }
        
         if($input['preview_fileType'] == 'preview_upload'){
          if($request->hasFile('preview_file')){
            $song->addMedia($request->file('preview_file'))->toMediaCollection('SongPreview');
            $song->preview_file = $song->getPreviewAudio($song->id);
             $song->preview_fileType = 'preview_s3';
             $song->save();
          }
              
         }
         if($input['full_fileType'] == 'full_upload'){
          if($request->hasFile('full_file')){
            $song->addMedia($request->file('full_file'))->toMediaCollection('SongFull');
            $song->full_file = $song->getFullAudio($song->id);
             $song->full_fileType = 'full_s3';
             $song->save();
           }   

         }
         
        return ($saved) ? $song : FALSE;
    }

    public function deleteSong($id) {
      $song =Song::find($id); 
      $song->artists()->detach();
      $song->feats()->detach();    
      $song->delete();
      
      return $song;
    }

    public function getSongList()
    {
        
      $song = Song::limit(50)->get();      
      return $song;
    }

    public function getSongApi($songlist,$id)
    {
        $song=[];
        foreach($songlist as $list)
        {
            $album = $list->albums->first();
        if(isset($album->id)){
            $albumid = $album->id;
            $albumimage =$album->album_image;
            $sub_menu = $album->sub_menu;
        }else{
            $albumid = null;
            $albumimage = null;
            $sub_menu = null;
        }
        $song[] =  [
            'id' => $list->id,
            'sub_menu' => $sub_menu,
            'name_mm' => $list->name_mm,
            'name_eng' => $list->name_eng,
            'prices' => $list->price,
            'category' =>$list->genre,
            'feeling' => $list->feeling,
            'artists' => ArtistResource::collection($list->artists),
            'feats' => $list->feats,
            'preview_fileType' => $list->preview_fileType,
            'preview_file' => $list->preview_file,
            'full_fileType' => $list->full_fileType,
            'full_file' => $list->full_file,
            'full_lowFileType' => $list->full_lowFileType,
            'full_lowFile'  => $list->full_lowFile,
            'duration' => $list->duration,
            'click_count' =>$list->getPlayCount(),
            'album_id'  => $albumid,
            'album_image' => $albumimage,
            'favourites' => $list->checkFavourite($list->id,$id),
            'bought' => $list->checkOrder($list->id,$id),
            'comments' => $list->comments
            
           
        ];
        }
        return $song;
    }

    public function getSongSortApi($songlist,$id)
    {
        $song=[];
        foreach($songlist as $list)
        {
            $album = $list->albums->first();
        if(isset($album->id)){
            $albumid = $album->id;
            $albumimage =$album->album_image;
        }else{
            $albumid = null;
            $albumimage = null;
        }
        $song[] =  [
            'id' => $list->id,
            'name_mm' => $list->name_mm,
            'name_eng' => $list->name_eng,
            'prices' => $list->price,
            'artists' => ArtistResource::collection($list->artists),
            'feats' => $list->feats,
            'preview_fileType' => $list->preview_fileType,
            'preview_file' => $list->preview_file,
            'full_fileType' => $list->full_fileType,
            'full_file' => $list->full_file,
            'full_lowFileType' => $list->full_lowFileType,
            'full_lowFile'  => $list->full_lowFile,
            'duration' => $list->duration,
            'click_count' =>$list->getPlayCount(),
            'favourites' => $list->checkFavourite($list->id,$id),
            'bought' => $list->checkOrder($list->id,$id),
            'comments' => $list->comments
            
           
        ];
        }
        return $song;
      }

      public function getSearchSong($request)
      {
          $search = $request->get('q');
          $song = Song::with('artists')
          ->select('songs.id','songs.name_mm')
          ->where('songs.name_mm','LIKE','%'. $search .'%')
          ->limit(50)
          ->get();
          return $song;
      }
}