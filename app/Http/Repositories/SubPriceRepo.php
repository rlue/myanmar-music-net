<?php

namespace App\Repositories;



use App\SubscriptionPrice;
use Yajra\Datatables\Datatables;
class SubPriceRepo
{
    public function getSubPrice() {
        $cat = $this->getSubPriceDT();
        $datatables = Datatables::of($cat) 
                        ->addColumn('no', function ($cat) { return ''; })
                        ->addColumn('action', function ($cat) {
                          $btn = '<a href="'. route('subprice.edit', $cat) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                          $btn .= '<a href="#" data-id="'.$cat->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getSubPriceDT() {
      $price = SubscriptionPrice::get();
      return $price;
    }

    public function getSubPriceName($id) {
        $price = SubscriptionPrice::find($id);    
        return $price;
    }  

    public function saveSubPrice($input, $id = NULL) {
     
        if ($id === NULL) {
          $price = new SubscriptionPrice();
        }
        else {
          $price = SubscriptionPrice::find($id);
        }

         $saved = $price->fill($input)->save();

        return ($saved) ? $price : FALSE;
    }

    public function deleteSubPrice($id) {
      return SubscriptionPrice::find($id)->delete();
    }

    public function getSubPriceList()
    {
        
      $price = SubscriptionPrice::get();      
      return $price;
    }
}