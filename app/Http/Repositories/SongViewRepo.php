<?php

namespace App\Repositories;


use App\Song;
use App\FullSongView;
use Yajra\Datatables\Datatables;
use DB;
class SongViewRepo
{

   

    public function getSongView() {
        $cat = $this->getSongViewDT();
        $datatables = Datatables::of($cat) 
                        ->addColumn('no', function ($artist) { return ''; })
                        // ->editColumn('artists', function ($cat) {
                        //     return $cat->artists->implode('artist_name_eng', ', ');
                        //   })
                        ->filterColumn('name_eng', function($query, $keyword) {
                    
                    $query->whereRaw('songs.name_eng like ?', ["%{$keyword}%"]);
                        });

        
        return $datatables->make(true);
    }

    private function getSongViewDT() {
      $song = Song::with('artists')->select('songs.*','full_song_views.count')
      ->rightJoin('full_song_views','full_song_views.song_id','=','songs.id');
      return $song;
    }

}