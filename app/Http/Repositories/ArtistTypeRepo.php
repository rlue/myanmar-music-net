<?php

namespace App\Repositories;



use App\ArtistType;
use Yajra\Datatables\Datatables;
class ArtistTypeRepo
{
    public function getArtistType() {
        $type = $this->getArtistTypeDT();
        $datatables = Datatables::of($type) 
                        ->addColumn('no', function ($type) { return ''; })
                        ->addColumn('action', function ($type) {
                             
                              
                              
                               $btn = '<a href="'. route('artisttype.edit', $type) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                              $btn .= '<a href="#" data-id="'.$type->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getArtistTypeDT() {
      $type = ArtistType::get();
      return $type;
    }

    public function getArtistTypeName($id) {
        $type = ArtistType::find($id);    
        return $type;
    }  

    public function saveArtistType($input, $id = NULL) {
     
        if ($id === NULL) {
          $type = new ArtistType();
        }
        else {
          $type = ArtistType::find($id);
        }

         $saved = $type->fill($input)->save();

        return ($saved) ? $type : FALSE;
    }

    public function deleteArtistType($id) {
        return ArtistType::find($id)->delete();
    }

    public function getArtistTypeList()
    {
        
        $types = ArtistType::get()->toArray();      
      
        return $types;
    }
}