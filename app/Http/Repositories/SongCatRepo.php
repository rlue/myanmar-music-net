<?php

namespace App\Repositories;



use App\SongCategory;
use Yajra\Datatables\Datatables;
class SongCatRepo
{
    public function getSongCategory() {
        $cat = $this->getSongCategoryDT();
        $datatables = Datatables::of($cat) 
                        ->addColumn('no', function ($cat) { return ''; })
                        ->addColumn('action', function ($cat) {
                          $btn = '<a href="'. route('songcategory.edit', $cat) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                          $btn .= '<a href="#" data-id="'.$cat->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getSongCategoryDT() {
      $category = SongCategory::get();
      return $category;
    }

    public function getSongCategoryName($id) {
        $category = SongCategory::find($id);    
        return $category;
    }  

    public function saveSongCategory($input, $id = NULL) {
     
        if ($id === NULL) {
          $category = new SongCategory();
        }
        else {
          $category = SongCategory::find($id);
        }

         $saved = $category->fill($input)->save();

        return ($saved) ? $category : FALSE;
    }

    public function deleteSongCategory($id) {
      return SongCategory::find($id)->delete();
    }

    public function getSongCategoryList()
    {
        
      $category = SongCategory::get();      
      return $category;
    }
}