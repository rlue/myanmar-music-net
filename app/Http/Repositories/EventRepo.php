<?php

namespace App\Repositories;



use App\Event;
use Yajra\Datatables\Datatables;
class EventRepo
{
    public function getEvent() {
        $category = $this->getEventDT();
        $datatables = Datatables::of($category) 
                        ->addColumn('no', function ($category) { return ''; })
                        ->addColumn('action', function ($category) {
                             
                              
                              
                               $btn = '<a href="'. route('events.edit', $category) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                              $btn .= '<a href="#" data-id="'.$category->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getEventDT() {
      $event = Event::get();
      return $event;
    }

    public function getEventName($id) {
        $event = Event::find($id);    
        return $event;
    }  

    public function saveEvent($request, $id = NULL) {
     
        $input = $request->all();
        if ($id === NULL) {
          $event = new Event();
        }
        else {
          $event = Event::find($id);
        }

         $saved = $event->fill($input)->save();

        return ($saved) ? $event : FALSE;
    }

    public function deleteEvent($id) {
        return Event::find($id)->delete();
    }

    public function getNewList()
    {
        
        $event = Event::get();      
       
        return $event;
    }
}