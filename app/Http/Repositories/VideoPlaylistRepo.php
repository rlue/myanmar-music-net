<?php

namespace App\Repositories;

use App\VideoPlaylist;
use App\Video;
use Yajra\Datatables\Datatables;
use DB;
class VideoPlaylistRepo
{
    public function getVideoPlaylist() {
        $cat = $this->getVideoPlaylistDT();
        $datatables = Datatables::of($cat) 
                        
                        ->addColumn('action', function ($cat) {
                          $btn = '<a href="'. route('videoplaylists.edit', $cat) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                          $btn .= '<a href="#" data-id="'.$cat->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getVideoPlaylistDT() {
      $playlist = VideoPlaylist::get();
      return $playlist;
    }

    public function getVideoPlaylistName($id) {
        $playlist = VideoPlaylist::find($id);    
        return $playlist;
    }  

    public function saveVideoPlaylist($request, $id = NULL) {
     
      $input = $request->all();
        if ($id === NULL) {
          $playlist = new VideoPlaylist();
        }
        else {
          $playlist = VideoPlaylist::find($id);
        }

         $saved = $playlist->fill($input)->save();
       
        if(isset($input['video_id'])){
            
            DB::table('playlist_videos')->where('playlist_id','=',$playlist->id)->delete();
            $playlist->videos()->sync($input['video_id']);
         } 
         
         
        
        return ($saved) ? $playlist : FALSE;
    }

    public function deleteVideoPlaylist($id) {
      $playlist =VideoPlaylist::find($id); 
      $playlist->videos()->detach(); 
      $playlist->delete();
      
      return $playlist;
    }

    public function getVideoPlayListList()
    {
        
      $playlist = VideoPlaylist::get();      
      return $playlist;
    }
}