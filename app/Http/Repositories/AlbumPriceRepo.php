<?php

namespace App\Repositories;



use App\AlbumPrice;
use Yajra\Datatables\Datatables;
class AlbumPriceRepo
{
    public function getAlbumPrice() {
        $cat = $this->getAlbumPriceDT();
        $datatables = Datatables::of($cat) 
                        ->addColumn('no', function ($cat) { return ''; })
                        ->addColumn('action', function ($cat) {
                          $btn = '<a href="'. route('albumprices.edit', $cat) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                          $btn .= '<a href="#" data-id="'.$cat->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getAlbumPriceDT() {
      $price = AlbumPrice::get();
      return $price;
    }

    public function getAlbumPriceName($id) {
        $price = AlbumPrice::find($id);    
        return $price;
    }  

    public function saveAlbumPrice($input, $id = NULL) {
     
        if ($id === NULL) {
          $price = new AlbumPrice();
        }
        else {
          $price = AlbumPrice::find($id);
        }

         $saved = $price->fill($input)->save();

        return ($saved) ? $price : FALSE;
    }

    public function deleteAlbumPrice($id) {
      return AlbumPrice::find($id)->delete();
    }

    public function getAlbumPriceList()
    {
        
      $price = AlbumPrice::get();      
      return $price;
    }
}