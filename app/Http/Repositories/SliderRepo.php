<?php

namespace App\Repositories;



use App\Slider;
use Yajra\Datatables\Datatables;
class SliderRepo
{
    public function getSlider() {
        $slider = $this->getSliderDT();
        $datatables = Datatables::of($slider) 
                        ->addColumn('no', function ($slider) { return ''; })
                        ->addColumn('action', function ($slider) {
                             
                              
                              
                               $btn = '<a href="'. route('sliders.edit', $slider) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                              $btn .= '<a href="#" data-id="'.$slider->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getSliderDT() {
      $slider = Slider::get();
      return $slider;
    }

    public function getSliderName($id) {
        $slider = Slider::find($id);    
        return $slider;
    }  

    public function saveSlider($request, $id = NULL) {
     
        $input = $request->all();
        if ($id === NULL) {
          $slider = new Slider();
        }
        else {
          $slider = Slider::find($id);
        }

         $saved = $slider->fill($input)->save();

        return ($saved) ? $slider : FALSE;
    }

    public function deleteSlider($id) {
        return Slider::find($id)->delete();
    }

    public function getSliderList()
    {
        
        $news = Slider::get();      
       
        return $news;
    }
}