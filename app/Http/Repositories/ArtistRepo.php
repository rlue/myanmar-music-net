<?php

namespace App\Repositories;



use App\Artist;
use Yajra\Datatables\Datatables;
class ArtistRepo
{
    public function getArtist() {
        $artist = $this->getArtistDT();
        $datatables = Datatables::of($artist) 
                        ->addColumn('no', function ($artist) { return ''; })
                        ->addColumn('action', function ($artist) {
                            $btn = '<a href="'. route('artists.edit', $artist) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                            $btn .= '<a href="#" data-id="'.$artist->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                            return "<div class='action-column'>" . $btn . "</div>";
                        });

             
        return $datatables->make(true);
    }

    private function getArtistDT() {
      $artist = Artist::with('category','type')->get();
      return $artist;
    }

    public function getArtistName($id) {
        $artist = Artist::find($id);    
        return $artist;
    }  

    public function saveArtist($input, $id = NULL) {
     
        if ($id === NULL) {
          $artist = new Artist();
        }
        else {
          $artist = Artist::find($id);
        }

         $saved = $artist->fill($input)->save();

        return ($saved) ? $artist : FALSE;
    }

    public function deleteArtist($id) {
        return Artist::find($id)->delete();
    }

    public function getArtistList()
    {
        return Artist::get();
    }
}