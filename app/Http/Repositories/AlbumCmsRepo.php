<?php

namespace App\Repositories;

use App\AlbumCms;
use App\Album;
use Yajra\Datatables\Datatables;
use DB;
class AlbumCmsRepo
{
    public function getAlbumCms() {
        $cat = $this->getAlbumCmsDT();
        $datatables = Datatables::of($cat) 
                        
                        ->addColumn('action', function ($cat) {
                          $btn = '<a href="'. route('albumcms.edit', $cat) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                          $btn .= '<a href="#" data-id="'.$cat->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getAlbumCmsDT() {
      $cms = AlbumCms::get();
      return $cms;
    }

    public function getAlbumCmsName($id) {
        $cms = AlbumCms::find($id);    
        return $cms;
    }  

    public function saveAlbumCms($request, $id = NULL) {
     
      $input = $request->all();
        if ($id === NULL) {
          $cms = new AlbumCms();
        }
        else {
          $cms = AlbumCms::find($id);
        }

         $saved = $cms->fill($input)->save();
       
        if(isset($input['album_id'])){
            
            DB::table('cms_albums')->where('cms_id','=',$cms->id)->delete();
            $cms->cms_albums()->sync($input['album_id']);
         } 
         
         
         
        return ($saved) ? $cms : FALSE;
    }

    public function deleteAlbumCms($id) {
      $cms =AlbumCms::find($id); 
      $cms->cms_albums()->detach();  
      $cms->delete();
      
      return $cms;
    }

    public function getAlbumCmsList()
    {
        
      $cms = AlbumCms::get();      
      return $cms;
    }

    public function createCms($name)
    {
      $cms = AlbumCms::where('cms_type','=',$name)->first();
      return $cms;
    }
    public function storeCms($request)
    {
      $input = $request->all();
      $check = AlbumCms::where('cms_type','=',$input['cms_type'])->first();

      if (empty($check)) {
          $cms = new AlbumCms();
        }
        else {
          $cms = AlbumCms::find($check->id);
        }

         $saved = $cms->fill($input)->save();
       
         
         if(isset($input['album_id'])){
            
            DB::table('cms_albums')->where('cms_id','=',$cms->id)->delete();
            $cms->cms_albums()->sync($input['album_id']);
         } 

         return $cms;
    }
}