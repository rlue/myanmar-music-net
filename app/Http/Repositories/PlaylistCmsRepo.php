<?php

namespace App\Repositories;

use App\PlaylistCms;
use App\PlayList;
use Yajra\Datatables\Datatables;
use DB;
class PlaylistCmsRepo
{
  
    public function createCms($name)
    {
      $cms = PlaylistCms::where('cms_type','=',$name)->first();
      return $cms;
    }
    public function storeCms($request)
    {
      $input = $request->all();
      $check = PlaylistCms::where('cms_type','=',$input['cms_type'])->first();

      if (empty($check)) {
          $cms = new PlaylistCms();
        }
        else {
          $cms = PlaylistCms::find($check->id);
        }

         $saved = $cms->fill($input)->save();
       
         if(isset($input['playlist_id'])){
            
            DB::table('cms_playlists')->where('cms_id','=',$cms->id)->delete();
            $cms->cms_playlists()->sync($input['playlist_id']);
         }
         

         return $cms;
    }
}