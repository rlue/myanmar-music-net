<?php

namespace App\Repositories;

use App\AlbumList;
use App\Album;
use App\Feeling;
use App\SongCategory;
use Yajra\Datatables\Datatables;
use App\Repositories\AlbumRepo;
use DB;
class AlbumListRepo
{
    protected $albumRepo;
    


    public function __construct(AlbumRepo $albumRepo) {
       
        $this->albumRepo = $albumRepo;
        
    }

    public function getAlbumList() {
        $cat = $this->getAlbumListDT();
        $datatables = Datatables::of($cat) 
                        
                        ->addColumn('action', function ($cat) {
                          $btn = '<a href="'. route('albumlists.edit', $cat) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                          $btn .= '<a href="#" data-id="'.$cat->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getAlbumListDT() {
      $albumlist = AlbumList::get();
      return $albumlist;
    }

    public function getAlbumListName($id) {
        $albumlist = AlbumList::find($id);    
        return $albumlist;
    }  

    public function saveAlbumList($request, $id = NULL) {
     
      $input = $request->all();
        if ($id === NULL) {
          $albumlist = new AlbumList();
        }
        else {
          $albumlist = AlbumList::find($id);
        }

         $saved = $albumlist->fill($input)->save();
       
         if(isset($input['album_id'])){
            
            DB::table('albumlist_albums')->where('albumlist_id','=',$albumlist->id)->delete();
            $albumlist->albums()->sync($input['album_id']);
         }
         
         
        
        return ($saved) ? $albumlist : FALSE;
    }

    public function deleteAlbumList($id) {
      $albumlist =AlbumList::find($id); 
      $albumlist->albums()->detach(); 
      $albumlist->delete();
      
      return $albumlist;
    }

    public function getAlbumListList()
    {
        
      $albumlist = AlbumList::get();      
      return $albumlist;
    }

    public function getAlbumListApi($albumlist,$id)
    {
      $playlist=[];
      
      foreach($albumlist as $list)
        {
        $playlist[] =  [
            'id' => $list->id,
            'name_mm' => $list->albumlist_name_mm,
            'name_eng' => $list->albumlist_name_eng,
            'image' => $list->albumlist_image,
            'album' => $this->albumRepo->getAlbumApi($list->albums,$id),
            'scheduled_date' => $list->albumlist_scheduled_date,
            'release_date' => $list->albumlist_release_date,
            'description' => $list->albumlist_desc,

             
          ];
      }
      return $playlist;
    }
}