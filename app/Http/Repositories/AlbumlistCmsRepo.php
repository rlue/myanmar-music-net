<?php

namespace App\Repositories;

use App\AlbumlistCms;
use App\AlbumList;
use Yajra\Datatables\Datatables;
use DB;
class AlbumlistCmsRepo
{
  
    public function createCms($name)
    {
      $cms = AlbumlistCms::where('cms_type','=',$name)->first();
      return $cms;
    }
    public function storeCms($request)
    {
      $input = $request->all();
      $check = AlbumlistCms::where('cms_type','=',$input['cms_type'])->first();

      if (empty($check)) {
          $cms = new AlbumlistCms();
        }
        else {
          $cms = AlbumlistCms::find($check->id);
        }

         $saved = $cms->fill($input)->save();
       
         if(isset($input['albumlist_id'])){
            
            DB::table('cms_albumlists')->where('cms_id','=',$cms->id)->delete();
            $cms->cms_albumlists()->sync($input['albumlist_id']);
         } 
         

         return $cms;
    }
}