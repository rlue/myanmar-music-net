<?php

namespace App\Repositories;



use App\Aggregator;
use Yajra\Datatables\Datatables;
class AggregatorRepo
{
    public function getAggregator() {
        $category = $this->getAggregatorDT();
        $datatables = Datatables::of($category) 
                        ->addColumn('no', function ($category) { return ''; })
                        ->addColumn('action', function ($category) {
                             
                              
                              
                               $btn = '<a href="'. route('aggregator.edit', $category) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                              $btn .= '<a href="#" data-id="'.$category->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getAggregatorDT() {
      $aggregator = Aggregator::get();
      return $aggregator;
    }

    public function getAggregatorName($id) {
        $aggregator = Aggregator::find($id);    
        return $aggregator;
    }  

    public function saveAggregator($input, $id = NULL) {
     
        if ($id === NULL) {
          $aggregator = new Aggregator();
        }
        else {
          $aggregator = Aggregator::find($id);
        }

         $saved = $aggregator->fill($input)->save();

        return ($saved) ? $aggregator : FALSE;
    }

    public function deleteAggregator($id) {
        return Aggregator::find($id)->delete();
    }

    public function getAggregatorList()
    {
        
        $aggregator = Aggregator::get();      
       
        return $aggregator;
    }
}