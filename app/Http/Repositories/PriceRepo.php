<?php

namespace App\Repositories;



use App\Price;
use Yajra\Datatables\Datatables;
class PriceRepo
{
    public function getPrice() {
        $cat = $this->getPriceDT();
        $datatables = Datatables::of($cat) 
                        ->addColumn('no', function ($cat) { return ''; })
                        ->addColumn('action', function ($cat) {
                          $btn = '<a href="'. route('prices.edit', $cat) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                          $btn .= '<a href="#" data-id="'.$cat->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getPriceDT() {
      $price = Price::get();
      return $price;
    }

    public function getPriceName($id) {
        $price = Price::find($id);    
        return $price;
    }  

    public function savePrice($input, $id = NULL) {
     
        if ($id === NULL) {
          $price = new Price();
        }
        else {
          $price = Price::find($id);
        }

         $saved = $price->fill($input)->save();

        return ($saved) ? $price : FALSE;
    }

    public function deletePrice($id) {
      return Price::find($id)->delete();
    }

    public function getPriceList()
    {
        
      $price = Price::get();      
      return $price;
    }
}