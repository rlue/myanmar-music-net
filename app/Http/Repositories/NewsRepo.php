<?php

namespace App\Repositories;



use App\News;
use Yajra\Datatables\Datatables;
class NewsRepo
{
    public function getNew() {
        $category = $this->getNewDT();
        $datatables = Datatables::of($category) 
                        ->addColumn('no', function ($category) { return ''; })
                        ->addColumn('action', function ($category) {
                             
                              
                              
                               $btn = '<a href="'. route('news.edit', $category) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                              $btn .= '<a href="#" data-id="'.$category->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getNewDT() {
      $news = News::with('category')->get();
      return $news;
    }

    public function getNewName($id) {
        $news = News::find($id);    
        return $news;
    }  

    public function saveNew($request, $id = NULL) {
     
        $input = $request->all();
        if ($id === NULL) {
          $news = new News();
        }
        else {
          $news = News::find($id);
        }

         $saved = $news->fill($input)->save();

        return ($saved) ? $news : FALSE;
    }

    public function deleteNew($id) {
        return News::find($id)->delete();
    }

    public function getNewList()
    {
        
        $news = News::get();      
       
        return $news;
    }
}