<?php

namespace App\Repositories;



use App\Feeling;
use Yajra\Datatables\Datatables;
class FeelingRepo
{
    public function getFeeling() {
        $feeling = $this->getFeelingDT();
        $datatables = Datatables::of($feeling) 
                        ->addColumn('no', function ($feeling) { return ''; })
                        ->addColumn('action', function ($feeling) {
                          $btn = '<a href="'. route('feelings.edit', $feeling) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                          $btn .= '<a href="#" data-id="'.$feeling->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getFeelingDT() {
      $feeling = Feeling::get();
      return $feeling;
    }

    public function getFeelingName($id) {
        $feeling = Feeling::find($id);    
        return $feeling;
    }  

    public function saveFeeling($input, $id = NULL) {
     
        if ($id === NULL) {
          $feeling = new Feeling();
        }
        else {
          $feeling = Feeling::find($id);
        }

         $saved = $feeling->fill($input)->save();

        return ($saved) ? $feeling : FALSE;
    }

    public function deleteFeeling($id) {
      return Feeling::find($id)->delete();
    }

    public function getFeelingList()
    {
        
      $feeling = Feeling::get();      
      return $feeling;
    }
}