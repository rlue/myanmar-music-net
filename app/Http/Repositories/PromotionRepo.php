<?php

namespace App\Repositories;

use App\Promotion;
use Yajra\Datatables\Datatables;
class PromotionRepo
{
    public function getPromotion() {
        $cat = $this->getPromotionDT();
        $datatables = Datatables::of($cat) 
                        ->addColumn('no', function ($artist) { return ''; })
                        ->addColumn('action', function ($cat) {
                          $btn = '<a href="'. route('promotions.edit', $cat) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                          $btn .= '<a href="#" data-id="'.$cat->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getPromotionDT() {
      $promotion = Promotion::get();
      return $promotion;
    }

    public function getPromotionName($id) {
        $promotion = Promotion::find($id);    
        return $promotion;
    }  

    public function savePromotion($request, $id = NULL) {
     
      $input = $request->all();
        if ($id === NULL) {
          $promotion = new Promotion();
        }
        else {
          $promotion = Promotion::find($id);
        }

         $saved = $promotion->fill($input)->save();
       
      
         
        return ($saved) ? $promotion : FALSE;
    }

    public function deletePromotion($id) {
      $promotion =Promotion::find($id);    
      $promotion->delete();
      
      return $promotion;
    }

    public function getPromotionList()
    {
        
      $promotion = Promotion::get();      
      return $promotion;
    }
}