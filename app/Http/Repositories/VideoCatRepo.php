<?php

namespace App\Repositories;



use App\VideoCategory;
use Yajra\Datatables\Datatables;
class VideoCatRepo
{
    public function getVideoCategory() {
        $category = $this->getVideoCategoryDT();
        $datatables = Datatables::of($category) 
                        ->addColumn('no', function ($category) { return ''; })
                        ->addColumn('action', function ($category) {
                             
                              
                              
                               $btn = '<a href="'. route('videocategory.edit', $category) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                              $btn .= '<a href="#" data-id="'.$category->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getVideoCategoryDT() {
      $category = VideoCategory::get();
      return $category;
    }

    public function getVideoCategoryName($id) {
        $category = VideoCategory::find($id);    
        return $category;
    }  

    public function saveVideoCategory($input, $id = NULL) {
     
        if ($id === NULL) {
          $category = new VideoCategory();
        }
        else {
          $category = VideoCategory::find($id);
        }

         $saved = $category->fill($input)->save();

        return ($saved) ? $category : FALSE;
    }

    public function deleteVideoCategory($id) {
        return VideoCategory::find($id)->delete();
    }

    public function getVideoCategoryList()
    {
        
        $categories = VideoCategory::get();      
       
        return $categories;
    }
}