<?php

namespace App\Repositories;



use App\ArtistCategory;
use Yajra\Datatables\Datatables;
class ArtistCatRepo
{
    public function getArtistCategory() {
        $category = $this->getArtistCategoryDT();
        $datatables = Datatables::of($category) 
                        ->addColumn('no', function ($category) { return ''; })
                        ->addColumn('action', function ($category) {
                             
                              
                              
                               $btn = '<a href="'. route('artistcategory.edit', $category) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                              $btn .= '<a href="#" data-id="'.$category->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getArtistCategoryDT() {
      $category = ArtistCategory::get();
      return $category;
    }

    public function getArtistCategoryName($id) {
        $category = ArtistCategory::find($id);    
        return $category;
    }  

    public function saveArtistCategory($input, $id = NULL) {
     
        if ($id === NULL) {
          $category = new ArtistCategory();
        }
        else {
          $category = ArtistCategory::find($id);
        }

         $saved = $category->fill($input)->save();

        return ($saved) ? $category : FALSE;
    }

    public function deleteArtistCategory($id) {
        return ArtistCategory::find($id)->delete();
    }

    public function getArtistCategoryList()
    {
        
        $categories = ArtistCategory::get()->toArray();      
       
        return $categories;
    }
}