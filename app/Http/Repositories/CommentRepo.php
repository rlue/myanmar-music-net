<?php

namespace App\Repositories;



use App\Comment;
class CommentRepo
{
    
    public  function getComment()
    {
        $comment = Comment::get();
        return $comment;
    }
    
    public function saveComment($input, $id = NULL) {
     
        if ($id === NULL) {
          $comment = new Comment();
        }
        else {
          $comment = Comment::find($id);
        }

         $saved = $comment->fill($input)->save();

        return ($saved) ? $comment : FALSE;
    }

    public function editComment($request)
    {
        $id = $request->get('comment_id');
        $comment = Comment::find($id);
        return $comment;
    }
    public function deleteComment($request) {
      $id = $request->get('comment_id');
      $user_id = $request->get('user_id');
      return Comment::where('id','=',$id)->where('user_id','=',$user_id)->delete();
    }
}