<?php

namespace App\Repositories;


use App\Video;
use App\Feeling;
use App\VideoCategory;
use Yajra\Datatables\Datatables;
class VideoRepo
{
    public function getVideo() {
        $cat = $this->getVideoDT();
        $datatables = Datatables::of($cat) 
                        ->addColumn('no', function ($cat) { return ''; })
                        // ->editColumn('category', function ($cat) {
                        //     foreach($cat->artists as $key=>$val){
                        //       $art = $val->artist_name_mm;
                        //     }  
                        //     return $art;                      
                        //   })
                       
                        ->addColumn('action', function ($cat) {
                          $btn = '<a href="'. route('videos.edit', $cat) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                          $btn .= '<a href="#" data-id="'.$cat->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getVideoDT() {
      $video = Video::with('category')->get();
      return $video;
    }

    public function getVideoName($id) {
        $video = Video::find($id);    
        return $video;
    }  

    public function saveVideo($request, $id = NULL) {
     
      $input = $request->all();
        if ($id === NULL) {
          $video = new Video();
        }
        else {
          $video = Video::find($id);
        }

         $saved = $video->fill($input)->save();
        if(isset($input['artist_id'])){
          $video->artists()->sync($input['artist_id']);
        }
         
        return ($saved) ? $video : FALSE;
    }

    public function deleteVideo($id) {
     return Video::find($id)->delete();
    }

    public function getVideoList()
    {
        
      $video = Video::get();      
      return $video;
    }
}