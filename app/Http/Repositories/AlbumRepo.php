<?php

namespace App\Repositories;

use App\Album;
use App\AlbumView;
use App\Song;
use App\Feeling;
use App\SongCategory;
use Yajra\Datatables\Datatables;
use App\Repositories\SongRepo;
use DB;
use App\Transformers\ArtistResource;
class AlbumRepo
{
    protected $songRepo;
    


    public function __construct(SongRepo $songRepo) {
       
        $this->songRepo = $songRepo;
        
    }
    public function getAlbum() {
        $cat = $this->getAlbumDT();
        $datatables = Datatables::of($cat) 
                        ->editColumn('artists', function ($cat) {
                            return $cat->artists->implode('artist_name_eng', ', ');
                          })
                        ->editColumn('sub_menu', function ($cat) {
                          $show =[];
                            
                              foreach($cat->sub_menu as $key=>$val){
                                if($val){
                                  $show[] = $key;
                                }
                                
                              } 
                              return implode($show , ', ');
                            
                          })
                        ->addColumn('action', function ($cat) {
                          $btn = '<a href="'. route('albums.edit', $cat) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                          $btn .= '<a href="#" data-id="'.$cat->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        })
                        ->filterColumn('price', function($query, $keyword) {
                    
                    $query->whereRaw('album_prices.name like ?', ["%{$keyword}%"]);
                        });

        
        return $datatables->make(true);
    }

    private function getAlbumDT() {
      $album = Album::select('albums.*','album_prices.name as price')
       ->with(['artists' => function ($query) {
          $query->select('id', 'artist_name_eng');
      }])
      ->join('album_prices','album_prices.id','=','albums.album_prices');
      return $album;
    }

    public function getAlbumName($id) {
      $album = Album::find($id);
      return $album;
    }  

    public function saveAlbum($request, $id = NULL) {
     
      $input = $request->all();
        if ($id === NULL) {
          $album = new Album();
        }
        else {
          $album = Album::find($id);
          $album->artists()->detach();
        }

         $input['sub_menu']['free'] = false;

         if (!isset($input['sub_menu']['store']) && !isset($input['sub_menu']['streaming'])) {
             $input['sub_menu']['free'] = true;
             $input['album_prices'] = 4;
         }

         if (!isset($input['sub_menu']['streaming'])) {
             $input['sub_menu']['streaming'] = false;
         }

         if (!isset($input['sub_menu']['store'])) {
             $input['sub_menu']['store'] = false;
         }

         $saved = $album->fill($input)->save();
       
         
          $album->artists()->sync($input['artist_id']);
         if(isset($input['song_id'])){
            
            DB::table('album_songs')->where('album_id','=',$album->id)->delete();
            $album->songs()->sync($input['song_id']);

            foreach($album->songs as $song) {
                $song->sub_menu = $input['sub_menu'];
                $song->save();
            }
         }
         
         if(isset($input['feat_id'])){
            $album->feats()->sync($input['feat_id']);
         }
        
         
        return ($saved) ? $album : FALSE;
    }

    public function deleteAlbum($id) {
      $album =Album::find($id); 
      $album->songs()->detach();
      $album->artists()->detach();
      $album->feats()->detach();    
      $album->delete();
      
      return $album;
    }

    public function getAlbumList()
    {
        
      $album = Album::where('status','=','publish')->get();      
      return $album;
    }

    public function getAlbumApi($albumlist,$id)
    {
      $album=[];
        foreach($albumlist as $list)
        {
          $count =AlbumView::select('view')->where('album_id','=',$list->id)->first();
        if(!$count){
            $count['view'] = 0;
        }  
        $album[] =  [
            'id' => $list->id,
            'sub_menu' => $list->sub_menu,
            'status'  => $list->status,
            'name_mm' => $list->album_name_mm,
            'name_eng' => $list->album_name_eng,
            'prices' => $list->prices,
            'image' => $list->album_image,
            'song' => $this->songRepo->getSongSortApi($list->songs,$id),
            'artist' => ArtistResource::collection($list->artists),
            'feat' => $list->feats,
            'scheduled_date' => $list->scheduled_date,
            'release_date' => $list->release_date,
            'copy_right' => $list->copy_right,
            'status'    => $list->status,
            'favourites' => $list->checkFavourite($list->id,$id),
            'bought' => $list->checkOrder($list->id,$id),
            'view_count' => $count['view']
           
        ];
        }
        return $album;
    }
}