<?php

namespace App\Repositories;



use App\Notification;
use Yajra\Datatables\Datatables;
class NotificationRepo
{
    public function getNoti() {
        $category = $this->getNotiDT();
        $datatables = Datatables::of($category) 
                        ->addColumn('no', function ($category) { return ''; })
                        ->addColumn('action', function ($category) {
                             
                              
                              
                               $btn = '<a href="'. route('notification.edit', $category) .'" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';

                              $btn .= '<a href="#" data-id="'.$category->id.'" class="btn btn-danger btn-sm sub-delete"><i class="fa fa-remove"></i></a>';
                             
                              return "<div class='action-column'>" . $btn . "</div>";
                        });

        
        return $datatables->make(true);
    }

    private function getNotiDT() {
      $noti = Notification::get();
      return $noti;
    }

    public function getNotiName($id) {
        $noti = Notification::find($id);    
        return $noti;
    }  

    public function saveNoti($request, $id = NULL) {
     
        $input = $request->all();
        if ($id === NULL) {
          $noti = new Notification();
        }
        else {
          $noti = Notification::find($id);
        }

         $saved = $noti->fill($input)->save();

        return ($saved) ? $noti : FALSE;
    }

    public function deleteNoti($id) {
        return Notification::find($id)->delete();
    }

    public function getNotiList()
    {
        
        $noti = Notification::get();      
       
        return $noti;
    }
}