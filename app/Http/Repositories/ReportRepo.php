<?php

namespace App\Repositories;


use App\User;
use App\PaymentOrder;
use App\Song;
use App\Album;
use App\Order;
use App\PlayCount;
use App\FullSongView;
use App\Subscribe;
use App\Recent;
use Yajra\Datatables\Datatables;
use Excel;
use DB;
use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;
class ReportRepo
{
  
    
    public function getPayment()
    {
        $cat = $this->getPaymentDT();
        $datatables = Datatables::of($cat)
            ->addColumn('no', function ($category) { return ''; })
            ->editColumn('created_at', function ($category) {
                                return date('d-m-Y', strtotime($category->created_at));
                            })
            ->filterColumn('name', function($query, $keyword) {
                    
                    $query->whereRaw('users.name like ?', ["%{$keyword}%"]);
                });
        return $datatables->make(true);
    }  
    
    private function getPaymentDT() {
      $payment = PaymentOrder::
      select('users.name as name','payment_orders.status','payment_orders.created_at','payment_orders.amount')
      ->join('users','users.id','=','payment_orders.user_id');
      return $payment;
    }

    public function getSongOrder()
    {
        $cat = $this->getOrderSongDT();
        $datatables = Datatables::of($cat)
            ->addColumn('no', function ($category) { return ''; })
            ->editColumn('artists', function ($cat) {
                            return $cat->artists->implode('artist_name_eng', ', ');
                          })
            ->filterColumn('name', function($query, $keyword) {
                    
                    $query->whereRaw('users.name like ?', ["%{$keyword}%"]);
                });
        return $datatables->make(true);
    }  
    
    private function getOrderSongDT() {
      $payment = Song::with('artists')
        ->select('users.name as name','songs.name_eng as name_eng','orders.*','prices.name as price')
        ->rightJoin('orders','orders.item_id','=','songs.id')
        ->join('users','users.id','=','orders.user_id')
        ->join('prices','prices.id','=','songs.prices')
        ->where('orders.type','song');
      return $payment;
    }

     public function getAlbumOrder()
    {
        $cat = $this->getOrderAlbumDT();
        $datatables = Datatables::of($cat)
            ->addColumn('no', function ($category) { return ''; })
            ->editColumn('artists', function ($cat) {
                            return $cat->artists->implode('artist_name_eng', ', ');
                          })
            ->filterColumn('name', function($query, $keyword) {
                    
                    $query->whereRaw('users.name like ?', ["%{$keyword}%"]);
                });
        return $datatables->make(true);
    }  
    
    private function getOrderAlbumDT() {
      $payment = Album::with('artists')
        ->select('users.name as name','albums.album_name_eng as album_name_eng','orders.*','album_prices.name as price')
        ->rightJoin('orders','orders.item_id','=','albums.id')
        ->join('users','users.id','=','orders.user_id')
        ->join('album_prices','album_prices.id','=','albums.album_prices')
        ->where('orders.type','song');
      return $payment;
    }

    public function reportPayment($request)
    {
      $user = $request->get('user');
      $from = $request->get('from_date');
      $to = $request->get('to_date');
      $status = $request->get('status');
      $username = 'MMN';
      $payment = PaymentOrder::orwhere('user_id','=',$user)
      ->where('created_at','>=',$from)
      ->where('created_at','<=',$to)
      ->orwhere('status','=',$status)
      ->get();
      
      $currentdate = Date('d-M-Y');
      $paymentArray[0] = ['Payment Report', '','','',''];
      $paymentArray[] = ['Report By : '.$username, '', '','Report Date : '.$currentdate,''];
      $paymentArray[] = ['Start Date : '.$from,'', '','End Date :'.$to];
      $paymentArray[] = ['', '', '','',''];
      $paymentArray[] = ['No', 'Name','Amount', 'Start Date','End Date'];
     return [
        new DownloadExcel(),
    ];
    }


   public function getSongListen() {
        $cat = $this->getSongListenDT();
        $datatables = Datatables::of($cat) 
                        ->addColumn('no', function ($cat) {
                            return "";
                          });
        return $datatables->make(true);
    }

    private function getSongListenDT() {
      $recent = FullSongView::select('songs.name_eng','full_song_views.count','full_song_views.counted_at')
        ->join('songs','songs.id','=','full_song_views.song_id')
        ->get();
        return $recent;
    }
    
    

    //for subscription report
    public function getSubscription()
    {
      $cat = $this->getSubscriptionDT();
        $datatables = Datatables::of($cat) 
                        ->addColumn('no', function ($cat) {
                            return "";
                          })
                         ->filterColumn('name', function($query, $keyword) {
                    
                    $query->whereRaw('users.name like ?', ["%{$keyword}%"]);
                });

        
        return $datatables->make(true);
    }
    private function getSubscriptionDT() {
      $subscribes = Subscribe::select('users.name','subscription_prices.num_day as day','subscribes.*')
      ->join('users','users.id','=','subscribes.user_id')
      ->join('subscription_prices','subscription_prices.id','=','subscribes.subscribe_price_id');
      return $subscribes;
    }
    
}