<?php

namespace App\Repositories;

use App\Support;
use Yajra\Datatables\Datatables;
use DB;
class SupportRepo
{
  
    public function create()
    {
      $cms = Support::first();
      return $cms;
    }
    public function save($request)
    {
      $input = $request->all();
      $id = $request->get('id');
      

        if ($id === NULL) {
          $cms = new Support();
        }
        else {
          $cms = Support::find($id);
        }
        
        

         $saved = $cms->fill($input)->save();

         return $cms;
    }
}