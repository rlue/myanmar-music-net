<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewCategory extends Model
{
    //
    protected $fillable = [
        'name'
    ];
}
