<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaylistCms extends Model
{
    //
    protected $table = 'playlist_cms';

    protected $fillable = [
        'cms_type'
    ];

    public function cms_playlists()
    {
      return $this->belongsToMany(PlayList::class,'cms_playlists','cms_id','playlist_id');  
    }
}
