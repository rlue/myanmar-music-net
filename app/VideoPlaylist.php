<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoPlaylist extends Model
{
    //
    protected $fillable = [
        'playlist_name_mm','playlist_name_eng','playlist_scheduled_date','playlist_release_date','playlist_desc','playlist_image','playlist_status'
    ];

     public function videos()
    {
      return $this->belongsToMany(Video::class,'playlist_videos','playlist_id','video_id');  
    }
}
