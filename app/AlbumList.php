<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlbumList extends Model
{
    //
    protected $fillable = [
        'albumlist_name_mm','albumlist_name_eng','albumlist_scheduled_date','albumlist_release_date','albumlist_desc','albumlist_image'
    ];

     public function albums()
    {
      return $this->belongsToMany(Album::class,'albumlist_albums','albumlist_id','album_id');  
    }
}
