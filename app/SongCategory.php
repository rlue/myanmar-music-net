<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongCategory extends Model
{
    //
     protected $fillable = [
        'name','songCategory_image'
    ];
}
