<?php

namespace App\Exports;

use App\Subscribe;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Carbon\Carbon;
class SubscribeExport implements FromQuery,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    public function headings(): array
    {
        return [
            'User Name',
            'Expired date',
            'Day'
        ];
    }

    public function __construct($request)
    {
    	$this->user = $request->get('user');
        $this->status = $request->get('status');
        $this->from = $request->get('from_date');
        $this->to = $request->get('to_date');
    }
    public function query()
    {
        $payment = Subscribe::query();
        $payment->select('users.name as username','subscribes.ends_at','subscription_prices.num_day as  day');
        $payment->join('users','users.id','=','subscribes.user_id');
        $payment->join('subscription_prices','subscription_prices.id','=','subscribes.subscribe_price_id');
        $days = Carbon::now()->format('Y-m-d');
        if($this->user){
        	$payment->where('subscribes.user_id','=',$this->user);
        }
        if($this->from && $this->to){
        	$payment->whereBetween('subscribes.created_at',[$this->from,$this->to]);
        	
        }
         if($this->status == 'active'){
         	$payment->where('subscribes.ends_at','>=',$days);
         }
         if($this->status == 'inactive'){
         	$payment->where('subscribes.ends_at','<=',$days);
         }
        
       return $payment;
    }
}
