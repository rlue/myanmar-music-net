<?php

namespace App\Exports;

use App\FullSongView;
use App\Song;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;
class SongOrderExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($request)
    {
    	$this->from_date = $request->get('from_date');
        $this->to_date = $request->get('to_date');
        $this->user_id = $request->get('user');
    }

   public function view(): View
    {
    	if($this->user_id && $this->from_date && $this->to_date){
    		$payment = Song::with(['artists','albums' => function($query){
          $query->select('id','album_name_mm');
      		}])
	        ->select('songs.id','songs.name_eng','prices.name as prices','users.name','orders.created_at')
	        ->rightJoin('orders','orders.item_id','=','songs.id')
	        ->join('users','users.id','=','orders.user_id')
	        ->join('prices','prices.id','=','songs.prices')
	        ->whereBetween('orders.created_at',[$this->from_date,$this->to_date])
	        ->where('orders.user_id','=',$this->user_id)
	    	->get();
    	}else if(empty($this->user_id) && $this->from_date && $this->to_date){
           $payment = Song::with(['artists','albums' => function($query){
          $query->select('id','album_name_mm');
      		}])
	        ->select('songs.id','songs.name_eng','prices.name as prices','users.name','orders.created_at')
	        ->rightJoin('orders','orders.item_id','=','songs.id')
	        ->join('users','users.id','=','orders.user_id')
	        ->join('prices','prices.id','=','songs.prices')
	        ->whereBetween('orders.created_at',[$this->from_date,$this->to_date])
	    	->get();
            
        }else if(empty($this->user_id) && $this->from_date && empty($this->to_date)){
           $payment = Song::with(['artists','albums' => function($query){
          $query->select('id','album_name_mm');
      		}])
	        ->select('songs.id','songs.name_eng','prices.name as prices','users.name','orders.created_at')
	        ->rightJoin('orders','orders.item_id','=','songs.id')
	        ->join('users','users.id','=','orders.user_id')
	        ->join('prices','prices.id','=','songs.prices')
	        ->where('orders.created_at','=',$this->from_date)
	    	->get();
            
        }else if($this->user_id){
           $payment = Song::with(['artists','albums' => function($query){
          $query->select('id','album_name_mm');
      		}])
	        ->select('songs.id','songs.name_eng','prices.name as prices','users.name','orders.created_at')
	        ->rightJoin('orders','orders.item_id','=','songs.id')
	        ->join('users','users.id','=','orders.user_id')
	        ->join('prices','prices.id','=','songs.prices')
	        ->orwhere('orders.user_id','=',$this->user_id)
	    	->get();
            
        }else{
        	$payment = Song::with(['artists','albums' => function($query){
          $query->select('id','album_name_mm');
      		}])
	        ->select('songs.id','songs.name_eng','prices.name as prices','users.name','orders.created_at')
	        ->rightJoin('orders','orders.item_id','=','songs.id')
	        ->join('users','users.id','=','orders.user_id')
	        ->join('prices','prices.id','=','songs.prices')
	    	->get();
        }
    	$collection = collect($payment);
    	$total = $collection->sum('prices');
        return view('admin.report.song',['songs' => $payment,'total' => $total]
        );
    }
   
}
