<?php

namespace App\Exports;

use App\FullSongView;
use App\Song;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;
class SongListenExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    

    public function __construct($request)
    {
    	$this->from_date = $request->get('from_date');
        $this->to_date = $request->get('to_date');
    }
    public function view(): View
    {
        
        if($this->from_date && $this->to_date){
            $payment = Song::with(['artists','albums' => function($query){
          $query->select('id','album_name_mm');
            }])
            ->select('songs.id','songs.name_eng','full_song_views.count','full_song_views.counted_at')
            ->rightJoin('full_song_views','full_song_views.song_id','=','songs.id')
            ->whereBetween('full_song_views.counted_at',[$this->from_date,$this->to_date])
            ->orderBy('full_song_views.count','desc')
            ->get();
            
        }else if($this->from_date && empty($this->to_date)){
            $payment = Song::with(['artists','albums' => function($query){
          $query->select('id','album_name_mm');
            }])
            ->select('songs.id','songs.name_eng','full_song_views.count','full_song_views.counted_at')
            ->rightJoin('full_song_views','full_song_views.song_id','=','songs.id')
            ->where('full_song_views.counted_at','=',$this->from_date)
            ->orderBy('full_song_views.count','desc')
            ->get();
            
        }else{
            $payment = Song::with(['artists','albums' => function($query){
          $query->select('id','album_name_mm');
            }])
            ->select('songs.id','songs.name_eng','full_song_views.count','full_song_views.counted_at')
            ->rightJoin('full_song_views','full_song_views.song_id','=','songs.id')
            ->orderBy('full_song_views.count','desc')
            ->get();
        }
       
       return view('admin.report.listen',['songlisten' => $payment]);
    }
}
