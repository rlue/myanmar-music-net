<?php

namespace App\Exports;


use App\Album;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;
class AlbumOrderExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($request)
    {
    	$this->from_date = $request->get('from_date');
        $this->to_date = $request->get('to_date');
        $this->user_id = $request->get('user');
    }

   public function view(): View
    {
    	if($this->user_id && $this->from_date && $this->to_date){
    		$payment = Album::with('artists')
	        ->select('albums.id','albums.album_name_eng','album_prices.name as prices','users.name','orders.created_at')
	        ->rightJoin('orders','orders.item_id','=','albums.id')
	        ->join('users','users.id','=','orders.user_id')
	        ->join('album_prices','album_prices.id','=','albums.album_prices')
	        ->whereBetween('orders.created_at',[$this->from_date,$this->to_date])
	        ->where('orders.user_id','=',$this->user_id)
	    	->get();
    	}else if(empty($this->user_id) && $this->from_date && $this->to_date){
           $payment = Album::with('artists')
	        ->select('albums.id','albums.album_name_eng','album_prices.name as prices','users.name','orders.created_at')
	        ->rightJoin('orders','orders.item_id','=','albums.id')
	        ->join('users','users.id','=','orders.user_id')
	        ->join('album_prices','album_prices.id','=','albums.album_prices')
	        ->whereBetween('orders.created_at',[$this->from_date,$this->to_date])
	    	->get();
            
        }else if(empty($this->user_id) && $this->from_date && empty($this->to_date)){
           $payment = Album::with('artists')
	        ->select('albums.id','albums.album_name_eng','album_prices.name as prices','users.name','orders.created_at')
	        ->rightJoin('orders','orders.item_id','=','albums.id')
	        ->join('users','users.id','=','orders.user_id')
	        ->join('album_prices','album_prices.id','=','albums.album_prices')
	        ->where('orders.created_at','=',$this->from_date)
	    	->get();
            
        }else if($this->user_id){
           $payment = Album::with('artists')
	        ->select('albums.id','albums.album_name_eng','album_prices.name as prices','users.name','orders.created_at')
	        ->rightJoin('orders','orders.item_id','=','albums.id')
	        ->join('users','users.id','=','orders.user_id')
	        ->join('album_prices','album_prices.id','=','albums.album_prices')
	        ->where('orders.user_id','=',$this->user_id)
	    	->get();
            
        }else{
        	$payment = Album::with('artists')
	        ->select('albums.id','albums.album_name_eng','album_prices.name as prices','users.name','orders.created_at')
	        ->rightJoin('orders','orders.item_id','=','albums.id')
	        ->join('users','users.id','=','orders.user_id')
	        ->join('album_prices','album_prices.id','=','albums.album_prices')
	    	->get();
        }
    	$collection = collect($payment);
    	$total = $collection->sum('prices');
        return view('admin.report.album',['album' => $payment,'total' => $total]
        );
    }
   
}
