<?php

namespace App\Exports;

use App\PaymentOrder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;
class PaymentExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    public function __construct($request)
    {
    	$this->user = $request->get('user');
        $this->status = $request->get('status');
        $this->from = $request->get('from_date');
        $this->to = $request->get('to_date');
    }
    public function view(): View
    {
       
        if($this->user && $this->from && $this->to && $this->status){
            $payment = PaymentOrder::select('users.name','payment_orders.amount','payment_orders.created_at')
            ->join('users','users.id','=','payment_orders.user_id')
            ->where('payment_orders.user_id','=', $this->user)
            ->whereBetween('payment_orders.created_at',[$this->from,$this->to])
            ->where('payment_orders.status','=', $this->status)
            ->get();
        }else if($this->user && $this->from && $this->to && empty($this->status)){
            $payment = PaymentOrder::select('users.name','payment_orders.amount','payment_orders.created_at')
            ->join('users','users.id','=','payment_orders.user_id')
            ->where('payment_orders.user_id','=', $this->user)
            ->whereBetween('payment_orders.created_at',[$this->from,$this->to])
            ->get();
            
        }else if(empty($this->user) && $this->from && $this->to && $this->status){
            $payment = PaymentOrder::select('users.name','payment_orders.amount','payment_orders.created_at')
            ->join('users','users.id','=','payment_orders.user_id')
            ->where('payment_orders.status','=', $this->status)
            ->whereBetween('payment_orders.created_at',[$this->from,$this->to])
            ->get();
            
        }else if(empty($this->user) && $this->from && $this->to && empty($this->status)){
            $payment = PaymentOrder::select('users.name','payment_orders.amount','payment_orders.created_at')
            ->join('users','users.id','=','payment_orders.user_id')
            ->whereBetween('payment_orders.created_at',[$this->from,$this->to])
            ->get();
        }else if($this->user && empty($this->from) && empty($this->to) && empty($this->status)){
            $payment = PaymentOrder::select('users.name','payment_orders.amount','payment_orders.created_at')
            ->join('users','users.id','=','payment_orders.user_id')
            ->where('payment_orders.user_id','=', $this->user)
            ->get();
        }else if(empty($this->user) && empty($this->from) && empty($this->to) && $this->status){
             $payment = PaymentOrder::select('users.name','payment_orders.amount','payment_orders.created_at')
            ->join('users','users.id','=','payment_orders.user_id')
            ->where('payment_orders.status','=', $this->status)
            ->get();
        }else if(empty($this->user) && $this->from && empty($this->to) && empty($this->status)){
            $payment = PaymentOrder::select('users.name','payment_orders.amount','payment_orders.created_at')
            ->join('users','users.id','=','payment_orders.user_id')
            ->where('payment_orders.created_at','=', $this->from)
            ->get();
        }else{
            $payment = PaymentOrder::select('users.name','payment_orders.amount','payment_orders.created_at')
            ->join('users','users.id','=','payment_orders.user_id')
            ->get();
        }
        $collection = collect($payment);
        $total = $collection->sum('amount');
       return view('admin.report.payments',['payment' => $payment,'total' => $total]
        );
    }
}
