<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    //
    protected $fillable = [
        'title','news_category','news_type','brief_desc','news_desc','news_image','event_date','post_date'
    ];

    public function category()
    {
    	return $this->belongsTo(NewCategory::class,'news_category');
    }
}
